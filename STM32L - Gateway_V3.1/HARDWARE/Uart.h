#ifndef 	_UART_H_
#define		_UART_H_

#define 	USART_REC_LEN  				400  
#define 	USART2_REC_LEN  			400  
#define 	USART3_REC_LEN  			400  

#include "compileconfig.h"
#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V100)

#define		SIM_COM			USART3
#define		LORA_COM		USART2
#define		GPRS_COM		USART3

#define 	GPRS_UART_BUF	g_USART3_RX_BUF
#define		GPRS_UART_CNT	g_USART3_RX_CNT

#define 	LORA_UART_BUF	g_USART2_RX_BUF
#define		LORA_UART_CNT	g_USART2_RX_CNT

#elif (HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)

#define		SIM_COM			USART2
#define		LORA_COM		USART3
#define		GPRS_COM		USART2

#define 	LORA_UART_BUF	g_USART3_RX_BUF
#define		LORA_UART_CNT	g_USART3_RX_CNT

#define 	GPRS_UART_BUF	g_USART2_RX_BUF
#define		GPRS_UART_CNT	g_USART2_RX_CNT

#endif

#define		DEBUG_BAUND_BATE	115200
#define		SIM_BAND_RATE		115200
#define		LORA_BAND_RATE		115200

#include "stm32l1xx.h"

void Uart_Send_Char(USART_TypeDef* USARTx, unsigned char ch);
void Uart_Send_Data(USART_TypeDef* USARTx, unsigned char *data, unsigned char len);
void Uart_Send_Str(USART_TypeDef* USARTx, char *data);
	
void USART3_ConfigEven(unsigned int bound);
	
extern unsigned char   g_UartRxFlag;					//串口1接收完标志
extern unsigned char  	g_USART_RX_BUF[USART_REC_LEN];	//串口1缓冲区
extern unsigned short int 	g_USART_RX_CNT;					//串口1消息长度

extern unsigned char   g_Uart2RxFlag;					//串口2接收完标志
extern unsigned char  	g_USART2_RX_BUF[USART2_REC_LEN];	//串口2缓冲区
extern unsigned short int 	g_USART2_RX_CNT;					//串口2消息长度

extern unsigned char 	g_Uart3RxFlag;					//串口3接收完标志
extern unsigned char  	g_USART3_RX_BUF[USART3_REC_LEN];//串口3缓冲区
extern unsigned short int 	g_USART3_RX_CNT;				//串口3消息长度

void USART1_Config(unsigned int bound);
void USART2_Config(unsigned int bound);
void USART3_Config(unsigned int bound);
void Awake_USART2_Config(void);

void Clear_Uart1Buff(void);
void Clear_GPRS_Buff(void);
void Clear_Lora_Buff(void);

#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)
void Clear_GPS_Buff(void);
#endif

void u2_printf(char* fmt, ...);
void u3_printf(char* fmt, ...);

unsigned char GetLoraUartRecFlag(void);
unsigned char GetGPRSUartRecFlag(void);
void GPRS_Uart_Init(unsigned int bound);
void Lora_Uart_Init(unsigned int bound);
#endif

