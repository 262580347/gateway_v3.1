//=================================
//
//estype.h: 数据结构头文件
//
//=================================
#ifndef _MYDEF_H_
#define _MYDEF_H_


////通信方式定义
//#define	CONNECT_TYPE_ZIGBEE	1
//#define	CONNECT_TYPE_GPRS	2
//#define	_COMM_MODE	CONNECT_TYPE_ZIGBEE			//通信方式，1、GPRS，2、Zigbee

//通信网络类型定义========================
#define NETTYPE_ZIGBEE 		1
#define NETTYPE_GPRS 		2
#define NETTYPE_XBEE		3  	//XBEE模块
#define NETTYPE_2G			4	//SIM800C模块
#define NETTYPE_LORA		5	//LORA模块
#define NETTYPE_NB_IOT		6	//NB模块
#define NETTYPE_WLAN		7	//WLAN模块
#define NETTYPE_4G			8	//4G模块
#define	NETTYPE_GPRS_LORA	37

//COM端口定义
#define DEBUG_PORT		1
#define ZIGBEE_PORT 	2
#define XBEE_PORT		2
#define LORA_PORT		2

#define GPRS_PORT		3
#define SIM800C_PORT	3


#define BOF_VAL		(0x7e)
#define EOF_VAL		(0x21)
#define CTRL_VAL	(0x7d)


#define UART_1	(0)
#define UART_2	(1)
#define UART_3	(2)

#define ZIGBEE_UART		(UART_2)
#define XBEE_UART		(UART_2)
#define GPRS_UART		(UART_3)

//#define TEMP_PORT		3
//#define	SENSOR_PORT		3
#define	CONTROL_PORT	3
//========================
#define COM_ILLUMINANCE		3
#define COM_CARBONDIOXIDE	3
#define COM_GPRS	3
#define COM_ZIGBEE	2
#define COM_DEBUG	1

#define PORT1		1
#define PORT2		2
#define PORT3		3

//通信协议定义
#define PROTOCOL_DATA_FORMAT	100	//数据格式协议版本1.00
#define PROTOCOL_FRAME_FORMAT	100 //帧格式协议版本


//传感器类型定义==========================================
//#define MAX_SENSOR_TYPE_NUM		11		//最大支持的传感器类型数
#define MAX_SENSOR_CHANNEL_NUM	17		//V1.3最多可接17个传感器

#define	SENSOR_TYPE_UNUSE		0	//未使用标识
#define	SENSOR_TYPE_AIR_TEMP	1 		//空气温度
#define	SENSOR_TYPE_AIR_HUMI	2 		//空气湿度
#define SENSOR_TYPE_SOIL_TEMP	3		//土壤温湿度
#define	SENSOR_TYPE_SOIL_HUMI	4		//土壤湿度
#define	SENSOR_TYPE_ILLUMINANCE	5	  	//光照度
#define SENSOR_TYPE_CARBONDIOXIDE	6		//二氧化碳传感器
#define SENSOR_TYPE_RAINFALL	7		//降雨量
#define	SENSOR_TYPE_WINDSPEED	8		//风速
#define SENSOR_TYPE_FLOW		9	 	//流量传感器
#define SENSOR_TYPE_WINDDIRECTION	10	//风向

#define SENSOR_TYPE_CONTROLLER	100		//控制器

#define SENSOR_TYPE_GATEWAY		200		//网关

#define	SENSOR_TYPE_COLLECTOR		(500)


//数据XML定义========================================
//监测数据类
//#define	XML_AIR_TEMP	1	//空气温度
//#define	XML_AIR_HUMI	2	//空气湿度
//
//#define XML_SOIL_TEMP	3	//土壤温度
//#define	XML_SOIL_HUMI	4	//土壤湿度
//
//#define XML_ILLUMINANCE	5	//光照度
//#define	XML_CARBONDIOXIDE	6	//二氧化碳浓度
//#define XML_FLOW		7	//流量
//
//#define XML_RAINFALL	8	//降雨量
//#define XML_WINDSPEED	9	//风速
#define XML_SENSOR_DATA	8		//传感器数据

#define XML_BAT_VOLTAGE	10	//电池电压

#define XML_SWITCH_STATE	11	//继电器状态

#define XML_WINDDIRECTION	12	//风向

//控制数据类
#define	XML_CONTROL_CHANNEL	21	//控制通道
#define XML_CONTROL_OPERATE	22	//控制动作
#define	XML_CONTROL_TIME	23	//控制时长

//系统数据
#define XML_ERR_CODE		50	//通信异常码
#define XML_DATA_VERSION	51
#define XML_FRAME_VERSION	52
#define	XML_HARDWARE_VERSION	53
#define XML_SOFTWARE_VERSION	54

#define XML_SERVER_TIME		60	//服务器时间

//系统参数类
#define XML_SYS_DEVICE_ID	101		//设备ID
#define XML_SYS_SENSOR_TYPE	102		//传感器类型
#define XML_SYS_SENSOR_CHANNEL	103	//传感器通道
#define XML_SYS_CHANNEL_EN	104	//传感器通道状态

#define	XML_SYS_AUTO_RESET		105	//午夜重启
#define	XML_SYS_AUTO_RESET_TIME	106	//午夜重启时间

#define XML_SYS_SENSOR_FIX		120	//修正值
#define XML_SYS_AIR_TEMP_FIX	121	//空气温度修正值
#define	XML_SYS_AIR_HUMI_FIX	122	//空气湿度修正值
#define	XML_SYS_SOIL_TEMP_FIX	123	//土壤温度修正值
#define	XML_SYS_SOIL_HUMI_FIX	124	//土壤湿度修正值
#define	XML_SYS_ILLUMINANCE_FIX	125	//光照度修正值
#define	XML_SYS_CARBONDIOXIDE_FIX	126	 //二氧化碳浓度修正值
#define	XML_SYS_FLOW_FIX		127	//流量修正值

#define XML_SYS_ZIGBEE_EN		140	//zigbee状态
#define	XML_SYS_COMM_TYPE		141	//通信类型
#define	XML_SYS_ZIGBEE_NET		142	//Zigbee网络号
#define	XML_SYS_ZIGBEE_ADDR		143	//Zigbee节点地址
#define	XML_SYS_ZIGBEE_SERVER_ID	144	//Zigbee主机地址
#define	XML_SYS_ZIGBEE_CHANNEL		145	//Zigbee通信频道
#define XML_SYS_ZIGBEE_TOPOLOGY		146

#define	XML_SYS_RELINK_TIME			150	//连接次数
#define	XML_SYS_HEART_BEART_TIME	151	//心跳间隔

#define XML_SYS_GPRS_SERVER_IP		152
#define XML_SYS_GPRS_SERVER_PORT	153
#define XML_SYS_GPRS_EN				154

	
//数据XML定义结束==========================================


//继电器控制状态定义
#define RELAY_STATE_ON	1
#define RELAY_STATE_OFF	0


#endif
