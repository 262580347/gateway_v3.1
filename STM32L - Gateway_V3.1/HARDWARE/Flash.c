#include "main.h"
#include "Flash.h"




unsigned char ReadFlash(unsigned int uAddress)
{
    unsigned char  OffSet = uAddress%4;
	unsigned int BaseAddr = uAddress - OffSet;
	unsigned int nData    = *((unsigned int *)BaseAddr);
	return (unsigned char)(nData>>((3-OffSet)*8)); 
}

//дָ��λ�õ�Flash
unsigned char WriteFlash(unsigned int uPageAddr,unsigned char *pBuf,unsigned int uLen)
{
    unsigned int  i=0;
    unsigned int  value; 
    unsigned char   OffSet = uLen%4;
	FLASH_Status FlashSta;
	
//    FLASH_ClearFlag(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_PGAERR | FLASH_FLAG_WRPERR); 
	FLASH_ClearFlag(FLASH_FLAG_EOP|FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_SIZERR | FLASH_FLAG_OPTVERR | FLASH_FLAG_OPTVERRUSR);
    FLASH_Unlock();
	FlashSta = FLASH_ErasePage(uPageAddr);
	if( FLASH_COMPLETE == FlashSta )
	{
	    if( uLen >=4 )
	    {
	        uLen = uLen- OffSet;
	    }
	    else
	    {
	        uLen = 0;
	    }
	    for(i=0;i<uLen;i+=4)
	    {
	        value=(unsigned int)(pBuf[0+i]<<24)|\
	              (unsigned int)(pBuf[1+i]<<16)|\
	              (unsigned int)(pBuf[2+i]<<8) |\
	              (unsigned int)(pBuf[3+i]);
			FlashSta = FLASH_FastProgramWord((uPageAddr+i),value);
	        if(FLASH_COMPLETE != FlashSta)
	        {
					return FALSE;
	        }
	    }
	    value = 0;
		if( OffSet >0 )
		{
		    switch(OffSet)
			{
			    case 1:
				     value = (unsigned int)(pBuf[uLen])<<24;
				break;
				case 2:
				     value = (unsigned int)(pBuf[uLen+1]<<16)|\
					         (unsigned int)(pBuf[uLen]<<24);
				break;
				case 3:
				     value = (unsigned int)(pBuf[uLen+2]<<8)|\
					         (unsigned int)(pBuf[uLen+1]<<16)|\
							 (unsigned int)(pBuf[uLen]<<24);
				break;
				default:break;
			} 

			FlashSta = FLASH_FastProgramWord((uPageAddr+uLen),value);
				  
	        if(FLASH_COMPLETE != FlashSta)
	        {
					return FALSE;
	        }
		}
	}
	else 
	{
	    return FALSE;
	} 

	return TRUE;
}
