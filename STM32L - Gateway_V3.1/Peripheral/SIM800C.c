#include "main.h"
#include "SIM800C.h"

#define COM_DATA_SIZE	256

#define APN_CMNET 	1		//中国移动

#define APN_UNINET 	2		//中国联通

static u8 s_SignalStrength = 0;		//信号强度

static void SimUartSendStr(char *str)
{
#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V100)

	u3_printf(str);

#elif (HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)

	u2_printf(str);
	
#endif	
}


void SIM800CPortInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA , ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = SIM800C_ENABLE_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(SIM800C_ENABLE_TYPE, &GPIO_InitStructure);	
}

void SIM800CProcess(unsigned short nMain10ms)
{
	u8 i;
	u16 nTemp = 0;
	static u16 s_LastTime = 0, s_OverCount = 0, s_LastTime2 = 0;
	static u8 s_APN = 0, s_APNFlag = 0,  s_Status = 1, s_Step = 1, RunOnce = FALSE, s_ErrCount = 0, s_SimSavingFlag = FALSE;
	static u8 s_ATAckCount = 0, s_SavingModeFlag = FALSE, s_RetryCount = 0;
	char * p1 = NULL, buf[10], str[100];
	static u16 s_StartTime = 0, s_EndTime = 0;
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	p_sys = GetSystemConfig();
	
	if(RunOnce == FALSE)
	{
		RunOnce = TRUE;
		s_LastTime = GetSystem10msCount();
		InitGprsLinkedList();
		u1_printf("\r\n Initializes the downstream data buffer...\r\n");
	}
			
	switch(s_Status)
	{	
		case 1:   //模块断电
			s_RetryCount++;
			SetTCProtocolRunFlag(FALSE);
		
			if(s_RetryCount >= 10)
			{
				u1_printf("\r\n Timeout not connected to the network waiting for restart...\r\n");
				SetLogErrCode(LOG_CODE_TIMEOUT);
				SetLogErrCode(LOG_CODE_RESET);
				StoreOperationalData();
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				Sys_Soft_Reset();
			}
			SIM800CPortInit();
			SIM_PWR_OFF();
			GPRS_Uart_Init(SIM_BAND_RATE);
			s_LastTime = GetSystem10msCount();
			s_Status++;
			u1_printf(" [SIM]Power off,delay 8s\r\n");
			s_StartTime = GetSystem10msCount();
		break;
		 
		case 2:	//断电后延时5S上电，模块上电自动开机
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 800 )
			{
				u1_printf(" [SIM]Power on,delay 18s.\r\n");
				SIM_PWR_ON();
				s_LastTime = GetSystem10msCount();
				s_Status++;
			}
		break;
			
		case 3: 	//上电完成后，初始化端口
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1800 )
			{
				u1_printf(" [SIM]Check baudrate\r\n");
				SimUartSendStr("AT\r");
				delay_ms(50);
				SimUartSendStr("AT\r");
				s_LastTime = GetSystem10msCount();
				s_Status++;
			}
		break;
			
		case 4:  	//检测模块的波特率
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 5 )
			{
				Clear_GPRS_Buff();
				SimUartSendStr("AT+CIURC=0\r");		//发送"AT\r"，查询模块是否连接，返回"OK"
				u1_printf(" [SIM]AT+CIURC=0\r\n");
				s_LastTime = GetSystem10msCount();
				s_Status++;
			}
		break;
		
		case 5:	//延时100MS,检测响应数据
			if(GetGPRSUartRecFlag())
			{		
				if(strstr((char *)GPRS_UART_BUF, "OK"))
				{
					s_ErrCount = 0;
					s_Status++;	
					u1_printf(" Close Call Ready\r\n");
				}			
				Clear_GPRS_Buff();
				
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 15 )
			{				
					u1_printf(" [SIM]OK无响应\r\n");
					s_Status--;   //回上一个状态
					s_ErrCount++;   //重试次数加1
					if(s_ErrCount >= 4 )
					{
						SetLogErrCode(LOG_CODE_NOMODULE);
						StoreOperationalData();
						s_ErrCount = 0;
						s_Status = 1;	//模块重新上电初始化
					}						
			}
	 	break;
		
		case 6:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 15) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			

				switch(s_Step)
				{
					case 1:
						SimUartSendStr("ATE0\r");
						u1_printf("[SIM]->ATE0\r\n");
					break;
					case 2:
						SimUartSendStr("AT+CPIN?\r");
					break;
					case 3:
						SimUartSendStr("AT+CCID\r");
					break;
					case 4:
						SimUartSendStr("AT+CIMI\r");
					break;
					case 5:
						SimUartSendStr("AT+CGSN\r");
					break;
					case 6:
						SimUartSendStr("AT+CSQ\r");
					break;
					case 7:
						SimUartSendStr("AT+COPS?\r");
					break;	
					case 8:
						SimUartSendStr("AT+CGATT=1\r");
					break;				
					case 9:
						SimUartSendStr("AT+CGREG?\r");
					break;		
					case 10:
						SimUartSendStr("AT+CREG?\r");
					break;					
					case 11:
						SimUartSendStr("AT+CGATT?\r");
					break;				
					case 12:
						SimUartSendStr("AT+CGATT=1\r");
						
					break;
				}
				s_LastTime = GetSystem10msCount();
				s_Status++;
			}		
		break;
		
		case 7:

			if(GetGPRSUartRecFlag())
			{
				u1_printf("%s\r\n", GPRS_UART_BUF);
											
				switch(s_Step)
				{
					case 1:
						if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
						{
							s_Step++;	
							s_Status--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();						
						}
						else
						{
							s_Status--;	
							s_ErrCount++;
						}
					break;
						
					case 2:
					//	u1_printf("检测SIM卡： ");
						if((strstr((const char *)GPRS_UART_BUF,"+CPIN: READY")) != NULL )
						{					
							s_Step++;	
							s_Status--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();		
							u1_printf("\r\n  SIM Card OK\r\n");
						}
						else
						{
							s_Status--;	
							s_ErrCount++;
							
							if(s_ErrCount >= 5)
							{
								SetLogErrCode(LOG_CODE_NO_CARD);
								u1_printf(" No Card!\r\n");
								s_ErrCount = 0;
								s_Status = 1;
								s_Step = 1;								
							}
						}						
					break;	
						
					case 3:				//ICCID查询
						//中国移动:898600；898602；898604；898607 
						//中国联通:898601、898606、898609
						//中国电信:898603、898611。
						if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
						{
							if((GPRS_UART_BUF[2]=='8')&&(GPRS_UART_BUF[3]=='9')&&(GPRS_UART_BUF[4]=='8')&&(GPRS_UART_BUF[5]=='6'))
							{
								i = (GPRS_UART_BUF[6] - '0') * 10 + (GPRS_UART_BUF[7] - '0');
								switch(i)
								{
									//移动卡
									case 0:
									case 2:
									case 4:
									case 7:
										s_APNFlag = 1;	
										s_APN = APN_CMNET;
										u1_printf("CCID: APN SET TO CMNET\r\n");
										break;
									//联通卡
									case 1:
									case 6:
									case 9:
										s_APNFlag = 1;	
										s_APN = APN_UNINET;
										u1_printf("CCID: APN SET TO UNINET\r\n");
										break;										
									default:
										s_APNFlag = 0;
										SetLogErrCode(LOG_CODE_UNDEFINE);
										StoreOperationalData();
										u1_printf("CCID: NO APN INFO\r\n");	
										break;
								}
							}
										
						//	u1_printf("制造商：%s\r\n", GPRS_UART_BUF);
							s_Step++;	
							s_Status--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();							
						}
						else
						{
							s_Status--;	
							s_ErrCount++;
						}
					break;
						
					case 4:			//IMSI查询
						if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
						{
							if(!s_APNFlag &&(GPRS_UART_BUF[2]=='4')&&(GPRS_UART_BUF[3]=='6')&&(GPRS_UART_BUF[4]=='0'))
							{
								i = (GPRS_UART_BUF[5] - '0') * 10 + (GPRS_UART_BUF[6] - '0');
								switch(i)
								{
									//移动卡
									case 0:
									case 2:
									case 4:
									case 7:
										s_APNFlag = 1;	
										s_APN = APN_CMNET;
										u1_printf("IMSI: APN SET TO CMNET\r\n");
										break;
									//联通卡
									case 1:
									case 6:
									case 9:
										s_APNFlag = 1;	
										s_APN = APN_UNINET;
										u1_printf("IMSI: APN SET TO UNINET\r\n");
										break;										
									default:
										s_APNFlag = 0;	
										u1_printf("IMSI: NO APN INFO\r\n");	
										break;
								}
							}
								
						//	u1_printf("模块名称：%s\r\n", GPRS_UART_BUF);
							s_Step++;	
							s_Status--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();							
						}
						else
						{
							s_Status--;	
							s_ErrCount++;
						}
					break;
						
					case 5:
						if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
						{
						//	u1_printf("序列号：%s\r\n", GPRS_UART_BUF);
							s_Step++;	
							s_Status--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();							
						}
						else
						{
							s_Status--;	
							s_ErrCount++;
						}
					break;
						
					case 6:	
					//	u1_printf("检测信号质量： ");
						if((p1 = strstr((const char *)GPRS_UART_BUF,"+CSQ:")) != NULL )
						{							
							//u1_printf("%s\r\n", GPRS_UART_BUF);	
							memset(buf, 0 ,sizeof(buf));
							strncpy(buf,p1+6,2);
							nTemp = atoi( buf);
							SetCSQValue(nTemp);
							s_SignalStrength = nTemp;
							if( nTemp < 10 )								
							{
								s_Status--;	
								s_ErrCount++;
								SetLogErrCode(LOG_CODE_WEAKSIGNAL);
								
								u1_printf(" Weak Signal\r\n");
							}
							else
							{
								s_Status--;	
								s_Step++;	
								s_ErrCount = 0;
								ClearLogErrCode(LOG_CODE_WEAKSIGNAL);
								u1_printf("\r\n Signal OK\r\n");
							}
						}
						else
						{
//							u1_printf("    Error\r\n");
							s_Status--;	
							s_ErrCount++;
						}		
					break;		
						
					case 7:
						//u1_printf(" 运营商名称： ");
						if((p1 = strstr((const char *)GPRS_UART_BUF,"+COPS:")) != NULL )
						{				
							if(!s_APNFlag)
							{
								if((strstr((const char *)GPRS_UART_BUF,"CHINA MOBILE")) != NULL )  
								{
									s_APN = APN_CMNET;
									s_APNFlag = 1;
									u1_printf("COPS: CHINA MOBILE\r\n");
								}
								else if((strstr((const char *)GPRS_UART_BUF,"UNICOM")) != NULL )  
								{
									s_APN = APN_UNINET;
									s_APNFlag = 1;
									u1_printf("COPS: UNICOM\r\n");
								}
								else
								{
									s_APNFlag = 0;
									u1_printf("COPS: UNRECOGNIZABLE\r\n");
								}
							}
	
							s_Step++;	
							s_Status--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();		
						//	u1_printf("%s\r\n", GPRS_UART_BUF);
						}
						else
						{
							u1_printf("    Error\r\n");
							s_Status--;	
							s_ErrCount++;
						}
					break;		
						
					case 8:
						if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
						{
							u1_printf(" Set GPRS Attach OK\r\n");
							s_Step++;	
							s_Status--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();
						}
						else
						{
							s_Status--;	
							s_ErrCount++;
							if(s_ErrCount == 30)
							{		
								u1_printf(" GPRS Attach Fail,Check SIM Card\r\n");
								s_ErrCount = 0;								
								s_Step++;
							}
						}
						
					break;
										
					case 9:
						//u1_printf(" 模块注册网络: ");
						if((p1 = strstr((const char *)GPRS_UART_BUF,"+CGREG:")) != NULL )
						{						
							//u1_printf("%s\r\n", GPRS_UART_BUF);
							
							nTemp = atoi( (const char *) (p1+10));
							switch( nTemp )								
							{
								case 0:
								u1_printf("\r\n [SIM]Module registration - not registered\r\n");
								s_Status--;
								s_ErrCount++;
								break;
								
								case 1:
								u1_printf("\r\n [SIM]Module registration network - successful\r\n");
								s_Status--;
								s_Step++;
								s_ErrCount = 0;
								break;
								
								case 2:
								u1_printf("\r\n [SIM]Registration in progress,Count=%d\r\n",s_ErrCount);
								s_Status--;
								s_ErrCount++;

								break;
								
								case 3:
								u1_printf("\r\n [SIM]Registration has been rejected,Count=%d\r\n",s_ErrCount);
								s_Status--;
								s_ErrCount++;
								break;
								
								default:
								u1_printf("\r\n [SIM]The registration status is not recognized\r\n");
								s_Status--;
								s_ErrCount++;
								break;	
							}
							
							if(s_ErrCount == 50)
							{		
								u1_printf(" GPRS注册网络失败，查询GSM\r\n");
								SetLogErrCode(LOG_CODE_UNATTACH);
								s_ErrCount = 0;								
								s_Step++;
							}
							
						}
						else
						{
//							u1_printf("    Error\r\n");
							s_Status--;	
							s_ErrCount++;
						}	
							
					break;
						
					case 10:
						//u1_printf(" 模块注册网络: ");
						if((p1 = strstr((const char *)GPRS_UART_BUF,"+CREG:")) != NULL )
						{						
							//u1_printf("%s\r\n", GPRS_UART_BUF);
							
							nTemp = atoi( (const char *) (p1+9));
							switch( nTemp )								
							{
								case 0:
								u1_printf("\r\n [SIM]Module registration - not registered\r\n");
								s_Status--;
								s_ErrCount++;
								break;
								
								case 1:
								u1_printf("\r\n [SIM]Module registration network - successful\r\n");
								s_Status--;
								s_Step++;
								s_ErrCount = 0;
								break;
								
								case 2:
								u1_printf("\r\n [SIM]Registration in progress,Count=%d\r\n",s_ErrCount);
								s_Status--;
								s_ErrCount++;

								break;
								
								case 3:
								u1_printf("\r\n [SIM]Registration has been rejected,Count=%d\r\n",s_ErrCount);
								s_Status--;
								s_ErrCount++;
								break;
								
								default:
								u1_printf("\r\n [SIM]The registration status is not recognized\r\n");
								s_Status--;
								s_ErrCount++;
								break;	
							}
							
							if(s_ErrCount == 50)
							{		
								u1_printf(" GSM registration network failed. Check attachment status\r\n");
								s_ErrCount = 0;								
								s_Step++;
							}
						}
						else
						{
//							u1_printf("    Error\r\n");
							s_Status--;	
							s_ErrCount++;
						}	
							
					break;
					
					case 11:	
						//u1_printf("GPRS附着状态: ");
						if((p1 = strstr((const char *)GPRS_UART_BUF,"+CGATT:")) != NULL )
						{					
							//u1_printf("%s\r\n", GPRS_UART_BUF);
							
							nTemp = atoi( (const char *) (p1+8));
							if( nTemp == 0 )								
							{
								s_Status--;	
								s_ErrCount++;
								if(s_ErrCount == 30)
								{									
									s_Step++;
									s_ErrCount++;
								}
							}
							else
							{
								s_Status++;	
								s_ATAckCount = 0;
								s_Step = 1;	
								s_ErrCount = 0;
							}
						}
						else
						{
							u1_printf("    Error\r\n");
							s_Status--;	
							s_ErrCount++;
						}		
					break;		
						
					case 12:
						if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
						{
							s_Step--;	
							s_Status--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();
						}
						else
						{
							s_Status--;	
							s_ErrCount++;
						}
						
					break;
						
				}
				
				if(s_ErrCount > 100 )
				{
					SetLogErrCode(LOG_CODE_UNATTACH);
					StoreOperationalData();
					s_ErrCount = 0;
					s_Status = 1;
					s_Step = 1;
				}
				
				Clear_GPRS_Buff();
				s_LastTime = GetSystem10msCount();
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1500) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				s_ATAckCount++;
				
				if(s_ATAckCount >= 5)
				{
					s_ATAckCount = 0;
					SetLogErrCode(LOG_CODE_ATTIMEOUT);
					StoreOperationalData();
					u1_printf(" AT No Ack\r\n");
					s_Status = 1;
					s_Step = 1;
				}
				else
				{
					s_Status--;
					u1_printf("AT Timeout\r\n");
				}
				
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
			}			
		break;
		
		case 8:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 15) 
			{
				switch(s_Step)
				{
					case 1:
						SimUartSendStr("AT+CIPCLOSE=1\r");
						u1_printf(" [SIM]->Close current link\r\n");
					break;
					case 2:
						SimUartSendStr("AT+CIPSHUT\r");
						u1_printf(" [SIM]->Close all links\r\n");
					break;
					case 3:
						SimUartSendStr("AT+CIPMODE=1\r");
						u1_printf(" [SIM]->Set to pass-through mode\r\n");
					break;
					case 4:
						if(s_APNFlag)
						{
							switch(s_APN)
							{
								case APN_CMNET:
									SimUartSendStr("AT+CSTT=\"CMNET\"\r");

									break;
								case APN_UNINET:
									SimUartSendStr("AT+CSTT=\"UNINET\"\r");
									break;
								default:
									SimUartSendStr("AT+CSTT?\r");
									break;
							}
						}	
						else 
						{								
							SimUartSendStr("AT+CSTT?\r");
							u1_printf("[SIM]->Undefine\r\n");	
						}
					break;
					case 5:
						SimUartSendStr("AT+CIICR\r");
						u1_printf(" [SIM]->Establish wireless link GPRS\r\n");
					break;
					case 6:
						SimUartSendStr("AT+CIFSR\r");
						u1_printf(" [SIM]->Get the local IP address\r\n\r\n");
					break;
					
					case 7:

					break;
					
					default:
						
						s_Status = 1;
						s_Step = 1;
					break;
				}
				s_LastTime = GetSystem10msCount();
				s_Status++;
			}
		
		break;
	
		case 9:
			if(GetGPRSUartRecFlag())
			{
				u1_printf("%s\r\n", GPRS_UART_BUF);
											
				switch(s_Step)
				{
					case 1:
						if((strstr((const char *)GPRS_UART_BUF,"+CIPCLOSE")) != NULL )
						{
							s_Step++;	
							s_Status--;		
													
						}
						else if((strstr((const char *)GPRS_UART_BUF,"ERROR")) != NULL )
						{
							s_Step++;	
							s_Status--;									
						}
						else
						{
							s_Status--;	
							s_ErrCount++;
						}
					break;
						
					case 2:
						if((strstr((const char *)GPRS_UART_BUF,"SHUT OK")) != NULL )
						{
						//	u1_printf("关闭移动场景\r\n");
							s_Step++;	
							s_Status--;															
						}
						else
						{
							s_Status--;	
							s_ErrCount++;
						}
					break;
						
					case 3:
						if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
						{
						//	u1_printf("设置为透传模式\r\n");
							s_Step++;	
							s_Status--;														
						}
						else
						{
							s_Status--;	
							s_ErrCount++;
						}
					break;
						
					case 4:
						if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
						{
						//	u1_printf("APN OK\r\n");
							s_Step++;	
							s_Status--;															
						}
						else
						{
							s_Status--;	
							s_ErrCount++;
						}
					break;
					
					case 5:
						if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
						{
						//	u1_printf("激活移动场景 OK\r\n");
							s_Step++;	
							s_Status--;														
						}
						else if((strstr((const char *)GPRS_UART_BUF,"+PDP: DEACT")) != NULL )
						{
							s_Status--;	
							s_Step = 1;
							s_ErrCount++;
						}
						else
						{
							s_Status--;	
							s_ErrCount++;
						}
						
					break;
						
					case 6:
						//u1_printf("模块本地IP:");
						if((strstr((const char *)GPRS_UART_BUF,".")) != NULL )
						{					
							s_Step = 1;	
							s_Status++;		
							s_ATAckCount = 0;
							//u1_printf("%s\r\n", GPRS_UART_BUF);
						}
						else
						{
							u1_printf("    Error\r\n");
							s_Status--;	
							s_ErrCount++;
						}						
					break;
					
					case 7:	
						u1_printf("IP Head:=%s\r\n", GPRS_UART_BUF);
						s_Status++;
						s_ATAckCount = 0;
						s_Step = 1;
					break;
						
				}
				
				if(s_ErrCount > 50 )
				{
					s_ErrCount = 0;
					s_Status = 1;
					s_Step = 1;
				}
				s_LastTime = GetSystem10msCount();
				
				Clear_GPRS_Buff();

			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 500) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				s_ATAckCount++;
				
				if(s_ATAckCount >= 3)
				{
					s_ATAckCount = 0;
					SetLogErrCode(LOG_CODE_ATTIMEOUT);
					u1_printf(" AT No Ack\r\n");
					s_Status = 1;
					s_Step = 1;
				}
				else
				{
					s_Status--;
					u1_printf("AT Timeout\r\n");
				}
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
			}	
		break;		
		
		case 10:
			u1_printf(" [SIM]Establishing TCP links\r\n");
			sprintf(str,"AT+CIPSTART=\"TCP\",\"%d.%d.%d.%d\",%d\r"
			,p_sys->Gprs_ServerIP[0]
			,p_sys->Gprs_ServerIP[1]
			,p_sys->Gprs_ServerIP[2]
			,p_sys->Gprs_ServerIP[3]
			,p_sys->Gprs_Port	);

			Clear_GPRS_Buff();
			SimUartSendStr(str);	
			u1_printf(str);	//串口输出AT命令
			u1_printf("\r\n");
			s_LastTime = GetSystem10msCount();
			s_Status++;			
		break;
		
		case 11:
			if(GetGPRSUartRecFlag())
			{
				u1_printf("%s\r\n", GPRS_UART_BUF);
					
				if((strstr((const char *)GPRS_UART_BUF,"ALREADY CONNECT")) != NULL )
				{
					s_Step = 1;	
					s_Status = 8;		
					SetTCModuleReadyFlag(FALSE);	
					u1_printf(" GPRS link established\r\n");				
				}				
				else if((strstr((const char *)GPRS_UART_BUF,"ERROR")) != NULL )
				{		
					s_ErrCount++;
					s_Status--;	
					s_Status = 8;
					s_Step = 1;					
				}				
				else if((strstr((const char *)GPRS_UART_BUF,"CONNECT OK")) != NULL )
				{
					s_Step = 1;	
					s_Status++;		
					s_ErrCount = 0;
					SetTCModuleReadyFlag(TRUE);		//连接成功========================
					u1_printf(" GPRS link successful\r\n");				
				}
				else if((strstr((const char *)GPRS_UART_BUF,"CONNECT FAIL")) != NULL )
				{		
					s_ErrCount++;
					s_Status--;
					u1_printf(" GPRS link failed\r\n");				
				}
				else if((strstr((const char *)GPRS_UART_BUF,"CONNECT")) != NULL )
				{
					ClearLogErrCode(LOG_CODE_UNATTACH);
					s_Step = 1;	
					s_Status++;		
					s_ErrCount = 0;
					SetTCModuleReadyFlag(TRUE);		//连接成功========================
					u1_printf(" GPRS link successful\r\n");				
				}
				else if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
				{		
					u1_printf("\r\n Wait Connect...\r\n");	
					s_LastTime = GetSystem10msCount();
				}
				else
				{
					s_Status--;	
					s_ErrCount++;
					u1_printf(" Unrecognized instruction\r\n");
				}
					
				
				if(s_ErrCount > 20 )
				{
					s_ErrCount = 0;
					s_Status = 1;
					s_Step = 1;
				}
				
				Clear_GPRS_Buff();
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1500) 
			{
				Clear_GPRS_Buff();
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				s_Status--;
				u1_printf(" AT No Ack\r\n");
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
				if(s_ErrCount >= 3)
				{
					SetLogErrCode(LOG_CODE_ATTIMEOUT);
					StoreOperationalData();
					s_Status = 1;
					s_Step = 1;
					s_ErrCount = 0;
				}
			}			
		break;
		
		case 12:		//SIM模块准备好
			s_Status = 50;
			s_LastTime = GetSystem10msCount();
			s_EndTime = GetSystem10msCount();
			SetLinkNetTime(CalculateTime(s_EndTime, s_StartTime) );
			u1_printf("\r\n 连接耗时:%.2fs\r\n\r\n", CalculateTime(s_EndTime, s_StartTime)/100.0);
		break;
			
			case 50:
				SetTCProtocolRunFlag(TRUE);
				s_Status++;
				g_TCRecStatus.AliveAckFlag = FALSE;
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d]", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
				Mod_Send_Alive();	//发送心跳包
				delay_ms(20);
				Mod_Report_Data();	//上报网关电压数据
				s_LastTime = GetSystem10msCount();
			break;
			
			case 51:
				if(g_TCRecStatus.AliveAckFlag)	//收到心跳包回复
				{
					s_Status++;
					s_LastTime = GetSystem10msCount();
					g_TCRecStatus.AliveAckFlag = FALSE;
					s_OverCount = 0;
				}
				else
				{
					if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 2500 )	//心跳包超时重发
					{
						s_Status--;
						s_LastTime = GetSystem10msCount();
						u1_printf(" No heartbeat reply packet was received for timeout\r\n");
						Clear_GPRS_Buff();
						s_OverCount++;
						if(s_OverCount >= 5)
						{
							SetLogErrCode(LOG_CODE_NOSERVERACK);
							StoreOperationalData();
							s_OverCount = 0;
							s_Status = 8;
							s_Step = 1;
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();
							SetTCModuleReadyFlag(0);
							SetOnline(FALSE);
							delay_ms(100);
							SimUartSendStr("+++");
							delay_ms(1100);
							SimUartSendStr("+++");
							u1_printf("[SIM]->Exit the passthrough\r\n");
							delay_ms(50);				
							SetTCProtocolRunFlag(FALSE);							
						}
					}
				}
			break;
									
			case 52:
				if( CalculateTime(GetSystem10msCount(),s_LastTime2) >= 200 )	//2S判断一次是否进入省电模式
				{
					if(GetPowerSavingFlag())
					{
						s_SavingModeFlag = GetPowerSavingFlag();
						if(JudgmentTimePoint(NETTYPE_2G))	//达到唤醒通信模块时间点
						{						
							if(s_SimSavingFlag)
							{
								s_Status = 1;
								s_Step = 1;
								s_ErrCount = 0;
								s_SimSavingFlag = FALSE;
							}
						}
						else	//到达省电模式时间点
						{
							s_SimSavingFlag = TRUE;
							SetTCModuleReadyFlag(FALSE);
							SetOnline(FALSE);
							SIM_PWR_OFF();
							s_LastTime2 = GetSystem10msCount();
						}
					}
					else
					{
						if(s_SavingModeFlag)	//退出省电模式，重启模块
						{
							s_SavingModeFlag = GetPowerSavingFlag();
							s_Status = 1;
							s_Step = 1;
							s_ErrCount = 0;
						}
						s_Status = 53;
					}
				}
				
				RetransmissionGPRSData();	//是否有GPRS下行包进行重发
				
			break;
				
			case 53:
				if( CalculateTime(GetSystem10msCount(),s_LastTime) >= p_sys->Heart_interval*100 )
				{				
					s_LastTime = GetSystem10msCount();
					s_Status = 50;
					u1_printf("\r\n Signal strength:%d\r\n", s_SignalStrength);
				}
				else
				{
					RetransmissionGPRSData();	//是否有GPRS下行包进行重发
					s_Status = 52;
					s_LastTime2 = GetSystem10msCount();
				}
				break;			
	}
	
}
