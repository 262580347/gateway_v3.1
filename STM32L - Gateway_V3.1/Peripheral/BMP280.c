/**********************************
说明：SSHT31温湿度传感器驱动程序
	  注意SDO接高电平，IIC地址为0x76
	  SDO接低电平，IIC地址为0x77
	  若接口更改则需修改宏定义及IIC_Init中的GPIO
作者：关宇晟
版本：V2017.12.26
***********************************/

#include "main.h"
#include "BMP280.h"
#include <math.h>

#define __STAND_TEMP      15.0f    // 15 C
#define __STAND_PRES      1013.25f // 101.325kPa = 1013.25mbar

#define  GPIO_I2C_DIR_SEND         ((unsigned char)0x00)
#define  GPIO_I2C_DIR_RECV         ((unsigned char)0x01)

#define BMP280_I2C_ADDR                         0x76 // SDO 接地 0x76，SDO接VCC  0x77

#define BMP280_REG_TEMP_XLSB                    0xFC
#define BMP280_REG_TEMP_LSB                     0xFB
#define BMP280_REG_TEMP_MSB                     0xFA
#define BMP280_REG_PRESS_XLSB                   0xF9
#define BMP280_REG_PRESS_LSB                    0xF8
#define BMP280_REG_PRESS_MSB                    0xF7
#define BMP280_REG_CONFIG                       0xF5
#define BMP280_REG_CTRL_MEAS                    0xF4
#define BMP280_REG_STATUS                       0xF3
#define BMP280_REG_RESET                        0xE0
#define BMP280_REG_ID                           0xD0

#define BMP280_REG_DIG_T1                       0x88
#define BMP280_REG_DIG_T2                       0x8A
#define BMP280_REG_DIG_T3                       0x8C

#define BMP280_REG_DIG_P1                       0x8E
#define BMP280_REG_DIG_P2                       0x90
#define BMP280_REG_DIG_P3                       0x92
#define BMP280_REG_DIG_P4                       0x94
#define BMP280_REG_DIG_P5                       0x96
#define BMP280_REG_DIG_P6                       0x98
#define BMP280_REG_DIG_P7                       0x9A
#define BMP280_REG_DIG_P8                       0x9C
#define BMP280_REG_DIG_P9                       0x9E
      
#define BMP280_ID                               0x58	//0x56 0x57 0x58?BMP280
#define BMP280_RESET                            0xB6


uint16 dig_T1;
int16  dig_T2;
int16  dig_T3;

uint16 dig_P1;
int16  dig_P2;
int16  dig_P3;
int16  dig_P4;
int16  dig_P5;
int16  dig_P6;
int16  dig_P7;
int16  dig_P8;
int16  dig_P9;

int32  t_fine;


float g_fBMPTemp = 0.0;
float g_fBMPPress = 0.0;
float g_fBMPAltitude = 0.0;

UINT8 g_ucBMP280SensorStatus = BMP_SENSOR_STATUS_NORMAL;

void SetBMP280SensorStatus( UINT8 ucStatus )
{
	g_ucBMP280SensorStatus = ucStatus;
}

UINT8 GetBMP280SensorStatus( void )
{
	return g_ucBMP280SensorStatus;
}

float GetBMPTemp( void )
{
	return g_fBMPTemp;
}

float GetBMPPress( void )
{
	return g_fBMPPress;
}

float GetBMPAltitude( void )
{
	return g_fBMPAltitude;
}


int BMP280_Init (void)
{
    unsigned char uc_ID = 0;
//    float f_temp, f_press;
    IIC_Init();
        
    IIC_Read_Reg (BMP280_I2C_ADDR, BMP280_REG_ID, &uc_ID);
    
    if (uc_ID != BMP280_ID) 
	{
        printf("[BMP280] uc_ID = 0x%02x，not BMP280 ID!Init failed!\r\n", uc_ID);
		//printf("[BMP280] Init failed!\n");
        return -1;
    }
   
    printf("[BMP280] ID = 0x%02x, Init Succeed ...\n", uc_ID);

    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_T1, (unsigned char *)&dig_T1, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_T2, (unsigned char *)&dig_T2, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_T3, (unsigned char *)&dig_T3, 2);

    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P1, (unsigned char *)&dig_P1, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P2, (unsigned char *)&dig_P2, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P3, (unsigned char *)&dig_P3, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P4, (unsigned char *)&dig_P4, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P5, (unsigned char *)&dig_P5, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P6, (unsigned char *)&dig_P6, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P7, (unsigned char *)&dig_P7, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P8, (unsigned char *)&dig_P8, 2);
    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_DIG_P9, (unsigned char *)&dig_P9, 2);
    
    IIC_Write_Reg (BMP280_I2C_ADDR, BMP280_REG_CTRL_MEAS, 0x3F);
//    delay_ms(10);
//	f_temp 		= BMP280Get()->Get_Temperature();
//	f_press 	= BMP280Get()->Get_Pressure();
//	delay_ms(10);//第一次数据不准确
//	f_temp 		= BMP280Get()->Get_Temperature();
//	f_press 	= BMP280Get()->Get_Pressure();
//	
//	printf("[BMP280] Temperature = %.2f ℃ ", f_temp);
//	printf(" Pressure    = %.2f hPa\r\n", f_press);
//	printf(" Altitude    = %.2f m\r\n", f_altitude);

    return 0;
}

static int BMP280Get_Temp_Raw (void)
{
    unsigned char temp[3] = {0};
    int     result = 0;

    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_TEMP_MSB, temp, 3);
    
    result = (temp[0] << 16) | (temp[1] << 8) | (temp[2]);
    
    result >>= 4;
    
    return result;
}

static int BMP280Get_Press_Raw (void)
{
    unsigned char press[3] = {0};
    int     result = 0;

    IIC_Read_Data (BMP280_I2C_ADDR, BMP280_REG_PRESS_MSB, press, 3);
    
    result = (press[0] << 16) | (press[1] << 8) | (press[2]);
    
    result >>= 4;
    
    return result;
}

// Returns temperature in DegC, resolution is 0.01 DegC. Output value of "5123" equals 51.23 DegC.
// t_fine carries fine temperature as global value
static int BMP280Get_Temp_x100 (void)
{
	
	//int32  t_fine;
    int raw_temp;   
    int adc_T;
    int32 var1, var2, T;
	
	raw_temp = BMP280Get_Temp_Raw();
	adc_T = raw_temp;
    
    var1 = ((((adc_T>>3) - ((int32)dig_T1<<1))) * ((int32)dig_T2)) >> 11;
    var2 = (((((adc_T>>4) - ((int32)dig_T1)) * ((adc_T>>4) - ((int32)dig_T1))) >> 12) * ((int32)dig_T3)) >> 14;
    
    t_fine = var1 + var2;
    
    T = (t_fine * 5 + 128) >> 8;
    
    return T;
}

static float BMP280Get_Temp (void)
{
    int tempc_x100 = BMP280Get_Temp_x100();
    return (float)(tempc_x100 / 100.0f);
}

// Returns pressure in Pa as unsigned 32 bit integer in Q24.8 format (24 integer bits and 8 fractional bits).
// Output value of "24674867" represents 24674867/256 = 96386.2 Pa = 963.862 hPa
static unsigned int BMP280Get_Press_Q24_8 (void)
{
    int raw_press = BMP280Get_Press_Raw();
    
    int adc_P = raw_press;
    
    long long var1, var2, p;
    
    var1 = ((long long)t_fine) - 128000;
    var2 = var1 * var1 * (long long)dig_P6;
    var2 = var2 + ((var1*(long long)dig_P5)<<17);
    var2 = var2 + (((long long)dig_P4)<<35);
    var1 = ((var1 * var1 * (long long)dig_P3)>>8) + ((var1 * (long long)dig_P2)<<12);
    var1 = (((((long long)1)<<47)+var1))*((long long)dig_P1)>>33;
    
    if (var1 == 0) {
        return 0; // avoid exception caused by division by zero
    }
    
    p = 1048576- adc_P;
    p = (((p<<31)-var2)*3125)/var1;
    var1 = (((long long)dig_P9) * (p>>13) * (p>>13)) >> 25;
    var2 = (((long long)dig_P8) * p) >> 19;
    p = ((p + var1 + var2) >> 8) + (((long long)dig_P7)<<4);
    
    return (unsigned int)p;
}

static float BMP280Get_Press (void)
{
    float pa;
    unsigned int press_q24_8 = BMP280Get_Press_Q24_8();
    pa = (float)(press_q24_8 / 256.0f);
    
    return pa / 100.0f;
}

// h = Z2-Z1 = 18400(1+atm)LgP1/P2
static float BMP280Get_Altitude (float temperature, float pressure)
{
    return 18400.0f * (1.0f + temperature / 273.0f) * log10(__STAND_PRES / pressure);
}

//static void New(void)
//{
//	pthis = &m_Instance;
//	
//	pthis->Init = BMP280_Init;
//	pthis->Get_Temperature = BMP280Get_Temp;
//	pthis->Get_Pressure = BMP280Get_Press;
//	pthis->Get_Altitude = BMP280Get_Altitude;
//}

//BMP280Fun  *BMP280Get(void)
//{
//	if(pthis == NULL)
//	{
//		New();
//	}
//	return pthis;
//}


UINT8 BMP280Detect( UINT16 nMain100ms )
{
	static UINT8 s_ucFirst = 1;
	static UINT16 s_nLast = 0;
	static UINT8 s_ucErrCount = 0;

	static float s_fLastBMPTemp = 25.0;
	static float s_fLastBMPPress = __STAND_PRES;
	
	static float s_fBMPTempBuf[10] =   {25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0 };
	static float s_fBMPPressBuf[10] =  {__STAND_PRES, __STAND_PRES, __STAND_PRES, __STAND_PRES, __STAND_PRES, __STAND_PRES, __STAND_PRES, __STAND_PRES, __STAND_PRES, __STAND_PRES };
	//static float fBMPAltitude[10]={25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0 };
	
	static UINT8 s_ucIndex = 0;
	
	float fBMPTemp = 0.0;
	float fBMPPress = 0.0;
	float fBMPAltitude = 0.0;
	
	UINT8 ucError = 0; 
	char str[250];
	
	if ( s_ucFirst )
	{
		s_ucFirst = 0;
		s_nLast = nMain100ms;
		BMP280_Init();
	}
	
	if ( GetTimeInterval( nMain100ms, s_nLast) < 2 * ONE_SECOND )
	{
		return 0;
	}
	
	s_nLast = nMain100ms;
	
	
	//ucError = GetBMP280();
	
	fBMPTemp = BMP280Get_Temp();
	fBMPPress = BMP280Get_Press();
	fBMPAltitude = BMP280Get_Altitude(fBMPTemp,fBMPPress);
	
	if( ucError == NO_ERROR )
	{
		s_ucErrCount =0;
		SetBMP280SensorStatus( BMP_SENSOR_STATUS_NORMAL );
		
		//温度数据两次检测数值差不大于2
		if( __fabs( fBMPTemp - s_fLastBMPTemp ) >= 2.0 )
		{
			s_fLastBMPTemp = fBMPTemp;
			sprintf(str,"[压力]:读取压力数据成功，温度=%5.3f,压力=%5.3fPa,方向=%5.3f,温度变化大于2度,上次温度=%5.3f，数据无效!\r\n",fBMPTemp,fBMPPress,fBMPAltitude,s_fLastBMPTemp);
			DEBUG_OUTPUT(str);
			return 0;
		}
		
		//两次检测压力数据差不大于10
		if( __fabs( fBMPPress - s_fLastBMPPress) >= 10.0 )
		{
			s_fLastBMPPress = fBMPPress;
			sprintf(str,"[压力]:读取压力数据成功，温度=%5.3f,压力=%5.3fPa,方向=%5.3f,压力变化大于10,上次压力=%5.3f，数据无效!\r\n",fBMPTemp,fBMPPress,fBMPAltitude,s_fLastBMPPress);
			DEBUG_OUTPUT(str);
			return 0;
		}
		
		//数值滤波和平均
		s_fLastBMPTemp = fBMPTemp;
		s_fLastBMPPress = fBMPPress;
		
		s_fBMPTempBuf[ s_ucIndex ] = fBMPTemp;
		s_fBMPPressBuf[ s_ucIndex++ ] = fBMPPress;
		
		if( s_ucIndex >= 10 )
		{
			s_ucIndex = 0;
		}
		
		g_fBMPTemp =  Mid_Filter(s_fBMPTempBuf,10);
		g_fBMPPress = Mid_Filter(s_fBMPPressBuf,10);
		g_fBMPAltitude = fBMPAltitude;
		
		sprintf(str,"[压力]:读取压力数据成功，温度=%5.3f,压力=%5.3fPa,方向=%5.3f\r\n",g_fBMPTemp,g_fBMPPress,g_fBMPAltitude);
//		sprintf(str,"[温湿度]:读取温湿度成功，温度=%5.3f, %5.3f 湿度=%5.3f, %5.3f\r\n",fTemp[0],g_fTemp,fHumi[0],g_fHumi);
		DEBUG_OUTPUT(str);
		
	}
	else
	{
		s_ucErrCount++;
		if( s_ucErrCount > 10 )
		{
			s_ucErrCount =0;
			SetBMP280SensorStatus( BMP_SENSOR_STATUS_ERR );
		}
		sprintf(str,"[压力]:读取压力数据失败,错误码=%d\r\n",ucError);
		DEBUG_OUTPUT(str);
		
	}
	
	
	return 0;
	
}



