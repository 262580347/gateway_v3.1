#ifndef __TIMER_H
#define __TIMER_H

#define ONE_SECOND	(10)
#define ONE_MINUTE	(60*ONE_SECOND)

void TIM4_Init(unsigned int arr,unsigned int psc);

unsigned short GetSystem100msCount(void);
unsigned short GetSystem10msCount(void);
unsigned short CalculateTime( unsigned short nNew, unsigned short nOld);

extern unsigned short   g_WakeUpTime;
	
#endif
