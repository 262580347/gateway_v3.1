#ifndef  _SIM800C_H_
#define	 _SIM800C_H_	

#include "compileconfig.h"
#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V100)

#define		SIM800C_ENABLE_TYPE		GPIOA		
#define		SIM800C_ENABLE_PIN		GPIO_Pin_6

#elif (HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)

#define		SIM800C_ENABLE_TYPE		GPIOA		
#define		SIM800C_ENABLE_PIN		GPIO_Pin_8

#endif

#define		SIM_PWR_ON()			GPIO_SetBits(SIM800C_ENABLE_TYPE, SIM800C_ENABLE_PIN);
#define		SIM_PWR_OFF()			GPIO_ResetBits(SIM800C_ENABLE_TYPE, SIM800C_ENABLE_PIN);

void SIM800CPortInit(void);

void SIM800CProcess(unsigned short nMain10ms);
	
unsigned char GetSim800cReadyFlag( void );

void SetSim800cResetFlag( unsigned char ucData );

void SetSim800cReadyFlag( unsigned char ucFlag );


#endif


