#ifndef  _GPRS_H_
#define	 _GPRS_H_

#define 	GPRS_BUFF_DATA_COUNT 30

#include "compileconfig.h"
#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V100)

#define		GPRS_PWR_PIN		GPIO_Pin_6
#define		GPRS_PWR_TYPE		GPIOA

#elif (HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)

#define		GPRS_PWR_PIN		GPIO_Pin_8
#define		GPRS_PWR_TYPE		GPIOA
#endif

#define		GPRS_PWR_ON()		GPIO_SetBits(GPRS_PWR_TYPE, GPRS_PWR_PIN);
#define		GPRS_PWR_OFF()		GPIO_ResetBits(GPRS_PWR_TYPE, GPRS_PWR_PIN);

#include "Lora.h"


typedef __packed struct 
{
	unsigned int 	m_lDeviceId;	  	//设备号
	unsigned char	m_lenth;		//数据长度
	unsigned char	m_data[64];		//数据
	unsigned short	m_LinkTimeNode; //时间点
	eDEVICETYPE		m_DeviceType;	
	void * m_pNext;
} GPRSDATALINKLIST;

extern GPRSDATALINKLIST * pGprsLinkedList;

extern unsigned char g_GPRSDataCount;

void GPRS_Process(unsigned short nMain10ms);

void OnGPRSComm(unsigned char CommData);

unsigned char GetGPRSReadyFlag( void );

void SetGPRSReadyFlag( unsigned char ucFlag );

void AddToGprsLinkedList( unsigned int lDeviceId, unsigned char lenth, unsigned char *data, unsigned short nMain100ms, eDEVICETYPE DeviceType);

void DelFromGprsLinkedList(unsigned int lDeviceId , unsigned short TimeNode);

void InitGprsLinkedList( void );

void RetransmissionGPRSData(void);

#endif
