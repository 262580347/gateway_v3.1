#include "main.h"
#include "GPS.h"

static float s_Latitude = 0, s_Longitude = 0;

static u8 s_GPSRunFlag = TRUE;

float GetLongitude(void)
{
	return s_Longitude;
}

void SetLongitude(float temp)
{
	s_Longitude = temp;
}

float GetLatitude(void)
{
	return s_Latitude;
}

void SetLatitude(float temp)
{
	s_Latitude = temp;
}

unsigned char GetGPSRunFlag(void)
{
	return s_GPSRunFlag;
}

void SetGPSRunFlag(unsigned char isTrue)
{
	s_GPSRunFlag = isTrue;
}


