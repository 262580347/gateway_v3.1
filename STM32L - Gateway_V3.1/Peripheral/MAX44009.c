/**********************************
说明：MAX44009底层驱动程序
	  需要进行系数补偿，默认补偿值为3.8
	  开启高亮寄存器以使用高亮度环境
作者：关宇晟
版本：V2018.4.3
***********************************/
#include "main.h"
#include "MAX44009.h"


#define CMD_MAX_READ		0x01
#define CMD_MAX_WRITE		0x00

#define MAX44009_ADDR 		0x94 //0x96 A0 to VCC , 0x94 A0 to GND
#define INT_STATUS 			0x00
#define INT_ENABLE 			0x01
#define CONFIG_REG 			0x02
#define HIGH_BYTE 			0x03
#define LOW_BYTE 			0x04
#define THRESH_HIGH 		0x05
#define THRESH_LOW 			0x06
#define THRESH_TIMER 		0x07

#define	SDA_PIN		GPIO_Pin_14
#define	SDA_TYPE	GPIOA
#define	SCK_PIN		GPIO_Pin_15
#define	SCK_TYPE	GPIOA

unsigned char g_MAXInitFlag = FALSE;

#define SDA_IN()  											\
{															\
	GPIO_InitTypeDef  GPIO_InitStructure;  					\
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					\
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;			\
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			\
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;		\
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		\
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);			\
}
#define SDA_OUT()   										\
{															\
	GPIO_InitTypeDef  GPIO_InitStructure;  					\
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					\
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			\
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;			\
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;		\
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		\
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);			\
}


#define MAX_SCK_HIGH    GPIO_SetBits(SCK_TYPE, SCK_PIN)
#define MAX_SCK_LOW 	GPIO_ResetBits(SCK_TYPE, SCK_PIN)
#define MAX_SDA_HIGH    GPIO_SetBits(SDA_TYPE, SDA_PIN)
#define MAX_SDA_LOW 	GPIO_ResetBits(SDA_TYPE, SDA_PIN)
#define READ_SDA   		GPIO_ReadInputDataBit(SDA_TYPE, SDA_PIN)

#define	MAX_DELAY	delay_us(4)

unsigned char g_MAXGetFlag = TRUE;

static float s_Ill_Val = 0;

static void IIC_Start(void)
{
	SDA_OUT();
	MAX_SDA_HIGH;
	MAX_SCK_HIGH;
	MAX_DELAY;
	MAX_SDA_LOW;
	MAX_DELAY;
	MAX_SCK_LOW;
}


static void IIC_Stop(void)
{
	SDA_OUT();
	MAX_SCK_LOW;
	MAX_SDA_LOW;
	MAX_DELAY;
	MAX_SCK_HIGH;
	MAX_DELAY;
	MAX_SDA_HIGH;
	MAX_DELAY;
}

static void SHT_ACK(void)
{
	MAX_SCK_LOW;
	SDA_OUT();
	MAX_SDA_LOW;
	MAX_DELAY;
	MAX_SCK_HIGH;
	MAX_DELAY;
	MAX_SCK_LOW;
}

static void No_ACK(void)
{
	MAX_SCK_LOW;	
	SDA_OUT();
	MAX_SDA_HIGH;
	MAX_DELAY;
	MAX_SCK_HIGH;
	MAX_DELAY;
	MAX_SCK_LOW;
}
static u8 Wait_ACK(void)
{
	u8 ucErrTime=0;
	SDA_IN();;
	MAX_SDA_HIGH;	
	MAX_DELAY;	
	MAX_SCK_HIGH;
	MAX_DELAY;
	while(READ_SDA)
	{
		ucErrTime++;
		if(ucErrTime >250)
		{
			IIC_Stop();
			return 1;
		}
	}
	MAX_SCK_LOW;
	return 0;
}

static void WriteByte(u8 data)
{
	u8 i;
	
	SDA_OUT();
	MAX_SCK_LOW;
	for(i=0; i<8; i++)
	{
		if(data & 0x80)
			MAX_SDA_HIGH;
		else
			MAX_SDA_LOW;
		data <<= 1;
		MAX_DELAY;
		MAX_SCK_HIGH;
		MAX_DELAY;
		MAX_SCK_LOW;
		MAX_DELAY;
	}	
}

static u8 ReadByte(u8 IsAck)
{
	u8 i, receive = 0;
	SDA_IN();;
	for(i=0; i<8; i++)
	{
		MAX_SCK_LOW;
		MAX_DELAY;
		MAX_SCK_HIGH;
		MAX_DELAY;
		receive <<= 1;
		if(READ_SDA)
			receive++;
		MAX_DELAY;
	}
	if(IsAck)
		SHT_ACK();
	else
		No_ACK();
	
	return receive;
}	


static u8 MAX44009_Read_Lux(u8 *HighData, u8 *LowData)
{
	u8  ack = 0;
	IIC_Start();  
	WriteByte(MAX44009_ADDR | CMD_MAX_WRITE); 
	ack = Wait_ACK(); 
	if(ack)
		return 1;
	WriteByte(HIGH_BYTE);
	ack = Wait_ACK(); 

	IIC_Start();  
	WriteByte(MAX44009_ADDR | CMD_MAX_READ); 
	ack = Wait_ACK();	

	*HighData = ReadByte(TRUE);

	IIC_Start();  
	WriteByte(LOW_BYTE);
	ack = Wait_ACK(); 

	*LowData = ReadByte(FALSE);
	
	IIC_Stop();
	return 0;
}

static u8 MAX44009_Write(u8 Reg, u8 data)
{
	u8 err;
	IIC_Start();
	WriteByte(MAX44009_ADDR | CMD_MAX_WRITE);
	err = Wait_ACK();
	if(err)
		return 1;
	
	WriteByte(Reg);
	err = Wait_ACK();
	if(err)
		return 1;

	WriteByte(data);
	err = Wait_ACK();
	if(err)
		return 1;
	
	IIC_Stop();
	return 0;
}

/********************************************************************************/
/********************************************************************************/
static void IIC_MAX44009_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;  	
	
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);		

	GPIO_InitStructure.GPIO_Pin = SCK_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SCK_TYPE, &GPIO_InitStructure);	

	GPIO_SetBits(SDA_TYPE, SDA_PIN);
	GPIO_SetBits(SCK_TYPE, SCK_PIN);
}


static float MAX44009_GetValue(void)
{
	uint8 LowByte = 0, HighByte = 0, Exponent, Mantissa, err;
	float Result;
	
	err = MAX44009_Read_Lux(&HighByte, &LowByte);
	if(err)
	{
		printf("\r\n MAX44009传感器错误，检测接线\r\n");
		return ERROR_VALUE;
	}
	delay_ms(10);

	Exponent = (HighByte & 0xF0) >> 4;
	if(Exponent == 0xF0)
	{
		printf("[MAX44009]超量程范围\r\n");
		return ERROR_VALUE;
	}
	Mantissa = (HighByte & 0x0F) << 4;

	Mantissa += LowByte & 0x0F; 

	Result = Mantissa * (1 << Exponent) * 0.045;
	return Result;	
}

unsigned char MAX44009_Init(void)
{
	u8 err;

	float  illuminance = 0.0;
	
//	printf("[MAX44009]初始化MAX44009...\r\n");
	
	IIC_MAX44009_Init();
	
	delay_ms(200); 
	
    err = MAX44009_Write(CONFIG_REG, 0x08);
	
	if(err)
	{
		printf("\r\n [MAX44009]初始化MAX44009失败\r\n");
		g_MAXInitFlag = TRUE;
		s_Ill_Val = ERROR_VALUE;
	}
	else
	{
//		printf("[MAX44009]初始化MAX44009成功\r\n");
		
		delay_ms(200);
		illuminance = MAX44009_GetValue();
		s_Ill_Val = illuminance;
		printf("\r\n [MAX44009]illuminance:%f\r\n", illuminance);
	}

	return err;
}

void MAX_Process(unsigned short nMain10ms)
{
	static unsigned short s_LastTime = 0;
	float f_Ill_Value;
	static u8 s_State = 1;
	
	if(g_MAXGetFlag == FALSE)
	{			
		switch(s_State)
		{
			case 1:					
//				SHT3X_SetPeriodicMeasurement();
				s_LastTime = nMain10ms;	
				s_State++;
			break;
					
			case 2:			
				if(CalculateTime(nMain10ms, s_LastTime) >= 3)
				{
					s_State--;				
					g_MAXGetFlag = TRUE;
					f_Ill_Value = MAX44009_GetValue();
					if(f_Ill_Value == ERROR_VALUE)
					{
						s_Ill_Val = ERROR_VALUE;
						printf("\r\n 错误的MAX44009数据\r\n");
						
					}
					else
					{
						s_Ill_Val = f_Ill_Value;
						printf("\r\n Ill:%2.2f\r\n", s_Ill_Val);
					}
				}
			break;
		}
	}
}

float Get_Ill_Value(void)
{
	return s_Ill_Val;
}
