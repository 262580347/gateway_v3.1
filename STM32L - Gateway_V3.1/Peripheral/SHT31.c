/**********************************
说明：SHT31温湿度传感器驱动程序
	  注意ADDR接高电平，CMD_WRITE为0x88
	  ADDR接低电平，CMD_WRITE为0x8A
	  芯片自带CRC检验，检验多项式为P(x) = x^8 + x^5 + x^4 + 1 = 100110001
	  若读数错误则返回ERROR_VALUE
	  若接口更改则需修改宏定义及IIC_Init中的GPIO
	  建议1S读一次温湿度值，最快至少需间隔0.5S
	  初始化时已强制失能自动加热功能
	  低功耗设备中使用单次测量模式降低功耗
作者：关宇晟
版本：V2018.4.3
***********************************/

#include "main.h"
#include "SHT31.h"

#define  CMD_WRITE           0x88	//ADDR Connect GND else Write :0x8A Read :0x8B
#define  CMD_READ            0x89

/*CRC*/
#define POLYNOMIAL             0x131           // P(x) = x^8 + x^5 + x^4 + 1 = 100110001


#define	SDA_PIN		GPIO_Pin_14
#define	SDA_TYPE	GPIOA
#define	SCK_PIN		GPIO_Pin_15
#define	SCK_TYPE	GPIOA

#define SDA_IN()  											\
{															\
	GPIO_InitTypeDef  GPIO_InitStructure;  					\
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					\
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;			\
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			\
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;		\
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		\
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);			\
}
#define SDA_OUT()   										\
{															\
	GPIO_InitTypeDef  GPIO_InitStructure;  					\
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					\
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			\
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;			\
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;		\
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		\
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);			\
}


#define SHT_SCK_HIGH    GPIO_SetBits(SCK_TYPE, SCK_PIN)
#define SHT_SCK_LOW 	GPIO_ResetBits(SCK_TYPE, SCK_PIN)
#define SHT_SDA_HIGH    GPIO_SetBits(SDA_TYPE, SDA_PIN)
#define SHT_SDA_LOW 	GPIO_ResetBits(SDA_TYPE, SDA_PIN)
#define READ_SDA   		GPIO_ReadInputDataBit(SDA_TYPE, SDA_PIN)

#define	SHT_DELAY	delay_us(4)

unsigned char g_SHTGetFlag = TRUE;
static float s_Temp_Val = 0;
static float s_Humi_Val = 0;
static void IIC_Start(void)
{
	SDA_OUT();
	SHT_SDA_HIGH;
	SHT_SCK_HIGH;
	SHT_DELAY;
	SHT_SDA_LOW;
	SHT_DELAY;
	SHT_SCK_LOW;
}


static void IIC_Stop(void)
{
	SDA_OUT();
	SHT_SCK_LOW;
	SHT_SDA_LOW;
	SHT_DELAY;
	SHT_SCK_HIGH;
	SHT_DELAY;
	SHT_SDA_HIGH;
	SHT_DELAY;
}

static void SHT_ACK(void)
{
	SHT_SCK_LOW;
	SDA_OUT();
	SHT_SDA_LOW;
	SHT_DELAY;
	SHT_SCK_HIGH;
	SHT_DELAY;
	SHT_SCK_LOW;
}

static void No_ACK(void)
{
	SHT_SCK_LOW;	
	SDA_OUT();
	SHT_SDA_HIGH;
	SHT_DELAY;
	SHT_SCK_HIGH;
	SHT_DELAY;
	SHT_SCK_LOW;
}
static u8 Wait_ACK(void)
{
	u8 ucErrTime=0;
	SDA_IN();
	SHT_SDA_HIGH;	
	SHT_DELAY;	
	SHT_SCK_HIGH;
	SHT_DELAY;
	while(READ_SDA)
	{
		ucErrTime++;
		if(ucErrTime >250)
		{
			IIC_Stop();
			return 1;
		}
	}
	SHT_SCK_LOW;
	return 0;
}

static void WriteByte(u8 data)
{
	u8 i;
	
	SDA_OUT();
	SHT_SCK_LOW;
	for(i=0; i<8; i++)
	{
		if(data & 0x80)
			SHT_SDA_HIGH;
		else
			SHT_SDA_LOW;
		data <<= 1;
		SHT_DELAY;
		SHT_SCK_HIGH;
		SHT_DELAY;
		SHT_SCK_LOW;
		SHT_DELAY;
	}	
}

static u8 ReadByte2(u8 IsAck)
{
	u8 i, receive = 0;
	SDA_IN();;
	for(i=0; i<8; i++)
	{
		SHT_SCK_LOW;
		SHT_DELAY;
		SHT_SCK_HIGH;
		SHT_DELAY;
		receive <<= 1;
		if(READ_SDA)
			receive++;
		SHT_DELAY;
	}
	if(IsAck)
		SHT_ACK();
	else
		No_ACK();
	
	return receive;
}	


/******************************************************/
static u8 SHT3X_WriteCMD(uint16 cmd)
{
	u8 err = 0;
	IIC_Start();
	WriteByte(CMD_WRITE);
	err = Wait_ACK();
	if(err)
		return 1;
	WriteByte(cmd>>8);
	err = Wait_ACK();
	if(err)
		return 1;
	WriteByte(cmd);
	err = Wait_ACK();
	if(err)
		return 1;
	IIC_Stop();
	
	return 0;
}

/********************************************************/
static u8 SHT3X_SetPeriodicMeasurement()
{
	u8 err = 0;
    err = SHT3X_WriteCMD(CMD_MEAS_POLLING_H);
	return err;
}


static u8 SHX3X_ReadResults(uint16 cmd,  uint8 *data)
{
	u8 err;
	IIC_Start();
	WriteByte(CMD_WRITE);
	err = Wait_ACK();
	if(err)
		return 1;
	WriteByte(cmd>>8);
	err = Wait_ACK();
	if(err)
		return 1;
	WriteByte(cmd);
	err = Wait_ACK();
	if(err)
		return 1;

    IIC_Start();
	WriteByte(CMD_READ);
	err = Wait_ACK();
	if(err)
		return 1;

	data[0] = ReadByte2(TRUE);

	data[1] = ReadByte2(TRUE);

	data[2] = ReadByte2(TRUE);

	data[3] = ReadByte2(TRUE);

	data[4] = ReadByte2(TRUE);

	data[5] = ReadByte2(FALSE);

	IIC_Stop();
	
	return 0;
}

/********************************************************************************/
/********************************************************************************/
void IIC_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;  	
	
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);		

	GPIO_InitStructure.GPIO_Pin = SCK_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SCK_TYPE, &GPIO_InitStructure);	

	GPIO_SetBits(SDA_TYPE, SDA_PIN);
	GPIO_SetBits(SCK_TYPE, SCK_PIN);
}

void IIC_Reset(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;  	
	
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);		

	GPIO_InitStructure.GPIO_Pin = SCK_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;		
	GPIO_Init(SCK_TYPE, &GPIO_InitStructure);	

}
unsigned char SHT_Init(void)
{
	u8 err;
	float Temp, Humi;
	
	IIC_Init();
	delay_ms(10); 
    err = SHT3X_SetPeriodicMeasurement();
	delay_ms(10);
	if(err)
	{
		printf("\r\n [SHT31]初始化SHT31失败\r\n");
	}
	else
	{
		SHT_GetValue(&Temp, &Humi);
		s_Temp_Val = Temp;
		s_Humi_Val = Humi;
		if(Temp == ERROR_VALUE || Humi == ERROR_VALUE)
		{
			printf("\r\n [SHT31]初始化SHT31失败 error data\r\n");
			return 1;
		}
		else
		{		
//			printf("[SHT31]初始化SHT31成功\r\n");			
			printf("\r\n [SHT31]Temp: %2.2f℃  Humi: %2.2f%%\r\n", Temp, Humi);
		}
	}
	
	return err;
}


static uint8 SHT3X_CalcCrc(uint8 *data, uint8 nbrOfBytes)
{
	uint8 bit;        // bit mask
    uint8 crc = 0xFF; // calculated checksum
    uint8 byteCtr;    // byte counter

    // calculates 8-Bit checksum with given polynomial
    for(byteCtr = 0; byteCtr < nbrOfBytes; byteCtr++) {
        crc ^= (data[byteCtr]);
        for(bit = 8; bit > 0; --bit) {
            if(crc & 0x80) {
                crc = (crc << 1) ^ POLYNOMIAL;
            }  else {
                crc = (crc << 1);
            }
        }
    }
	return crc;
}

static uint8 SHT3X_CheckCrc(uint8 *pdata, uint8 nbrOfBytes, uint8 checksum)
{
    uint8 crc;
	crc = SHT3X_CalcCrc(pdata, nbrOfBytes);// calculates 8-Bit checksum
    if(crc != checksum) 
    {   
        return 1;           
    }
    return 0;              
}

static float SHT3X_CalcTemperature(uint16 rawValue)
{
    // calculate temperature 
    float temp;
    temp = (175 * (float)rawValue / 65535 - 45) ; // T = -45 + 175 * rawValue / (2^16-1)
    return temp;
}

static float SHT3X_CalcRH(uint16 rawValue)
{
    // calculate relative humidity [%RH]
    float temp1 ;
	temp1 = (100 * (float)rawValue / 65535) ;  // RH = rawValue / (2^16-1) * 10
    return temp1;
}

void SHT_GetValue(float *f_Tem_Value, float *f_RH_Value)
{
    uint8 temp = 0, err;
    uint16 dat;
    uint8 data[3];
    static uint8 buffer[6];
	
    err = SHX3X_ReadResults(CMD_FETCH_DATA, buffer);
	if(err)
	{
		printf("\r\n 错误的SHT数据\r\n");
		*f_Tem_Value = ERROR_VALUE;//异常值
		*f_RH_Value = ERROR_VALUE;
		return;
	}
    /* check tem */
    data[0] = buffer[0];
    data[1] = buffer[1];
    data[2] = buffer[2];
    temp = SHT3X_CheckCrc(data,2,data[2]);
    if( !temp ) /* value is ture */
    {
        dat = ((uint16)buffer[0] << 8) | buffer[1];
        *f_Tem_Value = SHT3X_CalcTemperature( dat );    
    }
	else
	{
		printf("CheckCrc1\r\n");
		*f_Tem_Value = ERROR_VALUE;//异常值
	}
    /* check humidity */
    data[0] = buffer[3];
    data[1] = buffer[4];
    data[2] = buffer[5];
    temp = SHT3X_CheckCrc(data,2,data[2]);
    if( !temp )
    {
        dat = ((uint16)data[0] << 8) | data[1];
        *f_RH_Value = SHT3X_CalcRH( dat ); 
    }
	else
	{
		printf("CheckCrc2\r\n");
		*f_RH_Value = ERROR_VALUE;//异常值
	}
}

float Get_Temp_Value(void)
{
	return s_Temp_Val;
}

float Get_Humi_Value(void)
{
	return s_Humi_Val;
}
void SHT_Process(unsigned short nMain10ms)
{
	static unsigned short s_LastTime = 0;
	float f_Tem_Value, f_RH_Value;
	static u8 s_State = 1;
	
	if(g_SHTGetFlag == FALSE)
	{			
		switch(s_State)
		{
			case 1:					
				SHT3X_SetPeriodicMeasurement();
				s_LastTime = nMain10ms;	
				s_State++;
			break;
					
			case 2:			
				if(CalculateTime(nMain10ms, s_LastTime) >= 20)
				{
					s_State--;				
					g_SHTGetFlag = TRUE;
					SHT_GetValue(&f_Tem_Value, &f_RH_Value);
					if(f_Tem_Value == ERROR_VALUE)
					{
						s_Temp_Val = ERROR_VALUE;
						printf("\r\n 错误的温度数据      ");
					}
					else
					{
						s_Temp_Val = f_Tem_Value;
						printf("\r\n Temp:%2.2f℃    ", f_Tem_Value);
					}
					if(f_RH_Value == ERROR_VALUE)
					{
						s_Humi_Val = ERROR_VALUE;
						printf("    错误的湿度数据\r\n ");
					}
					else
					{
						s_Humi_Val = f_RH_Value;
						printf("    Humi:%2.1f%% \r\n", f_RH_Value);
					}
				}
			break;
		}
	}
}
