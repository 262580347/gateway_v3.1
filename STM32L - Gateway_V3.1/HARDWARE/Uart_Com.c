/**********************************
说明:调试使用的串口代码
	  
作者:关宇晟
版本:V2017.4.3
***********************************/
#include "Uart_Com.h"
#include "main.h"

void Create_Alive_Message(unsigned int Device_ID)
{
	u16 nMsgLen, i;
	unsigned char send_buf[64];
	unsigned char AlivePack[64];
	unsigned int len;
	static uint16 seq_no = 0;
	CLOUD_HDR *hdr;

	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(PROTOCOL_GATEWAY_ID);							//通信协议版本号
	hdr->device_id = swap_dword(Device_ID);		//设备号
	hdr->dir = 0;													//方向
	hdr->seq_no = swap_word(seq_no++);								//序号

	hdr->payload_len = swap_word(6);							//信息域长度
	hdr->cmd = CMD_HEATBEAT;									//命令字

	len = sizeof(CLOUD_HDR);									//未加校验的数据包长度

	send_buf[len++] = 	SENSOR_TYPE_GATEWAY;	//传感器网关  200
	
	send_buf[len++] = 1;	//通道1
	
	send_buf[len++] = 0;
	send_buf[len++] = 100;	//协议版本
	
	send_buf[len++] = XML_BAT_VOLTAGE;	//电压标识
	send_buf[len++] = 84;
	
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
		
	nMsgLen = PackMsg(send_buf, len, AlivePack, 200);	//数据包转义处理	
	u1_printf("[模拟心跳] -> %d(%d): ", Device_ID, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",AlivePack[i]);
	}
	u1_printf("\r\n");	
}

void Create_Data_Message(unsigned int Device_ID)
{
	u16 nMsgLen, i;
	unsigned char send_buf[64];
	unsigned char Pack[64];
	unsigned int len = 0;
	static u8 seq_no = 0;


	send_buf[len++] = PROTOCOL_CONTROL_ID >> 8;
	send_buf[len++] = PROTOCOL_CONTROL_ID&0xff;
	
	send_buf[len++] = Device_ID >> 24;
	send_buf[len++] = Device_ID >> 16;	
	send_buf[len++] = Device_ID >> 8;
	send_buf[len++] = Device_ID;	
	
	send_buf[len++]  = 0;
	
	send_buf[len++]  = seq_no >> 8;
	send_buf[len++]  = seq_no++;
	
	send_buf[len++]  = 0;
	send_buf[len++]  = 8;

	send_buf[len++] = CMD_REPORT_D;									//命令字
	
	send_buf[len++] = 0x01;	
	send_buf[len++] = 0x00;
	send_buf[len++] = 0x01;	
	send_buf[len++] = 0x00;
	send_buf[len++] = 0xff;	
	send_buf[len++] = 0x04;	
	send_buf[len++] = 0x20;
	send_buf[len++] = 0xD0;
	
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, Pack, 120);		//数据包转义处理	
	u1_printf(" [模拟数据包] %d(%d) ->: ", Device_ID, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",Pack[i]);
	}
	u1_printf("\r\n");	
}

static void COM1Protocol(void)
{
	unsigned char PackBuff[100], DataPack[100];
	u16   PackLength = 0, RecLength = 0, i;
	static CLOUD_HDR *hdr;
	
	UnPackMsg(g_USART_RX_BUF+1, g_USART_RX_CNT-2, PackBuff, &PackLength);	//解包

	if (PackBuff[(PackLength)-1] == Calc_Checksum(PackBuff, (PackLength)-1))
	{	
		hdr = (CLOUD_HDR *)PackBuff;
		hdr->protocol = swap_word(hdr->protocol);
		hdr->device_id = swap_dword(hdr->device_id);
		hdr->seq_no = swap_word(hdr->seq_no);
		hdr->payload_len = swap_word(hdr->payload_len);
		
		RecLength = hdr->payload_len + sizeof(CLOUD_HDR) + 1;
					
		if (RecLength != (PackLength))
		{
			u1_printf(" Pack Length Err\r\n");
		}
		else
		{
			PackLength--;	
			memcpy(&DataPack, PackBuff, sizeof(CLOUD_HDR));

			if(hdr->payload_len != 0)
				memcpy(&DataPack[sizeof(CLOUD_HDR)], &PackBuff[sizeof(CLOUD_HDR)], hdr->payload_len);

			OnDebug(DataPack, PackLength);
		}	
	}
	else
	{
		for(i=0; i<PackLength; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");
		u1_printf(" CRC Error :%02X\r\n", Calc_Checksum(PackBuff, (PackLength)-1));
	}	
}

void TaskForDebugCOM(void)
{	
	unsigned int l_DeviceID = 0;
	unsigned short DelayTime = 0, Channel = 0;
	uint16 i;
	GPIO_InitTypeDef  GPIO_InitStructure;

	if(g_UartRxFlag == TRUE)
	{
		
		if(strncmp((char *)g_USART_RX_BUF, "RESET", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n RESET CMD\r\n");
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			Sys_Soft_Reset();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "LORA SLEEP", g_USART_RX_CNT) == 0)
		{
			u1_printf("LORA SLEEP\r\n");
			LORA_SLEEP_MODE();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "LORA WORK", g_USART_RX_CNT) == 0)
		{
			u1_printf("LORA WORK\r\n");
			LORA_WORK_MODE();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "WIFI ON", g_USART_RX_CNT) == 0)
		{
			u1_printf("WIFI ON\r\n");
			WiFi_Port_Init();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "WIFI RESET", g_USART_RX_CNT) == 0)
		{
			u1_printf("WIFI RESET\r\n");
			WIFI_RST_HIGH();
			delay_ms(200);			
			WIFI_RST_LOW();		
			delay_ms(200);		
			WIFI_RST_HIGH();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "WIFI OFF", g_USART_RX_CNT) == 0)
		{
			u1_printf("WIFI OFF\r\n");
			WiFi_Port_Reset();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "DEL ", 4) == 0)
		{
			l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[4], g_USART_RX_CNT-4);
			u1_printf(" Del %d\r\n", l_DeviceID);
			DelFromLoraLinkedList(l_DeviceID);

		}
		else if(strncmp((char *)g_USART_RX_BUF, "CCC ", 4) == 0)
		{
			l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[4], g_USART_RX_CNT-4);
			u1_printf("CCC: %d\r\n", l_DeviceID);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "BOARD15S", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n 手动控制:15S广播发送     ");
			Lora_Send_Boardcast(1500);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "BOARD3S", g_USART_RX_CNT) == 0)
		{
			u1_printf(" 手动控制:3S广播发送     ");
			Lora_Send_Boardcast(300);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "BOARD5S", g_USART_RX_CNT) == 0)
		{
			u1_printf(" 手动控制:5S广播发送     ");	
			Lora_Send_Boardcast(500);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "POLL ", 5) == 0)
		{
			l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[5], g_USART_RX_CNT-5);
			u1_printf(" 手动控制:轮询 -> (%d)     ", l_DeviceID);
			Lora_Send_Polling(l_DeviceID, 300);

		}
		else if(strncmp((char *)g_USART_RX_BUF, "GATE ", 5) == 0)
		{
			l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[5], g_USART_RX_CNT-5);
			u1_printf(" 网关查询 -> (%d)     ", l_DeviceID);
			Lora_Send_TestInfo(l_DeviceID);

		}
		else if(strncmp((char *)g_USART_RX_BUF, "CHON ", 5) == 0)
		{
			LORA_ROUSE_MODE();
			
			Channel = ArrayToInt32(&g_USART_RX_BUF[5], 2);
			
			l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[8], g_USART_RX_CNT-8);
			
			u1_printf(" 手动控制 CH(%d)开启 -> (%d)  (%dmin)   ", Channel, l_DeviceID, 1);
			LORA_WORK_DELAY();
			Lora_Send_Control(l_DeviceID, Channel, 1, 1);
			LORA_SLEEP_DELAY();
			LORA_WORK_MODE();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "CHST ", 5) == 0)
		{
			LORA_ROUSE_MODE();
			Channel = ArrayToInt32(&g_USART_RX_BUF[5], 2);
			
			l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[8], g_USART_RX_CNT-8);
			
			u1_printf(" 手动控制 CH(%d)停止 -> (%d)  (%dmin)   ", Channel, l_DeviceID, 1);
			LORA_WORK_DELAY();
			Lora_Send_Control(l_DeviceID, Channel, 0, 1);
			LORA_SLEEP_DELAY();
			LORA_WORK_MODE();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "CHOF ", 5) == 0)
		{
			LORA_ROUSE_MODE();
			Channel = ArrayToInt32(&g_USART_RX_BUF[5], 2);
			
			l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[8], g_USART_RX_CNT-8);
			
			u1_printf(" 手动控制 CH(%d)关闭 -> (%d)  (%dmin)   ", Channel, l_DeviceID, 1);
			LORA_WORK_DELAY();
			Lora_Send_Control(l_DeviceID, Channel, 2, 1);
			LORA_SLEEP_DELAY();
			LORA_WORK_MODE();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "TEST ", 5) == 0)
		{
			LORA_ROUSE_MODE();
			LORA_WORK_DELAY();
			l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[5], g_USART_RX_CNT-5);
			u1_printf(" 测试 -> ");
			Lora_Send_TestInfo(l_DeviceID);
			LORA_SLEEP_DELAY();
			LORA_WORK_MODE();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "LORA ", 5) == 0)
		{
			l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[5], g_USART_RX_CNT-5);
			u1_printf(" LORA测试 -> %d", l_DeviceID);
			Lora_Send_TestInfo(l_DeviceID);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "ROUSE ", 6) == 0)
		{
			LORA_ROUSE_MODE();
			DelayTime = ArrayToInt32(&g_USART_RX_BUF[6], g_USART_RX_CNT-6);
			delay_ms(DelayTime)	;		
			Lora_Send_TestInfo(4444);
			u1_printf(" 延时测试 -> %dms\r\n", DelayTime);
			LORA_SLEEP_DELAY();
			LORA_WORK_MODE();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "CHTEST ", 7) == 0)
		{
			LORA_ROUSE_MODE();
			DelayTime = ArrayToInt32(&g_USART_RX_BUF[7], g_USART_RX_CNT-7);			
			delay_ms(DelayTime)	;	
			Lora_Send_Control(4444, 2, 1, 1);
			u1_printf(" TEST:CH2开启 -> (%d)(%dms)\r\n", 4444, DelayTime);
			LORA_SLEEP_DELAY();
			LORA_WORK_MODE();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "REBOOT ", 7) == 0)
		{
			LORA_ROUSE_MODE();
			LORA_WORK_DELAY();
			l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[7], g_USART_RX_CNT-7);
			u1_printf(" 远程重启 -> ");
			Lora_Send_Reboot(l_DeviceID);
			LORA_SLEEP_DELAY();
			LORA_WORK_MODE();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "Alive ", 6) == 0)
		{
			l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[6], g_USART_RX_CNT-6);
			Create_Alive_Message(l_DeviceID);
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "Data ", 5) == 0)
		{
			l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[5], g_USART_RX_CNT-5);
			Create_Data_Message(l_DeviceID);
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "Data1 ", 6) == 0)
		{
			l_DeviceID = ArrayToInt32(&g_USART_RX_BUF[6], g_USART_RX_CNT-6);
			Create_Data_Message(l_DeviceID|0x10000000);
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "LinkList", g_USART_RX_CNT) == 0)
		{
			PrintLoraLinkedList();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "SIM800C", g_USART_RX_CNT) == 0)
		{
			u1_printf("SIM800C ON/OFF \r\n");
			GPIO_ToggleBits(SIM800C_ENABLE_TYPE,SIM800C_ENABLE_PIN);
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "GPRS_Send_Alive", g_USART_RX_CNT) == 0)
		{
			Mod_Send_Alive();
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "BATTEST", g_USART_RX_CNT) == 0)
		{
			g_PowerDetectFlag = FALSE;
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "ShowTime", g_USART_RX_CNT) == 0)
		{
			RTC_TimeShow();
		}
		else if(strncmp((char *)g_USART_RX_BUF, "GPRSVOL", g_USART_RX_CNT) == 0)
		{
			Mod_Report_Data();
		}			
		else if(strncmp((char *)g_USART_RX_BUF, "SIM_Send_Alive", g_USART_RX_CNT) == 0)
		{
			u1_printf("SIM_Send_Alive\r\n");
			Mod_Send_Alive();
		}	
		else if(strncmp((char *)g_USART_RX_BUF, "DTR", 3) == 0)
		{
			u1_printf("DTR\r\n");
			GPIO_ToggleBits(SIM7600CE_DTR_TYPE, SIM7600CE_DTR_PIN);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "+++", 3) == 0)
		{
			u2_printf("+++", g_USART_RX_BUF);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "AT", 2) == 0)
		{
			u1_printf("%s\r\n", g_USART_RX_BUF);
			u2_printf("%s\r", g_USART_RX_BUF);
			
//			LastTime = GetSystem10msCount();
//			
//			while( CalculateTime(GetSystem10msCount(),LastTime) <= 100 )
//			{
//				if(GetGPRSUartRecFlag())
//				{
//					u1_printf("%s\r\n", GPRS_UART_BUF);
//					Clear_GPRS_Buff();
//					break;
//				}
//				
//			}
		}
		
		else if(strncmp((char *)g_USART_RX_BUF, "U2", 2) == 0)
		{
			u1_printf("%s\r\n", &g_USART_RX_BUF[2]);
			g_USART_RX_BUF[g_USART_RX_CNT] = 0x1a;
			u2_printf("%s\r", &g_USART_RX_BUF[2]);
		}
	
		else if((strncmp((char *)g_USART_RX_BUF, "log", g_USART_RX_CNT) == 0) || (strncmp((char *)g_USART_RX_BUF, "LOG", g_USART_RX_CNT) == 0))
		{
			ShowLogContent();
		}
		else if((strncmp((char *)g_USART_RX_BUF, "12V ON", g_USART_RX_CNT) == 0))
		{
			u1_printf("12V ON\r\n");
			GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 ;	
			GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
			GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
			GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
			GPIO_Init(GPIOB, &GPIO_InitStructure);	
			GPIO_SetBits(GPIOB, GPIO_Pin_1);
		}
		else if((strncmp((char *)g_USART_RX_BUF, "12V OFF", g_USART_RX_CNT) == 0))
		{
			u1_printf("12V OFF\r\n");
			GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 ;	
			GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
			GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
			GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
			GPIO_Init(GPIOB, &GPIO_InitStructure);	
			GPIO_ResetBits(GPIOB, GPIO_Pin_1);
		}
		else if(strncmp((char *)g_USART_RX_BUF, "EraseLog", g_USART_RX_CNT) == 0)
		{
			u1_printf("\r\n Erase EEPROM Data\r\n");
			for(i=9; i< EEPROM_BLOCK_COUNT; i++)
			{
				EEPROM_EraseWords(i);
				u1_printf(" Clear %d Block\r\n", i+1);
			}
			LogStorePointerInit();
		}
		else
		{		
			if(g_USART_RX_BUF[0] == BOF_VAL && g_USART_RX_BUF[g_USART_RX_CNT-1] == EOF_VAL && g_USART_RX_CNT >= 15)
			{
				COM1Protocol();
			}
		}
		Clear_Uart1Buff();
	}	
	

}










