#include "main.h"
#include "Gpio.h"


void STM32_GPIO_Init(void)
{
	static u8 RunOnce = FALSE;

	GPIO_InitTypeDef GPIO_InitStructure;
	
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOH, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOC, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOH, &GPIO_InitStructure);	
	
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;	//GPS POWER OFF
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
//	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
//	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
//	GPIO_Init(GPIOA, &GPIO_InitStructure);	
//	GPIO_ResetBits(GPIOA, GPIO_Pin_11);
	
	if(RunOnce == FALSE)		//开机即开启BQ24650  防止接入空电电池时无法充电
	{
		RunOnce = TRUE;
		GPIO_InitStructure.GPIO_Pin = BQ24650_EN_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(BQ24650_EN_TYPE, &GPIO_InitStructure);
		
		BQ24650_ENABLE();
	}

}


void RunLED(unsigned short nMain10ms)
{
	static u16 s_LastTime = 0, s_LastTime2 = 0;
	static u8 RunOnce = FALSE;
	unsigned short BatVol = 0;
	
	GPIO_InitTypeDef GPIO_InitStructure;
	
	if(RunOnce == FALSE)
	{
		GPIO_InitStructure.GPIO_Pin = LED_POWER_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(LED_POWER_TYPE, &GPIO_InitStructure);
		/* Force a low level on LEDs*/ 	
		GPIO_SetBits(LED_POWER_TYPE,LED_POWER_PIN);

		GPIO_InitStructure.GPIO_Pin = LED_NET_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(LED_NET_TYPE, &GPIO_InitStructure);
		/* Force a low level on LEDs*/ 	
		GPIO_SetBits(LED_NET_TYPE,LED_NET_PIN);
		
		RunOnce = TRUE;
	}
	
	BatVol = Get_Battery_Vol();
	if(BatVol > 7700)			//电压值正常，不亮
	{
		GPIO_ResetBits(LED_POWER_TYPE,LED_POWER_PIN);	
	}
	else if(BatVol <= 7700 && BatVol > 7000)			//电压低，电压越低闪烁越快
	{
		if(CalculateTime(nMain10ms, s_LastTime2) <= 5)
		{
			GPIO_SetBits(LED_POWER_TYPE,LED_POWER_PIN);
		}
		else if(CalculateTime(nMain10ms, s_LastTime2) <= ((BatVol-6700)/10))
		{
			GPIO_ResetBits(LED_POWER_TYPE,LED_POWER_PIN);
		}
		else if(CalculateTime(nMain10ms, s_LastTime2) > ((BatVol-6700)/10))
		{
			s_LastTime2 = nMain10ms;
		}	
	}
	else												
	{
		if(CalculateTime(nMain10ms, s_LastTime2) <= 5)
		{
			GPIO_SetBits(LED_POWER_TYPE,LED_POWER_PIN);
		}
		else if(CalculateTime(nMain10ms, s_LastTime2) <= 30)
		{
			GPIO_ResetBits(LED_POWER_TYPE,LED_POWER_PIN);
		}
		else if(CalculateTime(nMain10ms, s_LastTime2) > 30)
		{
			s_LastTime2 = nMain10ms;
		}	
	}
	
	if(GetLoraReadyFlag() == FALSE)		//LORA初始化失败 	NET灯常亮
	{
		GPIO_SetBits(LED_NET_TYPE,LED_NET_PIN);
	}
	else if(GetOnline())				//模块在线正常		1S闪烁
	{
		if(CalculateTime(nMain10ms, s_LastTime) <= 5)
		{
			GPIO_SetBits(LED_NET_TYPE,LED_NET_PIN);
		}
		else if(CalculateTime(nMain10ms, s_LastTime) <= 150)
		{
			GPIO_ResetBits(LED_NET_TYPE,LED_NET_PIN);
		}
		else if(CalculateTime(nMain10ms, s_LastTime) > 150)
		{
			s_LastTime = nMain10ms;
		}		
	}
	else								//模块不在线		200ms闪烁
	{
		if(CalculateTime(nMain10ms, s_LastTime) <= 5)
		{
			GPIO_SetBits(LED_NET_TYPE,LED_NET_PIN);
		}
		else if(CalculateTime(nMain10ms, s_LastTime) <= 30)
		{
			GPIO_ResetBits(LED_NET_TYPE,LED_NET_PIN);
		}
		else if(CalculateTime(nMain10ms, s_LastTime) > 30)
		{
			s_LastTime = nMain10ms;
		}
	}
}
