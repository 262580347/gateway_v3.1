#include "main.h"
#include "PowerManage.h"

#define		VOLBUFCOUNT		10

static unsigned short s_Battery_Vol = 0;

static unsigned short s_DC_Power_Vol = 0;

static float s_VolBuff[VOLBUFCOUNT];

unsigned char g_PowerDetectFlag = FALSE;

unsigned char s_BQ24650_EN_Flag = 1;

unsigned char g_PluseChannel_1_Flag = FALSE, g_PluseChannel_2_Flag = FALSE;

unsigned char s_ForcedChargeFlag = 0;

static unsigned char s_PowerSavingFlag = FALSE;

unsigned int g_FirstConnectTime = 0;

BAT_STATUS BatStatus;	//电池电压检测状态

u8 s_CfgData[sizeof(CHARGING_CFG)] ;

CHARGING_CFG	ChargingConfig;
CHARGING_CFG	*p_this = NULL;

unsigned char GetPowerSavingFlag(void)
{
	return s_PowerSavingFlag;
}	

CHARGING_CFG* GetChargingStatus(void)
{
	return p_this;
}

unsigned char GetForcedChargeFlag(void)
{
	return s_ForcedChargeFlag;
}

unsigned char GetBQ24650ENFlag(void)
{
	return s_BQ24650_EN_Flag;
}

void SetBQ24650ENFlag(unsigned char isTrue)
{
	s_BQ24650_EN_Flag = isTrue;
}



void ReadBQ24650Flag(void)
{		
	p_this = &ChargingConfig;
	
	if (Check_Area_Valid(BQ_FLAG_ADDR))
	{
		EEPROM_ReadBytes(BQ_FLAG_ADDR, s_CfgData, sizeof(CHARGING_CFG));
		
		p_this = (CHARGING_CFG *)s_CfgData;
		
		ChargingConfig.Magic 		= p_this->Magic;
		ChargingConfig.Length 		= p_this->Length;
		ChargingConfig.Chksum 		= p_this->Chksum;
		ChargingConfig.Enable 		= p_this->Enable;
		ChargingConfig.ForceFlag 	= p_this->ForceFlag;

	}
	else
	{
		ChargingConfig.Magic 		= 0x55;
		ChargingConfig.Length 		= 0X02;
		ChargingConfig.Enable 		= BQ24650_ENABLE_FLAG;
		ChargingConfig.ForceFlag 	= FORCED_CHARGE_DISABLE;
		ChargingConfig.Chksum = Calc_Checksum((unsigned char *)&ChargingConfig.Enable, 2);
		EEPROM_WriteBytes(BQ_FLAG_ADDR, (unsigned char *)&ChargingConfig, sizeof(ChargingConfig));
		u1_printf("\r\n First Init\r\n");
	}
	
	if(ChargingConfig.ForceFlag == FORCED_CHARGE_ENABLE)
	{
		s_ForcedChargeFlag = 1;
		u1_printf("\r\n Forced Charge on\r\n");
	}
	else if(ChargingConfig.ForceFlag == FORCED_CHARGE_DISABLE)
	{
		s_ForcedChargeFlag = 0;
		u1_printf("\r\n Forced Charge off\r\n");
	}
	
	if(ChargingConfig.Enable == BQ24650_ENABLE_FLAG)
	{
		s_BQ24650_EN_Flag = 1;
		u1_printf("\r\n Charge on\r\n");
	}
	else if(ChargingConfig.Enable == BQ24650_DISABLE_FLAG)
	{
		s_BQ24650_EN_Flag = 0;
		u1_printf("\r\n Charge off\r\n");
	}
	else
	{
		s_BQ24650_EN_Flag = 1;
		u1_printf("\r\n First\r\n");
	}	
}

unsigned short Get_Battery_Vol(void)
{
	if(s_BQ24650_EN_Flag)
	{
		return (s_Battery_Vol | 0x0001);
	}
	else
	{
		return (s_Battery_Vol & 0xfffe);		
	}
}

unsigned short GetDCPowerVol(void)
{
	return s_DC_Power_Vol;
}


void Power_Vol_Detect(unsigned short nMain10ms)
{
	u16 ADCdata[10], i, Sum_ADCdata = 0, BatVol = 0;
	u16 Sum_DCPowerdata = 0, DCPowerVol = 0;
	static u16 s_LastTime = 0;
	static u8 s_Status = 1, s_First = FALSE, s_DetCount = 0, s_DetNum = 0, s_FirstSaveLogFlag = FALSE;
	SYSTEMCONFIG *p_sys;
		
	if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 6000)
	{
		g_PowerDetectFlag = FALSE;
		s_LastTime = GetSystem10msCount();
	}
	
	if(g_PowerDetectFlag == FALSE)
	{			
		switch(s_Status)	
		{
			case 1:
				Adc_Init();	

				Open_AdcChannel();
			
				s_LastTime = GetSystem10msCount();
				s_Status++;
			break;
		
			case 2:
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 50)//等待传感器完成测量再测量电压值
				{
					RCC_HSICmd(ENABLE);
					
					RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
					
					/* Enable ADC1 */
					ADC_Cmd(ADC1, ENABLE);
					
					/* Wait until ADC1 ON status */
					while (ADC_GetFlagStatus(ADC1, ADC_FLAG_ADONS) == RESET)
					{
					}
					
					for(i=0; i<10; i++)
					{
						/* ADC1 regular channel5 or channel1 configuration */
						ADC_RegularChannelConfig(ADC1, ADC_CHANNEL, 1, ADC_SampleTime_384Cycles);
						
						/* Start ADC1 Software Conversion */
						ADC_SoftwareStartConv(ADC1);

						/* Wait until ADC Channel 5 or 1 end of conversion */
						while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET)
						{	
						}	
						
						ADCdata[i] = ADC_GetConversionValue(ADC1);
					}
					
					#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)

					for(i=0; i<10; i++)
					{
						Sum_ADCdata += ADCdata[i];
					}
					
					while (ADC_GetFlagStatus(ADC1, ADC_FLAG_ADONS) == RESET)
					{
					}
					
					for(i=0; i<10; i++)
					{
						/* ADC1 regular channel5 or channel1 configuration */
						ADC_RegularChannelConfig(ADC1, ADC_DCPOWER_CHANNEL, 1, ADC_SampleTime_384Cycles);
						
						/* Start ADC1 Software Conversion */
						ADC_SoftwareStartConv(ADC1);

						/* Wait until ADC Channel 5 or 1 end of conversion */
						while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET)
						{	
						}	
						
						ADCdata[i] = ADC_GetConversionValue(ADC1);
					}
							
					for(i=0; i<10; i++)
					{
						Sum_DCPowerdata += ADCdata[i];
					}	
					
					Sum_DCPowerdata *= 0.1;
					DCPowerVol = (Sum_DCPowerdata*8.95); 
					s_DC_Power_Vol = DCPowerVol;

					#endif	
												
					Sum_ADCdata *= 0.1;					
					BatVol = (Sum_ADCdata*3.2234); 
															
					if(s_DetCount < 3)
					{
						s_VolBuff[s_DetCount] = BatVol;
						s_Battery_Vol = BatVol;
					}
					else
					{
						s_VolBuff[s_DetNum] = BatVol;
						s_Battery_Vol = Mid_Filter(s_VolBuff, s_DetCount);
					}

					u1_printf("\r\n Vol:%dmV Avg:%dmV, DC Power:%dmV\r\n", BatVol, s_Battery_Vol, s_DC_Power_Vol);		
					
					s_DetCount++;
					s_DetNum++;
					
					if(s_DetCount >= VOLBUFCOUNT)
					{
						s_DetCount = VOLBUFCOUNT;
					}
					
					if(s_DetNum >= VOLBUFCOUNT)
					{
						s_DetNum = 0;
					}	
					
					Adc_Reset();

					if(s_FirstSaveLogFlag == FALSE)
					{
						s_FirstSaveLogFlag = TRUE;
						SetLogErrCode(LOG_CODE_START);
						StoreOperationalData();
					}
					
					
					
					if(s_First == FALSE)
					{
						if(g_FirstConnectTime)
						{
							if(DifferenceOfRTCTime(GetRTCSecond(), g_FirstConnectTime) >= 300)	//第一次连接成功5分钟省电模式才能开启
							{
								s_First = TRUE;
								g_FirstConnectTime = 0;
								u1_printf("\r\n ----允许开启省电模式----\r\n");
							}
						}
					}
					else
					{
						if(s_Battery_Vol < 7200 && s_Battery_Vol > 2000)//开启省电模式
						{
							p_sys = GetSystemConfig();	
							if(p_sys->Data_interval >= 480)
							{						
								s_PowerSavingFlag = TRUE;							
							}
						}
						else if(s_Battery_Vol > 7600)	//关闭省电模式
						{
							p_sys = GetSystemConfig();	
							if(p_sys->Data_interval >= 480)
							{
								u1_printf(" 关闭省电模式\r\n");
								s_PowerSavingFlag = FALSE;
							}
						}
					}
					
					if(s_PowerSavingFlag)
					{
						u1_printf(" 开启省电模式\r\n");
						SetLogErrCode(LOG_CODE_LOWPOWER);
					}
					
										
					s_Status++;
					
				}				
			break;
				
			case 3:
				g_PowerDetectFlag = TRUE;
			
				s_LastTime = GetSystem10msCount();					
				s_Status = 1;
			break;
		}
	}
}



