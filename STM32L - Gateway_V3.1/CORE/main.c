/**********************************
说明:STM32L网关
作者:关宇晟
版本:V2018.5.15
***********************************/

#include "main.h"
#include "sys.h"

int main(void)
{
	SYSTEMCONFIG *p_sys;

	SysInit();	//系统外设、参数初始化
	
	p_sys = GetSystemConfig();

	while (1)
	{
		Updata_IWDG(GetSystem100msCount());	//看门狗，超时重启
		
		TaskForDebugCOM();		//串口测试

		LoraProcess(GetSystem10msCount());	//LORA 初始化
			
		TCProtocolForLoraProcess();
		
		if(GetGPSPowerFlag())
		{
			GPSProcess();
		}
		else
		{
			if((p_sys->Type>>4) == NETTYPE_2G)		//SIM800C模块
			{
				SIM800CProcess(GetSystem10msCount());
			}
			else if((p_sys->Type>>4) == NETTYPE_GPRS)	//汉枫模块
			{
				GPRS_Process(GetSystem10msCount());
			}	
			else if((p_sys->Type>>4) == NETTYPE_4G)		//4G模块
			{
				SIM7600CEProcess(GetSystem10msCount());
			//	ME909SProcess(GetSystem10msCount());
			}	
			else if((p_sys->Type>>4) == NETTYPE_WLAN)		//WIFI模块
			{
				WiFiProcess();
			}	
			else if(((p_sys->Type>>4) == NETTYPE_NB_IOT) || ((p_sys->Type) == NETTYPE_NB_IOT))		//NB-IOT	SIM7020
			{
				//SIM7020TestProcess();
				SIM7020Process(GetSystem10msCount());
			}
		}
		
		TCProtocolProcess();
		
		RunLED(GetSystem10msCount());		//LED闪烁
		
		Power_Vol_Detect(GetSystem10msCount());	//电池电压检测
			
	}
}


