/**********************************
说明:慧云配置工具串口通信程序
	  
作者:关宇晟
版本:V2017.4.3
***********************************/
#include "Comm_Debug.h"
#include "main.h"

#define		MAX_BUFF	256

static void Debug_Send_Packet(u8 *packet, u8 len)
{
	u8 send_buf[MAX_BUFF];
	u8 send_len;
	
	send_len = PackMsg(packet, len, send_buf, MAX_BUFF);			//数据包转义处理					
	USART1_DMA_Send(send_buf, send_len);	
}

static void Debug_Get_Time(u8 *data, u8 lenth)
{
	u8 send_buf[128];
	u32 send_len;
	
	CLOUD_HDR *hdr;
	u8 *p_data;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(6);
	p_data = &send_buf[sizeof(CLOUD_HDR)];
	
	*(p_data++) = RTC_TimeStructure.RTC_Seconds;
	*(p_data++) = RTC_TimeStructure.RTC_Minutes; 
	*(p_data++) = RTC_TimeStructure.RTC_Hours;

	*(p_data++) = RTC_DateStruct.RTC_Date;
	*(p_data++) = RTC_DateStruct.RTC_Month;
	*(p_data++) = (RTC_DateStruct.RTC_Year);
	
	send_len += 6;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;

	Debug_Send_Packet(send_buf, send_len);
	
}	

static void Debug_Updata_Time(u8 *data, u8 lenth)
{
	u8 *p_data;
	u8 send_buf[128];
	u32 send_len;
	CLOUD_HDR *hdr;
	
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
		
	p_data = (u8 *)&data[sizeof(CLOUD_HDR)];
	
	RTC_TimeStructure.RTC_Seconds	 = *(p_data++);
	RTC_TimeStructure.RTC_Minutes 	 = *(p_data++); 
	RTC_TimeStructure.RTC_Hours	 	 = *(p_data++);
	RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure);

	RTC_DateStruct.RTC_Date  	= *(p_data++);
	RTC_DateStruct.RTC_Month  	= *(p_data++);
	RTC_DateStruct.RTC_Year  	= *(p_data++);	
	RTC_DateStruct.RTC_WeekDay  = 1;
	RTC_SetDate(RTC_Format_BIN, &RTC_DateStruct);
	
	RTC_WaitForSynchro();//等待RTC寄存器同步   
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = 0;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	Debug_Send_Packet(send_buf, send_len);
	
	RTC_TimeShow();
}

static void Debug_Read_SystemInfo(u8 *data, u8 lenth)
{
	u8 send_buf[128];
	u32 send_len;
	
	SYSTEM_INFO 	*sys_info;
	
	CLOUD_HDR *hdr;
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEM_INFO));
	sys_info = Get_SystemInfo();
	memcpy(&send_buf[sizeof(CLOUD_HDR)], sys_info, sizeof(SYSTEM_INFO));
	send_len += sizeof(SYSTEM_INFO);
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
}


static void Debug_Read_SystemConfig(u8 *data, u8 lenth)
{
	u8 send_buf[128];
	u32 send_len;
	
	SYSTEMCONFIG 	*sys_cfg;
	
	CLOUD_HDR *hdr;
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG));
	sys_cfg = GetSystemConfig();
	memcpy(&send_buf[sizeof(CLOUD_HDR)], sys_cfg, sizeof(SYSTEMCONFIG));
	send_len += sizeof(SYSTEMCONFIG);
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
}

static unsigned char Debug_Set_SystemConfig(u8 *data, u8 lenth)
{
	u8 send_buf[128], ReturnVal = 0;
	u32 send_len;
	SYSTEMCONFIG 	*sys_cfg;
	
	CLOUD_HDR *hdr;
	
	sys_cfg = (SYSTEMCONFIG *)&data[sizeof(CLOUD_HDR)];
//	sys_cfg->AutoResetFlag = 0;
//	sys_cfg->AutoResetTime = 0;
	ReturnVal = Set_System_Config(sys_cfg);
	if(ReturnVal == FALSE)
	{
		return FALSE;
	}
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = 0;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	

	return TRUE;
}

static void WLAN_Read_Name(u8 *data, u32 len)
{
	u8 send_buf[80], Name_Length = 0, User_Name[64];
	u8 send_len = 0;
	
	CLOUD_HDR *hdr;
	memset(send_buf, 0 ,sizeof(send_buf));
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	Name_Length = Check_Area_Valid(WIFI_USER_ADDR);
	if (Name_Length)
	{
		memset(User_Name, 0 , sizeof(User_Name));
		EEPROM_ReadBytes(WIFI_USER_ADDR, (u8 *)User_Name, sizeof(SYS_TAG) +  Name_Length);
		hdr->payload_len = swap_word(Name_Length);
		memcpy(&send_buf[sizeof(CLOUD_HDR)], &User_Name[sizeof(SYS_TAG)], Name_Length);
		send_len += Name_Length;
		send_buf[send_len] = Calc_Checksum(send_buf, send_len);
		send_len++;
		Debug_Send_Packet(send_buf, send_len);		
		
		
		User_Name[sizeof(SYS_TAG) + Name_Length] = 0;
//		u1_printf("(Size:%d)%s\r\n", Name_Length, &User_Name[sizeof(SYS_TAG)]);
	}
}

static void WLAN_Read_Password(u8 *data, u32 len)
{
	u8 send_buf[80], Password_Length = 0, User_Password[64];
	u8 send_len = 0;
	
	CLOUD_HDR *hdr;
	memset(send_buf, 0 ,sizeof(send_buf));
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	
	Password_Length = Check_Area_Valid(WIFI_PASSWORD_ADDR);
	if (Password_Length)
	{	
		memset(User_Password, 0 , sizeof(User_Password));
		EEPROM_ReadBytes(WIFI_PASSWORD_ADDR, (u8 *)User_Password, sizeof(SYS_TAG) +  Password_Length);
		hdr->payload_len = swap_word(Password_Length);
		memcpy(&send_buf[sizeof(CLOUD_HDR)], &User_Password[sizeof(SYS_TAG)], Password_Length);
		send_len += Password_Length;
		send_buf[send_len] = Calc_Checksum(send_buf, send_len);
		send_len++;
		Debug_Send_Packet(send_buf, send_len);	
		User_Password[sizeof(SYS_TAG) + Password_Length] = 0;
//		u1_printf("(Size:%d)%s\r\n", Password_Length, &User_Password[sizeof(SYS_TAG)]);
	}
		
	
}

static void WLAN_Set_Name(u8 *data, u32 len)
{
	SYS_TAG tag;
	u8 send_buf[80], NameBuf[80], send_len = 0, NameLength = 0;
	
	CLOUD_HDR *hdr;
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	NameLength = hdr->payload_len;
	hdr->payload_len = swap_word(hdr->payload_len);
		
	memcpy(&send_buf[sizeof(CLOUD_HDR)], &data[sizeof(CLOUD_HDR)], NameLength);
	send_len += NameLength;
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
	
	tag.magic = VAILD;
	tag.length = NameLength;
	tag.chksum = Calc_Checksum((unsigned char *)&data[sizeof(CLOUD_HDR)], NameLength);
	
	memcpy(NameBuf, &tag, sizeof(SYS_TAG));
	memcpy(&NameBuf[sizeof(SYS_TAG)], &data[sizeof(CLOUD_HDR)], NameLength);
	
	EEPROM_EraseWords(WIFI_USER_BLOCK);
	EEPROM_WriteBytes(WIFI_USER_ADDR, (unsigned char *)NameBuf, sizeof(SYS_TAG) + NameLength);

	NameBuf[sizeof(SYS_TAG) + NameLength] = 0;
//	u1_printf("(Size:%d)%s\r\n", NameLength, &NameBuf[sizeof(SYS_TAG)]);
}

static void WLAN_Set_Password(u8 *data, u32 len)
{
	SYS_TAG tag;
	u8 send_buf[80], PasswordBuf[80], send_len = 0, PasswordLength = 0;
	
	CLOUD_HDR *hdr;
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	PasswordLength = hdr->payload_len;
	hdr->payload_len = swap_word(hdr->payload_len);
		
	memcpy(&send_buf[sizeof(CLOUD_HDR)], &data[sizeof(CLOUD_HDR)], PasswordLength);
	send_len += PasswordLength;
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
	
	tag.magic = VAILD;
	tag.length = PasswordLength;
	tag.chksum = Calc_Checksum((unsigned char *)&data[sizeof(CLOUD_HDR)], PasswordLength);
	
	memcpy(PasswordBuf, &tag, sizeof(SYS_TAG));
	memcpy(&PasswordBuf[sizeof(SYS_TAG)], &data[sizeof(CLOUD_HDR)], PasswordLength);
	
	EEPROM_EraseWords(WIFI_PASSWORD_BLOCK);
	EEPROM_WriteBytes(WIFI_PASSWORD_ADDR, (unsigned char *)PasswordBuf, sizeof(SYS_TAG) + PasswordLength);

	PasswordBuf[sizeof(SYS_TAG) + PasswordLength] = 0;
//	u1_printf("(Size:%d)%s\r\n", PasswordLength, &PasswordBuf[sizeof(SYS_TAG)]);
}

//读取Http服务器
static void Get_HttpServer(u8 *data, u32 len)
{
	u8 send_buf[128], Http_Length, HttpBuf[128];
	u8 send_len;
	
	CLOUD_HDR *hdr;
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	
	Http_Length = Check_Area_Valid(HTTP_SERVER_ADDR);
	if (Http_Length)
	{
		EEPROM_ReadBytes(HTTP_SERVER_ADDR, (u8 *)HttpBuf, sizeof(SYS_TAG) +  Http_Length);
		hdr->payload_len = swap_word(Http_Length);
		memcpy(&send_buf[sizeof(CLOUD_HDR)], &HttpBuf[sizeof(SYS_TAG)], Http_Length);
		send_len += Http_Length;
		send_buf[send_len] = Calc_Checksum(send_buf, send_len);
		send_len++;
		Debug_Send_Packet(send_buf, send_len);	
		HttpBuf[sizeof(SYS_TAG) + Http_Length] = 0;
//		u1_printf("(Size:%d)%s\r\n", Http_Length, &HttpBuf[sizeof(SYS_TAG)]);
	}
		
	
}
//设置Http服务器
static void Set_HttpServer(u8 *data, u32 len)
{
	SYS_TAG tag;
	u8 send_buf[128], HttpBuf[128], send_len = 0, Http_Length = 0;
	
	CLOUD_HDR *hdr;
	
	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	Http_Length = hdr->payload_len;
	hdr->payload_len = swap_word(hdr->payload_len);
		
	memcpy(&send_buf[sizeof(CLOUD_HDR)], &data[sizeof(CLOUD_HDR)], Http_Length);
	send_len += Http_Length;
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	Debug_Send_Packet(send_buf, send_len);	
	
	tag.magic = VAILD;
	tag.length = Http_Length;
	tag.chksum = Calc_Checksum((unsigned char *)&data[sizeof(CLOUD_HDR)], Http_Length);
	
	memcpy(HttpBuf, &tag, sizeof(SYS_TAG));
	memcpy(&HttpBuf[sizeof(SYS_TAG)], &data[sizeof(CLOUD_HDR)], Http_Length);
	
	EEPROM_EraseWords(HTTP_BLOCK1);
	EEPROM_EraseWords(HTTP_BLOCK2);
	EEPROM_WriteBytes(HTTP_SERVER_ADDR, (unsigned char *)HttpBuf, sizeof(SYS_TAG) + Http_Length);

	HttpBuf[sizeof(SYS_TAG) + Http_Length] = 0;
//	u1_printf("(Size:%d)%s\r\n", Http_Length, &HttpBuf[sizeof(SYS_TAG)]);

}

void OnDebug(u8 *data, u8 lenth)
{
	CLOUD_HDR *hdr;
	u8 ReturnVal = 0;
	
	hdr = (CLOUD_HDR *)data;
	switch(hdr->cmd)
	{
		case CMD_GET_DATETIME:	//获取设备时间	--0xF1
			Debug_Get_Time(data, lenth);
			break;
		case CMD_SET_DATETIME:	 //设置设备时间 --0xF2
			Debug_Updata_Time(data, lenth);
			break;
		
		case CMD_RD_SYS_INFO:	//读取系统信息 --0xF3
			Debug_Read_SystemInfo(data, lenth);
			break;
	
		case CMD_RD_SYS_CFG:	//读取系统配置 --0xF5
			Debug_Read_SystemConfig(data, lenth);
			break;
		case CMD_WR_SYS_CFG:	//设置系统配置 --0xF6
			ReturnVal = Debug_Set_SystemConfig(data, lenth);
			if(ReturnVal == FALSE)
			{
				return;
			}
			delay_ms(300);
			u1_printf("\r\n 等待重启...\r\n");
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			Sys_Soft_Reset();
			break;
		
					//WLAN 功能区
		case CMD_READ_WLAN_NAME://读取WLAN名称
			WLAN_Read_Name(data, lenth);
		break;
		
		case CMD_READ_WLAN_PASSWORD://读取WLAN密码
			WLAN_Read_Password(data, lenth);
		break;
		
		case CMD_SET_WLAN_NAME://读取WLAN名称
			WLAN_Set_Name(data, lenth);
		break;
		
		case CMD_SET_WLAN_PASSWORD://读取WLAN密码
			WLAN_Set_Password(data, lenth);
		break;
		
		case CMD_GET_HTTPSERVER://读取HTTP服务器
			Get_HttpServer(data, lenth);		
		break;
		
		case CMD_SET_HTTPSERVER://设置HTTP服务器
			Set_HttpServer(data, lenth);
		break;
	}
}

//static u8 ChenckSum(CLOUD_HDR *pMsg, u8 *data, u8 lenth)
//{
//	u8 checksum = 0, i;
//	
//	checksum += pMsg->protocol & 0xff;
//	checksum += pMsg->protocol >> 8;
//	checksum += pMsg->device_id & 0xff;
//	checksum += (pMsg->device_id >> 8) & 0xff;
//	checksum += (pMsg->device_id >> 16) & 0xff;
//	checksum += (pMsg->device_id >> 24) & 0xff;
//	checksum += pMsg->dir;
//	checksum += pMsg->seq_no & 0xff;
//	checksum += pMsg->seq_no >> 8;
//	checksum += pMsg->payload_len & 0xff;
//	checksum += pMsg->payload_len >> 8;
//	checksum += pMsg->cmd;
//	for(i=0; i<lenth; i++)
//	{
//		checksum += data[i];
//	}
//	return checksum;
//}



