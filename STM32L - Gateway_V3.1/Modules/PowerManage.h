#ifndef __POWERMANAGE_H_
#define __POWERMANAGE_H_	

#define		BQ24650_ENABLE_FLAG		0X55
#define		BQ24650_DISABLE_FLAG	0XAA

#define		FORCED_CHARGE_ENABLE	0X55
#define		FORCED_CHARGE_DISABLE	0XAA

typedef enum
{
	BAT_WORKING 	= 0,//���ڼ���ص�ѹ
	BAT_ERROR  		= 1,//��ѹ״̬����
	BAT_IDLE 	 	= 2,//��ѹ������
	BAT_OTHER    	= 3,//����״̬
}BAT_STATUS;

extern BAT_STATUS BatStatus;

typedef __packed struct
{
	unsigned char 			Magic; 	//0x55
	unsigned short 			Length;	//�洢���ݳ���	
	unsigned char 			Chksum;	//У���
	unsigned char			Enable;
	unsigned char			ForceFlag;
}CHARGING_CFG;

extern unsigned char g_BQ24650_EN_Flag;

extern unsigned char g_PluseChannel_1_Flag, g_PluseChannel_2_Flag ;

void Adc_Init(void);

void Power_Vol_Detect(unsigned short nMain100ms);

unsigned short Get_Battery_Vol(void);

extern unsigned char g_PowerDetectFlag;

void ReadBQ24650Flag(void);

void SetBQ24650ENFlag(unsigned char isTrue);
	
unsigned char GetBQ24650ENFlag(void);

unsigned char GetForcedChargeFlag(void);

void SetForcedChargeFlag(unsigned char isTrue);

CHARGING_CFG* GetChargingStatus(void);

unsigned char GetPowerSavingFlag(void);

extern unsigned int g_FirstConnectTime;

unsigned short GetDCPowerVol(void);

#endif 
