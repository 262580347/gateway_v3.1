#include "main.h"
#include "ME909S.h"

#define APN_CMNET 	1		//中国移动

#define APN_UNINET 	2		//中国联通

#define APN_CTNET 	3		//中国电信

static void SimUartSendStr(char *str)
{
#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V100)

	u3_printf(str);

#elif (HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)

	u2_printf(str);
	
#endif	
}

void ME909SPortInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB, ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = ME909S_ENABLE_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(ME909S_ENABLE_TYPE, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = ME909S_WAKEUP_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(ME909S_WAKEUP_TYPE, &GPIO_InitStructure);	
	
	GPIO_SetBits(ME909S_WAKEUP_TYPE, ME909S_WAKEUP_PIN);
	
	GPIO_InitStructure.GPIO_Pin = ME909S_RST_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(ME909S_RST_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = ME909S_RF_DISABLE_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(ME909S_RF_DISABLE_TYPE, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = ME909S_DTR_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(ME909S_DTR_TYPE, &GPIO_InitStructure);	
	
	GPIO_ResetBits(ME909S_DTR_TYPE, ME909S_DTR_PIN);
	
			
}

void ME909SProcess(unsigned short nMain10ms)
{
	u16 nTemp = 0;
	char str[512];
	static u16 s_LastTime = 0, s_LastTime2 = 0, s_OverCount = 0, s_ATAckCount = 0;
	static unsigned int s_AliveRTCCount = 0xfffe0000;
	static u8 s_APN = 0, s_APNFlag = 0, s_Status = 1, s_Step = 1, RunOnce = FALSE, s_ErrCount = 0, s_RetryCount = 0;
	static u8 s_PowerOnFlag = FALSE, s_SavingModeFlag = FALSE, s_SimSavingFlag = FALSE, s_SignalStrength = 0;
	char * p1 = NULL, buf[10];
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;

	static u16 s_StartTime = 0, s_EndTime = 0;
	RTC_DateTypeDef RTC_DateStruct;
	
	p_sys = GetSystemConfig();
	
	if(RunOnce == FALSE)
	{
		RunOnce = TRUE;
		s_LastTime = GetSystem10msCount();
		InitGprsLinkedList();
		RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);	
	}		
		
	switch(s_Status)
	{	
		case 1:   //模块断电
			s_RetryCount++;
			SetTCModuleReadyFlag(FALSE);
			if(s_RetryCount >= 10)
			{
				u1_printf(" Timeout not connected to the network waiting for restart...\r\n");
				SetLogErrCode(LOG_CODE_TIMEOUT);
				SetLogErrCode(LOG_CODE_RESET);
				StoreOperationalData();
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				Sys_Soft_Reset();	
			}
			ME909SPortInit();
			ME909S_PWR_OFF();
			ME909S_RST_ON();
			ME909S_RF_DISABLE_ON();
			SetTCProtocolRunFlag(FALSE);
			GPRS_Uart_Init(SIM_BAND_RATE);
			s_LastTime = GetSystem10msCount();
			s_Status++;
			u1_printf("\r\n[ME909S]模块断电，延时2s.\r\n");
			s_PowerOnFlag = TRUE;	
		break;
		 
		case 2:	//断电后延时3S上电，模块上电自动开机				
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1500 )
			{
				s_LastTime = GetSystem10msCount();
				s_Status++;
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 200 )
			{
				if(s_PowerOnFlag)
				{
					s_PowerOnFlag = FALSE;
					u1_printf("\r\n[ME909S]模块上电，延时20s.\r\n");
					s_StartTime = GetSystem10msCount();
					ME909S_PWR_ON();	
					
					ME909S_RST_ON();
					delay_ms(250);
					ME909S_RST_OFF();
					delay_ms(250);
					ME909S_RST_ON();
				}
			}
		break;
			
		case 3: 	//上电完成后，初始化端口
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 50 )
			{
				u1_printf("\r\n[ME909S]模块上电完成，检测模块.\r\n");
				SimUartSendStr("AT\r");
				delay_ms(50);
				SimUartSendStr("AT\r");
				s_LastTime = GetSystem10msCount();
				s_Status++;
			}
		break;
		
		case 4:	//延时100MS,检测响应数据
			if(GetGPRSUartRecFlag())
			{		
				if(strstr((char *)GPRS_UART_BUF, "OK"))
				{
					s_ErrCount = 0;
					s_Status++;	
				}			
				Clear_GPRS_Buff();
				
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 20 )
			{				
					u1_printf("[ME909S]AT无响应\r\n");
					s_Status--;   //回上一个状态
					s_ErrCount++;   //重试次数加1
					if(s_ErrCount >= 5 )
					{
						SetLogErrCode(LOG_CODE_NOMODULE);
						s_ErrCount = 0;
						s_Status = 1;	//模块重新上电初始化
					}						
			}
	 	break;
		
		case 5:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 30) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				sprintf(str, "\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				u1_printf(str);		

				switch(s_Step)
				{
					case 1:
						SimUartSendStr("ATE0\r");
						u1_printf("[ME909S]->ATE0\r\n");
						s_LastTime = GetSystem10msCount();
					break;
					case 2:
						SimUartSendStr("AT+CPIN?\r");
						u1_printf("[ME909S]->查询模块SIM状态\r\n");
						s_LastTime = GetSystem10msCount();
					break;
					case 3:
						SimUartSendStr("AT+CIMI\r");
						u1_printf("[ME909S]->查询IMSI\r\n");
						s_LastTime = GetSystem10msCount();
					break;
					case 4:				//根据CIMI信息设置APN
						SimUartSendStr("AT+CGMM\r");
						u1_printf("[ME909S]->查询模块名称\r\n");
						s_LastTime = GetSystem10msCount();		
					break;
					case 5:
						SimUartSendStr("AT+CSQ\r");
						u1_printf("[ME909S]->查询模块信号质量\r\n");
						s_LastTime = GetSystem10msCount();		
					break;	
					case 6:
						SimUartSendStr("AT+COPS?\r");
						u1_printf("[ME909S]->查询网络运营商\r\n");
						s_LastTime = GetSystem10msCount();
					break;	
					case 7: 
						SimUartSendStr("AT+CREG?\r");
						u1_printf("[ME909S]->查询模块网络注册状态\r\n");
						s_LastTime = GetSystem10msCount();
					case 8:
						SimUartSendStr("AT+CGATT?\r");
						u1_printf("[ME909S]->查询GPRS附着状态\r\n");
						s_LastTime = GetSystem10msCount();
					break;	
				
				}
				s_Status++;
			}	
					
		break;
		
		case 6:
				if(GetGPRSUartRecFlag())
				{
					u1_printf("%s\r\n", GPRS_UART_BUF);
												
					switch(s_Step)
					{
						case 1:
							if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
							{
								s_Step++;	
								s_Status--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();							
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
							}
						break;
							
						case 2:
						//	u1_printf("检测SIM卡： ");
							if((strstr((const char *)GPRS_UART_BUF,"+CPIN: READY")) != NULL )
							{					
								s_Step++;	
								s_Status--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();		
								u1_printf("\r\n  SIM卡正常\r\n");
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
								if(s_ErrCount >= 10)
								{
									SetLogErrCode(LOG_CODE_NO_CARD);
									u1_printf(" No Card!\r\n");
									s_ErrCount = 0;
									s_Status = 1;
									s_Step = 1;
								}
							}						
						break;	
							
						case 3:			//CIMI查询
							//中国移动:46000、46002、46004、46007、46008
							//中国联通:46001、46006、46009
							//中国电信:46003、46005、46011。
								//越南MCC:452
								//越南MNC   	运营商				APN:
								// 	  01 	MobiFone			m-wap
								// 	  02 	Vinaphone			m3-world / m-world
								// 	  03 	S-Fone 
								// 	  04 	Viettel Mobile		v-internet
								// 	  05 	Vietnamobile
								// 	  06 	EVNTelecom
								// 	  07 	Beeline VN
								// 	  08 	3G EVNTelecom
							if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )			//===============此处判断可能容易出错  
							{
//								MCC = (GPRS_UART_BUF[2] - '0')*100 + (GPRS_UART_BUF[3] - '0')*10 +(GPRS_UART_BUF[4] - '0');
//								MNC = (GPRS_UART_BUF[5]*10 - '0') + (GPRS_UART_BUF[6] - '0');
								
								s_APNFlag = 1;	
								s_APN = APN_CMNET;
								u1_printf("IMSI: APN SET TO CMNET\r\n");

								if(s_APNFlag)
								{
								//	u1_printf("模块名称：%s\r\n", GPRS_UART_BUF);
									s_Step++;	
									s_Status--;		
									s_ErrCount = 0;		
								}
								else
								{
									s_Status--;		
									s_ErrCount ++;
								}
								s_LastTime = GetSystem10msCount();	
									
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
							}
						break;
							
						case 4:
							if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
							{
							//	u1_printf("APN OK\r\n");
								s_Step++;	
								s_Status--;		
								s_ErrCount = 0;
														
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
							}
						break;
							
						case 5:				//	u1_printf("检测信号质量： ");					
							if((p1 = strstr((const char *)GPRS_UART_BUF,"+CSQ:")) != NULL )
							{							
								//u1_printf("%s\r\n", GPRS_UART_BUF);							
								strncpy(buf,p1+6,2);
								nTemp = atoi( buf);
								SetCSQValue(nTemp);
								if( nTemp == 99 )								
								{
									s_Status--;	
									s_ErrCount++;
									u1_printf(" 无信号\r\n");
									SetLogErrCode(LOG_CODE_WEAKSIGNAL);
								}
								else if( nTemp < 6 )								
								{
									s_Status--;	
									s_ErrCount++;
									u1_printf(" 信号差\r\n");
									SetLogErrCode(LOG_CODE_WEAKSIGNAL);
								}
								else
								{
									s_SignalStrength = nTemp;
									s_Status--;	
									s_Step++;	
									s_ErrCount = 0;
									u1_printf("\r\n  信号正常\r\n");
									ClearLogErrCode(LOG_CODE_WEAKSIGNAL);
									
								}
							}
							else
							{
	//							u1_printf("    Error\r\n");
								s_Status--;	
								s_ErrCount++;
							}		
						break;		
							
						case 6:
							//u1_printf(" 运营商名称： ");
							if((p1 = strstr((const char *)GPRS_UART_BUF,"+COPS:")) != NULL )
							{				
								if(!s_APNFlag)
								{
									if((strstr((const char *)GPRS_UART_BUF,"CHINA MOBILE")) != NULL )  
									{
										s_APN = APN_CMNET;
										s_APNFlag = 1;
										u1_printf("COPS: CHINA MOBILE\r\n");
										s_Step++;	
										s_Status--;		
										s_ErrCount = 0;
									}
									else if((strstr((const char *)GPRS_UART_BUF,"UNICOM")) != NULL )  
									{
										s_APN = APN_UNINET;
										s_APNFlag = 1;
										u1_printf("COPS: UNICOM\r\n");
										s_Step++;	
										s_Status--;		
										s_ErrCount = 0;
									}
									else if((strstr((const char *)GPRS_UART_BUF,"CHINA TELECOM")) != NULL )  	//电信运营商信息不一定正确
									{
										s_APN = APN_UNINET;
										s_APNFlag = 1;
										u1_printf("COPS: CTNET\r\n");
										
										s_Step++;	
										s_Status--;		
										s_ErrCount = 0;
									}
									else
									{
										u1_printf("COPS: UNRECOGNIZABLED NETWORK\r\n");
										s_Status--;		
										s_ErrCount ++;
									}
									s_Step++;
								}
								else
								{
									s_Step++;	
									s_Step++;
									s_Status--;		
									s_ErrCount = 0;
								}
									
								s_LastTime = GetSystem10msCount();		
							//	u1_printf("%s\r\n", GPRS_UART_BUF);
							}
							else
							{
								u1_printf("    Error\r\n");
								s_Status--;	
								s_ErrCount++;
							}
						break;	
											
						case 7:	//AT+CREG?	查询模块是否注册网络,+CREG: 0,1
							
							u1_printf(str,"[ME909]模块注册网络=%s\r\n",GPRS_UART_BUF);
							//DEBUG_OUTPUT(str);
							if( (p1=strstr((const char *)GPRS_UART_BUF,"+CREG:")) != NULL )
							{
								//s_ErrCount = 0;
								nTemp = atoi( (const char *) (p1+9));
								switch( nTemp  )
								{
									case 0:
										s_ErrCount++;
										u1_printf("[ME909]模块注册--未注册\r\n");
										s_Status--;
										//s_Step = 6;	//重新检查SIM卡和网络信号
										
										if( s_ErrCount > 60 )	//60次超时约30秒
										{
											s_ErrCount = 0;
											s_Status = 1;
											s_Step = 1;
												
										}
										break;
										
									case 1:
										u1_printf("[ME909]模块注册网络--成功\r\n");
										s_Status--;
										s_Step++;
										s_ErrCount = 0;
										break;
									
									case 2:
										s_ErrCount++;
										u1_printf(str,"[ME909]模块注册--正在注册，查询次数=%d\r\n",s_ErrCount);
										//DEBUG_OUTPUT(str);
										s_Status--;
										
										if( s_ErrCount > 60 )	//60次超时约30秒
										{
											s_ErrCount = 0;
											s_Status = 1;
											s_Step = 1;	
										}
										//s_Step = 6;	//重新检查SIM卡和网络信号
										break;
										
									case 3:
										s_ErrCount++;
										u1_printf(str,"[ME909]模块注册--注册被拒，查询次数=%d\r\n",s_ErrCount);
										//DEBUG_OUTPUT(str);
										s_Status--;
										//s_Step = 6;	//重新检查SIM卡和网络信号
										
										if( s_ErrCount > 60 )	//60次超时约30秒
										{
											s_ErrCount = 0;
											s_Status = 1;
											s_Step = 1;
										}
										break;
										
									default:
										s_ErrCount++;
										u1_printf(str,"[ME909]模块注册--注册状态未识别,查询次数=%d\r\n",s_ErrCount);
										//DEBUG_OUTPUT(str);
										s_Status--;
										s_Step = 6;	//重新检查SIM卡和网络信号
										break;										
								}
							}
							else		//响应数据格式错误，重新发送检测命令，连续5次错误则重启模块
							{
								u1_printf("[ME909]响应数据不符合格式，重新检测注册状态\r\n");
								s_Status--;
								//s_Step--;	//重新检查网络信号
								s_ErrCount++;
								if( s_ErrCount > 5 )
								{
									s_ErrCount = 0;
									s_Status = 1;
									s_Step = 1;
								}
							}
						break;					
													
						case 8: //AT+CGATT?	模块是否GPRS +CGATT: 1
					
							u1_printf(str,"[ME909]模块附着状态=%s\r\n",GPRS_UART_BUF);
							//DEBUG_OUTPUT(str);
							if( (p1=strstr((const char *)GPRS_UART_BUF,"+CGATT:")) != NULL )
							{
								
								nTemp = atoi( (const char *) (p1+8));
								if( nTemp == 0)		//没有附着GPRS
								{
									s_Status--;
									//s_Step = 5;
									s_ErrCount++;
									if( s_ErrCount > 250 )
									{
										s_ErrCount = 0;
										s_Status = 1;
										s_Step = 1;	
									}
								}
								else	//已经附着GPRS网络
								{
									s_ErrCount = 0;
									s_Status = 9;	//完成后，转入下一步
									s_Step=1;						
								}
							}
							else	//响应数据格式错误，重新发送检测命令，连续5次错误则重启模块
							{
								s_Status--;	//重新查询GPRS附着状态
								//s_Step--;	
								s_ErrCount++;
								if( s_ErrCount > 5 )
								{
									s_ErrCount = 0;
									s_Status = 1;
									s_Step = 1;
								}
							}
						break;
					}
					
					if(s_ErrCount > 200 )
					{
						s_ErrCount = 0;
						s_Status = 1;
						s_Step = 1;
					}
					Clear_GPRS_Buff();
				}
				else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 300) 
				{
					s_ATAckCount++;
					if(s_ATAckCount >= 5)
					{
						s_ATAckCount = 0;
						s_ErrCount = 0;
						u1_printf(" AT无响应\r\n");
						SetLogErrCode(LOG_CODE_TIMEOUT);
						s_Status = 1;
						s_Step = 1;
					}
					else
					{
						s_Status--;
						u1_printf(" AT指令超时\r\n");
					}
					
					s_LastTime = GetSystem10msCount();
				}
					
		break;

		case 9:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{
				switch(s_Step)
				{
//						case 1:
//							SimUartSendStr("AT+CIPMODE=1\r");
//							u1_printf("[ME909S]->设置为透传模式\r\n");
//							s_LastTime = GetSystem10msCount();
//						break;
//						case 2:
//							SimUartSendStr("AT+NETOPEN\r");
//							u1_printf("[ME909S]->激活PDP\r\n");
//							s_LastTime = GetSystem10msCount();
//						break;
//						case 3:
//							SimUartSendStr("AT+IPADDR\r");
//							u1_printf("[ME909S]->获得本地IP地址\r\n");
//							s_LastTime = GetSystem10msCount();
//						break;
//						
						
					case 1: 
						u1_printf("[ME909]定义PDP\n");
						SimUartSendStr("AT+CGDCONT=1,\"IP\",\"CMNET\"\r"); 
						break;
					case 2: 
						u1_printf("[ME909]激活PDP\n");
						SimUartSendStr("AT+CGACT=1,1\r"); 
						break;
					case 3: 
						u1_printf("[ME909]初始化TCP\n");
						SimUartSendStr("AT^IPINIT=\"CMNET\"\r"); 
						break;	//设置为透传模式
					case 4:
						u1_printf("[ME909]查询模块注册网络制式\n");	
						SimUartSendStr("AT^SYSINFOEX\r");
						//if( s_ucAPNFlg ) sprintf(str,"AT+CSTT=\"uninet\"\r");
						//else			 sprintf(str,"AT+CSTT=\"cmnet\"\r");
						break;	//APN,移动：cmnet,联通：uninet
					case 5: 
						u1_printf("[ME909]查询初始化结果\r\n");
						SimUartSendStr("AT^IPINIT?\r");
						break;	// 激活移动场景
					case 6: 
						u1_printf("[ME909]建立监听\r\n");
						SimUartSendStr("AT^IPLISTEN=\"TCP\",5555\r");
						break;	//获取本地IP
					//case 7: sprintf(str,"AT+CIPHEAD=1\r");break;
					default:
						s_LastTime = GetSystem10msCount();
						s_Status = 1;
						s_Step = 1;
					break;
			
				}
				s_Status++;
			}
		
		break;
	
		case 10:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{
				if(GetGPRSUartRecFlag())
				{
					u1_printf("%s\r\n", GPRS_UART_BUF);
												
					switch(s_Step)
				{
				//GPRS设置=====================================================
				case 1: //AT+CGDCONT

				
					if( (p1=strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
					{
						s_Status--;
						s_Step++; 
						s_ErrCount = 0;
					}			
					else if( (p1=strstr((const char *)GPRS_UART_BUF,"ERROR")) != NULL )//刚上电还没有建立过连接，这个地方会返回ERROR
					{
						s_Status--;
						s_Step++; 
						s_ErrCount = 0;
					}
					else
					{
						s_Status--;	//重新发送命令
						s_ErrCount++;
						if( s_ErrCount > 5 )
						{
							s_ErrCount = 0;
							s_Status = 1;
							s_Step = 1;
						}	
					}		
					break;
					
				case 2:  //AT+CGACT

					if( (p1=strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
					{
						s_Status--;
						s_Step++; 
						s_ErrCount = 0;
					}
					else
					{
						s_Status--;	//重新发送命令
						s_ErrCount++;
						if( s_ErrCount > 5 )
						{
							s_ErrCount = 0;
							s_Status = 1;
							s_Step = 1;
						}	
					}			
					break;
					
				case 3: //AT^IPINIT

					if( (p1=strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
					{
						s_Status--;
						s_Step++; 
						s_ErrCount = 0;
					}
					else if( (p1=strstr((const char *)GPRS_UART_BUF,"1013")) != NULL )
					{
						s_Status--;
						s_Step++; 
						s_ErrCount = 0;
					}
					else
					{
						s_Status--;	//重新发送命令
						s_ErrCount++;
						if( s_ErrCount > 5 )
						{
							s_ErrCount = 0;
							s_Status = 1;
							s_Step = 1;
						}	
					}
					break;
				
				case 4: //AT^SYSINFOEX 查询模块注册网络制式

					if( (p1=strstr((const char *)GPRS_UART_BUF,"^SYSINFOEX")) != NULL )
					{
						s_Status--;
						s_Step++; 
						s_ErrCount = 0;
					}
					else if( (p1=strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
					{
						s_Status--;
						s_Step++; 
						s_ErrCount = 0;
					}
					else
					{
						s_Status--;	//重新发送命令
						s_ErrCount++;
						if( s_ErrCount > 5 )
						{
							s_ErrCount = 0;
							s_Status = 1;
							s_Step = 1;
						}	
					}
					break;
				case 5: //AT^IPINIT?

				if( (p1=strstr((const char *)GPRS_UART_BUF,"^IPINIT: 1")) != NULL )
					{
						s_Status--;
						s_Step++; 
						s_ErrCount = 0;
					}
					else
					{
						s_Status--;	//重新发送命令
						s_ErrCount++;
						if( s_ErrCount > 5 )
						{
							s_ErrCount = 0;
							s_Status = 1;
							s_Step = 1;
						}	
					}
					break;
				case 6: //AT^IPLISTEN=\"TCP\",5555

					if( (p1=strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
					{
						s_Status++;	//下一步
						s_Step=1; 
						s_ErrCount = 0;
					}
					else if( (p1=strstr((const char *)GPRS_UART_BUF,"1008")) != NULL )
					{
						s_Status++;	//下一步
						s_Step=1; 
						s_ErrCount = 0;
					}
					else
					{
						s_Status--;	//重新发送命令
						s_ErrCount++;
						if( s_ErrCount > 5 )
						{
							s_ErrCount = 0;
							s_Status = 1;
							s_Step = 1;
						}	
					}
					break;
	//			case 7: //AT+CIPHEAD	收到数据包含IP头
	//				sprintf(str,"[SIM800C]模块收到数据包含IP头=%s\r\n",GPRS_UART_BUF);
	//				DEBUG_OUTPUT(str);
	//				s_Status++;		//初始化完成
	//				s_Step = 1;
	//				break;
				default:
					s_Status = 1;
					s_Step = 1;
					break;	
				}
					
					if(s_ErrCount > 50 )
					{
						s_ErrCount = 0;
						s_Status = 1;
						s_Step = 1;
					}
					s_LastTime = GetSystem10msCount();	
					
					Clear_GPRS_Buff();
				}
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 800) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				sprintf(str, "\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				u1_printf(str);		
	
				s_ATAckCount++;
				
				if(s_ATAckCount >= 5)
				{
					s_ATAckCount = 0;
					u1_printf(" AT无响应\r\n");
					SetLogErrCode(LOG_CODE_TIMEOUT);
					s_Status = 1;
					s_Step = 1;
				}
				else
				{
					s_Status--;
					u1_printf(" AT指令超时\r\n");
				}
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
			}	
		break;		
		
//		case 11:
//			u1_printf("[ME909S]建立TCP链接\r\n");
////			sprintf(str,"AT+CIPOPEN=0,\"TCP\",\"%d.%d.%d.%d\",%d\r"
////			,p_sys->Gprs_ServerIP[0]
////			,p_sys->Gprs_ServerIP[1]
////			,p_sys->Gprs_ServerIP[2]
////			,p_sys->Gprs_ServerIP[3]
////			,p_sys->Gprs_Port	);
//		
//			sprintf(str,"AT^IPOPEN=1,\"TCP\",\"42.159.233.88\",6070,5555\r");
//			SimUartSendStr(str);	
//			u1_printf(str);	//串口输出AT命令
//			u1_printf("\n");
//			s_LastTime = GetSystem10msCount();
//			s_Status++;		
//		//	s_ErrCount = 0;		
//		break;
		case 11:
			
			if( CalculateTime(GetSystem10msCount(),s_LastTime) > 100 )
			{
				switch( s_Step )
				{
					case 1: //
						u1_printf("[ME909]建立TCP连接\n");
						sprintf(str,"AT^IPOPEN=1,\"TCP\",\"42.159.233.88\",6070\r");
					break;
					case 2: 
						u1_printf("[ME909]查询TCP连接\n");
						sprintf(str,"AT+CGACT=1,1\r"); 
					break;
				}
				
				SimUartSendStr(str);	
				u1_printf(str);	//串口输出AT命令
				u1_printf("\n");
				s_LastTime = GetSystem10msCount();
				s_Status++;			
				break;
			}
			
		case 12:	//TCP连接响应

			if(GetGPRSUartRecFlag())
			{
				u1_printf("%s\r\n", GPRS_UART_BUF);
												
				switch(s_Step)
				{
				//=====================================================
				case 1: //AT^IPOPEN
				
					if( (p1=strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
					{
						s_Status--;
						s_Step++; 
						s_ErrCount = 0;
					}			
					else if( (p1=strstr((const char *)GPRS_UART_BUF,"1003")) != NULL )//刚上电还没有建立过连接，这个地方会返回ERROR
					{
						s_Status--;
						s_Step++; 
						s_ErrCount = 0;
					}
					else
					{
						s_Status--;	//重新发送命令
						s_ErrCount++;
						if( s_ErrCount > 5 )
						{
							s_ErrCount = 0;
							s_Status = 1;
							s_Step = 1;
						}	
					}		
					break;
					
				case 2:  //AT^IPOPEN?
					u1_printf("[ME909]查询连接状态=%s\r\n",GPRS_UART_BUF);
					if( (p1=strstr((const char *)GPRS_UART_BUF,"^IPOPEN")) != NULL )
					{
						s_Status++;	//连接成功
						s_Step=1; 
						s_ErrCount = 0;
					}
					else if( (p1=strstr((const char *)GPRS_UART_BUF,"1003")) != NULL )//刚上电还没有建立过连接，这个地方会返回ERROR
					{
						s_Status--;
						s_Step++; 
						s_ErrCount = 0;
					}
					else
					{
						s_Status--;	//重新发送命令
						s_ErrCount++;
						if( s_ErrCount > 5 )
						{
							s_ErrCount = 0;
							s_Status = 1;
							s_Step = 1;
						}	
					}			
					break;
				}
				s_LastTime = GetSystem10msCount();
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) > 200 )
			{		
				s_Status--;	//重新发送命令
				u1_printf("[ME909]模块超时无响应数据\r\n");
				s_ErrCount++;
				if( s_ErrCount >= 5 )
				{
					s_ErrCount = 0;
					s_Status = 1;
					s_Step = 1;
				}	
				s_LastTime = GetSystem10msCount();
			}

		break;
			
		
		case 13:
			s_OverCount = 0;
			s_Status = 50;
			s_LastTime = GetSystem10msCount();
			SetTCProtocolRunFlag(TRUE);
			s_EndTime = GetSystem10msCount();
			SetLinkNetTime(CalculateTime(s_EndTime, s_StartTime) );
			u1_printf("\r\n 连接耗时:%.2fs\r\n\r\n", CalculateTime(s_EndTime, s_StartTime)/100.0);
		break;
		
		case 50:
			if(GetTCModuleReadyFlag())		//SIM就绪
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), s_AliveRTCCount) >= p_sys->Heart_interval)	
				{
					g_TCRecStatus.AliveAckFlag = FALSE;
					s_AliveRTCCount = GetRTCSecond();
					Mod_Send_Alive();	//发送心跳包
					delay_ms(20);
					
					Mod_Report_Data();	//上报网关电压数据
					s_LastTime = GetSystem10msCount();
					s_Status++;
				}				
			}
			else
			{
				s_Status = 1;
				s_Step = 1;
				s_ErrCount = 0;
				s_LastTime = GetSystem10msCount();
			}
		break;
		
		case 51:
			if(g_TCRecStatus.AliveAckFlag)//非长连接设备取消心跳包  2019.2.12
			{
				s_Status++;
				s_LastTime = GetSystem10msCount();
				g_TCRecStatus.AliveAckFlag = FALSE;
				s_OverCount = 0;
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 2500 )
			{
				s_Status--;
				s_LastTime = GetSystem10msCount();
				u1_printf("\r\n  Timeout did not receive a reply packet reissued\r\n");
				Clear_GPRS_Buff();
				s_OverCount++;
				if(s_OverCount >= 2)
				{
					u1_printf("[SIM]->Exit the passthrough\r\n");
					SetLogErrCode(LOG_CODE_NOSERVERACK);
					StoreOperationalData();
					s_OverCount = 0;
					s_Status = 9;
					s_Step = 1;
					s_ErrCount = 0;
					s_LastTime = GetSystem10msCount();
					SetOnline(FALSE);
					delay_ms(100);
					SimUartSendStr("+++");
					delay_ms(1100);
					SimUartSendStr("+++");				
					delay_ms(50);				
					SetTCProtocolRunFlag(FALSE);	
				}
			}		
		break;
					
		case 52:
			if( CalculateTime(GetSystem10msCount(),s_LastTime2) >= 200 )	//2S判断一次是否进入省电模式
			{
				if(GetPowerSavingFlag())
				{
					s_SavingModeFlag = GetPowerSavingFlag();
					if(JudgmentTimePoint(NETTYPE_4G))	//达到唤醒通信模块时间点
					{						
						if(s_SimSavingFlag)
						{
							s_Status = 1;
							s_Step = 1;
							s_ErrCount = 0;
							s_SimSavingFlag = FALSE;
						}
					}
					else	//到达省电模式时间点
					{
						s_SimSavingFlag = TRUE;
						SetTCModuleReadyFlag(FALSE);
						SetOnline(FALSE);
						ME909S_PWR_OFF();
						s_LastTime2 = GetSystem10msCount();
					}
				}
				else
				{
					if(s_SavingModeFlag)	//退出省电模式，重启模块
					{
						s_SavingModeFlag = GetPowerSavingFlag();
						s_Status = 1;
						s_Step = 1;
						s_ErrCount = 0;
					}
					s_Status = 53;
				}
			}
				
			RetransmissionGPRSData();	//是否有GPRS下行包进行重发
			
		break;
			
		case 53:			//退出透传，关闭TCP链接，PDP去激活	
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= p_sys->Heart_interval*100 )
			{				
				s_LastTime = GetSystem10msCount();
				s_Status = 50;
				u1_printf("\r\n Signal strength:%d\r\n", s_SignalStrength);
				
//				RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
//				if(s_LastRTCDate != RTC_DateStruct.RTC_Date)
//				{
//					s_LastRTCDate = RTC_DateStruct.RTC_Date;
//					SetGPSRunFlag(TRUE);
//				}
				
			}
			else
			{
				RetransmissionGPRSData();	//是否有GPRS下行包进行重发
				s_Status = 52;
				s_LastTime2 = GetSystem10msCount();
			}
		break;			

			

			
	
	}
	
}
