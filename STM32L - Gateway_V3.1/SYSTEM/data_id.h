
#ifndef __DATA_ID_H
#define __DATA_ID_H

#define KQW_ID				(0x0001)   //Air Temperature
#define KQS_ID      		(0x0002)   //空气湿度
#define TRW_ID      		(0x0003)   //Soil Temperature
#define TRS_ID      		(0x0004)   //土壤水分
#define GZD_ID      		(0x0005)   //Illuminance
#define CO2_ID      		(0x0006)   //Carbon Dioxide
#define JYL_ID      		(0x0007)   //RainFall
#define FS_ID       		(0x0008)   //Wind Speed
#define LL_ID       		(0x0009)   //Flow
#define FX_ID       		(0x000A)   //Wind Direction
#define SYL_ID      		(0x000B)   //Water Pressure
#define QY_ID       		(0x000C)   //气压
#define YW_ID       		(0x000D)   //Liquid Level 		0-5m
#define PH_ID       		(0x000E)   //酸碱度

//add 2014.8.7
#define YMSD_ID				(0x0010)   //Leaf Wetness
#define NOISE_ID			(0x0011)   //Noise DecibelDB
#define HWDS_ID				(0x0012)   //红外对射(光束遮断式)
#define HWSJ_ID				(0x0013)   //红外双鉴(微波+红外,空间探测)

#define H2S_ID      		(0x0014)   //H2S
#define NH3_ID				(0x0015)   //NH3
#define AIR_PRESS_ID		(0x0016) 	//Atmospheric Pressure
#define PHOTOSYNTHETIC_ACTIVE_RADIATION_ID	(0x0017) 	//Photosynthetically Active Radiation
#define EVAPORATION_ID		(0x0018) 	//Evaporation
#define SOLAR_RADIATION_ID	(0x0019)	//Solar Radiation

#define	AIR_ION_ID			(0x001A)	//AIR ION
#define	DEW_POINT			(0x001B)	//Dew Point
#define O2_ID      			(0x001E)   //O2
#define CO2_PER_ID  		(0x001F)   //Carbon Dioxide
#define CH4_ID       		(0x0020)   //CH4

#define CO_ID				(0x0021)   //CO(CO)
#define NO2_ID				(0x0022)   //NO2(N2O)
#define SO2_ID				(0x0023)   //SO2(S2O)
#define SMOKE_ID			(0x0024)   //Smoke
#define HCL_ID				(0x0025)   //HCL浓度（HCl，PPM）

#define DDL_ID				(0x002A)   //Water Electric
#define RJY_ID				(0x002B)   //Dissolved Oxygen

#define COD_ID				(0x0034)   //COD
#define NH3_NH4_ID			(0x0035)   //NH3 NH4
#define SOIL_M_ID			(0x0036)   //Soil Tension0-93KPa

#define SOIL_EC_ID			(0x0038)  	//土壤EC值,0-20mS,精度0.1mS
#define SOIL_YF_ID			(0x0039)	//Soil Salinity，0-8000mg/L，精度1mg
#define	SOIL_WATER_POTENTIAL (0x003A)	//土壤水势传感器  -9 ~ -100,000kPa

#define	H2O_NH3_ID			(0x003D)		//Water NH3 NH4NH3 NH4	ppm
#define	H2O_TURBIDITY_ID	(0x003E)		//Water NH3 NH4浊度	NTU
#define H2O_CL2_ID			(0x003F)		//Water NH3 NH4余氯	ppm

#define	ORP_POTENTIOMETER_ID (0x0040)	//氧化还原电位计
#define CHLOROPHYLL_ID		(0x0041)	//Chlorophyll传感器		

#define	LONGITUDE_ID		(0x0050)		//Longitude
#define	LATITUDE_ID			(0x0051)		//维度

#define C_LEVEL_ID  		(0x0064)   //电平控制器
#define C_RT_P_ID   		(0x0065)   //脉冲型正反转
#define C_RT_L_ID   		(0x0066)   //电平型正反转
#define C_PULSE_ID			(0x0068)   //脉冲开关控制

#define V263_C_ID			(0x00FD)   //RS485控制器标志
//#define RS485_C_ID			(0x00FE)   //RS485控制器标志

#define	CSQ_ID				(0x00F0)	//信号强度
#define	LINKTIME_ID			(0x00F1)	//连接时间
#define DCPOWER_ID			(0x00FE)		//外部接入电压
#define BAT_VOL     		(0x00FF)   //本地电源电压


//固定通道号

#define	CH_CSQ				240		//CSQ通道号
#define	CH_LINKTIME			241		//联网时间通道号
#define	CH_BATVOL			242		//电池电压通道号
#define	CH_LONGITUDE		243		//经度通道号
#define	CH_LATITUDE			244		//纬度通道号
#define	CH_DCPOWERVOL		245		//DC电压通道号

//数据标识符号
#define MARK_FLOAT			(0x00)
#define MARK_INT32			(0x01)
#define MARK_UINT32			(0x02)
#define MARK_INT16			(0x03)
#define MARK_UINT16			(0x04)
#define MARK_UINT8			(0x05)
#define MARK_STR			(0x06)


#endif
