/**********************************
说明:基本定时器TIM4
作者:关宇晟
版本:V2018.02.01
***********************************/
#include "Timer.h"
#include "main.h"

static unsigned short	n100msCount = 0;//100ms计数 
static unsigned short	n10msCount  = 0;	//10ms计数 
unsigned short   g_WakeUpTime = 0;

void TIM4_Init(unsigned int arr,unsigned int psc)
{	
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE); //时钟使能//TIM4时钟使能    
	
	TIM_TimeBaseStructure.TIM_Period = arr; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	
	TIM_TimeBaseStructure.TIM_Prescaler = psc*10/11; //设置用来作为TIMx时钟频率除数的预分频值+ 16
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位
 
	TIM_ITConfig(TIM4,TIM_IT_Update,ENABLE ); //使能指定的TIM4中断,允许更新中断
	
	TIM_Cmd(TIM4, ENABLE);  //使能TIMx	
}
	    
void TIM4_IRQHandler(void)
{ 		
	if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET)//是更新中断
	{	 	
		n10msCount++;	   
		if(n10msCount%10 == 0)
		{
			n100msCount++;	
		}
		TIM_ClearITPendingBit(TIM4, TIM_IT_Update);	
	}	
}
unsigned short CalculateTime( unsigned short nNew, unsigned short nOld)
{
	unsigned short nTemp = 0;
	if (nNew >= nOld)
	{
		nTemp = nNew - nOld;
	}
	else
	{
		nTemp = 65535 - nOld + nNew;
	}
	return (nTemp);
}
unsigned short GetSystem100msCount(void)
{
	return (n100msCount);
}
unsigned short GetSystem10msCount(void)
{
	return (n10msCount);
}

