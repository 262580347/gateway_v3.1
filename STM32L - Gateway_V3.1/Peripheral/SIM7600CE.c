#include "main.h"
#include "SIM7600CE.h"

#define APN_CMNET 	1		//中国移动

#define APN_UNINET 	2		//中国联通

#define APN_CTNET 	3		//中国电信

static void SimUartSendStr(char *str)
{
#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V100)

	u3_printf(str);

#elif (HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)

	u2_printf(str);
	
#endif	
}

void SIM7600CEPortInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB, ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = SIM7600CE_ENABLE_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(SIM7600CE_ENABLE_TYPE, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = SIM7600CE_WAKEUP_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(SIM7600CE_WAKEUP_TYPE, &GPIO_InitStructure);	
	
	GPIO_SetBits(SIM7600CE_WAKEUP_TYPE, SIM7600CE_WAKEUP_PIN);
	
	GPIO_InitStructure.GPIO_Pin = SIM7600CE_RST_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(SIM7600CE_RST_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = SIM7600CE_RF_DISABLE_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(SIM7600CE_RF_DISABLE_TYPE, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = SIM7600CE_DTR_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(SIM7600CE_DTR_TYPE, &GPIO_InitStructure);	
	
	GPIO_ResetBits(SIM7600CE_DTR_TYPE, SIM7600CE_DTR_PIN);
	
			
}

void SIM7600CEProcess(unsigned short nMain10ms)
{
	u16 nTemp = 0;
	char str[512];
	static u16 s_LastTime = 0, s_LastTime2 = 0, s_OverCount = 0, s_ATAckCount = 0;
	static unsigned int s_AliveRTCCount = 0xfffe0000;
	static u8 s_APN = 0, s_APNFlag = 0, s_Status = 1, s_Step = 1, RunOnce = FALSE, s_ErrCount = 0, s_RetryCount = 0;
	static u8 s_PowerOnFlag = FALSE, s_SavingModeFlag = FALSE, s_SimSavingFlag = FALSE, s_SignalStrength = 0;
	char * p1 = NULL, buf[10];
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;

	static u16 s_StartTime = 0, s_EndTime = 0;
	RTC_DateTypeDef RTC_DateStruct;
	
	p_sys = GetSystemConfig();
	
	if(RunOnce == FALSE)
	{
		RunOnce = TRUE;
		s_LastTime = GetSystem10msCount();
		InitGprsLinkedList();
		RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);	
	}		
		
	switch(s_Status)
	{	
		case 1:   //模块断电
			s_RetryCount++;
			SetTCModuleReadyFlag(FALSE);
			if(s_RetryCount >= 10)
			{
				u1_printf(" Timeout not connected to the network waiting for restart...\r\n");
				SetLogErrCode(LOG_CODE_TIMEOUT);
				SetLogErrCode(LOG_CODE_RESET);
				StoreOperationalData();
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				Sys_Soft_Reset();	
			}
			SIM7600CEPortInit();
			SIM7600CE_PWR_OFF();
			SIM7600CE_RST_ON();
			SIM7600CE_RF_DISABLE_ON();
			SetTCProtocolRunFlag(FALSE);
			GPRS_Uart_Init(SIM_BAND_RATE);
			s_LastTime = GetSystem10msCount();
			s_Status++;
			u1_printf("\r\n[SIM7600CE]模块断电，延时2s.\r\n");
			s_PowerOnFlag = TRUE;	
		break;
		 
		case 2:	//断电后延时3S上电，模块上电自动开机				
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 2000 )
			{
				s_LastTime = GetSystem10msCount();
				s_Status++;
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 200 )
			{
				if(s_PowerOnFlag)
				{
					s_PowerOnFlag = FALSE;
					u1_printf("\r\n[SIM7600CE]模块上电，延时20s.\r\n");
					s_StartTime = GetSystem10msCount();
					SIM7600CE_PWR_ON();	
					
					SIM7600CE_RST_ON();
					delay_ms(250);
					SIM7600CE_RST_OFF();
					delay_ms(250);
					SIM7600CE_RST_ON();
				}
			}
		break;
			
		case 3: 	//上电完成后，初始化端口
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 50 )
			{
				u1_printf("\r\n[SIM7600CE]模块上电完成，检测模块.\r\n");
				SimUartSendStr("AT\r");
				delay_ms(50);
				SimUartSendStr("AT\r");
				s_LastTime = GetSystem10msCount();
				s_Status++;
			}
		break;
		
		case 4:	//延时100MS,检测响应数据
			if(GetGPRSUartRecFlag())
			{		
				if(strstr((char *)GPRS_UART_BUF, "OK"))
				{
					s_ErrCount = 0;
					s_Status++;	
				}			
				Clear_GPRS_Buff();
				
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 20 )
			{				
					u1_printf("[SIM7600CE]AT无响应\r\n");
					s_Status--;   //回上一个状态
					s_ErrCount++;   //重试次数加1
					if(s_ErrCount >= 5 )
					{
						SetLogErrCode(LOG_CODE_NOMODULE);
						s_ErrCount = 0;
						s_Status = 1;	//模块重新上电初始化
					}						
			}
	 	break;
		
		case 5:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 30) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				sprintf(str, "\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				u1_printf(str);		

				switch(s_Step)
				{
					case 1:
						SimUartSendStr("ATE0\r");
						u1_printf("[SIM7600CE]->ATE0\r\n");
						s_LastTime = GetSystem10msCount();
					break;
					case 2:
						SimUartSendStr("AT+CPIN?\r");
						u1_printf("[SIM7600CE]->查询模块SIM状态\r\n");
						s_LastTime = GetSystem10msCount();
					break;
					case 3:
						SimUartSendStr("AT+CIMI\r");
						u1_printf("[SIM7600CE]->查询IMSI\r\n");
						s_LastTime = GetSystem10msCount();
					break;
					case 4:				//根据CIMI信息设置APN
						switch(s_APN)
						{
							case APN_CMNET:		//连接移动APN
//										SimUartSendStr("AT+CSTT=\"CMNET\"\r");
//										sprintf(str, "[SIM7600CE]->建立APN:CMNET\r\n");
								SimUartSendStr("AT+CGDCONT=1,\"IP\",\"CMIOT\"\r");
								sprintf(str, "[SIM7600CE]->建立APN:CMIOT\r\n");
								break;
							case APN_UNINET:		//连接联通APN
//										SimUartSendStr("AT+CSTT=\"UNINET\"\r");
//										sprintf(str, "[SIM7600CE]->建立APN:UNINET\r\n");
								SimUartSendStr("AT+CGDCONT=1,\"IP\",\"UNIM2M.NJM2MAPN\"\r");
								sprintf(str, "[SIM7600CE]->建立APN:UNIM2M.NJM2MAPN\r\n");
								break;
							case APN_CTNET:			//连接电信APN
								SimUartSendStr("AT+CGDCONT=1,\"IP\",\"ctnet\"\r");
								sprintf(str, "[SIM7600CE]->建立APN:ctnet\r\n");
								break;
//								SimUartSendStr("AT+CGDCONT=1,\"IP\",\"ctnet\"\r");
//								sprintf(str, "[SIM7600CE]->建立APN:ctnet\r\n");
//								break;
							default:
								SimUartSendStr("AT+CGDCONT?\r");
								sprintf(str, "[SIM7600CE]->未能识别的运营商\r\n");									
								break;
						}
						u1_printf(str);			
					break;
					case 5:
						SimUartSendStr("AT+CSQ\r");
						u1_printf("[SIM7600CE]->查询模块信号质量\r\n");
						s_LastTime = GetSystem10msCount();		
					break;	
					case 6:
						SimUartSendStr("AT+COPS?\r");
						u1_printf("[SIM7600CE]->查询网络运营商\r\n");
						s_LastTime = GetSystem10msCount();
					break;	
					case 7:
						SimUartSendStr("AT+CPSI?\r");
						u1_printf("[SIM7600CE]->查询驻网详细信息\r\n");
						s_LastTime = GetSystem10msCount();
					break;	
					case 8:
						SimUartSendStr("AT+CGATT?\r");
						u1_printf("[SIM7600CE]->查询模块附着GPRS状态\r\n");
						s_LastTime = GetSystem10msCount();				
					break;	
				}
				s_Status++;
			}	
					
		break;
		
		case 6:
				if(GetGPRSUartRecFlag())
				{
					u1_printf("%s\r\n", GPRS_UART_BUF);
												
					switch(s_Step)
					{
						case 1:
							if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
							{
								s_Step++;	
								s_Status--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();							
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
							}
						break;
							
						case 2:
						//	u1_printf("检测SIM卡： ");
							if((strstr((const char *)GPRS_UART_BUF,"+CPIN: READY")) != NULL )
							{					
								s_Step++;	
								s_Status--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();		
								u1_printf("\r\n  SIM卡正常\r\n");
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
								if(s_ErrCount >= 10)
								{
									SetLogErrCode(LOG_CODE_NO_CARD);
									u1_printf(" No Card!\r\n");
									s_ErrCount = 0;
									s_Status = 1;
									s_Step = 1;
								}
							}						
						break;	
							
						case 3:			//CIMI查询
							//中国移动:46000、46002、46004、46007、46008
							//中国联通:46001、46006、46009
							//中国电信:46003、46005、46011。
								//越南MCC:452
								//越南MNC   	运营商				APN:
								// 	  01 	MobiFone			m-wap
								// 	  02 	Vinaphone			m3-world / m-world
								// 	  03 	S-Fone 
								// 	  04 	Viettel Mobile		v-internet
								// 	  05 	Vietnamobile
								// 	  06 	EVNTelecom
								// 	  07 	Beeline VN
								// 	  08 	3G EVNTelecom
							if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )			//===============此处判断可能容易出错  
							{
//								MCC = (GPRS_UART_BUF[2] - '0')*100 + (GPRS_UART_BUF[3] - '0')*10 +(GPRS_UART_BUF[4] - '0');
//								MNC = (GPRS_UART_BUF[5]*10 - '0') + (GPRS_UART_BUF[6] - '0');
								
								s_APNFlag = 1;	
								s_APN = APN_CMNET;
								u1_printf("IMSI: APN SET TO CMNET\r\n");

								if(s_APNFlag)
								{
								//	u1_printf("模块名称：%s\r\n", GPRS_UART_BUF);
									s_Step++;	
									s_Status--;		
									s_ErrCount = 0;		
								}
								else
								{
									s_Status--;		
									s_ErrCount ++;
								}
								s_LastTime = GetSystem10msCount();	
									
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
							}
						break;
							
						case 4:
							if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
							{
							//	u1_printf("APN OK\r\n");
								s_Step++;	
								s_Status--;		
								s_ErrCount = 0;
														
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
							}
						break;
							
						case 5:				//	u1_printf("检测信号质量： ");					
							if((p1 = strstr((const char *)GPRS_UART_BUF,"+CSQ:")) != NULL )
							{							
								//u1_printf("%s\r\n", GPRS_UART_BUF);	
								memset(buf, 0 ,sizeof(buf));								
								strncpy(buf,p1+6,2);
								nTemp = atoi( buf);
								SetCSQValue(nTemp);
								if( nTemp == 99 )								
								{
									s_Status--;	
									s_ErrCount++;
									u1_printf(" 无信号\r\n");
									SetLogErrCode(LOG_CODE_WEAKSIGNAL);
								}
								else if( nTemp < 6 )								
								{
									s_Status--;	
									s_ErrCount++;
									u1_printf(" 信号差\r\n");
									SetLogErrCode(LOG_CODE_WEAKSIGNAL);
								}
								else
								{
									s_SignalStrength = nTemp;
									s_Status--;	
									s_Step++;	
									s_ErrCount = 0;
									u1_printf("\r\n  信号正常\r\n");
									ClearLogErrCode(LOG_CODE_WEAKSIGNAL);
									
								}
							}
							else
							{
	//							u1_printf("    Error\r\n");
								s_Status--;	
								s_ErrCount++;
							}		
						break;		
							
						case 6:
							//u1_printf(" 运营商名称： ");
							if((p1 = strstr((const char *)GPRS_UART_BUF,"+COPS:")) != NULL )
							{				
								if(!s_APNFlag)
								{
									if((strstr((const char *)GPRS_UART_BUF,"CHINA MOBILE")) != NULL )  
									{
										s_APN = APN_CMNET;
										s_APNFlag = 1;
										u1_printf("COPS: CHINA MOBILE\r\n");
										s_Step++;	
										s_Status--;		
										s_ErrCount = 0;
									}
									else if((strstr((const char *)GPRS_UART_BUF,"UNICOM")) != NULL )  
									{
										s_APN = APN_UNINET;
										s_APNFlag = 1;
										u1_printf("COPS: UNICOM\r\n");
										s_Step++;	
										s_Status--;		
										s_ErrCount = 0;
									}
									else if((strstr((const char *)GPRS_UART_BUF,"CHINA TELECOM")) != NULL )  	//电信运营商信息不一定正确
									{
										s_APN = APN_UNINET;
										s_APNFlag = 1;
										u1_printf("COPS: CTNET\r\n");
										
										s_Step++;	
										s_Status--;		
										s_ErrCount = 0;
									}
									else
									{
										u1_printf("COPS: UNRECOGNIZABLED NETWORK\r\n");
										s_Status--;		
										s_ErrCount ++;
									}
								}
								else
								{
									s_Step++;	
									s_Status--;		
									s_ErrCount = 0;
								}
									
								s_LastTime = GetSystem10msCount();		
							//	u1_printf("%s\r\n", GPRS_UART_BUF);
							}
							else
							{
								u1_printf("    Error\r\n");
								s_Status--;	
								s_ErrCount++;
							}
						break;	
											
						case 7:
							//u1_printf(" 模块注册网络： ");
							if((p1 = strstr((const char *)GPRS_UART_BUF,"+CPSI:")) != NULL )
							{						
								//u1_printf("%s\r\n", GPRS_UART_BUF);
								if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
								{
									u1_printf("\r\n[SIM7600CE]模块注册网络--成功\r\n");
									s_Status--;
									s_Step++;
									s_ErrCount = 0;
								}
								else
								{
									s_Status--;
									s_ErrCount++;
									sprintf(str, "\r\n [SIM7600CE]模块注册获取失败, 次数=%d\r\n",s_ErrCount);
									u1_printf(str);
								}	
								
								if(s_ErrCount == 150)
								{		
									u1_printf("\r\n  GPRS注册网络失败，重启模块\r\n");
									s_ErrCount = 0;
									s_Status = 1;
									s_Step = 1;
								}
								
							}
							else
							{
	//							u1_printf("    Error\r\n");
								s_Status--;	
								s_ErrCount++;
							}	
								
						break;						
													
						case 8:
							u1_printf("GPRS附着状态： ");
							if((p1 = strstr((const char *)GPRS_UART_BUF,"+CGATT:")) != NULL )
							{					
								//u1_printf("%s\r\n", GPRS_UART_BUF);
								
								nTemp = atoi( (const char *) (p1+8));
								if( nTemp == 0 )								
								{
									u1_printf(" 未附着\r\n");
									s_Status--;	
									s_ErrCount++;
									if(s_ErrCount >= 150)
									{					
										u1_printf("\r\n  网络附着失败，重启模块\r\n");
										s_ErrCount = 0;
										s_Status = 1;
										s_Step = 1;
									}
								}
								else
								{
									u1_printf(" 附着成功\r\n");
									

									s_Status = 9;	

									s_Step = 1;	
									s_ATAckCount = 0;
									s_ErrCount = 0;
								}
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
							}							
						break;
					}
					
					if(s_ErrCount > 200 )
					{
						s_ErrCount = 0;
						s_Status = 1;
						s_Step = 1;
					}
					Clear_GPRS_Buff();
				}
				else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 300) 
				{
					s_ATAckCount++;
					if(s_ATAckCount >= 5)
					{
						s_ATAckCount = 0;
						s_ErrCount = 0;
						u1_printf(" AT无响应\r\n");
						SetLogErrCode(LOG_CODE_TIMEOUT);
						s_Status = 1;
						s_Step = 1;
					}
					else
					{
						s_Status--;
						u1_printf(" AT指令超时\r\n");
					}
					
					s_LastTime = GetSystem10msCount();
				}
				
		
		break;

//		case 7:			//电信LBS定位指令不同
//			if((s_APN == APN_CMNET) || (s_APN == APN_UNINET))		//移动联通卡LBS定位
//			{
//				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20) 
//				{
//					switch(s_Step)
//					{
//						case 1:
//							SimUartSendStr("AT+CNETSTART\r");
//							u1_printf("[SIM7600CE]->开启LBS定位业务\r\n");
//						break;							
//						case 2:
//							SimUartSendStr("AT+CNETIPADDR?\r");
//							u1_printf("[SIM7600CE]->获取定位服务器IP\r\n");
//						break;
//						case 3:
//							SimUartSendStr("AT+CLBS=1\r");
//							u1_printf("[SIM7600CE]->获取基站定位信息\r\n");
//						break;
//						case 4:
//							SimUartSendStr("AT+CNETSTOP\r");
//							u1_printf("[SIM7600CE]->关闭LBS\r\n");
//						break;
//											
//						default:
//							s_Step = 1;	
//							s_Status++;			//跳过LBS定位
//							s_ATAckCount = 0;
//							s_ErrCount = 0;
//						break;
//					}
//					s_Status++;
//					s_LastTime = GetSystem10msCount();	
//				}
//			}
//			else 		//电信卡定位：
//			{
//				
//				s_Status=9;
//				s_LastTime = GetSystem10msCount();	
//			}			
//		
//		break;
//			
//		case 8:
//			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10)
//			{
//				if(GetGPRSUartRecFlag())
//				{
//					u1_printf("%s\r\n", GPRS_UART_BUF);
//												
//					switch(s_Step)
//					{	
//						case 1:						
//							if((strstr((const char *)GPRS_UART_BUF,"+CNETSTART: 0")) != NULL )
//							{									
//								s_Step++;			
//								s_Status--;	
//								s_ATAckCount = 0;
//								s_ErrCount = 0;
//							}				
//							else if((strstr((const char *)GPRS_UART_BUF,"+CNETSTART: 1")) != NULL )
//							{					
//								u1_printf("[SIM7600CE]正在启动。。\r\n");						
//								s_Status--;	
//								s_ErrCount ++;
//							}	
//							else	
//							{
//								u1_printf("[SIM7600CE]收到未识别的指令\r\n");	
//								s_Status--;	
//								s_ErrCount ++;
//							}
//						break;					
//							
//						case 2:
//							if((strstr((const char *)GPRS_UART_BUF,".")) != NULL )
//							{					
//								s_Step ++;	
//								s_Status--;		
//								s_ATAckCount = 0;
//								s_ErrCount = 0;
//							}
//							else
//							{
//								u1_printf("    Error\r\n");
//								s_Status--;	
//								s_ErrCount++;
//							}						
//						break;	
//							
//						case 3:
//							if((strstr((const char *)GPRS_UART_BUF,".")) != NULL )
//							{
//								s_Step++;	
//								s_Status--;		
//								s_ATAckCount = 0;
//								s_ErrCount = 0;
//								memcpy(str, GPRS_UART_BUF, GPRS_UART_CNT);
//								p_str = strtok(str, ",");
//								p_str = strtok(NULL, ",");  
//								SetLatitude(atof(p_str));
//								p_str = strtok(NULL, ",");  
//								SetLongitude(atof(p_str));
//								s_LocationFailCount = 0;
//								u1_printf("\r\n 经度:%f  纬度:%f \r\n", GetLongitude(), GetLatitude());
//								SetGPSRunFlag(FALSE);
//							}
//							else
//							{
//								s_Status--;	
//								s_ErrCount++;								
//							}
//						break;
//							
//						case 4:
//							if((strstr((const char *)GPRS_UART_BUF,"+CNETSTOP: 0")) != NULL )
//							{					
//								s_Step = 1;	
//								s_Status++;		
//								s_ATAckCount = 0;
//								s_ErrCount = 0;
//							}
//							else
//							{
//								s_Status--;	
//								s_ErrCount++;
//							}						
//						break;
//							
//						default:

//						break;
//							
//					}		
//					if(s_ErrCount > 5 )
//					{
//						u1_printf("[SIM7600CE] LBS定位失败, 关闭LBS\r\n");
//						SimUartSendStr("AT+CNETSTOP\r");
//						SetLongitude(0);
//						SetLatitude(0);
//						s_LocationFailCount++;
//						if(s_LocationFailCount >= 3)
//						{
//							s_LocationFailCount = 0;
//							SetGPSRunFlag(FALSE);
//						}
//						s_ErrCount = 0;
//						s_ATAckCount = 0;
//						s_Status ++;
//						s_Step = 1;
//					}
//					
//					Clear_GPRS_Buff();
//				}
//				else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 300)
//				{		
//					s_ATAckCount++;			
//					if(s_ATAckCount > 5 )
//					{
//						u1_printf("[SIM7600CE] LBS定位超时, 关闭LBS\r\n");
//						SimUartSendStr("AT+CNETSTOP\r");
//						SetLongitude(0);
//						SetLatitude(0);
//						s_LocationFailCount++;
//						if(s_LocationFailCount >= 3)
//						{
//							s_LocationFailCount = 0;
//							SetGPSRunFlag(FALSE);
//						}
//						s_ErrCount = 0;
//						s_ATAckCount = 0;
//						s_Status ++;
//						s_Step = 1;
//					}
//					else
//					{				
//						u1_printf("[SIM7600CE] 响应超时\r\n");		
//						s_Status--;	
//					}
//										
//				}				
//				s_LastTime = GetSystem10msCount();		
//			}
//		break;		
			
		case 9:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{
				switch(s_Step)
				{
						case 1:
							SimUartSendStr("AT+CIPMODE=1\r");
							u1_printf("[SIM7600CE]->设置为透传模式\r\n");
							s_LastTime = GetSystem10msCount();
						break;
						case 2:
							SimUartSendStr("AT+NETOPEN\r");
							u1_printf("[SIM7600CE]->激活PDP\r\n");
							s_LastTime = GetSystem10msCount();
						break;
						case 3:
							SimUartSendStr("AT+IPADDR\r");
							u1_printf("[SIM7600CE]->获得本地IP地址\r\n");
							s_LastTime = GetSystem10msCount();
						break;
						
						default:
							s_LastTime = GetSystem10msCount();
							s_Status = 1;
							s_Step = 1;
						break;
				}
				s_Status++;
			}
		
		break;
	
		case 10:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{
				if(GetGPRSUartRecFlag())
				{
					u1_printf("%s\r\n", GPRS_UART_BUF);
												
					switch(s_Step)
					{
						case 1:
							if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
							{
							//	u1_printf("设置为透传模式\r\n");
								s_Step++;	
								s_Status--;		
								s_ErrCount = 0;
													
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
							}
						break;
							
						case 2:
							if((strstr((const char *)GPRS_UART_BUF,"+NETOPEN: 0")) != NULL )
							{
								s_Step++;	
								s_Status--;		
								s_ErrCount = 0;
														
							}
							else if((strstr((const char *)GPRS_UART_BUF,"already opened")) != NULL )
							{
								s_Step++;	
								s_Status--;		
								s_ErrCount = 0;
								
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
							}
						break;
							
						case 3:
							//u1_printf("模块本地IP：");
							if((strstr((const char *)GPRS_UART_BUF,".")) != NULL )
							{					
								s_Step = 1;	
								s_Status++;		
								s_ATAckCount = 0;
								s_ErrCount = 0;
							
								//sprintf(str, "%s\r\n", GPRS_UART_BUF));
								//u1_printf(str);
							}
							else
							{
								u1_printf("    Error\r\n");
								s_Status--;	
								s_ErrCount++;
							}						
						break;
						
						default:
							
						break;
							
					}
					
					if(s_ErrCount > 50 )
					{
						s_ErrCount = 0;
						s_Status = 1;
						s_Step = 1;
					}
					s_LastTime = GetSystem10msCount();	
					
					Clear_GPRS_Buff();
				}
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 800) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				sprintf(str, "\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				u1_printf(str);		
	
				s_ATAckCount++;
				
				if(s_ATAckCount >= 5)
				{
					s_ATAckCount = 0;
					u1_printf(" AT无响应\r\n");
					SetLogErrCode(LOG_CODE_TIMEOUT);
					s_Status = 1;
					s_Step = 1;
				}
				else
				{
					s_Status--;
					u1_printf(" AT指令超时\r\n");
				}
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
			}	
		break;		
		
		case 11:
			u1_printf("[SIM7600CE]建立TCP链接\r\n");
			sprintf(str,"AT+CIPOPEN=0,\"TCP\",\"%d.%d.%d.%d\",%d\r"
			,p_sys->Gprs_ServerIP[0]
			,p_sys->Gprs_ServerIP[1]
			,p_sys->Gprs_ServerIP[2]
			,p_sys->Gprs_ServerIP[3]
			,p_sys->Gprs_Port	);

			SimUartSendStr(str);	
			u1_printf(str);	//串口输出AT命令
			u1_printf("\n");
			s_LastTime = GetSystem10msCount();
			s_Status++;		
		//	s_ErrCount = 0;		
		break;
		
		case 12:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
			{
				if(GetGPRSUartRecFlag())
				{
					u1_printf("%s\r\n", GPRS_UART_BUF);	
													
					if((p1 = (strstr((const char *)GPRS_UART_BUF,"CONNECT FAIL"))) != NULL )
					{					
						s_ErrCount++;
						s_Status--;
						u1_printf("GPRS链接失败\r\n");	
					}							
					else if((p1 = (strstr((const char *)GPRS_UART_BUF,"CONNECT 115200"))) != NULL )
					{	
						s_Step = 1;	
						s_Status++;		
						s_ErrCount = 0;
						SetTCModuleReadyFlag(TRUE);		//连接成功========================
						u1_printf("\r\n  GPRS链接已建立\r\n");							
					}								
					else if((p1 = (strstr((const char *)GPRS_UART_BUF,"+CIPOPEN:"))) != NULL )
					{
						nTemp = atoi( (const char *) (p1+12));
						if(nTemp == 0)
						{						
							s_Step = 1;	
							s_Status++;		
							s_ErrCount = 0;
							SetTCModuleReadyFlag(TRUE);			//连接成功========================
							u1_printf("\r\n  GPRS链接已建立\r\n");				
						}
						else
						{
							s_ErrCount++;
							s_Status--;
							u1_printf("GPRS链接失败\r\n");
						}						
					}
					else
					{
						s_Status--;	
						s_ErrCount++;
						u1_printf("\r\n  未识别指令\r\n");
					}
						
					
					if(s_ErrCount > 5 )
					{
						s_ErrCount = 0;
						s_Status = 1;
						s_Step = 1;
					}
					s_LastTime = GetSystem10msCount();	
					
					Clear_GPRS_Buff();

				}
				else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1000) 
				{
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
					u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
		
					s_ATAckCount++;
					
					if(s_ATAckCount >= 5)
					{
						s_ATAckCount = 0;
						u1_printf(" AT无响应\r\n");
						SetLogErrCode(LOG_CODE_TIMEOUT);
						s_Status = 1;
						s_Step = 1;
						s_LastTime = GetSystem10msCount();
						s_ErrCount++;
					}
					else
					{
						s_Status--;
						u1_printf(" AT指令超时， 重连\r\n");
					}
				}	
			}				
		break;
		
		case 13:
			s_OverCount = 0;
			s_Status = 50;
			s_LastTime = GetSystem10msCount();
			SetTCProtocolRunFlag(TRUE);
			s_EndTime = GetSystem10msCount();
			SetLinkNetTime(CalculateTime(s_EndTime, s_StartTime) );
			u1_printf("\r\n 连接耗时:%.2fs\r\n\r\n", CalculateTime(s_EndTime, s_StartTime)/100.0);
		break;
		
		case 50:
			if(GetTCModuleReadyFlag())		//SIM就绪
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), s_AliveRTCCount) >= p_sys->Heart_interval)	
				{
					g_TCRecStatus.AliveAckFlag = FALSE;
					s_AliveRTCCount = GetRTCSecond();
					Mod_Send_Alive();	//发送心跳包
					delay_ms(20);
					
					Mod_Report_Data();	//上报网关电压数据
					s_LastTime = GetSystem10msCount();
					s_Status++;
				}				
			}
			else
			{
				s_Status = 1;
				s_Step = 1;
				s_ErrCount = 0;
				s_LastTime = GetSystem10msCount();
			}
		break;
		
		case 51:
			if(g_TCRecStatus.AliveAckFlag)//非长连接设备取消心跳包  2019.2.12
			{
				s_Status++;
				s_LastTime = GetSystem10msCount();
				g_TCRecStatus.AliveAckFlag = FALSE;
				s_OverCount = 0;
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 2500 )
			{
				s_Status--;
				s_LastTime = GetSystem10msCount();
				u1_printf("\r\n  Timeout did not receive a reply packet reissued\r\n");
				Clear_GPRS_Buff();
				s_OverCount++;
				if(s_OverCount >= 2)
				{
					u1_printf("[SIM]->Exit the passthrough\r\n");
					SetLogErrCode(LOG_CODE_NOSERVERACK);
					StoreOperationalData();
					s_OverCount = 0;
					s_Status = 9;
					s_Step = 1;
					s_ErrCount = 0;
					s_LastTime = GetSystem10msCount();
					SetOnline(FALSE);
					delay_ms(100);
					SimUartSendStr("+++");
					delay_ms(1100);
					SimUartSendStr("+++");				
					delay_ms(50);				
					SetTCProtocolRunFlag(FALSE);	
				}
			}		
		break;
					
		case 52:
			if( CalculateTime(GetSystem10msCount(),s_LastTime2) >= 200 )	//2S判断一次是否进入省电模式
			{
				if(GetPowerSavingFlag())
				{
					s_SavingModeFlag = GetPowerSavingFlag();
					if(JudgmentTimePoint(NETTYPE_4G))	//达到唤醒通信模块时间点
					{						
						if(s_SimSavingFlag)
						{
							s_Status = 1;
							s_Step = 1;
							s_ErrCount = 0;
							s_SimSavingFlag = FALSE;
						}
					}
					else	//到达省电模式时间点
					{
						s_SimSavingFlag = TRUE;
						SetTCModuleReadyFlag(FALSE);
						SetOnline(FALSE);
						SIM7600CE_PWR_OFF();
						s_LastTime2 = GetSystem10msCount();
					}
				}
				else
				{
					if(s_SavingModeFlag)	//退出省电模式，重启模块
					{
						s_SavingModeFlag = GetPowerSavingFlag();
						s_Status = 1;
						s_Step = 1;
						s_ErrCount = 0;
					}
					s_Status = 53;
				}
			}
				
			RetransmissionGPRSData();	//是否有GPRS下行包进行重发
			
		break;
			
		case 53:			//退出透传，关闭TCP链接，PDP去激活	
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= p_sys->Heart_interval*100 )
			{				
				s_LastTime = GetSystem10msCount();
				s_Status = 50;
				u1_printf("\r\n Signal strength:%d\r\n", s_SignalStrength);
				
//				RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
//				if(s_LastRTCDate != RTC_DateStruct.RTC_Date)
//				{
//					s_LastRTCDate = RTC_DateStruct.RTC_Date;
//					SetGPSRunFlag(TRUE);
//				}
				
			}
			else
			{
				RetransmissionGPRSData();	//是否有GPRS下行包进行重发
				s_Status = 52;
				s_LastTime2 = GetSystem10msCount();
			}
		break;			

			

			
	
	}
	
}
