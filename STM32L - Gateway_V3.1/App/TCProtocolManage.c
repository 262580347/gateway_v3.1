#include	"main.h"
#include 	"TCProtocolManage.h"

#define		MAX_DATA_BUF  800

static unsigned char s_RecDataBuff[800]; 
static unsigned char s_UpdataTimeFlag = FALSE;

static unsigned char s_TCModuleReadyFlag = FALSE;

static unsigned char s_LockinTimeFlag = FALSE;

static u8 s_TCProtocolRunFlag = FALSE;

static u16 s_ControlRecTime = 0;//最新的控制指令接收时间点

static void OnRecTCProtocol(USART_TypeDef* USARTx, TC_COMM_MSG *pMsg);
	
static unsigned char s_GetTCPCloseInfo = FALSE;

void SetTCPCloseInfoFlag(unsigned char isTrue)
{
	s_GetTCPCloseInfo = isTrue;
}

unsigned char GetTCPCloseInfoFlag(void)
{
	return s_GetTCPCloseInfo;
}

unsigned short GetControlRecTime(void)	
{
	return s_ControlRecTime;
}

COM_RECSTATUS	g_TCRecStatus;

void SetTCModuleReadyFlag(unsigned char isTrue)
{
	s_TCModuleReadyFlag = isTrue;
}

unsigned char GetTCModuleReadyFlag(void)
{
	return s_TCModuleReadyFlag;
}

void SetTCProtocolRunFlag(unsigned char isTrue)
{
	s_TCProtocolRunFlag = isTrue;
	Clear_GPRS_Buff();
}


unsigned char GetUpdataTimeFlag(void)
{
	return s_UpdataTimeFlag;
}

void SetUpdataTimeFlag(unsigned char isTrue)
{
	s_UpdataTimeFlag = isTrue;
}

void ClearLockinTimeFlag(void)
{
	s_LockinTimeFlag = FALSE;	
} 

unsigned char GetLockinTimeFlag(void)
{
	return s_LockinTimeFlag;
}

static void DecodeAppMsg(uint8 *pBuf,uint16 uLen,TC_COMM_MSG *pMsg)
{
    u32 uPoint = 0;
	
    pMsg->Protocol = pBuf[uPoint++];
    pMsg->Protocol = (pMsg->Protocol<<8)|pBuf[uPoint++];
	
	pMsg->DeviceID = pBuf[uPoint++];
    pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	
	pMsg->Dir   = pBuf[uPoint++];
	
	pMsg->Seq = pBuf[uPoint++];
    pMsg->Seq = (pMsg->Seq<<8)|pBuf[uPoint++];
	
    pMsg->Length = pBuf[uPoint++];
    pMsg->Length = (pMsg->Length<<8)|pBuf[uPoint++];
	
    pMsg->OPType   = pBuf[uPoint++];
	
    if(pMsg->Length > TC_DATA_SIZE)
    {
	    pMsg->Length = TC_DATA_SIZE;
    }
	memcpy((void *)pMsg->UserBuf,(void *)&pBuf[uPoint],pMsg->Length);
}

static BOOL TCProtocol(unsigned char *Data, unsigned short Length, TC_COMM_MSG *s_RxFrame)
{
	unsigned char PackBuff[MAX_DATA_BUF];
	uint16   PackLengthgth = 0, i;
	
	memset(PackBuff, sizeof(PackBuff), 0);
	
	UnPackMsg(Data+1, Length-2, PackBuff, &PackLengthgth);	//解包

	if (PackBuff[(PackLengthgth)-1] == Calc_Checksum(PackBuff, (PackLengthgth)-1))
	{		
		DecodeAppMsg(PackBuff, PackLengthgth-1, s_RxFrame);
		return TRUE;
	}
	else
	{
		u1_printf("COM2:");
		for(i=0; i<PackLengthgth; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");
		u1_printf(" CRC Error :%02X\r\n", Calc_Checksum(PackBuff, (PackLengthgth)-1));
		return FALSE;
	}	
	
}
//协议栈0 应用报文接收观察者处理任务
void TaskForTCRxObser(void)
{
	static TC_COMM_MSG s_RxFrame;
	unsigned char PackBuff[MAX_DATA_BUF];
	u16 RecPackNum, i, PackStartCount = 0, PackEndCount = 0, RecLength = 0;
	static u8 s_PackCount = 0, s_PackStartAddr[10], s_PackEndAddr[10];
	char *p1 = NULL;
	
	if(GetGPRSUartRecFlag())
	{	
		if((GPRS_UART_BUF[0] == BOF_VAL) && (GPRS_UART_BUF[GPRS_UART_CNT - 1] == EOF_VAL) && (GPRS_UART_CNT >= 15))	//透传处理，非透传可无视
		{
			//连包处理
			memset(PackBuff, 0, sizeof(PackBuff));
			RecPackNum = GPRS_UART_CNT;
			if(GPRS_UART_CNT < MAX_DATA_BUF)
			{
				memcpy(PackBuff, GPRS_UART_BUF, RecPackNum);
				Clear_GPRS_Buff();	
			}
			else
			{
				u1_printf(" Data Count Over\r\n");
				Clear_GPRS_Buff();	
				return;
			}
			
			PackStartCount = 0;
			PackEndCount = 0;
			
			for(i=0; i<RecPackNum; i++)
			{
				if(PackBuff[i] == 0x7E)	//寻找包头个数
				{
					s_PackStartAddr[PackStartCount] = i;
					PackStartCount++;
				}
				else if(PackBuff[i] == 0x21)	//寻找包尾个数
				{
					s_PackEndAddr[PackEndCount] = i;
					PackEndCount++;
				}
			}
			
			if(PackStartCount == PackEndCount)	//是完整的数据包
			{
				if(PackStartCount > 1)
				{
					u1_printf("\r\n %d Packs\r\n", PackStartCount);
				}
				s_PackCount = PackStartCount;
			}
			else
			{
				u1_printf(" PackStartCount != PackEndCount\r\n");
				Clear_GPRS_Buff();	
				return;
			}
			
			if(s_PackCount >= 1 && s_PackCount <= 10)	//最多处理10个数据连包
			{
				for(i=0; i<s_PackCount; i++)
				{
					memset(s_RecDataBuff, 0 ,sizeof(s_RecDataBuff));
					
//					memcpy(PackBuff+s_PackStartAddr[i]+1, s_PackEndAddr[i] - s_PackStartAddr[i] - 1, s_RecDataBuff, &PackLength);
					
					RecLength = s_PackEndAddr[i] - s_PackStartAddr[i] + 1;
					memcpy(s_RecDataBuff, PackBuff+s_PackStartAddr[i], RecLength);
					
					if(TCProtocol(s_RecDataBuff, RecLength, &s_RxFrame))
					{
						OnRecTCProtocol(USART2, &s_RxFrame);
					}
				}
			}

		}
		else if(((p1 = strstr((const char *)GPRS_UART_BUF,"I:")) != NULL) && (GPRS_UART_CNT >= 20))		//+CSONMI: 接收出现少几个头部字符串
		{
//			u1_printf("%s\r\n", GPRS_UART_BUF);
//			RecPackNum = atoi( (const char *) (p1+11));
			RecPackNum = atoi( (const char *) (p1+5));
			RecPackNum = RecPackNum/2;
			memset(PackBuff, 0, sizeof(PackBuff));
//			u1_printf("Rec:%d\r\n", RecPackNum);
			p1 = strstr((const char *)GPRS_UART_BUF,",7E");
			p1++;
			
			if(RecPackNum < MAX_DATA_BUF/2)
			{
				for(i=0; i<RecPackNum; i++)
				{
					StrToHex(&PackBuff[i], (u8 *)(p1+i*2), RecPackNum);	//将接收到的字符串数据转换为hex
//					u1_printf("%02X ", PackBuff[i]);
				}	
//				u1_printf("\r\n");
				Clear_GPRS_Buff();	
			}
			else
			{
				u1_printf(" NB Data Count Over\r\n");
				Clear_GPRS_Buff();	
				return;
			}
			
			
			PackStartCount = 0;
			PackEndCount = 0;
			
			for(i=0; i<RecPackNum; i++)
			{
				if(PackBuff[i] == 0x7E)	//寻找包头个数
				{
					s_PackStartAddr[PackStartCount] = i;
					PackStartCount++;
				}
				else if(PackBuff[i] == 0x21)	//寻找包尾个数
				{
					s_PackEndAddr[PackEndCount] = i;
					PackEndCount++;
				}
			}
			
			if(PackStartCount == PackEndCount)	//是完整的数据包
			{
				s_PackCount = PackStartCount;
			}
			else
			{
				u1_printf(" PackStartCount != PackEndCount\r\n");
				Clear_GPRS_Buff();	
				return;
			}
			
			if(s_PackCount >= 1 && s_PackCount <= 10)	//最多处理10个数据连包
			{
				for(i=0; i<s_PackCount; i++)
				{
					memset(s_RecDataBuff, 0 ,sizeof(s_RecDataBuff));
					
//					memcpy(PackBuff+s_PackStartAddr[i]+1, s_PackEndAddr[i] - s_PackStartAddr[i] - 1, s_RecDataBuff, &PackLength);
					
					RecLength = s_PackEndAddr[i] - s_PackStartAddr[i] + 1;
					memcpy(s_RecDataBuff, PackBuff+s_PackStartAddr[i], RecLength);
					
					if(TCProtocol(s_RecDataBuff, RecLength, &s_RxFrame))
					{
						OnRecTCProtocol(USART2, &s_RxFrame);
					}
				}				
			}
			else
			{
				u1_printf(" Packs too much\r\n");
				Clear_GPRS_Buff();	
				return;
			}
			
			Clear_GPRS_Buff();	
		}
		else if((p1 = strstr((const char *)GPRS_UART_BUF,"OK")) != NULL)
		{
//			u1_printf(" Send OK\r\n");
			Clear_GPRS_Buff();	
		}
		else if((p1 = strstr((const char *)GPRS_UART_BUF,"CLOSE")) != NULL)
		{
			s_GetTCPCloseInfo = TRUE;
			u1_printf(" TCP 已关闭\r\n");
			Clear_GPRS_Buff();	
		}
		else if((p1 = strstr((const char *)GPRS_UART_BUF,"ERR")) != NULL)
		{
			s_GetTCPCloseInfo = TRUE;
			u1_printf(" TCP ERROR\r\n");
			Clear_GPRS_Buff();	
		}
		else 
		{
			u1_printf("TCErr(%d):", GPRS_UART_CNT);
			for(i=0; i<GPRS_UART_CNT; i++)
			{
				u1_printf("%02X ", GPRS_UART_BUF[i]);
			}
			u1_printf("\r\n");
			
			u1_printf("%s\r\n", GPRS_UART_BUF);
			
			Clear_GPRS_Buff();	
		}
	}
}


static void OnRecTCProtocol(USART_TypeDef* USARTx, TC_COMM_MSG *pMsg)
{
	u8 i, ReturnVal = 0, s_First = FALSE, SetTimeFlag = FALSE, DeviceType = 0, NowSecond = 0;
	u16 nMain100ms;
	static u16 s_LastTime = 0, s_LastControlTime = 0, s_Count = 0;
	u32 RTCTime = 0;
	RTC_TimeTypeDef RTC_TimeStructure;	
	SYSTEMCONFIG *p_sys;

	p_sys = GetSystemConfig();		
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
	//解析协议包
	u1_printf("\r\n [%02d:%02d:%02d][TC]<-Version:%02d Device:(%d)%04d Dir:%X Seq:%d Len:%d CMD:%02X Data:", \
	RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, pMsg->Protocol, ((pMsg->DeviceID)&0x10000000) >> 28, (pMsg->DeviceID)&0xfffffff, pMsg->Dir, pMsg->Seq, pMsg->Length, pMsg->OPType);
	
	if(pMsg->Length > MAX_DATA_BUF)
	{
		u1_printf("Length Over :%d\r\n", pMsg->Length);
		return;
	}
	else
	{
		for(i=0; i<pMsg->Length; i++)
			u1_printf("%02X ", pMsg->UserBuf[i]);
		u1_printf("\r\n");
	}
	
	
	g_TCRecStatus.LastLinkTime = GetRTCSecond();
	
	if(pMsg->OPType == CMD_CLOSE_SOCKET)	//关闭Socket指令
	{	
		return;	
	}
	
	if(pMsg->OPType == CMD_HEATBEAT && pMsg->DeviceID == p_sys->Device_ID)
	{
		g_TCRecStatus.AliveAckFlag = TRUE;
		
		if(s_First == FALSE)
		{
			s_First = TRUE;
			RTC_TimeStructure.RTC_H12     = RTC_H12_AM;				
			RTC_TimeStructure.RTC_Hours   = pMsg->UserBuf[7];
			RTC_TimeStructure.RTC_Minutes = pMsg->UserBuf[8];
			RTC_TimeStructure.RTC_Seconds = RTC_TimeStructure.RTC_Seconds;
			u1_printf(" Server Time:[%02d:%02d]\r\n", pMsg->UserBuf[7], pMsg->UserBuf[8]);
			RTCTime = pMsg->UserBuf[7]*3600 + pMsg->UserBuf[8]*60;

			
			if(RTCTime > GetRTCSecond())		//与服务器误差超过120S则同步服务器时间
			{
				if(DifferenceOfRTCTime(RTCTime, GetRTCSecond()) > 120)					
				{
					SetTimeFlag = TRUE;
				}
				else 
				{
					SetTimeFlag = FALSE;
				}
			}
			else
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), RTCTime) > 120)
				{
					
					SetTimeFlag = TRUE;
				}
				else 
				{
					SetTimeFlag = FALSE;
				}
			}
			
			if(SetTimeFlag)
			{
				if(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure) != ERROR)
				{
					u1_printf(" The first time to connect GPRS- synchronous remote time successfully\r\n");
					RTC_TimeShow();
				}
			}
			g_FirstConnectTime = GetRTCSecond();
		}
		
		u1_printf(" The heartbeat packet replies to itself (%d)\r\n", pMsg->DeviceID);
				
		SetOnline(TRUE);
		
		SetTCModuleReadyFlag(TRUE);
		
		g_TCRecStatus.LastLinkTime = GetRTCSecond();		//防止更新时间后重启
	}

	if(pMsg->OPType == CMD_HEATBEAT && pMsg->DeviceID != p_sys->Device_ID)	//非自身的心跳包回复
	{
		u1_printf(" The heartbeat packet replies to (%d)\r\n", (pMsg->DeviceID&0xfffffff));
		
		if( CalculateTime(GetSystem100msCount(),s_LastTime) >= 600 )
		{
			s_LastTime = GetSystem100msCount();
			RTC_TimeStructure.RTC_H12     = RTC_H12_AM;				
			RTC_TimeStructure.RTC_Hours   = pMsg->UserBuf[2];
			RTC_TimeStructure.RTC_Minutes = pMsg->UserBuf[1];
			RTC_TimeStructure.RTC_Seconds = pMsg->UserBuf[0];;
			u1_printf(" Server Time:[%02d:%02d:%02d]\r\n", pMsg->UserBuf[2], pMsg->UserBuf[1], pMsg->UserBuf[0]);
			RTCTime = pMsg->UserBuf[2]*3600 + pMsg->UserBuf[1]*60 + pMsg->UserBuf[0];
			
			if(RTCTime > GetRTCSecond())		//与服务器误差超过20S则同步服务器时间
			{
				if(DifferenceOfRTCTime(RTCTime, GetRTCSecond()) > 20)					
				{
					SetTimeFlag = TRUE;
				}
				else 
				{
					SetTimeFlag = FALSE;
				}
			}
			else
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), RTCTime) > 20)
				{					
					SetTimeFlag = TRUE;
				}
				else 
				{
					SetTimeFlag = FALSE;
				}
			}
			
			if(SetTimeFlag)
			{
				if(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure)  != ERROR)
				{
					u1_printf(" Synchronize remote time successfully\r\n");
					RTC_TimeShow();
				}
				else
				{
					u1_printf(" Failed to synchronize remote time\r\n");
				}
			}
			else
			{
				u1_printf(" Unsynchronized server time\r\n");
			}
		}
	}
	
	if(pMsg->OPType == CMD_REPORT_D)	//数据包应答
	{
		u1_printf(" Packet reply received (%d)\r\n", ((int)pMsg->DeviceID&0xfffffff));
			
		SetOnline(TRUE);
	}

	if(pMsg->OPType == CMD_REPORT_S)	//状态包应答
	{
		u1_printf(" Control status response received (%d)\r\n", ((int)pMsg->DeviceID&0xfffffff));	
		
		SetOnline(TRUE);
	}
	
	if(pMsg->OPType == CMD_CONTROL)	//控制指令
	{
		DeviceType = GetTypeFromLoraLinkedList((pMsg->DeviceID&0xfffffff));		//判断节点设备的设备类型
		if(DeviceType == CONTROL)
		{			
			if( CalculateTime(GetSystem100msCount(),s_LastControlTime) >= 15 )//1.5S内收到的消息包视为连包
			{			
				s_Count = 0;
				LORA_ROUSE_MODE();
				LORA_WORK_DELAY();
				Lora_Send_GPRS_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);//切换至唤醒模式向控制器发送控制指令 
				s_ControlRecTime = GetSystem100msCount() + 15;
				Lora_Save_GPRS_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length, CONTROL, s_ControlRecTime);
				LORA_SLEEP_DELAY();
				LORA_WORK_MODE();
				s_LastControlTime = GetSystem100msCount();
			}
			else	//分割1.5S内的消息包，每个包间隔1.5s后发送
			{
				s_Count++;
				u1_printf(" SIM with packet,%ds after sending...\r\n", s_Count);
				nMain100ms = s_Count*15 + GetSystem100msCount()+ 15;
				Lora_Save_GPRS_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length, CONTROL, nMain100ms);	//存入缓冲区
			}
			
		}
		else if(DeviceType == V263_CONTROL)
		{
			RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
			NowSecond = (RTC_TimeStructure.RTC_Seconds)%10;
			
			if(NowSecond >= 2)
			{
				if( CalculateTime(GetSystem100msCount(),s_LastControlTime) >= 10 )	//1.5S内收到的消息包视为连包
				{			
					s_Count = 0;
					Lora_Send_GPRS_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);	//切换至唤醒模式向控制器发送控制指令
					s_LastControlTime = GetSystem100msCount();
				}
				else		//分割1.5S内的消息包，每个包间隔1.5s后发送
				{
					s_Count++;
					u1_printf(" SIM with packet,%ds after sending...\r\n", s_Count);
					nMain100ms = s_Count*10 + GetSystem100msCount();
					Lora_Save_GPRS_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length, V263_CONTROL, nMain100ms);	//存入缓冲区
				}	
			}
			else
			{
				s_Count++;
				u1_printf(" Collector communicates time zone,%ds after sending...\r\n", s_Count+1);
				nMain100ms = (s_Count+1)*10 + GetSystem100msCount();
				Lora_Save_GPRS_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length, V263_CONTROL, nMain100ms);	//存入缓冲区
			}
		}
		else if(DeviceType == ERRORTYPE)
		{
			u1_printf(" The device cannot be found\r\n");
			return;
		}
		else
		{
			Lora_Save_GPRS_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length, SENSOR, GetSystem100msCount());
		}
		
		u1_printf(" Cloud control(channel %d)-> %d  ", pMsg->UserBuf[1] - 32, (pMsg->DeviceID&0xfffffff));	
		
		switch(pMsg->UserBuf[3])
		{
			case C_LEVEL_ID:
				u1_printf(" Level switch--");
			break;
			
			case C_RT_P_ID:
				u1_printf(" Forward and reverse rotation of pulse--");
			break;
			
			case C_RT_L_ID:
				u1_printf(" Positive and negative rotation of level--");
			break;
			
			case C_PULSE_ID:
				u1_printf(" Pulse switch--");
			break;
		}
		
		if(pMsg->UserBuf[8] == 1)
		{
			u1_printf("Open");
			u1_printf(" %d min\r\n", pMsg->UserBuf[6]+(pMsg->UserBuf[5]<<8));
			if(pMsg->UserBuf[6]+(pMsg->UserBuf[5]<<8) >= 1440)
			{
				u1_printf("----Warning, the controller will not close after the duration of setting is greater than 1440min----\r\n");
			}
		}
		else if(pMsg->UserBuf[8] == 0)
		{
			u1_printf("Stop\r\n");
		}
		else if(pMsg->UserBuf[8] == 2)
		{
			u1_printf("Close\r\n");
		}
		
		Mod_Send_Ack((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);
		
		SetOnline(TRUE);
	}
	
	if(pMsg->OPType == CMD_REBOOT)
	{
		u1_printf(" A restart command for %d is received\r\n", (pMsg->DeviceID&0xfffffff));		
		Lora_Send_GPRS_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);	
	}
	
	if(pMsg->OPType == CMD_GET_CONFIG && pMsg->DeviceID == p_sys->Device_ID)//获取设备配置
	{
		ProcQuerySysPrm((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);
	}
	else if(pMsg->OPType == CMD_GET_CONFIG)
	{
		DeviceType = GetTypeFromLoraLinkedList((pMsg->DeviceID&0xfffffff));
		if(DeviceType == CONTROL)
		{			
			LORA_ROUSE_MODE();
			LORA_WORK_DELAY();
			Lora_Send_GPRS_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);	//控制器直接转发
			LORA_SLEEP_DELAY();
			LORA_WORK_MODE();
		}
		else if(DeviceType == V263_CONTROL)
		{
			Lora_Send_GPRS_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);
		}
		else
		{
			Lora_Save_GPRS_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length, SENSOR, GetSystem100msCount());
		}

		u1_printf(" Query (%d) for device information\r\n", (pMsg->DeviceID&0xfffffff));
		
		Mod_Send_Ack((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);
	}
	
	if(pMsg->OPType == CMD_SET_CONFIG && pMsg->DeviceID == p_sys->Device_ID) //设置设备配置
	{		
		Mod_Send_Ack((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);
		ReturnVal = ProcSetSysPrm((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);
		if(ReturnVal == 0)
		{
			return;
		}
		u1_printf("\r\n Update the device parameters and wait for the device to restart\r\n");
		delay_ms(1500);
		delay_ms(1500);
		delay_ms(1500);
		delay_ms(1500);
		delay_ms(1500);
		SetLogErrCode(LOG_CODE_RESET);
		StoreOperationalData();
		while (DMA_GetCurrDataCounter(DMA1_Channel4));
		Sys_Soft_Reset();
	}
	else if(pMsg->OPType == CMD_SET_CONFIG)
	{
		DeviceType = GetTypeFromLoraLinkedList((pMsg->DeviceID&0xfffffff));
		if(DeviceType == CONTROL)
		{			
			LORA_ROUSE_MODE();
			LORA_WORK_DELAY();
			Lora_Send_GPRS_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);//控制器直接转发
			LORA_SLEEP_DELAY();
			LORA_WORK_MODE();
		}
		else if(DeviceType == V263_CONTROL)
		{
			Lora_Send_GPRS_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);
		}
		else
		{
			Lora_Save_GPRS_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length, SENSOR, GetSystem100msCount());
		}

		u1_printf(" Update the device Config for (%d)\r\n", (pMsg->DeviceID&0xfffffff));
		
		Mod_Send_Ack((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);
	}
}
	
void TCProtocolProcess(void)
{
	if(s_TCProtocolRunFlag)
	{
		TaskForTCRxObser();
	}
}























