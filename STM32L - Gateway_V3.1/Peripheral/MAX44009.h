#ifndef	_MAX44009_H_
#define	_MAX44009_H_

#define		DEFAULT_MAX_RATIO		3.85
float Get_Ill_Value(void);
unsigned char MAX44009_Init(void);
void MAX_Process(unsigned short nMain10ms);
extern unsigned char g_MAXGetFlag;

extern unsigned char g_MAXInitFlag;
#endif

