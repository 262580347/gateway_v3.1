#include "main.h"
#include "GPRS.h"

#define COM_DATA_SIZE	256

GPRSDATALINKLIST * pGprsLinkedList;	//链表头

unsigned char g_GPRSDataCount = 0;	

//初始化GPRS缓冲区数据链表
void InitGprsLinkedList( void )
{
	u8 i;
	pGprsLinkedList = (GPRSDATALINKLIST*)malloc(sizeof( GPRSDATALINKLIST));
	if( pGprsLinkedList == NULL )
	{
	//	u1_printf("\r\n [GPRS]:初始化已连接设备链表失败\n");
	}
	else
	{

		u1_printf("\r\n [GPRS]:Initializing GPRS buffer list successfully\r\n");

		pGprsLinkedList->m_lDeviceId = 65530;
		pGprsLinkedList->m_lenth = 0;
		for(i=0; i<64; i++)
			pGprsLinkedList->m_data[i] = 0;
		pGprsLinkedList->m_LinkTimeNode = 0;
		pGprsLinkedList->m_pNext = NULL;
		pGprsLinkedList->m_DeviceType = SENSOR;
		
		g_GPRSDataCount = 0;
	}

}

void PrintGprsLinkedList( void )	//输出缓冲区数据
{
	UINT8 i=0;
	GPRSDATALINKLIST * p1;
	
	p1 = pGprsLinkedList->m_pNext;
	u1_printf("  [GPRS] Pending data:\r\n");
	for(i=0;i<g_GPRSDataCount && i<GPRS_BUFF_DATA_COUNT;i++)
	{
		u1_printf("Num:%02d  DeviceID:%05d  lenth:%d\r\n", i+1, p1->m_lDeviceId, p1->m_lenth);

		p1 = p1->m_pNext;
		if( p1 == NULL )
		{
			break;
		}
	}
}
//向GPRS数据缓冲区添加一条缓存数据
void AddToGprsLinkedList( unsigned int lDeviceId, unsigned char lenth, unsigned char *data, unsigned short nMain100ms, eDEVICETYPE DeviceType)
{
	UINT8 i=0;
	static u8 GprsCmpFlag = FALSE;
	GPRSDATALINKLIST * p1;

	p1 = pGprsLinkedList->m_pNext;

	//查找该设备号的连接
	for(i=0;i<g_GPRSDataCount && i<GPRS_BUFF_DATA_COUNT;i++)
	{
		if( p1->m_lDeviceId == lDeviceId && DeviceType == SENSOR)	//更新采集器列表信息
		{		
			GprsCmpFlag = FALSE;
			for(i=0; i<p1->m_lenth; i++)
			{
				if(p1->m_data[i] != data[i])
				{
					GprsCmpFlag = TRUE;
				}
			}
			if(GprsCmpFlag)	//不同的数据，则直接覆盖
			{
				u1_printf(" 更新%d的缓冲区数据\r\n", lDeviceId);
				GprsCmpFlag = FALSE;	
				p1->m_lDeviceId = lDeviceId;
				p1->m_lenth = lenth;
				p1->m_LinkTimeNode = nMain100ms;
				p1->m_DeviceType = DeviceType;
				for(i=0; i<lenth; i++)
					p1->m_data[i] = data[i];
			}
			else
			{
				u1_printf("相同的GPRS数据(%d)\r\n", p1->m_lDeviceId);						
			}
			PrintGprsLinkedList();
			return;
		}

		p1 = p1->m_pNext;
		if( p1 == NULL )
		{
			break;
		}
	}

	//申请内存空间，新增加一条缓存数据
	p1 = (GPRSDATALINKLIST *)malloc( sizeof(GPRSDATALINKLIST));
	if( p1 == NULL )
	{
		u1_printf("[GPRS]:AddToLinkedList插入记录失败\r\n");
		return;
	}

	p1->m_lDeviceId = lDeviceId;
	p1->m_lenth = lenth;
	for(i=0; i<lenth; i++)
		p1->m_data[i] = data[i];
	p1->m_LinkTimeNode = nMain100ms;
	p1->m_DeviceType = DeviceType;
	
	//插入链表表头之后
	p1->m_pNext = pGprsLinkedList->m_pNext;
	pGprsLinkedList->m_pNext = p1;

	g_GPRSDataCount++;
	if(g_GPRSDataCount >= GPRS_BUFF_DATA_COUNT + 1)
	{
		g_GPRSDataCount = GPRS_BUFF_DATA_COUNT;
	}
	u1_printf(" 待发送数据包数量:%d\r\n", g_GPRSDataCount);

}

//从设备列表中移除设备
void DelFromGprsLinkedList(unsigned int lDeviceId, unsigned short TimeNode)
{
	GPRSDATALINKLIST * p1 =NULL;
	GPRSDATALINKLIST * pPrev = NULL;
	UINT8 i=0;

	p1 = pGprsLinkedList->m_pNext;
	pPrev = pGprsLinkedList;

	for( i=0;i<g_GPRSDataCount && i<GPRS_BUFF_DATA_COUNT;i++)
	{
		if( p1->m_lDeviceId == lDeviceId )
		{
			if(p1->m_DeviceType == SENSOR)	//采集器
			{
				pPrev->m_pNext = p1->m_pNext;
				g_GPRSDataCount--;
				
				u1_printf(" Sensor Residual packet(%d)\r\n", g_GPRSDataCount);
				free( p1 );	//释放内存

				if(g_GPRSDataCount > 0)
					PrintGprsLinkedList();
				return;
			}
			else if(p1->m_DeviceType == CONTROL || p1->m_DeviceType == V263_CONTROL)	//控制器
			{
				if(p1->m_LinkTimeNode == TimeNode)	//删除控制器缓冲区数据根据接收的时间节点来判断
				{
					pPrev->m_pNext = p1->m_pNext;
					g_GPRSDataCount--;
					
					u1_printf(" Control Residual packet(%d)\r\n", g_GPRSDataCount);
					free( p1 );	//释放内存

					if(g_GPRSDataCount > 0)
						PrintGprsLinkedList();
					return;
				}			
			}
		}

		pPrev = p1;
		p1 = p1->m_pNext;
		if( p1 == NULL )
		{
			break;
		}
	}
	u1_printf("\n[LORA]:DelFromLoraLinkedList,There is no such equipment=%d\n",lDeviceId);
	
	PrintGprsLinkedList();
}

void RetransmissionGPRSData(void)	//重发发送失败的GPRS控制指令
{
	static u8 s_State = 1;
	SYSTEMCONFIG *p_sys;
	static GPRSDATALINKLIST * s_pNode;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	p_sys = GetSystemConfig();		
	
	if((g_GPRSDataCount > 0) && (g_GPRSDataCount < GPRS_BUFF_DATA_COUNT + 1))	//判断是否有数据或编号是否溢出
	{
		while(s_State)
		{
			switch(s_State)
			{
				case 0:
					s_State++;
				case 1:		
					if(g_GPRSDataCount == 0)
					{
						s_State = 0;
					}
					else
					{
						s_pNode = pGprsLinkedList->m_pNext;
						s_State ++;
					}
				break;
					
				case 2:											
					if(s_pNode == NULL)
					{
						s_State = 0;				
					}
					else
					{
						if(s_pNode->m_LinkTimeNode > 0 && s_pNode->m_DeviceType == CONTROL)//依次发送同时接收到的控制器GPRS指令
						{
							if((CalculateTime(GetSystem100msCount(), s_pNode->m_LinkTimeNode) >= 1) && (CalculateTime(GetSystem100msCount(), s_pNode->m_LinkTimeNode) <= 200) )	// 到达重发节点后进行重发，只重发一次
							{
								RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
								u1_printf("\r\n [%02d:%02d:%02d]Retransmit GPRS buffer data\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);		
								LORA_ROUSE_MODE();	//切换至唤醒模式
								LORA_WORK_DELAY();
								Lora_Send_Data(s_pNode->m_lDeviceId, p_sys->Channel, &s_pNode->m_data[0], s_pNode->m_lenth);	
								s_pNode->m_LinkTimeNode = 0;								
								DelFromGprsLinkedList(s_pNode->m_lDeviceId, s_pNode->m_LinkTimeNode);		//删除缓冲区数据						
								LORA_SLEEP_DELAY();
								LORA_WORK_MODE();
							}						
						}
						else if(s_pNode->m_LinkTimeNode > 0 && s_pNode->m_DeviceType == V263_CONTROL)
						{
							if((CalculateTime(GetSystem100msCount(), s_pNode->m_LinkTimeNode) >= 1) && (CalculateTime(GetSystem100msCount(), s_pNode->m_LinkTimeNode) <= 200) )	// 到达重发节点后进行重发，只重发一次
							{
								RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
								u1_printf("\r\n [%02d:%02d:%02d]Retransmit GPRS buffer2 data\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);		
								Lora_Send_Data(s_pNode->m_lDeviceId, p_sys->Channel, &s_pNode->m_data[0], s_pNode->m_lenth);	
								s_pNode->m_LinkTimeNode = 0;								
								DelFromGprsLinkedList(s_pNode->m_lDeviceId, s_pNode->m_LinkTimeNode);		//删除缓冲区数据							
							}	
						}
						s_State++;
					}
				break;
					
				case 3:
					s_pNode = s_pNode->m_pNext;
					s_State = 2;
				break;
			}
		}
		s_State = 1;
	}
	else if(g_GPRSDataCount >= GPRS_BUFF_DATA_COUNT)
	{
		InitGprsLinkedList();
		u1_printf("\r\n !!!!Clear GPRS Buff!!!!\r\n");
		return;
	}	
}

void GPRS_Process(unsigned short nMain10ms)
{
	static unsigned short s_LastTime;
	static unsigned char Run_Once = FALSE, s_State = 1, s_RetryCount = 0;
	static u8 s_OverCount, s_GPRSModeFlag = FALSE, s_SavingModeFlag = FALSE;
	SYSTEMCONFIG *p_sys;
	GPIO_InitTypeDef GPIO_InitStructure;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	p_sys = GetSystemConfig();
	
	if(Run_Once == FALSE)
	{
		Run_Once = TRUE;
		GPIO_InitStructure.GPIO_Pin = GPRS_PWR_PIN ;	
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
		GPIO_Init(GPRS_PWR_TYPE, &GPIO_InitStructure);

		GPIO_ResetBits(GPRS_PWR_TYPE,GPRS_PWR_PIN);
		
		u1_printf("\r\n Eport Power On delay 1s...\r\n");
		delay_ms(1000);
		GPIO_SetBits(GPRS_PWR_TYPE,GPRS_PWR_PIN);

		InitGprsLinkedList();
		
		GPRS_Uart_Init(SIM_BAND_RATE);
		SetTCProtocolRunFlag(TRUE);
		s_LastTime = GetSystem10msCount();
	}
	
	switch(s_State)
	{
		case 1: //等待连上网络
			
			if(GetTCModuleReadyFlag())
			{
				s_LastTime = GetSystem10msCount();
				s_State = 5;
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1200)
			{
				Mod_Send_Alive();	//心跳包
				delay_ms(20);
				Mod_Report_Data();	//电压数据
				s_LastTime = GetSystem10msCount();
				s_RetryCount++;
				if(s_RetryCount >= 30)
				{
					u1_printf("\r\n Timeout not connected to the network waiting for restart...\r\n");
					SetLogErrCode(LOG_CODE_NOSERVERACK);
					SetLogErrCode(LOG_CODE_RESET);
					StoreOperationalData();
					while (DMA_GetCurrDataCounter(DMA1_Channel4));
					Sys_Soft_Reset();
				}
			}			
		break;
		
		case 2:
			if(GetTCModuleReadyFlag())	//等待服务器应答
			{
				s_State++;
				s_LastTime = GetSystem10msCount();
			}
			else
			{
				s_State--;
			}
		break;
		
		case 3:		//发送心跳包
			g_TCRecStatus.AliveAckFlag = FALSE;
			RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
			u1_printf("\r\n [%02d:%02d:%02d]", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
			Mod_Send_Alive();
			delay_ms(20);
			Mod_Report_Data();
			s_State++;
			s_LastTime = GetSystem10msCount();
		break;

		case 4:		//收到心跳应答
			if(g_TCRecStatus.AliveAckFlag)
			{
				g_TCRecStatus.AliveAckFlag = FALSE;
				s_State++;
				s_LastTime = GetSystem10msCount();
				s_OverCount = 0;
			}
			else
			{
				if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 2000 )
				{
					s_State--;
					s_LastTime = GetSystem10msCount();
					u1_printf("\r\n No heartbeat reply packet was received for timeout\r\n");
					s_OverCount++;
					if(s_OverCount >= 5)
					{
						SetLogErrCode(LOG_CODE_NOSERVERACK);
						StoreOperationalData();
						s_OverCount = 0;
						s_State = 1;
						SetTCModuleReadyFlag(FALSE);
						SetOnline(FALSE);
						u1_printf("\r\n Switch to offline mode\r\n");
					}
				}
			}
		break;	
		
		case 5:			//判断是否执行省电模式
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 200 )
			{
				if(GetPowerSavingFlag())
				{
					s_SavingModeFlag = GetPowerSavingFlag();
					if(JudgmentTimePoint(NETTYPE_GPRS))
					{
						if(s_GPRSModeFlag != JudgmentTimePoint(NETTYPE_GPRS))	//到达数据上报节点，开启通信模块
						{
							s_GPRSModeFlag = JudgmentTimePoint(NETTYPE_GPRS);
							RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
							u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
							u1_printf("GPRS_PWR_ON()\r\n");
							GPRS_PWR_ON();
							s_State = 1;
						}
						
						s_LastTime = GetSystem10msCount();
					}
					else		//未到达数据上报节点，关闭通信模块
					{
						if(s_GPRSModeFlag != JudgmentTimePoint(NETTYPE_GPRS))
						{
							s_GPRSModeFlag = JudgmentTimePoint(NETTYPE_GPRS);
							RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
							u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
							u1_printf("GPRS_PWR_OFF()\r\n");
							SetTCModuleReadyFlag(FALSE);
							SetOnline(FALSE);						
						}
						GPRS_PWR_OFF();
						s_LastTime = GetSystem10msCount();
					}
				}
				else
				{					
					if(s_SavingModeFlag)
					{
						s_SavingModeFlag = GetPowerSavingFlag();
						GPRS_PWR_ON();
						SetTCModuleReadyFlag(TRUE);
						SetOnline(TRUE);
					}					
					s_State++;
				}
			}
		break;
			
		case 6:
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= p_sys->Heart_interval*100 )
			{
				s_State = 2;
			}

			RetransmissionGPRSData();	//是否有GPRS下行包进行重发
			
		break;
	}
}




