/**********************************
说明:LORA通信模块，使用500ms唤醒间隔，TXD，AUX推挽输出,RXD 上拉输入
	  FEC关闭，20dBm发射功率，19.2K空中速率
作者:关宇晟
版本:V2018.4.3
***********************************/

#include "Lora.h"
#include "main.h"

#define		ERROR_485VALUE		0xEEEEEEEE

#define		PACK_INIT_NUM	255

#define MAX_DATA_BUF			400

#define		LORA_BUFF_MAX		250
#define		POLLIINTERVAL		24000   //4min
#define REGISTTIME_20S  		2000		//20S == 2000 * 10MS
#define REGISTTIME_15S  		1500		//15S == 1500 * 10MS
#define REGISTTIME_10S  		1000		//10S == 1000 * 10MS
#define REGISTTIME_5S  			500			// 5S ==  500 * 10MS
#define REGISTTIME_3S  			300			// 3S ==  300 * 10MS
#define REGISTTIMESLOT  		30					//注册时隙间隙:300ms
#define POLLINGTIMESLOT 		200					//轮询时隙间隙:1s（300ms）
#define GPRSDOWNLINKTIMESLOT 	350			//GPRS下行时隙间隙:200ms

LORA_CONFIG g_LoraConfig;
LORA_CONFIG *p_Lora_Config;


//#define	LORA_SPEED_INIT 	0x3A	//2.4K
#define	LORA_SPEED_INIT 	0x3D		//19.2K
#define	LORA_OPTION			0xC0
	
#define MAX_LORA_LINKED_LIST	80

static unsigned char s_RecDataBuff[400]; 

LORALINKEDLIST * pLoraLinkedList;	//链表头

unsigned char	g_ucLoraLinkedListNum = 0;	 //LORA设备连接数量

static unsigned char s_LoraReadFlag = FALSE;

static unsigned char s_LoraTestModeFlag = FALSE;

static u8 s_TCProtocolForLoraRunFlag = FALSE;

void SetTCProtocolForLoraRunFlag(unsigned char isTrue)
{
	s_TCProtocolForLoraRunFlag = isTrue;
	Clear_Lora_Buff();
}

unsigned char GetLoraTestModeFlag(void)
{
	return s_LoraTestModeFlag;
}

LORALINKEDLIST * GetLinkedPtr( void )
{
	return (pLoraLinkedList);
}

unsigned char GetLoraReadyFlag( void )
{
	return s_LoraReadFlag;
}

unsigned char GetLoraNodeCount(void)
{
	return g_ucLoraLinkedListNum;
}

const LORA_CONFIG DefaultLoraConfig = 
{
	0x0f,
	0xff,
	LORA_SPEED_INIT,
	0x0F,
	LORA_OPTION,
};



void Lora_Send_Buff(unsigned char *buffer, unsigned short buffer_size)
{

#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V100)

	USART2_DMA_Send(buffer, buffer_size);	

#elif (HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)

	USART3_DMA_Send(buffer, buffer_size);	
	
#endif
	
}



void Lora_Send_Data(unsigned short int Tagetaddr, unsigned char channel, unsigned char *data, unsigned int len)
{
	u8 LoraPack[256];
	u8 Lora_lenth;
	
	LoraPack[0] = Tagetaddr>>8;
	LoraPack[1] = Tagetaddr;
	LoraPack[2] = channel;
	Lora_lenth = 3;
	memcpy(&LoraPack[Lora_lenth], data, len);
	
	Lora_lenth += len;
	
#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V100)

	USART2_DMA_Send(LoraPack, Lora_lenth);

#elif (HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)

	USART3_DMA_Send(LoraPack, Lora_lenth);
	
#endif
	
}

void Lora_Send_GPRS_Data(CLOUD_HDR *hdr, unsigned char *data, unsigned char lenth)
{
	unsigned char send_buf[120], RegisterPack[120], len = 0, nMsgLen, i;
	SYSTEMCONFIG *p_sys;
	u32 lTemp = 0;
	
	RTC_TimeTypeDef RTC_TimeStructure;
	p_sys = GetSystemConfig();	

	
	send_buf[len++] = hdr->protocol >> 8;
	send_buf[len++] = hdr->protocol & 0xff;
	
	lTemp = (hdr->device_id)&0xefffffff;
	send_buf[len++] = (lTemp >> 24);
	send_buf[len++] = lTemp >> 16;	
	send_buf[len++] = lTemp >> 8;
	send_buf[len++] = lTemp;	
	
	send_buf[len++]  = hdr->dir;
	
	send_buf[len++]  = hdr->seq_no >> 8;
	send_buf[len++]  = hdr->seq_no;
	
	send_buf[len++]  = hdr->payload_len >> 8;
	send_buf[len++]  = hdr->payload_len;

	send_buf[len++] = hdr->cmd;									//命令字
	
	if(hdr->payload_len != 0)
	{
		for(i=0; i<hdr->payload_len; i++)
		{
			send_buf[len++] = data[i];
		}	
	}
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, RegisterPack, 120);	//数据包转义处理
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);

	Lora_Send_Data(lTemp, p_sys->Channel, RegisterPack, nMsgLen);
	
	u1_printf("\r\n [%02d:%02d:%02d][GPRS->LORA] -> %d(%d): ",  RTC_TimeStructure.RTC_Hours,  RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, (hdr->device_id)&0xfffffff, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",RegisterPack[i]);
	}
	u1_printf("\r\n");		
}

void Lora_Save_GPRS_Data(CLOUD_HDR *hdr, unsigned char *data, unsigned char lenth, eDEVICETYPE DeviceType, unsigned short nMain100ms)
{
	unsigned char send_buf[100], RegisterPack[100], len = 0, nMsgLen, i;

	len = 0;
	
	send_buf[len++] = hdr->protocol >> 8;
	send_buf[len++] = hdr->protocol & 0xff;
	
	send_buf[len++] = (hdr->device_id >> 24)&0xef;
	send_buf[len++] = hdr->device_id >> 16;	
	send_buf[len++] = hdr->device_id >> 8;
	send_buf[len++] = hdr->device_id;	
	
	send_buf[len++]  = hdr->dir;
	
	send_buf[len++]  = hdr->seq_no >> 8;
	send_buf[len++]  = hdr->seq_no;
	
	send_buf[len++]  = hdr->payload_len >> 8;
	send_buf[len++]  = hdr->payload_len;

	send_buf[len++] = hdr->cmd;									//命令字
	
	if(hdr->payload_len != 0)
	{
		for(i=0; i<hdr->payload_len; i++)
		{
			send_buf[len++] = data[i];
		}	
	}
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, RegisterPack, 120);	//数据包转义处理
	
	if(nMsgLen >= 64)
	{
		u1_printf("\r\n GPRS over 64byte\r\n");
		return ;
	}
	
	AddToGprsLinkedList((hdr->device_id)&0xfffffff, nMsgLen, RegisterPack, nMain100ms, DeviceType);

}

static void Lora_Transmit_GPRS_Data(unsigned int l_DeviceID)
{
	unsigned char i;
	static u8 s_State = 1;
	SYSTEMCONFIG *p_sys;
	static GPRSDATALINKLIST * s_pNode;
	RTC_TimeTypeDef RTC_TimeStructure;	
	
	p_sys = GetSystemConfig();		
	
	if((g_GPRSDataCount > 0) && (g_GPRSDataCount < GPRS_BUFF_DATA_COUNT + 1))
	{
		while(s_State)
		{
			switch(s_State)
			{
				case 0:
					s_State++;
				case 1:		
					if(g_GPRSDataCount == 0)
					{
						s_State = 0;
					}
					else
					{
						s_pNode = pGprsLinkedList->m_pNext;
						s_State ++;
					}
				break;
					
				case 2:											
					if(s_pNode == NULL)
					{
						s_State = 0;				
					}
					else
					{
						if(l_DeviceID == s_pNode->m_lDeviceId)
						{
							if(CalculateTime(GetSystem100msCount(), s_pNode->m_LinkTimeNode) <= 20)
							{
//								u1_printf("接Rec节点应答，删除缓冲区数据   ");		
//								s_pNode->m_LinkTimeNode = 0;
//								DelFromGprsLinkedList(l_DeviceID);		
							}
							else
							{
								Lora_Send_Data(l_DeviceID, p_sys->Channel, &s_pNode->m_data[0], s_pNode->m_lenth);
								RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
								u1_printf("\r\n [%02d:%02d:%02d][Transmit->GPRS] -> %d(%d): ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, s_pNode->m_lDeviceId, s_pNode->m_lenth);
								for(i=0;i<s_pNode->m_lenth;i++)
								{
									u1_printf("%02X ",s_pNode->m_data[i]);
								}
								u1_printf("\r\n");	
															
								delay_ms(15);
								DelFromGprsLinkedList(l_DeviceID, 0);		
							}							
						}
						s_State++;
					}
				break;
					
				case 3:
					s_pNode = s_pNode->m_pNext;
					s_State = 2;
				break;
			}
		}
		s_State = 1;
	}
	else
	{
		return;
	}
}

void LoraPort_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB , ENABLE);	 
	
	LORA_SLEEP_MODE();
	
	GPIO_InitStructure.GPIO_Pin = LORA_M0_PIN;//LORA M0 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = LORA_M1_PIN ;//LORA  M1
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = LORA_PWR_PIN;   //LORA  PWR
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(LORA_PWR_TYPE, &GPIO_InitStructure);	

}


static unsigned char DetLoraModule(void)
{
	static unsigned char s_Status = 1;
	static unsigned short s_LastTime = 0;
	static u8 ErrCount = 0, ErrOver = 0;
	const u8 CMD_C1[3] = {0xc1, 0xc1, 0xc1};
	const u8 CMD_C5[3] = {0xc5, 0xc5, 0xc5};
	
	switch(s_Status)
	{
		case 1:
			SetTCProtocolForLoraRunFlag(FALSE);
			u1_printf(" Detect the presence of LORA module!\r\n");
			s_LoraTestModeFlag = TRUE;
			ErrCount = 0;
			s_LastTime = GetSystem10msCount();
			Lora_Uart_Init(9600);	
			LORA_SLEEP_MODE();
			s_Status++;
			ErrOver = 0;
		break;
		
		case 2:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 12) 
			{
				s_LastTime = GetSystem10msCount();
				Lora_Send_Buff((u8 *)CMD_C5, 3);
//				u1_printf(" Det Vol\r\n");
				s_Status++;	
			}
		break;
		
		case 3:
			while(1)
			{
				if(GetLoraUartRecFlag())
				{
//					u1_printf(" %1.3fV",(LORA_UART_BUF[1]*256 + LORA_UART_BUF[2])/1000.0);
//					Clear_Lora_Buff();
//					u1_printf("\r\n");		
					Clear_Lora_Buff();
					s_LastTime = GetSystem10msCount();
					s_Status++;
					break;
				}
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
				{
					s_LastTime = GetSystem10msCount();					
					u1_printf("\r\n [Lora]Get Batvol over time\r\n");
					s_LoraReadFlag = FALSE;
					
					ErrOver++;
					if(ErrOver >= 3)
					{
						s_Status++;
						ErrOver = 0;
						ErrCount++;
					}
					else
					{
						s_Status--;
					}
					
					break;
				}
			}	
		break;
		
		case 4:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1) 
			{
				s_LastTime = GetSystem10msCount();
				Lora_Send_Buff((u8 *)CMD_C1, 3);
//				u1_printf(" Det Config\r\n");
				s_Status++;	
			}
		break;
		
		case 5:
			while(1)
			{
				if(GetLoraUartRecFlag())
				{				
//					for(i=1; i<LORA_UART_CNT; i++)
//						u1_printf("%02X ", LORA_UART_BUF[i]);
//					u1_printf("\r\n");
					Clear_Lora_Buff();
					s_Status++;
					break;
				}
				
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 10) 
				{
					s_LastTime = GetSystem10msCount();					
					u1_printf("\r\n [Lora]Get Config over time\r\n");
					s_LoraReadFlag = FALSE;
					ErrOver++;
					if(ErrOver >= 3)
					{
						s_Status++;
						ErrOver = 0;
						ErrCount++;
					}
					else
					{
						s_Status--;
					}
					break;
				}
			}
		break;
		
		case 6:
			if(ErrCount)
			{
				u1_printf(" No Lora!!!Reset\r\n");
				s_LoraTestModeFlag = FALSE;
				SetLogErrCode(LOG_CODE_NOLORA);
				StoreOperationalData();
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				Sys_Soft_Reset();
			}
			else
			{
				s_LoraTestModeFlag = FALSE;
				s_LastTime = GetSystem10msCount();
				Lora_Uart_Init(LORA_BAND_RATE);	
				LORA_WORK_MODE();	
				s_Status = 1;
				ErrCount = 0;
				SetTCProtocolForLoraRunFlag(TRUE);
				s_LoraReadFlag = TRUE;
				u1_printf(" Start Lora Rec\r\n");
				return 1;
			}
			
		break;
		
	}
	
	return 0;
}

static u8 LoraInit()
{
	static u8 s_State = 0, Reset_LoraConfig_Flag = FALSE;
	static unsigned short s_LastTime = 0;
	u8 i, sendbuf[6];
	SYSTEMCONFIG *p_sys;
	const u8 CMD_C1[3] = {0xc1, 0xc1, 0xc1};
	const u8 CMD_C3[3] = {0xc3, 0xc3, 0xc3};
	const u8 CMD_C5[3] = {0xc5, 0xc5, 0xc5};
	const u8 CMD_F3[3] = {0xf3, 0xf3, 0xf3};
//	const u8 CMD_RSSI[6] = {0xAF, 0xAF, 0x74, 0x00, 0xAF, 0xF4};
	p_sys = GetSystemConfig();	
	
	switch(s_State)
	{
		case 0:			
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 3) 
			{
				u1_printf("\r\n [Lora]Sleep Mode...\r\n");
				LORA_SLEEP_MODE();
				Lora_Uart_Init(9600);	
				s_LastTime = GetSystem10msCount();
				s_State++;	
			}	
		break;
		case 1:
			u1_printf("\r\n [Lora]Software Version:");
			Lora_Send_Buff((u8 *)CMD_F3, 3);

			while(1)
			{
				if(GetLoraUartRecFlag())
				{
					for(i=0; i<LORA_UART_CNT; i++)
						u1_printf("%c", LORA_UART_BUF[i]);
					Clear_Lora_Buff();
					break;
				}
				
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20) 
				{
					s_LastTime = GetSystem10msCount();					
					u1_printf("\r\n [Lora]Get versions over time\r\n");
					break;
				}
			
			}	
			s_State++;
			LORA_UART_CNT = 0;		
		break;
		case 2:
			u1_printf("\r\n [Lora]Hardware Version:");
			Lora_Send_Buff((u8 *)CMD_C3, 3);

			while(1)
			{
				if(GetLoraUartRecFlag())
				{
					for(i=0; i<LORA_UART_CNT; i++)
						u1_printf("%c", LORA_UART_BUF[i]);
					Clear_Lora_Buff();
					break;
				}
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20) 
				{
					s_LastTime = GetSystem10msCount();					
					u1_printf("\r\n [Lora]Get hardware over time\r\n");
					break;
				}

			}	
			s_State++;
			LORA_UART_CNT = 0;

		break;
		case 3:
			u1_printf("\r\n [Lora]Batvol:");
			Lora_Send_Buff((u8 *)CMD_C5, 3);

			while(1)
			{
				if(GetLoraUartRecFlag())
				{
					u1_printf(" %1.3fV",(LORA_UART_BUF[1]*256 + LORA_UART_BUF[2])/1000.0);
					u1_printf("\r\n");	
					Clear_Lora_Buff();					
					break;
				}
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20) 
				{
					s_LastTime = GetSystem10msCount();					
					u1_printf("\r\n [Lora]Get Batvol over time\r\n");
					s_LoraReadFlag = FALSE;
					break;
				}
			}	
			s_State++;
			LORA_UART_CNT = 0;
			delay_ms(80);
		break;

		case 4:	
			u1_printf("\r\n [Lora]Config:");
			Lora_Send_Buff((u8 *)CMD_C1, 3);


			while(1)
			{
				if(GetLoraUartRecFlag())
				{				
					for(i=1; i<LORA_UART_CNT; i++)
						u1_printf("%02X ", LORA_UART_BUF[i]);
					u1_printf("\r\n");
					
					if(((p_sys->Node)&0xffff) != ((LORA_UART_BUF[1]<<8) + LORA_UART_BUF[2]) || p_sys->Channel != LORA_UART_BUF[4] || LORA_UART_BUF[3] != LORA_SPEED_INIT || LORA_UART_BUF[5] != LORA_OPTION )					
					{
						Reset_LoraConfig_Flag = TRUE;
						u1_printf("\r\n Updata Config\r\n");
					}					
					else
					{
						s_State++;
						s_LoraReadFlag = TRUE;
						break;
					}
					Clear_Lora_Buff();
				}
						
					if(Reset_LoraConfig_Flag)		//重新配置LORA参数，系统 重启
					{
						Reset_LoraConfig_Flag = FALSE;
						sendbuf[0] = 0xC0;
						sendbuf[1] = (p_sys->Node >> 8)&0xff;
						sendbuf[2] = p_sys->Node &0xff;
						sendbuf[3] = LORA_SPEED_INIT;
						if(p_sys->Channel <= 31)
							sendbuf[4] = p_sys->Channel;
						else
							sendbuf[4] = 15;
						sendbuf[5] = LORA_OPTION;		
						Lora_Send_Buff(sendbuf, sizeof(sendbuf));
						s_LastTime = GetSystem10msCount();	

						while(1)
						{
							if(GetLoraUartRecFlag())
							{
								if(LORA_UART_BUF[0] == 'O' && LORA_UART_BUF[1] == 'K' )
								{
									
									s_State++;
									u1_printf("\r\n [Lora]Upadata Config OK\r\n");		

//									p_Lora->Addr_H  = sendbuf[1];
//									p_Lora->Addr_L  = sendbuf[2];
//									p_Lora->Speed   = sendbuf[3];
//									p_Lora->Channel = sendbuf[4];
//									p_Lora->Option  = sendbuf[5];
//									
//									u1_printf("\r\n 等待重启...\r\n");
									delay_ms(100);
									StoreOperationalData();
									while (DMA_GetCurrDataCounter(DMA1_Channel4));
									Sys_Soft_Reset();
								}	
								else
								{
									u1_printf("\r\n [Lora]Updata Config fail\r\n");	
									s_State++;
								}
								Clear_Lora_Buff();							
								break;
							}
							
							if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 30) 
							{
								s_LastTime = GetSystem10msCount();					
								u1_printf("\r\n [Lora]Updata Config over time,Fail...\r\n");
								s_LoraReadFlag = FALSE;
								return TRUE;
							}
						}
						break;
					}

				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 30) 
				{
					s_LastTime = GetSystem10msCount();					
					u1_printf("\r\n [Lora]Get Config over time\r\n");
					
					SetLogErrCode(LOG_CODE_NOLORA);
					StoreOperationalData();
					s_LoraReadFlag = FALSE;
					LORA_UART_CNT = 0;
					s_State++;
					break;
				}
		
			}				
		break;
		case 5:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20) 
			{
				s_LastTime = GetSystem10msCount();		
				u1_printf("\r\n [Lora]Enter Work mode...\r\n");
				LORA_WORK_MODE();
				return TRUE;	
			}
		
		default:
			
		break;
	}
	return FALSE;
}

void InitLoraLinkedList( void )
{

	pLoraLinkedList = (LORALINKEDLIST*)malloc(sizeof( LORALINKEDLIST));
	if( pLoraLinkedList == NULL )
	{
		u1_printf("\r\n [LORA]:Init Linklist Fail\n");
	}
	else
	{

		u1_printf("\r\n [LORA]:Init Linklist Success\r\n");

		pLoraLinkedList->m_lDeviceId = 65530;
		pLoraLinkedList->m_Num = 65530;
		pLoraLinkedList->m_nNetChannel = 255;
		pLoraLinkedList->m_nLastLinkTime = GetRTCSecond();
		pLoraLinkedList->m_nAliveTime= GetSystem100msCount();
		pLoraLinkedList->m_CellVol = 255.0;
		pLoraLinkedList->m_OffLineFlag = 0;
		pLoraLinkedList->m_DeviceType = SENSOR;
		pLoraLinkedList->m_ChargingFlag = 0;
		pLoraLinkedList->m_pNext = NULL;

		g_ucLoraLinkedListNum = 0;
	}

}


void PrintLoraLinkedList( void )
{
	UINT8 i=0;
	LORALINKEDLIST * p1;

	p1 = pLoraLinkedList->m_pNext;
	u1_printf("\r\n");
	for(i=0;i<g_ucLoraLinkedListNum && i<MAX_LORA_LINKED_LIST;i++)
	{
		u1_printf("%02d: ID:%05d Int:%04ds Vol:%dmV"
				, p1->m_Num+1
				, p1->m_lDeviceId
				, (DifferenceOfRTCTime( GetRTCSecond(), p1->m_nLastLinkTime))
				, (p1->m_CellVol)
				);
		if(p1->m_ChargingFlag)
		{
			u1_printf(" Y ");
		}
		else
		{
			u1_printf(" N ");
		}
		
		if(p1->m_DeviceType == SENSOR)
		{
			u1_printf("Sensor\r\n");
		}
		else if(p1->m_DeviceType == CONTROL)
		{
			u1_printf("Control\r\n");
		}
		else if(p1->m_DeviceType == RS485_CONTROL)
		{
			u1_printf("RS485\r\n");
		}
		else if(p1->m_DeviceType == V263_CONTROL)
		{
			u1_printf("V263_CTR\r\n");
		}
		p1 = p1->m_pNext;
		if( p1 == NULL )
		{
			break;
		}
	}
}

unsigned char CheckListNum(unsigned int lDeviceId)
{
	UINT8 i=0;
	LORALINKEDLIST * p1;

	p1 = pLoraLinkedList->m_pNext;

	//查找该设备号的连接
	for(i=0;i<g_ucLoraLinkedListNum && i<MAX_LORA_LINKED_LIST;i++)
	{
		if( p1->m_lDeviceId == lDeviceId )	//更新列表信息
		{
			return p1->m_Num;
		}

		p1 = p1->m_pNext;
		if( p1 == NULL )
		{
			break;
		}
	}	
	return 255;
}
void AddToLoraLinkedList( unsigned int lDeviceId, unsigned Num,UINT8 nNetChannel, UINT16 nMain100ms, UINT16 nCell_Vol, eDEVICETYPE eType)
{
	UINT8 i=0;
	LORALINKEDLIST *pPre, *p1;

	p1 = pLoraLinkedList->m_pNext;
	pPre = pLoraLinkedList;
	//查找该设备号的连接
	if(p1 != NULL)
	{
		for(i=0;i<g_ucLoraLinkedListNum && i<MAX_LORA_LINKED_LIST;i++)
		{
			if( p1->m_lDeviceId == lDeviceId )	//更新列表信息
			{
				if(nCell_Vol != 0)
				{
					p1->m_CellVol = nCell_Vol;
					p1->m_ChargingFlag = (nCell_Vol&0x0001);
				}				
//				u1_printf(" [LORA]:更新列表信息,时序号设备号:%d,地址:%d,频道:%d,电池电压:%dmV,连接数:%d\r\n"
//				, p1->m_Num
//				, lDeviceId
//				, nNetChannel
//				, nCell_Vol
//				, g_ucLoraLinkedListNum);

				p1->m_nNetChannel = nNetChannel;
				p1->m_nLastLinkTime = GetRTCSecond();
				p1->m_nAliveTime = nMain100ms;
				p1->m_DeviceType = eType;
//				PrintLoraLinkedList();
				return;
			}
			pPre = p1;
			p1 = p1->m_pNext;
			if( p1 == NULL )
			{
				
				break;
			}
		}
	}
	
	p1 = (LORALINKEDLIST *)malloc( sizeof(LORALINKEDLIST));
	if( p1 == NULL )
	{
		u1_printf("[AddToLinkList] -> failed\r\n");
		return;
	}
	
	if(nCell_Vol != 0)
	{
		p1->m_CellVol = nCell_Vol;
		p1->m_ChargingFlag = (nCell_Vol&0x0001);
	}
	p1->m_lDeviceId = lDeviceId;
	p1->m_Num = Num;
	p1->m_nNetChannel = nNetChannel;
	p1->m_nLastLinkTime = GetRTCSecond();
	p1->m_nAliveTime = nMain100ms;
	p1->m_OffLineFlag = 0;
	p1->m_DeviceType = eType;
	pLoraLinkedList->m_DeviceType = SENSOR;
	//插入链表表头之后
	pPre->m_pNext = p1;
	p1->m_pNext = NULL;
	
	g_ucLoraLinkedListNum++;

	u1_printf(" [LORA]Insert:%d DeviceID:%d, Channel:%d, Vol:%dmV, Linked Count:%d\r\n"
		, Num
		, lDeviceId
		, nNetChannel
		, nCell_Vol
		, g_ucLoraLinkedListNum);

//	PrintLoraLinkedList();
}

eDEVICETYPE GetTypeFromLoraLinkedList( unsigned int lDeviceId )
{
	LORALINKEDLIST * p1=NULL;
	LORALINKEDLIST * pPrev = NULL;
	UINT8 i=0;
	
	pPrev = pLoraLinkedList;
	p1 = pPrev->m_pNext;
	
	if(g_ucLoraLinkedListNum == 0)
	{
		u1_printf(" No Device\r\n");
		return ERRORTYPE;
	}
	
	for( i=0;i<g_ucLoraLinkedListNum && i<MAX_LORA_LINKED_LIST;i++ )
	{
		if( p1->m_lDeviceId == lDeviceId )	//更新列表信息
		{
			return p1->m_DeviceType;
		}												//36600
		pPrev = p1;
		p1=pPrev->m_pNext;
		if( p1 == NULL )
		{
			u1_printf(" Device(%d) outside of the list\r\n", lDeviceId);
			return ERRORTYPE;
		}
	}
	
	if( p1 == NULL )
	{
		return ERRORTYPE;
	}	
	return ERRORTYPE;
}


//从设备列表中移除设备
void DelFromLoraLinkedList( unsigned int lDeviceId )
{
	LORALINKEDLIST * p1=NULL;
	LORALINKEDLIST * pPrev = NULL;
	UINT8 i=0;

	
	pPrev = pLoraLinkedList;
	p1 = pPrev->m_pNext;
	
	if(g_ucLoraLinkedListNum == 0)
	{
		u1_printf(" No Device\r\n");
		return;
	}
	
	for( i=0;i<g_ucLoraLinkedListNum && i<MAX_LORA_LINKED_LIST;i++ )
	{
		if( p1->m_lDeviceId == lDeviceId )	//更新列表信息
		{
			u1_printf("\r\n\r\n !!!!!!!!!!!!!!Device=%d Remove,Now Linked Count=%d!!!!!!!!!!!!!!!!!!\r\n\r\n", p1->m_lDeviceId, g_ucLoraLinkedListNum - 1 );
			pPrev->m_pNext = p1->m_pNext;
			free( p1 );
			break;
		}												//36600
		pPrev = p1;
		p1=pPrev->m_pNext;
		if( p1 == NULL )
		{
			u1_printf(" Device(%d) Outside of the list\r\n", lDeviceId);
			return;
		}
	}
	
	if( p1 == NULL )
	{
		u1_printf("p1 Null\r\n");
		g_ucLoraLinkedListNum--;
		PrintLoraLinkedList();
		return;
	}	
	
	for( ;i<g_ucLoraLinkedListNum && i<MAX_LORA_LINKED_LIST;i++)
	{
		p1->m_Num = p1->m_Num - 1;
		
		pPrev = p1;
		p1 = p1->m_pNext;
		if( p1 == NULL )
		{
			break;
		}
	}
	
	g_ucLoraLinkedListNum--;
	
	PrintLoraLinkedList();
}

//维护连接列表
void UpdateLoraLinkList()
{
	LORALINKEDLIST * p1=NULL;
	LORALINKEDLIST * pPrev = NULL;
	UINT8 i=0;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();
	
	pPrev = pLoraLinkedList;
	p1 = pPrev->m_pNext;
	

	if(g_ucLoraLinkedListNum == 0)
	{
		u1_printf(" No Node...\r\n");
		return;
	}
	for( i=0;i<g_ucLoraLinkedListNum && i<MAX_LORA_LINKED_LIST;i++ )
	{
		if(DifferenceOfRTCTime(GetRTCSecond(), p1->m_nLastLinkTime) > p_sys->Data_interval + 48 && DifferenceOfRTCTime(GetRTCSecond(), p1->m_nLastLinkTime) < p_sys->Data_interval*10)
		{
			u1_printf("\r\n *****************Device(%d) Over Time*********************\r\n", p1->m_lDeviceId);	
		}
																//36600
		if( (DifferenceOfRTCTime(GetRTCSecond(), p1->m_nLastLinkTime) > 3660 && DifferenceOfRTCTime(GetRTCSecond(), p1->m_nLastLinkTime) < 4000) || (p1->m_OffLineFlag == 1))   //时间片改为10ms，3分钟为(3*60*100 )个10mS
		{
			//g_ucLoraLinkedListNum--;
			u1_printf("\r\n\r\n !!!!!!!!!!!!!!Device(%d) Remove,Now Linked Count=%d!!!!!!!!!!!!!!!!!!\r\n\r\n", p1->m_lDeviceId, g_ucLoraLinkedListNum );
			pPrev->m_pNext = p1->m_pNext;
			free( p1 );
			break;
		}
		pPrev = p1;
		p1=pPrev->m_pNext;
		if( p1 == NULL )
		{
			return;
		}

	}
	
	if( p1 == NULL )
	{
		g_ucLoraLinkedListNum--;
		PrintLoraLinkedList();
		return;
	}	
	
	for( ;i<g_ucLoraLinkedListNum && i<MAX_LORA_LINKED_LIST;i++)
	{
		p1->m_Num = p1->m_Num - 1;
		
		pPrev = p1;
		p1 = p1->m_pNext;
		if( p1 == NULL )
		{
			break;
		}
	}
	g_ucLoraLinkedListNum--;
	
	PrintLoraLinkedList();
}
//系统功能: 上报心跳信息包======================================================
void Lora_Send_ACK(CLOUD_HDR *hdr)
{
	u16 nMsgLen, i;
	unsigned char send_buf[128];
	unsigned char AckPack[128];
	unsigned int len = 0, DeviceID = 0;
	static uint16 seq_no = 0;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();
	DeviceID = hdr->device_id;
	
	send_buf[len++] = PROTOCOLVERSION >> 8;
	send_buf[len++] = PROTOCOLVERSION & 0xff;
	
	send_buf[len++] = p_sys->Device_ID >> 24;
	send_buf[len++] = p_sys->Device_ID >> 16;	
	send_buf[len++] = p_sys->Device_ID >> 8;
	send_buf[len++] = p_sys->Device_ID;	
	
	send_buf[len++]  = 1;
	
	send_buf[len++]  = seq_no >> 8;
	send_buf[len++]  = seq_no++;
	
	send_buf[len++]  = 0;
	send_buf[len++]  = 2;

	send_buf[len++] = CMD_TEST;									//命令字

	send_buf[len++] = 'O';
	send_buf[len++] = 'K';

	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, AckPack, 120);	//数据包转义处理	
	u1_printf("\r\n [Lora][Ack OK] -> %d(%d): ", DeviceID, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",AckPack[i]);
	}
	u1_printf("\r\n");	

	Lora_Send_Data(DeviceID, p_sys->Channel, AckPack, nMsgLen);
}

unsigned int Lora_Send_Polling(unsigned int lDeviceId, u16 PollingTime)
{
	u16 nTemp, nPos, nMsgLen;
	static u16 seq;
	u32 lTemp;
	u8 i, sTemp[128], send_buf[128];
	SYSTEMCONFIG *p_system;
	RTC_TimeTypeDef RTC_TimeStructure;	

	
	p_system = GetSystemConfig();	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	
	nPos= 0;

	nTemp = PROTOCOL_GATEWAY_ID;
	sTemp[nPos++] = nTemp >> 8;
	sTemp[nPos++] = nTemp;

	//设备ID
	lTemp = p_system->Device_ID;
	sTemp[ nPos++] = lTemp >> 24;
	sTemp[ nPos++] = lTemp >> 16;
	sTemp[ nPos++] = lTemp >> 8;
	sTemp[ nPos++] = lTemp;


	sTemp[nPos++] = 0;  //发送包标志

	//序号
	seq++;
	if ( 0xffff == seq )
		seq = 1;

	sTemp[ nPos++ ] = seq >>8;
	sTemp[ nPos++ ] = seq;


	//nContextLen += 1;  //内容长度
	sTemp[ nPos++ ] = 0;
	sTemp[ nPos++ ] = 2;

	sTemp[nPos++] = CMD_POLLING;  //命令字
	sTemp[nPos++] = PollingTime >> 8;
	sTemp[nPos++] = PollingTime;
	
	nTemp = Calc_Checksum(sTemp, nPos);
	sTemp[nPos++] = nTemp;

	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( sTemp,nPos,send_buf,100);
	//发送消息
	u1_printf("\r\n [%02d:%02d:%02d][Lora][Poll] -> %d(%d): ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, lDeviceId, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ",send_buf[i]);
	}
	u1_printf("\r\n");	
	Lora_Send_Data(lDeviceId, p_system->Channel, send_buf, nMsgLen);

	return nMsgLen;
}

unsigned int Lora_Send_Control(unsigned int lDeviceId, unsigned char channel, unsigned char IsOn, unsigned char time)
{
	u16 nTemp, nPos, nMsgLen;
	static u16 seq;
	u32 lTemp;
	u8 i, sTemp[128], send_buf[128];
	SYSTEMCONFIG *p_system;
	RTC_TimeTypeDef RTC_TimeStructure;	

	
	p_system = GetSystemConfig();	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	
	nPos= 0;

	nTemp = PROTOCOL_CONTROL_ID;
	sTemp[nPos++] = nTemp >> 8;
	sTemp[nPos++] = nTemp;

	//设备ID
	lTemp = p_system->Device_ID;
	sTemp[ nPos++] = lTemp >> 24;
	sTemp[ nPos++] = lTemp >> 16;
	sTemp[ nPos++] = lTemp >> 8;
	sTemp[ nPos++] = lTemp;


	sTemp[nPos++] = 0;  //发送包标志

	//序号
	seq++;
	if ( 0xffff == seq )
		seq = 1;

	sTemp[ nPos++ ] = seq >>8;
	sTemp[ nPos++ ] = seq;


	//nContextLen += 1;  //内容长度
	sTemp[ nPos++ ] = 0;
	sTemp[ nPos++ ] = 0x0b;

	sTemp[nPos++] = CMD_CONTROL;  //命令字
	sTemp[nPos++] = 0;
	sTemp[nPos++] = 32+channel;
	
	sTemp[nPos++] = 0;
	sTemp[nPos++] = 0;			//类型
	
	sTemp[nPos++] = 1;			//模式
	
	sTemp[nPos++] = 0;	
	sTemp[nPos++] = time;			//时长
	
	sTemp[nPos++] = 5;
	sTemp[nPos++] = IsOn;		//开关
	
	sTemp[nPos++] = 5;
	sTemp[nPos++] = 0;

	
	nTemp = Calc_Checksum(sTemp, nPos);
	sTemp[nPos++] = nTemp;

	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( sTemp,nPos,send_buf,100);
	//发送消息
	Lora_Send_Data(lDeviceId, p_system->Channel, send_buf, nMsgLen);
	
	u1_printf("\r\n [%02d:%02d:%02d][Control] -> %d(%d): ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, lDeviceId, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ",send_buf[i]);
	}
	u1_printf("\r\n");	
	

	return nMsgLen;
}

unsigned int Lora_Send_TestInfo(unsigned int lDeviceId)
{
	u16 nTemp, nPos, nMsgLen;
	static u16 seq;
	u32 lTemp;
	u8 i, sTemp[128], send_buf[128];
	SYSTEMCONFIG *p_system;
	RTC_TimeTypeDef RTC_TimeStructure;	

	
	p_system = GetSystemConfig();	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	
	nPos= 0;

	nTemp = PROTOCOL_GATEWAY_ID;
	sTemp[nPos++] = nTemp >> 8;
	sTemp[nPos++] = nTemp;

	//设备ID
	lTemp = p_system->Device_ID;
	sTemp[ nPos++] = lTemp >> 24;
	sTemp[ nPos++] = lTemp >> 16;
	sTemp[ nPos++] = lTemp >> 8;
	sTemp[ nPos++] = lTemp;


	sTemp[nPos++] = 0;  //发送包标志

	//序号
	seq++;
	if ( 0xffff == seq )
		seq = 1;

	sTemp[ nPos++ ] = seq >>8;
	sTemp[ nPos++ ] = seq;


	//nContextLen += 1;  //内容长度
	sTemp[ nPos++ ] = 0;
	sTemp[ nPos++ ] = 3;

	sTemp[nPos++] = CMD_TEST;  //命令字
	
	sTemp[nPos++] = RTC_TimeStructure.RTC_Hours;
	sTemp[nPos++] = RTC_TimeStructure.RTC_Minutes;
	sTemp[nPos++] = RTC_TimeStructure.RTC_Seconds;
	
	nTemp = Calc_Checksum(sTemp, nPos);
	sTemp[nPos++] = nTemp;

	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( sTemp,nPos,send_buf,100);
	//发送消息
	Lora_Send_Data(lDeviceId, p_system->Channel, send_buf, nMsgLen);
	u1_printf("[%02d:%02d:%02d][Lora][Test] -> %d(%d): ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, lDeviceId, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ",send_buf[i]);
	}
	u1_printf("\r\n");	
	

	return nMsgLen;
}

unsigned int Lora_Send_Reboot(unsigned int lDeviceId)
{
	u16 nTemp, nPos, nMsgLen;
	static u16 seq;
	u32 lTemp;
	u8 i, sTemp[128], send_buf[128];
	SYSTEMCONFIG *p_system;
	RTC_TimeTypeDef RTC_TimeStructure;	

	
	p_system = GetSystemConfig();	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	
	nPos= 0;

	nTemp = PROTOCOL_GATEWAY_ID;
	sTemp[nPos++] = nTemp >> 8;
	sTemp[nPos++] = nTemp;

	//设备ID
	lTemp = p_system->Device_ID;
	sTemp[ nPos++] = lTemp >> 24;
	sTemp[ nPos++] = lTemp >> 16;
	sTemp[ nPos++] = lTemp >> 8;
	sTemp[ nPos++] = lTemp;


	sTemp[nPos++] = 0;  //发送包标志

	//序号
	seq++;
	if ( 0xffff == seq )
		seq = 1;

	sTemp[ nPos++ ] = seq >>8;
	sTemp[ nPos++ ] = seq;


	//nContextLen += 1;  //内容长度
	sTemp[ nPos++ ] = 0;
	sTemp[ nPos++ ] = 3;

	sTemp[nPos++] = CMD_REBOOT;  //命令字
	
	sTemp[nPos++] = RTC_TimeStructure.RTC_Hours;
	sTemp[nPos++] = RTC_TimeStructure.RTC_Minutes;
	sTemp[nPos++] = RTC_TimeStructure.RTC_Seconds;
	
	nTemp = Calc_Checksum(sTemp, nPos);
	sTemp[nPos++] = nTemp;

	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( sTemp,nPos,send_buf,100);
	//发送消息
	u1_printf("[%02d:%02d:%02d][Lora][Reboot] -> %d(%d): ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, lDeviceId, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ",send_buf[i]);
	}
	u1_printf("\r\n");	
	Lora_Send_Data(lDeviceId, p_system->Channel, send_buf, nMsgLen);

	return nMsgLen;
}

unsigned int Lora_Send_DataACK(unsigned char Num, unsigned int lDeviceId, unsigned char cmd)
{
	u16 nTemp, nPos, nMsgLen, Data_interval;
	static u16 seq;
	u32 lTemp;
	u8  sTemp[128], send_buf[128];
	SYSTEMCONFIG *p_system;
	RTC_TimeTypeDef RTC_TimeStructure;	
	
	p_system = GetSystemConfig();	
	
	
	nPos= 0;

	nTemp = PROTOCOL_GATEWAY_ID;
	sTemp[nPos++] = nTemp >> 8;
	sTemp[nPos++] = nTemp;

	//设备ID
	lTemp = p_system->Device_ID;
	sTemp[ nPos++] = lTemp >> 24;
	sTemp[ nPos++] = lTemp >> 16;
	sTemp[ nPos++] = lTemp >> 8;
	sTemp[ nPos++] = lTemp;


	sTemp[nPos++] = 1;  //发送包标志

	//序号
	seq++;
	if ( 0xffff == seq )
		seq = 1;

	sTemp[ nPos++ ] = seq >>8;
	sTemp[ nPos++ ] = seq;


	//nContextLen += 1;  //内容长度
	sTemp[ nPos++ ] = 0;
	sTemp[ nPos++ ] = 5;

	sTemp[nPos++] = cmd;  //命令字

	
	if(GetOnline())
	{
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
		sTemp[nPos++] = RTC_TimeStructure.RTC_Hours;
		sTemp[nPos++] = RTC_TimeStructure.RTC_Minutes;
		sTemp[nPos++] = RTC_TimeStructure.RTC_Seconds;
	}
	else
	{
//		u1_printf(" 网关不在线\r\n");
		sTemp[nPos++] = 255;
		sTemp[nPos++] = 255;
		sTemp[nPos++] = 255;
	}
	
	sTemp[nPos++] = Num;
	if(p_system->Data_interval < 60)
	{
		Data_interval = 60;
		u1_printf(" Report Time < 60s，Change to 60s\r\n");
	}
	else if(p_system->Data_interval > 7200)
	{
		Data_interval = 7200;
		u1_printf("  Report Time > 7200s，Change to 7200s\r\n");
	}
	else
	{
		Data_interval = p_system->Data_interval;
	}
	sTemp[nPos++] = Data_interval/60;
	
	nTemp = Calc_Checksum(sTemp, nPos);
	sTemp[nPos++] = nTemp;

	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( sTemp,nPos,send_buf,100);
	//发送消息
	Lora_Send_Data(lDeviceId, p_system->Channel, send_buf, nMsgLen);

	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	
	u1_printf("\r\n [%0.2d:%0.2d:%0.2d]----[LORA]Rec(%d)Data,Ack Time(%ds)\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, lDeviceId, Data_interval);
	

	return nMsgLen;
}

void Lora_Send_Boardcast(unsigned short BoardcastTime)
{
	u16 nMsgLen, i;
	unsigned char send_buf[128];
	unsigned char RegisterPack[128];
	unsigned int len;
	static uint16 seq_no = 0;
	CLOUD_HDR *hdr;
	RTC_TimeTypeDef RTC_TimeStructure;	
	SYSTEMCONFIG *p_sys;

	p_sys = GetSystemConfig();
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(PROTOCOLVERSION);							//通信协议版本号
	hdr->device_id = swap_dword(p_sys->Device_ID);		//设备号
	hdr->dir = 0;													//方向
	hdr->seq_no = swap_word(seq_no++);								//序号

	hdr->payload_len = swap_word(8);							//信息域长度
	hdr->cmd = CMD_REGISTER;									//命令字

	len = sizeof(CLOUD_HDR);

	send_buf[len++] = BoardcastTime >> 8;
	send_buf[len++] = BoardcastTime&0xff;
	send_buf[len++] = REGISTTIMESLOT >> 8;
	send_buf[len++] = REGISTTIMESLOT&0xff;
	send_buf[len++] = POLLINGTIMESLOT >> 8;
	send_buf[len++] = POLLINGTIMESLOT&0xff;
	send_buf[len++] = GPRSDOWNLINKTIMESLOT >> 8;
	send_buf[len++] = GPRSDOWNLINKTIMESLOT&0xff;

//	send_buf[len++] = RTC_TimeStructure.RTC_Hours;
//	send_buf[len++] = RTC_TimeStructure.RTC_Minutes;
//	send_buf[len++] = RTC_TimeStructure.RTC_Seconds;
	
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;

	nMsgLen = PackMsg(send_buf, len, RegisterPack, 120);	//数据包转义处理
	
	u1_printf("\r\n [Lora][CCTV] -> %04X (%d): ", 0XFFFF, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",RegisterPack[i]);
	}
	u1_printf("\r\n");	
	Lora_Send_Data(0XFFFF, p_sys->Channel, RegisterPack, nMsgLen);
}

unsigned char JudgmentTimePoint(unsigned char Type)
{
	u16 Data_Interval, Second, Minute;
	unsigned char ReturnVal = 0;
	RTC_TimeTypeDef RTC_TimeStructure;
	SYSTEMCONFIG *p_sys;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	p_sys = GetSystemConfig();	
	
	Data_Interval = p_sys->Data_interval/60;
	Minute = RTC_TimeStructure.RTC_Minutes%Data_Interval;
	Second = RTC_TimeStructure.RTC_Seconds;
	
	switch(Type)
	{
		
		case NETTYPE_GPRS:
			if((Minute*60 + Second > (g_ucLoraLinkedListNum + 1)*Data_Interval+5) && (Minute*60 + Second < Data_Interval*60 - 20 ))	
			{
				ReturnVal = FALSE;
			}
			else
			{
				ReturnVal = TRUE;
			}				
		break;

		case NETTYPE_2G:
			if((Minute*60 + Second > (g_ucLoraLinkedListNum + 1)*Data_Interval+5) && (Minute*60 + Second < Data_Interval*60 - 40 ))	
			{
				ReturnVal = FALSE;
			}
			else
			{
				ReturnVal = TRUE;
			}	
		break;
		
		case NETTYPE_LORA:
			if((Minute*60 + Second > (g_ucLoraLinkedListNum + 1)*Data_Interval) && (Minute*60 + Second < Data_Interval*60 - 10 ))	
			{
				ReturnVal = FALSE;
			}
			else
			{
				ReturnVal = TRUE;
			}		
		break;
			
		default:
			if((Minute*60 + Second > (g_ucLoraLinkedListNum + 1)*Data_Interval) && (Minute*60 + Second < Data_Interval*60 - 25 ))	
			{
				ReturnVal = FALSE;
			}
			else
			{
				ReturnVal = TRUE;
			}	
		break;
			
		
	}

	return ReturnVal;
}

void LoraProcess(unsigned short nMain10ms)
{
	static u8 RunOnce = FALSE, s_LoraModeFlag = FALSE, s_State = 0, s_DetLoraFlag = FALSE;
	static u16 s_LastTime = 0, s_LastAliveTime = 0;
	static LORALINKEDLIST * s_pNode;	
	
	RTC_TimeTypeDef RTC_TimeStructure;
	SYSTEMCONFIG *p_sys;

	p_sys = GetSystemConfig();	
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
	
	if(RunOnce == FALSE)
	{	
		RunOnce = TRUE;
		
		SetTCProtocolForLoraRunFlag(FALSE);
		
		LoraPort_Init();	//Lora端口配置
		
		LORA_SLEEP_MODE();
		
		LORA_PWR_OFF();
		
		delay_ms(100);	
		
		LORA_PWR_ON();
		
		delay_ms(100);
		
		Lora_Uart_Init(9600);	
		
		u1_printf("\r\n Init LORA\r\n");
		
		while(LoraInit() == FALSE)		//初始化LORA，配置参数
		{
		}
		
		Lora_Uart_Init(LORA_BAND_RATE);
		
		SetTCProtocolForLoraRunFlag(TRUE);
		
		s_LastTime = GetSystem10msCount();
				
		s_LastAliveTime = GetSystem10msCount();
		
		InitLoraLinkedList();		//初始化LORA链表
		
		return;
	}
	
	if( CalculateTime( GetSystem10msCount(), s_LastTime) >= 5000)		//50S输出一次链表信息，剔除超时设备
	{
		s_LastTime = GetSystem10msCount();
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
		u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			

		PrintLoraLinkedList();
		
		UpdateLoraLinkList();	
	}

	
	switch(s_State)
	{
		case 0:
			RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				
			if((RTC_TimeStructure.RTC_Minutes%10 == 9) && RTC_TimeStructure.RTC_Seconds < 3)
			{				
				s_DetLoraFlag = TRUE;
				
				s_State = 5;
			}
			else if(CalculateTime( GetSystem10msCount(), s_LastAliveTime) >= 200 )	
			{		
				s_State++;
				s_LastAliveTime = GetSystem10msCount();	
			}
		break;
		
		case 1:		
		
			if(g_ucLoraLinkedListNum == 0)
			{
				s_State = 0;
			}
			else
			{
				s_pNode = pLoraLinkedList->m_pNext;		//下一设备
				s_State ++;
			}
		break;
			
		case 2:									
			if(s_pNode == NULL)		//所有节点查询完毕
			{
				if(GetPowerSavingFlag())		//是否进入省电模式
				{
					s_State = 4;	
					s_LastAliveTime = GetSystem10msCount();	
				}
				else
				{
					s_State = 0;	
					s_LastAliveTime = GetSystem10msCount();	
				}					
			}
			else		//判断节点是否到达上报数据包时间
			{
				p_sys = GetSystemConfig();	
				if(CalculateTime( GetSystem100msCount(), s_pNode->m_nAliveTime) >= p_sys->State_interval*10 )	//通信模块自动生成心跳包
				{
					if(GetOnline() && s_pNode->m_DeviceType == V263_CONTROL)			//模块在线，自动生成心跳包
					{
						RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
						u1_printf("\r\n [%0.2d:%0.2d:%0.2d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);

						Mod_Auto_Alive(s_pNode->m_lDeviceId);
						s_pNode->m_nAliveTime = GetSystem100msCount();
					}
					s_LastAliveTime = GetSystem10msCount();
					s_State ++;
				}
				else
				{
					s_State ++;
				}
			}
		break;
			
		case 3:
			if(CalculateTime( GetSystem10msCount(), s_LastAliveTime) >= 50 )	//间隔500ms查询下一节点
			{
				s_pNode = s_pNode->m_pNext;
				s_State = 2;
			}
		break;
			
		case 4:
			if(CalculateTime( GetSystem10msCount(), s_LastAliveTime) >= 200 )		//2S判断一次是否进入省电模式
			{
				s_LastAliveTime = GetSystem10msCount();	
				if(GetPowerSavingFlag())	//是否进入省电模式
				{				
					if(JudgmentTimePoint(NETTYPE_LORA))		//是否到达设备上报数据时间区域
					{
						if(s_LoraModeFlag != JudgmentTimePoint(NETTYPE_LORA))
						{
							s_LoraModeFlag = JudgmentTimePoint(NETTYPE_LORA);
							RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
							u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
							u1_printf("LORA_WORK_MODE()\r\n");
						}					
						LORA_WORK_MODE();			
					}
					else	//省电模式，非上报时区LORA不工作
					{
						if(s_LoraModeFlag != JudgmentTimePoint(NETTYPE_LORA))
						{
							s_LoraModeFlag = JudgmentTimePoint(NETTYPE_LORA);
							RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
							u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
							u1_printf("LORA_SLEEP_MODE()\r\n");
						}		
						LORA_SLEEP_MODE();
					}				
				}
				else
				{
					LORA_WORK_MODE();	
					s_State = 0;
					s_LastAliveTime = GetSystem10msCount();	
				}					
			}
		break;
		
		case 5:
			if(s_DetLoraFlag)
			{
				if(RTC_TimeStructure.RTC_Seconds >= 3)
				{				
					while(DetLoraModule() == FALSE);
					s_State = 0;
					s_DetLoraFlag = FALSE;
				}
			}
		break;
	}
}

void OnRecLoraData(USART_TypeDef* USARTx, TC_COMM_MSG *pMsg)		//Rec完整的LORA数据包
{
	u8 i, BatNum, buf[4], data_count = 0, DeviceNum;
	u16 Cell_Vol = 0, nMain100ms, nBatID;
	u32 error_mark = 0xEEEEEEEE;
	float f_DataValue = 0.0;
	RTC_TimeTypeDef RTC_TimeStructure;	
	eDEVICETYPE DeviceType;
	SYSTEMCONFIG *p_sys;

	p_sys = GetSystemConfig();
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
	
	if(pMsg->Length > LORA_BUFF_MAX)
	{
		u1_printf(" Buf Length Over(%d)!!!!!!!!!!!!\r\n", pMsg->Length);
		return;
	}
	


	//解析协议包
	if(pMsg->OPType != CMD_REPORT_D)		//若Rec节点数据上报，则不打印具体信息，直接回复应答，以减少节点的等待时间，降低功耗
	{
		u1_printf("\r\n [%02d:%02d:%02d][LORA]<-Version:%02d Device:%04d Dir:%X Seq:%d Len:%d CMD:%02X Data:", \
		RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, pMsg->Protocol, pMsg->DeviceID, pMsg->Dir, pMsg->Seq, pMsg->Length, pMsg->OPType);
		for(i=0; i<pMsg->Length; i++)
			u1_printf("%02X ", pMsg->UserBuf[i]);
		u1_printf("\r\n");
		if(pMsg->DeviceID == 0XFFFF)	
		{
			u1_printf("广播信息\r\n");
			return ;
		}
	}
		
	if(pMsg->OPType == CMD_REPORT_D)		//节点数据包
	{
		Lora_Transmit_GPRS_Data(pMsg->DeviceID);	//若该节点存在GPRS下行数据，则下发

		DeviceNum = CheckListNum(pMsg->DeviceID);	//查询该节点对应时序号
		if(DeviceNum < MAX_LORA_LINKED_LIST)
		{
			Lora_Send_DataACK(DeviceNum, pMsg->DeviceID, CMD_REPORT_D);	//已在列表中，回复对应是序号
		}
		else
		{
			DeviceNum = g_ucLoraLinkedListNum;
			Lora_Send_DataACK(g_ucLoraLinkedListNum, pMsg->DeviceID, CMD_REPORT_D);	//不在列表中，新增1时序号
		}
			
		u1_printf(" ----[LORA]Deivce(%d)Num:(%d)\r\n", pMsg->DeviceID, DeviceNum);
		
		data_count = pMsg->UserBuf[0] - 1;
		u1_printf(" -----Data(%d):", pMsg->DeviceID);
		for(i=0; i<data_count; i++)			//输出节点数据信息
		{
			buf[0] = pMsg->UserBuf[i*9+7];   //次低
			buf[1] = pMsg->UserBuf[i*9+6];   //低
			buf[2] = pMsg->UserBuf[i*9+9];   //高
			buf[3] = pMsg->UserBuf[i*9+8];   //次高
		
			f_DataValue = *(float *)&buf[0];
			
			if(memcmp((uint8 *)&f_DataValue, (uint8 *)&error_mark, sizeof(uint32)) == 0)
			{
				f_DataValue = -1;
			}
			switch(pMsg->UserBuf[i*9 + 4])		//输出节点数据信息
			{				
				case KQW_ID:							
					u1_printf(" 空温度:%.2f℃", f_DataValue);			
				break;
				
				case KQS_ID:
					u1_printf(" 空湿度:%.2f%%", f_DataValue);
				break;
				
				case GZD_ID:			
					u1_printf(" 光照:%.1fLux", f_DataValue);
				break;
				
				case AIR_PRESS_ID:		
					u1_printf(" 大气压力:%.1fhPa", f_DataValue);
				break;
				
				case TRW_ID:		
					u1_printf(" 土温度:%.1f℃", f_DataValue);
				break;
				
				case TRS_ID:		
					u1_printf(" 土湿度:%.1f%%", f_DataValue);
				break;
				
				case PH_ID:		
					u1_printf(" PH:%.1f", f_DataValue);
				break;
				
				case FS_ID:			
					u1_printf(" 风速:%.1fm/s", f_DataValue);
				break;
				
				case FX_ID:		
					u1_printf(" 风向:%.1f°", f_DataValue);
				break;
				
				case SOIL_EC_ID:			
					u1_printf(" 土EC:%.1fmS/cm", f_DataValue);
				break;
				
				case SOIL_YF_ID:		
					u1_printf(" 土盐分:%.1fmg/L", f_DataValue);
				break;
				
				case JYL_ID:		
					u1_printf(" 降雨量:%.1f", f_DataValue);
				break;	
				
				case LL_ID:
					u1_printf(" 流量:%.1f", f_DataValue);
				break;
				
				case RJY_ID:
					u1_printf(" 溶解氧:%.1f", f_DataValue);
				break;
				
				case SOIL_M_ID:
					u1_printf(" 土张力:%.1fkPa", f_DataValue);
				break;
				
				case YMSD_ID:
					u1_printf(" 叶面湿度:%.1f%%", f_DataValue);
				break;
				
				case AIR_ION_ID:
					u1_printf(" 负氧离子:%.1f%%", f_DataValue);
				break;
				
				case O2_ID:
					u1_printf(" O2:%.1f%%", f_DataValue);
				break;
				
				case H2S_ID:
					u1_printf(" H2S:%.1f%%", f_DataValue);
				break;
				
				case NH3_ID:
					u1_printf(" NH3:%.1f%%", f_DataValue);
				break;
				
				case YW_ID:
					u1_printf(" 液位:%.1f%%", f_DataValue);
				break;
				
				case SMOKE_ID:
					u1_printf(" 粉尘:%.1f", f_DataValue);
				break;
				
				case H2O_TURBIDITY_ID:			
					u1_printf(" 浊度:%.1f", f_DataValue);
				break;
				case CO2_ID:		//Carbon Dioxide
					u1_printf(" CO2:%.1f", f_DataValue);
				break;
				case EVAPORATION_ID:			  //Evaporation
					u1_printf(" 蒸发量:%.1f", f_DataValue);
				break;
				case PHOTOSYNTHETIC_ACTIVE_RADIATION_ID:			  //Photosynthetically Active Radiation
					u1_printf(" 光合有效辐射:%.1f", f_DataValue);
				break;
				
				case SOIL_WATER_POTENTIAL:
					u1_printf(" 水压力:%.1f", f_DataValue);
				break;
				
				case LONGITUDE_ID:
					u1_printf(" 经度:%.5f", f_DataValue);
				break;
				
				case LATITUDE_ID:
					u1_printf(" 纬度:%.5f", f_DataValue);
				break;
				case DEW_POINT:			  //Dew Point
					u1_printf(" 露点:%.1f", f_DataValue);
				break;
				
				case BAT_VOL:
					u1_printf(" DC电压:%.3fV", f_DataValue);
				break;
				
				default:
					u1_printf(" ID:0x%02 Data:%.2f ", pMsg->UserBuf[i*9 + 4], f_DataValue);
				break;
			}
		}
		
		BatNum = pMsg->UserBuf[0] - 1;	//节点电池电压
		nBatID = (pMsg->UserBuf[BatNum*9+3] << 8) + pMsg->UserBuf[BatNum*9+4];	
		
		if(nBatID == 0x00FF)
		{
			Cell_Vol = (pMsg->UserBuf[BatNum*9+6] << 8) + pMsg->UserBuf[BatNum*9+7];	//得到节点电压数据
			u1_printf(" Vol:%4dmV", Cell_Vol);
		}
		u1_printf("\r\n\r\n");

		if(GetTCModuleReadyFlag())
		{
			Mod_Transmit_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);		//转发节点数据
//				delay_ms(10);
//				Transpond_Alive(pMsg);		//生成节点心跳包
		}
		
		nMain100ms = GetSystem100msCount();
		AddToLoraLinkedList( pMsg->DeviceID, DeviceNum, p_sys->Channel, nMain100ms, Cell_Vol, SENSOR);	//添加至传感器设备
	}
	else if(pMsg->OPType == CMD_REPORT_S)	//Rec节点控制器状态
	{	
//		Lora_Transmit_GPRS_Data(pMsg->DeviceID);	//若该节点存在GPRS下行数据，则下发

		DeviceNum = CheckListNum(pMsg->DeviceID);	//查询该节点对应时序号
		
		if(DeviceNum < MAX_LORA_LINKED_LIST)
		{
			Lora_Send_DataACK(DeviceNum, pMsg->DeviceID, CMD_REPORT_S);	//已在列表中，回复对应是序号
		}
		else
		{
			DeviceNum = g_ucLoraLinkedListNum;
			Lora_Send_DataACK(g_ucLoraLinkedListNum, pMsg->DeviceID, CMD_REPORT_S);	//不在列表中，新增1时序号
		}
		
		DelFromGprsLinkedList(pMsg->DeviceID, GetControlRecTime());
		
		data_count = pMsg->UserBuf[0] - 1;
		u1_printf(" -----Control Info(%d):\r\n", pMsg->DeviceID);
		for(i=0; i<data_count; i++)	
		{
			u1_printf(" ---(%d)Channel(%d):", i+1, pMsg->UserBuf[i*8+2] - 32);
			switch(pMsg->UserBuf[i*8+4])
			{
				case C_LEVEL_ID:
					u1_printf("Level Switch");
				break;
				
				case C_RT_P_ID:
					u1_printf("Forward and reverse rotation of pulse");
				break;
				
				case C_RT_L_ID:
					u1_printf("Positive and negative rotation of level");
				break;
				
				case C_PULSE_ID:
					u1_printf("Pulse Switch");
				break;
			}
			
			if(pMsg->UserBuf[i*8+8] == 0)
			{
				u1_printf("--Stop\r\n");
			}
			else if(pMsg->UserBuf[i*8+8] == 1)
			{
				u1_printf("--Open\r\n");
			}
			else if(pMsg->UserBuf[i*8+8] == 2)
			{
				u1_printf("--Close\r\n");
			}
			
		}
		
		BatNum = pMsg->UserBuf[0] - 1;	//电池电压信息
		nBatID = (pMsg->UserBuf[BatNum*8+3] << 8) + pMsg->UserBuf[BatNum*8+4];	//电压ID
		if(nBatID == BAT_VOL)
		{
			Cell_Vol = (pMsg->UserBuf[BatNum*8+6] << 8) + pMsg->UserBuf[BatNum*8+7];	//得到节点电压数据
			u1_printf(" Vol:%4dmV\r\n", Cell_Vol);

			if(GetTCModuleReadyFlag())
			{
				Mod_Transmit_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);		//转发节点数据	通道信息
				delay_ms(10);
				Mod_Transmit_Vol_Data(BAT_VOL, (CLOUD_HDR *)pMsg, Cell_Vol);			//控制器电压信息
			}
				
			nMain100ms = GetSystem100msCount();
			AddToLoraLinkedList( pMsg->DeviceID, DeviceNum, p_sys->Channel, nMain100ms, Cell_Vol, CONTROL);//添加至控制器设备
			return;
		}
		else if(nBatID == DCPOWER_ID)	//2021.1.15 GYS新增 外部输入电压，为兼容旧版本解析方式
		{
			Cell_Vol = (pMsg->UserBuf[BatNum*8+6] << 8) + pMsg->UserBuf[BatNum*8+7];	//得到节点电压数据
			u1_printf(" DCVol:%4dmV\r\n", Cell_Vol);

			if(GetTCModuleReadyFlag())
			{
				Mod_Transmit_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);		//转发节点数据
				delay_ms(10);
				Mod_Transmit_Vol_Data(DCPOWER_ID, (CLOUD_HDR *)pMsg, Cell_Vol);			//控制器电压信息
			}
				
			nMain100ms = GetSystem100msCount();
			AddToLoraLinkedList( pMsg->DeviceID, DeviceNum, p_sys->Channel, nMain100ms, Cell_Vol, CONTROL);//添加至控制器设备
			return;
		}
		else if(nBatID == V263_C_ID)
		{
			Cell_Vol = (pMsg->UserBuf[BatNum*8+6] << 8) + pMsg->UserBuf[BatNum*8+7];	//得到节点电压数据
			Cell_Vol *= 100;
			u1_printf(" Vol:%4dmV\r\n", Cell_Vol);

			if(GetTCModuleReadyFlag())
			{
				Mod_Transmit_RS485Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);
			}
		
			nMain100ms = GetSystem100msCount();
			AddToLoraLinkedList( pMsg->DeviceID, DeviceNum, p_sys->Channel, nMain100ms, Cell_Vol, V263_CONTROL);//添加至控制器设备
			return;
		}
	}
	else 	//直接转发，配置信息等
	{

		DeviceType = GetTypeFromLoraLinkedList((pMsg->DeviceID&0xfffffff));
		if(DeviceType == V263_CONTROL)
		{
			if(GetTCModuleReadyFlag())
			{
				Mod_Transmit_RS485Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);	
			}
		}
		else
		{
			if(GetTCModuleReadyFlag())
			{
				Mod_Transmit_Data((CLOUD_HDR *)pMsg, pMsg->UserBuf, pMsg->Length);	
			}
		}
			

	}
	
	if(pMsg->OPType == CMD_TEST)
	{
		if(pMsg->Dir)
		{
			u1_printf("\r\n Rec(%d) Gateway Ack!!\r\n", pMsg->DeviceID);
		}
		else
		{
			u1_printf("\r\n Rec(%d) Gateway query status!!\r\n", pMsg->DeviceID);
			Lora_Send_ACK((CLOUD_HDR *)pMsg);
		}				
		return;
	}
	
	if(pMsg->OPType == CMD_REGISTER)
	{
		u1_printf("\r\n Rec(%d)Register Pack\r\n", pMsg->DeviceID);
		Lora_Send_ACK((CLOUD_HDR *)pMsg);
		return;
	}
	
	if(pMsg->OPType == CMD_POLLING)
	{
		u1_printf("\r\n Rec(%d)Poll Pack\r\n", pMsg->DeviceID);
		return;
	}
	
	if(pMsg->OPType == CMD_TEST)
	{
		if(pMsg->Dir)
		{
			u1_printf("\r\n Rec(%d) Gateway Ack!!\r\n", pMsg->DeviceID);
		}
		else
		{
			u1_printf("\r\n Rec(%d) Gateway query status!!\r\n", pMsg->DeviceID);
			Lora_Send_ACK((CLOUD_HDR *)pMsg);
		}
		
		
		return;
	}
	
	if(pMsg->Protocol == PROTOCOL_GATEWAY_ID)
	{
		u1_printf("\r\n Another gateway exists in the channel and is ignored\r\n");
		
		return;
	}
	

	
	if(pMsg->OPType == CMD_REBOOT)
	{
		u1_printf("Rec (%d) Reply,Equipment is normal\r\n", pMsg->DeviceID);
		return;
	}
}
	
static void DecodeAppMsg(uint8 *pBuf,uint16 uLen,TC_COMM_MSG *pMsg)
{
    u32 uPoint = 0;
	
    pMsg->Protocol = pBuf[uPoint++];
    pMsg->Protocol = (pMsg->Protocol<<8)|pBuf[uPoint++];
	
	pMsg->DeviceID = pBuf[uPoint++];
    pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	pMsg->DeviceID = (pMsg->DeviceID<<8)|pBuf[uPoint++];
	
	pMsg->Dir   = pBuf[uPoint++];
	
	pMsg->Seq = pBuf[uPoint++];
    pMsg->Seq = (pMsg->Seq<<8)|pBuf[uPoint++];
	
    pMsg->Length = pBuf[uPoint++];
    pMsg->Length = (pMsg->Length<<8)|pBuf[uPoint++];
	
    pMsg->OPType   = pBuf[uPoint++];
	
    if(pMsg->Length > TC_DATA_SIZE)
    {
	    pMsg->Length = TC_DATA_SIZE;
    }
	memcpy((void *)pMsg->UserBuf,(void *)&pBuf[uPoint],pMsg->Length);
}

static BOOL TCProtocol(unsigned char *Data, unsigned short Length, TC_COMM_MSG *s_RxFrame)
{
	unsigned char PackBuff[MAX_DATA_BUF];
	uint16   PackLengthgth = 0, i;
	
	memset(PackBuff, sizeof(PackBuff), 0);
	
	UnPackMsg(Data+1, Length-2, PackBuff, &PackLengthgth);	//解包

	if (PackBuff[(PackLengthgth)-1] == Calc_Checksum(PackBuff, (PackLengthgth)-1))
	{		
		DecodeAppMsg(PackBuff, PackLengthgth-1, s_RxFrame);
		return TRUE;
	}
	else
	{
		u1_printf("COM3(%d):", PackLengthgth);
		for(i=0; i<PackLengthgth; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");
		u1_printf(" CRC Error :%02X\r\n", Calc_Checksum(PackBuff, (PackLengthgth)-1));
		return FALSE;
	}	
	
}
//协议栈0 应用报文接收观察者处理任务
static void TaskForTCLoraRxObser(void)
{
	static TC_COMM_MSG s_RxFrame;
	static unsigned char PackBuff[MAX_DATA_BUF];
	u16 i, PackStartCount = 0, PackEndCount = 0, RecLength = 0;
	static u8 s_PackCount = 0, s_PackStartAddr[10], s_PackEndAddr[10], s_RecStartFlag = FALSE;
	static u16 s_LastTime = 0, RecPackNum = 0;
	static u8 s_BuffStartNum = 0, s_BuffEndNum = 0, s_FullPackFlag = FALSE;
	u8 DataPackCount = 0;
	
	if(GetLoraUartRecFlag() && LORA_UART_CNT > 0)
	{	
		s_BuffStartNum = PACK_INIT_NUM;
		s_BuffEndNum = 0;
		s_FullPackFlag = FALSE;
		
		if(LORA_UART_CNT < MAX_DATA_BUF)
		{
//			memcpy(&PackBuff[RecPackNum], LORA_UART_BUF, LORA_UART_CNT);    
//			RecPackNum += LORA_UART_CNT;
			for(i=0; i<LORA_UART_CNT; i++)
			{
				if(LORA_UART_BUF[i] == BOF_VAL)
				{
					s_BuffStartNum = i;					
				}
				else if(LORA_UART_BUF[i] == EOF_VAL)
				{
					s_BuffEndNum = i;
				}
				
				if((s_BuffStartNum != PACK_INIT_NUM) && (s_BuffEndNum - s_BuffStartNum >= 14) && (s_BuffEndNum > 0))
				{
					DataPackCount = (s_BuffEndNum - s_BuffStartNum + 1);
					memcpy(&PackBuff[s_BuffStartNum], &LORA_UART_BUF[s_BuffStartNum], DataPackCount);
					RecPackNum += DataPackCount;
					s_FullPackFlag = TRUE;
					break;
				}				
			}
			
			if(s_FullPackFlag == FALSE)
			{
				memcpy(&PackBuff[RecPackNum], LORA_UART_BUF, LORA_UART_CNT);	
					
				RecPackNum += LORA_UART_CNT;				
			}
			
			if(RecPackNum > MAX_DATA_BUF)
			{
				u1_printf(" Buf Over\r\n");
				s_RecStartFlag = FALSE; 
				RecPackNum = 0;
				memset(PackBuff, 0, sizeof(PackBuff));
				Clear_Lora_Buff();
				return;
			}
			
			s_RecStartFlag = TRUE;	
			s_LastTime = GetSystem10msCount();
		}
		else
		{
			u1_printf(" Data Count Over\r\n");
			s_RecStartFlag = FALSE; 
			RecPackNum = 0;
			memset(PackBuff, 0, sizeof(PackBuff));
			Clear_Lora_Buff();
			return;
		}
		Clear_Lora_Buff();
	}
	
	if(PackBuff[0] == BOF_VAL && PackBuff[RecPackNum - 1] == EOF_VAL && RecPackNum >= 15)
	{
		//连包处理
		PackStartCount = 0;
		PackEndCount = 0;
		
		for(i=0; i<RecPackNum; i++)
		{
			if(PackBuff[i] == 0x7E)	//寻找包头个数
			{
				s_PackStartAddr[PackStartCount] = i;
				PackStartCount++;
			}
			else if(PackBuff[i] == 0x21)	//寻找包尾个数
			{
				s_PackEndAddr[PackEndCount] = i;
				PackEndCount++;
			}
		}
		
		if(PackStartCount == PackEndCount)	//是完整的数据包
		{
			if(PackStartCount > 1)
			{
				u1_printf("\r\n %d Lora Packs\r\n", PackStartCount);
			}
			s_PackCount = PackStartCount;
		}
		else
		{
			u1_printf(" StartCount != EndCount\r\n");
			s_RecStartFlag = FALSE; 
			RecPackNum = 0;
			memset(PackBuff, 0, sizeof(PackBuff));
			return;
		}
		
		if(s_PackCount >= 1 && s_PackCount <= 10)	//最多处理10个数据连包
		{
			for(i=0; i<s_PackCount; i++)
			{
				memset(s_RecDataBuff, 0 ,sizeof(s_RecDataBuff));				
//					memcpy(PackBuff+s_PackStartAddr[i]+1, s_PackEndAddr[i] - s_PackStartAddr[i] - 1, NB_RxdBuff, &PackLength);				
				RecLength = s_PackEndAddr[i] - s_PackStartAddr[i] + 1;
				
				if(RecLength > LORA_BUFF_MAX)
				{
					u1_printf(" Err Lora Length(%d)\r\n", RecLength);
					s_RecStartFlag = FALSE;
					memset(PackBuff, 0, sizeof(PackBuff));
					RecPackNum = 0;
					return;
				}
				
				memcpy(s_RecDataBuff, PackBuff+s_PackStartAddr[i], RecLength);
				
				if(TCProtocol(s_RecDataBuff, RecLength, &s_RxFrame))
				{
					OnRecLoraData(USART3, &s_RxFrame);
				}
			}		
		}
		s_RecStartFlag = FALSE;
		memset(PackBuff, 0, sizeof(PackBuff));
		RecPackNum = 0;

	}
	else
	{
			

	}
	
	if(s_RecStartFlag && (CalculateTime(GetSystem10msCount(), s_LastTime) >= 30)) 
	{
		u1_printf(" Lora Rec OverTime(%d)\r\n", RecPackNum);
		for(i=0; i<RecPackNum; i++)
		{
			u1_printf("%02X ", PackBuff[i]);
		}
		u1_printf("\r\n");	
		s_RecStartFlag = FALSE; 
		RecPackNum = 0;
		s_LastTime = GetSystem10msCount();
		memset(PackBuff, 0, sizeof(PackBuff));
		Clear_Lora_Buff();	
	}
}

void TCProtocolForLoraProcess(void)
{
	if(s_TCProtocolForLoraRunFlag)
	{
		TaskForTCLoraRxObser();
	}
}


