#ifndef __LED_H
#define __LED_H	 

#include "compileconfig.h"
#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V100)

#define		LED_POWER_PIN	GPIO_Pin_8
#define		LED_POWER_TYPE	GPIOA

#define		LED_NET_PIN		GPIO_Pin_14
#define		LED_NET_TYPE	GPIOB

#elif (HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)

#define		LED_POWER_PIN	GPIO_Pin_0
#define		LED_POWER_TYPE	GPIOA

#define		LED_NET_PIN		GPIO_Pin_1
#define		LED_NET_TYPE	GPIOA

#endif

void STM32_GPIO_Init(void);
void RunLED(unsigned short nMain100ms);

#endif
