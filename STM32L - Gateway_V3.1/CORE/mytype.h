//=================================
//
//estype.h: 数据结构头文件
//
//=================================
#ifndef _MYTYPE_H_
#define _MYTYPE_H_

#include "mydef.h"

//==============================================================================
//数据类型重定义
//==============================================================================
#define UCHAR	unsigned char
#define INT8	unsigned char
#define UINT8	unsigned char
#define INT16	short
#define UINT16	unsigned short
#define INT32	long
#define UINT32	unsigned long
#define BOOL	unsigned char

//#define UCHAR	unsigned char
#define int8	unsigned char
#define uint8	unsigned char
#define int16	short
#define uint16	unsigned short
#define int32	long
#define uint32	unsigned int
//#define BOOL	unsigned char

#define FP32	float
#define FP64	double

#define SUCHAR	static unsigned char
#define SINT8	static unsigned char
#define SUINT8	static unsigned char
#define SINT16	static int
#define SUINT16	static unsigned short
#define SINT32	static long
#define SUINT32	static unsigned long
#define SBOOL	static unsigned char

#define SFP32	static float
#define SFP64	static double

typedef struct _CHANNEL 
{
	UINT8 m_ucChannel;		//通道号
	UINT8 m_ucSensorType;	//类型
	UINT8 m_ucSensorEn;		//使能
	float m_fSensorFix;		//修正值
} CHANNELCONF;

//系统配置描述符
//typedef __packed struct
//{
//	UINT32 device_id;		//设备ID
//	UINT8 net_type;			//网络连接类型
//	UINT16 zigbee_net;		//zigbee网络号
//	UINT16 zigbee_node;		//zigbee节点号
//	UINT16 zigbee_gateway;	//zigbee网关号
//	UINT8 zigbee_channel;	//zigbee通信频道
//	UINT8 zigbee_topology;	//zigbee拓扑结构
//	UINT8 gprs_server[4];	//GPRS服务器IP地址
//	UINT16 gprs_port;		//GPRS服务器端口号
//	UINT8 retry_times;		//重发次数
//	UINT16 h_interval;		//心跳间隔
//	UINT16 d_interval;		//数据上报间隔
//	UINT16 s_interval;		//状态上报间隔
//}SYSTEM_CFG;


//设备参数数据结构定义
typedef struct _SYSTEMPRM
{
	UINT32	m_lDeviceId;			//设备ID
	UINT8	m_ucLocalNettype;		//Zigbee网络状态
	UINT16	m_nZigbeeNet;			//Zigbee网络号
	UINT16	m_nZigbeeAddr;			//Zigbee节点地址
	UINT16	m_nZigbeeGatewayAddr;	//Zigbee目标地址
	UINT8	m_ucZigbeeChannel;		//Zigbee通信频道
	UINT8 	m_ucZigbeeTopology;		//Zigbee拓扑类型

	UINT8	m_ucLinkNettype;		//与服务器通信方式
	UINT8	m_ucTcpServerIp[4];		//TCP服务器IP
	UINT16	m_nTcpServerPort;		//TCP服务器端口

	//通信链路维护参数
	UINT8	m_ucReLinkTime;			//重连次数
	UINT16	m_nHeartBeatInterval;	//心跳间隔时间

	//系统维护数据
	UINT8	m_ucAutoResetFlag;		//午夜重启标识
	UINT16	m_nAutoResetTime;		//午夜重启时间，高8位小时，低8位分钟

	UINT32	  m_ucCheck;

} SYSTEMPRMDEF;

//通信协议头格式
typedef __packed struct
{
	uint16 protocol;   	//协议版本
	uint32 device_id;	//设备号
	uint8 dir;			//方向
	uint16 seq_no;		//通信序号
	uint16 payload_len;	//信息域长度
	uint8 cmd;			//命令
}CLOUD_HDR;

//系统信息描述符
//typedef __packed struct
//{
//	uint16 software_ver;  	//软件版本
//	uint16 hardware_ver;	//硬件版本
//	uint8 pn[8];			//?? 	如:20150330
//	uint8 sn[8];			//?? 如:nostore
//}SYSTEM_INFO;

typedef __packed struct
{
	uint8 second;
	uint8 minute;
	uint8 hour;
	uint8 day;
	uint8 month;
	uint8 year;
}CLOUD_DATETIME;

//系统配置描述符
typedef __packed struct
{
	uint32 device_id;		//设备ID
	uint8 net_type;			//网络连接类型
	uint16 zigbee_net;		//zigbee网络号
	uint16 zigbee_node;		//zigbee节点号
	uint16 zigbee_gateway;	//zigbee网关号
	uint8 zigbee_channel;	//zigbee通信频道
	uint8 zigbee_topology;	//zigbee拓扑结构
	uint8 gprs_server[4];	//GPRS服务器IP地址
	uint16 gprs_port;		//GPRS服务器端口号
	uint8 retry_times;		//重发次数
	uint16 h_interval;		//心跳间隔
	uint16 d_interval;		//数据上报间隔
	uint16 s_interval;		//状态上报间隔
}SYSTEM_CFG;


#endif



