#ifndef		_WIFI_H_
#define		_WIFI_H_

#include "TCProtocolManage.h"

#define		WIFI_POWER_PIN		GPIO_Pin_8
#define		WIFI_POWER_TYPE		GPIOA

#define		WIFI_RST_PIN		GPIO_Pin_3
#define		WIFI_RST_TYPE		GPIOB

#define		WIFI_RST_HIGH()		GPIO_SetBits(WIFI_RST_TYPE, WIFI_RST_PIN)
#define		WIFI_RST_LOW()		GPIO_ResetBits(WIFI_RST_TYPE, WIFI_RST_PIN)

#define		WIFI_POWER_ON()		GPIO_SetBits(WIFI_POWER_TYPE, WIFI_POWER_PIN)
#define		WIFI_POWER_OFF()	GPIO_ResetBits(WIFI_POWER_TYPE, WIFI_POWER_PIN)

void WiFi_Init(void);

unsigned char Set_WiFi_AP(unsigned char *WiFiPassword, unsigned char lenth);

void AddWiFiRxObser(TCCOMMRXOBSER hRxObser);

void WiFi_Port_Init(void);

void WiFi_Port_Reset(void);

void RemoveWiFiCommunicationTask(void);

void WiFiProcess(void);


#endif












