#ifndef		_EEPROM_H_
#define 	_EEPROM_H_

#define EEPROM_BASE_ADDR	0x08080000	
#define EEPROM_BYTE_SIZE	0x0FFF

#define	EEPROM_BLOCK_SIZE	64
#define	EEPROM_BLOCK_COUNT	64


#define EN_INT      	__enable_irq();		//使能全局中断
#define DIS_INT     	__disable_irq();	//关闭全局中断

void EEPROM_ReadBytes(unsigned int Addr,unsigned char *Buffer,unsigned short Length);

void EEPROM_ReadWords(unsigned int Addr,unsigned int *Buffer,unsigned short Length);

void EEPROM_WriteBytes(unsigned int Addr,unsigned char *Buffer,unsigned short Length);

void EEPROM_WriteWords(unsigned int Addr,unsigned int *Buffer,unsigned short Length);

void EEPROM_EraseWords(unsigned int BlockNum);

unsigned char EEPROM_CheckSum(unsigned int addr, unsigned int lenth);



#endif



