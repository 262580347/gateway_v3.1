#ifndef		_GPSDRIVE_H_
#define		_GPSDRIVE_H_

#define		GPSPRODUCTINFORMATION		0x01
#define		GPSANTTEST					0x02
#define		GPSGETLOCATION				0x03

#define		GPS_POWER_PIN		GPIO_Pin_11
#define		GPS_POWER_TYPE		GPIOA

//#define		GPS_RESET_PIN		GPIO_Pin_5
//#define		GPS_RESET_TYPE		GPIOB


void GPSPowerOn(void);

void GPSPowerOff(void);

void GetPosition(float *Latitude, float *Longitude);

unsigned char GetGPSDataFlag(void);

float GetAltitude(void);

unsigned char GetGPSPowerFlag(void);
	
void GPSProcess(void);

void SetGPSPowerFlag(unsigned char isTrue);

unsigned char GetGPSRunFlag(void);

void SetGPSRunFlag(unsigned char isTrue);

float GetLongitude(void);

void SetLongitude(float temp);

float GetLatitude(void);

void SetLatitude(float temp);

void SetShowGPSInfoFlag(unsigned char isTrue);

#endif



