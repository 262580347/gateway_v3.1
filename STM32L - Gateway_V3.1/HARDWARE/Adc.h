#ifndef __ADC_H
#define __ADC_H	

#define 	ERROR_VALUE		-1000

#define		HWL_INPUT_COUNT		8



#include "compileconfig.h"
#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V100)

#define		BQ24650_EN_PIN		GPIO_Pin_13
#define		BQ24650_EN_TYPE		GPIOB

#define		POWER_ADC_PIN		GPIO_Pin_0
#define		POWER_ADC_TYPE		GPIOB

#define		BAT_FULL_PIN		GPIO_Pin_12
#define		BAT_FULL_TYPE		GPIOB

#define		ADC_ENABLE_GPIO_TYPE		GPIOB
#define 	ADC_ENABLE_GPIO_PIN			GPIO_Pin_1

#define		ADC_CHANNEL			ADC_Channel_8

#elif (HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)

#define		BQ24650_EN_PIN		GPIO_Pin_5
#define		BQ24650_EN_TYPE		GPIOA

#define		POWER_ADC_PIN		GPIO_Pin_6
#define		POWER_ADC_TYPE		GPIOA

#define		DC_POWER_ADC_PIN	GPIO_Pin_0
#define		DC_POWER_ADC_TYPE	GPIOB

#define		BAT_FULL_PIN		GPIO_Pin_4
#define		BAT_FULL_TYPE		GPIOA

#define 	ADC_ENABLE_GPIO_PIN			GPIO_Pin_7
#define		ADC_ENABLE_GPIO_TYPE		GPIOA

#define		ADC_CHANNEL			ADC_Channel_6

#define		ADC_DCPOWER_CHANNEL	ADC_Channel_8	//PB0��ӦCh8

#endif

#define		ADC_PIN_ENABLE()		GPIO_SetBits(ADC_ENABLE_GPIO_TYPE, ADC_ENABLE_GPIO_PIN)
#define		ADC_PIN_DISABLE()		GPIO_ResetBits(ADC_ENABLE_GPIO_TYPE, ADC_ENABLE_GPIO_PIN)

#define		BQ24650_ENABLE()		GPIO_ResetBits(BQ24650_EN_TYPE, BQ24650_EN_PIN)
#define		BQ24650_DISABLE()		GPIO_SetBits(BQ24650_EN_TYPE, BQ24650_EN_PIN)




void Adc_Init(void);

void Open_AdcChannel(void);

void Adc_Reset(void);

#endif 
