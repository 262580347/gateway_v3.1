/**********************************
说明：低功耗模式下的进入与退出管理
	  主要重新对时钟、外设、端口进行配置
	  清除对应标志位
	  
作者：关宇晟
版本：V2017.4.3
***********************************/
#include "main.h"
#include "LowPower.h"
 
unsigned char g_AwakeFlag = FALSE;

unsigned short s_AwakeCount = 0;	//唤醒计数

unsigned char g_RTCAlarmFlag = FALSE, g_Exti2Flag = FALSE;

unsigned short GetAwakeCount(void)
{
	return s_AwakeCount;
}

void ResetAwakeCount(void)
{
	s_AwakeCount = 0;
}

void Clear_Flag(void)
{
	g_PowerDetectFlag = FALSE;
	g_SHTGetFlag = FALSE;
	g_MAXGetFlag = FALSE;
}

void EXTI2_Config(void)
{
	EXTI_InitTypeDef   EXTI_InitStructure;
	GPIO_InitTypeDef   GPIO_InitStructure;
	NVIC_InitTypeDef   NVIC_InitStructure;
	/* Enable GPIOA clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	/* Configure PA0 pin as input floating */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Enable SYSCFG clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	/* Connect EXTI0 Line to PA0 pin */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource2);

	/* Configure EXTI0 line */
	EXTI_InitStructure.EXTI_Line = EXTI_Line2;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Enable and set EXTI0 Interrupt to the lowest priority */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void EXTI2_Reset(void)
{
	EXTI_InitTypeDef   EXTI_InitStructure;
	GPIO_InitTypeDef   GPIO_InitStructure;
	NVIC_InitTypeDef   NVIC_InitStructure;
	/* Enable GPIOA clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	/* Configure PA0 pin as input floating */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Enable SYSCFG clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, DISABLE);

	/* Configure EXTI0 line */
	EXTI_InitStructure.EXTI_Line = EXTI_Line2;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  
	EXTI_InitStructure.EXTI_LineCmd = DISABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Enable and set EXTI0 Interrupt to the lowest priority */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void EXTI17_Reset(void)
{
	EXTI_InitTypeDef   EXTI_InitStructure;
	NVIC_InitTypeDef   NVIC_InitStructure;
	/* Enable GPIOA clock */
	EXTI_ClearITPendingBit(EXTI_Line17);
	EXTI_InitStructure.EXTI_Line = EXTI_Line17;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = DISABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Enable the RTC Alarm Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = RTC_Alarm_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	RTC_ITConfig(RTC_IT_ALRA, DISABLE);
	RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
}
void Stop_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOH, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_3 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOC, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOH, &GPIO_InitStructure);	
		
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;//USART1
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);		
////	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14 | GPIO_Pin_15;//IIC
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);		
	
	GPIO_InitStructure.GPIO_Pin = LORA_AUX_PIN;	//LORA  AUX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(LORA_AUX_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = LORA_TX_PIN ; //LORA TX 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(LORA_UART_TYPE, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = LORA_RX_PIN; //LORA  RX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(LORA_UART_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = LORA_M0_PIN;//LORA M0 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = LORA_M1_PIN;//LORA M1 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(LORA_GPIO_TYPE, &GPIO_InitStructure);		
	
	GPIO_InitStructure.GPIO_Pin = LORA_PWR_PIN;   //LORA  PWR 上拉输出
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(LORA_PWR_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = POWER_ADC_PIN;   //AD7
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(POWER_ADC_TYPE, &GPIO_InitStructure);		
	
	GPIO_InitStructure.GPIO_Pin = ADC_ENABLE_GPIO_PIN ;	//ADC CTR
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(ADC_ENABLE_GPIO_TYPE, &GPIO_InitStructure);
}

void Stop_GPIO_Init2(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);	

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
		
////	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14 | GPIO_Pin_15;//IIC
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOC, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(GPIOH, &GPIO_InitStructure);		
}
//进入停止模式前的配置，GPIO、中断、外设，使用快速唤醒模式

void EnterStopMode(unsigned char Second)	
{  	
	u32 i;
	
	if(Second == 0)
	{
		return;
	}	
	
	TIM_ITConfig(TIM4,TIM_IT_Update,DISABLE );
	
	TIM_Cmd(TIM4, DISABLE); 
	
	if(GPIO_ReadInputDataBit(LORA_AUX_TYPE, LORA_AUX_PIN) == RESET)
	{
		while(GPIO_ReadInputDataBit(LORA_AUX_TYPE, LORA_AUX_PIN) == RESET);
		delay_ms(1);
	}
	
	USART_Cmd(USART3,DISABLE); 
	USART_DeInit(USART3);	
	__disable_irq();
	delay_ms(3);
	
	printf("\r\n Go to Stop\r\n");
	
	Clear_Uart3Buff();
	
	Clear_COMBuff();
	
	IWDG_ReloadCounter();
	
	ADC_DeInit(ADC1); 
			
	LORA_LOWPOWER_MODE;	
	
	Lora_Power(ENABLE);			
	
	EXTI2_Config();	
	
	RTC_SetAlarmA(Second);
	
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);	
		
	USART_Cmd(USART1, DISABLE);
	
	USART_DeInit(USART1);
	
	Stop_GPIO_Init();		
	/* Disable the SysTick timer */
	SysTick->CTRL &= (~SysTick_CTRL_ENABLE);

	/* Enable Power Interface clock */
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;

	/* configure MCU to go in stop mode after WFI instruction and not in standby */
	PWR->CR &= (~PWR_CR_PDDS);
	
	PWR->CR |= PWR_CR_FWU;
	
	/* configure MCU to go in stop/standby mode after WFI instruction and not in sleep */
	SCB->SCR |= SCB_SCR_SLEEPDEEP;

	/* Configure MCU to go in stop mode with regulator in low power mode */
	PWR->CR |= PWR_CR_LPSDSR;

	/* Disable VREFINT to save current */
//	PWR->CR |= PWR_CR_ULP;

	/* Disable PVDE to save current */
	PWR->CR &= (~PWR_CR_PVDE);
	
	__enable_irq();
    /* Wait for interrupt instruction, device go to sleep mode */
	__WFI();
	
	for(i=0; i<0x0fffff; i++)
	{
	  
	}

}

void EnterStopMode_SIM(unsigned char Second)	
{  	
	u32 i;
	
	if(Second == 0)
	{
		return;
	}	
	
	TIM_ITConfig(TIM4,TIM_IT_Update,DISABLE );
	
	TIM_Cmd(TIM4, DISABLE); 	
	
	USART_Cmd(USART2,DISABLE); 
	USART_DeInit(USART2);	
	__disable_irq();
	delay_ms(3);
	
	SIM_PWR_OFF;
	
	printf("\r\n Go to Stop\r\n");
	
	IWDG_ReloadCounter();
	
	ADC_DeInit(ADC1); 	
	
	RTC_SetAlarmA(Second);
	
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);	
		
	USART_Cmd(USART1, DISABLE);
	
	USART_DeInit(USART1);
	
	Stop_GPIO_Init2();		
	/* Disable the SysTick timer */
	SysTick->CTRL &= (~SysTick_CTRL_ENABLE);

	/* Enable Power Interface clock */
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;

	/* configure MCU to go in stop mode after WFI instruction and not in standby */
	PWR->CR &= (~PWR_CR_PDDS);
	
	PWR->CR |= PWR_CR_FWU;
	
	/* configure MCU to go in stop/standby mode after WFI instruction and not in sleep */
	SCB->SCR |= SCB_SCR_SLEEPDEEP;

	/* Configure MCU to go in stop mode with regulator in low power mode */
	PWR->CR |= PWR_CR_LPSDSR;

	/* Disable VREFINT to save current */
//	PWR->CR |= PWR_CR_ULP;

	/* Disable PVDE to save current */
	PWR->CR &= (~PWR_CR_PVDE);
	
	__enable_irq();
    /* Wait for interrupt instruction, device go to sleep mode */
	__WFI();
	
	for(i=0; i<0x0fffff; i++)
	{
	  
	}

}
void EnterSleepMode(void)
{
	printf("\r\n Go to SLeep\r\n");
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);	//等待串口发送完成
	USART_DeInit(USART1);
	
	USART_DeInit(USART3);
	
	Clear_Uart3Buff();
	
	LORA_LOWPOWER_MODE;	
	
	Lora_Power(ENABLE);	
	
	IWDG_ReloadCounter();
	
	ADC_DeInit(ADC1); 
		
	Stop_GPIO_Init();	
	
	EXTI2_Config();	

  /* Disable the SysTick timer */
  SysTick->CTRL &= (~SysTick_CTRL_ENABLE);

  /* configure MCU to go in sleep mode after WFI instruction and not in deep sleep */
  SCB->SCR &= (~SCB_SCR_SLEEPDEEP);

  /* Put FLASH in low-power mode during SLEEP */
  FLASH->ACR |= FLASH_ACR_SLEEP_PD;

#if ENABLE_DEBUG
  /* By setting DBG_SLEEP bit, we provide HCLK to the MCU during SLEEP, keeping the DBG connection */
  DBGMCU->CR |= DBGMCU_CR_DBG_SLEEP;
#endif

  /* Wait for interrupt instruction, device go to sleep mode */
  __WFI();	
}
void EnterStandbyMode(void)
{
	/* Enable Power Interface clock */
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;

	/* configure MCU to go in standby mode after WFI instruction and not in stop */
	PWR->CR |= PWR_CR_PDDS;

	/* configure MCU to go in stop/standby mode after WFI instruction and not in sleep */
	SCB->SCR |= SCB_SCR_SLEEPDEEP;

	/* Disable VREFINT to save current */
	PWR->CR |= PWR_CR_ULP;

	/* Disable PVDE to save current */
	PWR->CR &= (~PWR_CR_PVDE);

	/* Wait for interrupt instruction, device go to sleep mode */
	__WFI();	

}

void RCC_Config(void)
{
	/* HCLK = SYSCLK /1*/
	RCC->CFGR |= RCC_CFGR_HPRE_DIV1;
	/* PCLK2 = HCLK /1*/
	RCC->CFGR |= RCC_CFGR_PPRE2_DIV1;
	/* PCLK1 = HCLK /1*/
	RCC->CFGR |= RCC_CFGR_PPRE1_DIV1;

	/* Set wait state and prefetchaccording to the frequency */
	#if FHCLK > 8000000
	/* Enable 64-bit access */
	FLASH->ACR |= FLASH_ACR_ACC64;
	/* Enable Prefetch Buffer */
	FLASH->ACR |= FLASH_ACR_PRFTEN;
	/* Flash 1 wait state */
	FLASH->ACR |= FLASH_ACR_LATENCY;
	#endif

	/* Enable the HSE Bypass clock */
	RCC->CR |= RCC_CR_HSEBYP;
	/* Enable the HSE external clock */
	RCC->CR |= RCC_CR_HSEON;

	/* Wait until the HSE Bypass clock is ready */
	while (RCC_GetFlagStatus(RCC_FLAG_HSERDY) == RESET)
	{}

	/* Set HSE as system clock source */
	RCC_SYSCLKConfig(RCC_SYSCLKSource_HSE);

	/* Wait until the HSE is used as system clock source */
	while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSE)
	{}

	/* Enable Power Interface clock */
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	/* Configure the voltage scaling in Range 2 */
	PWR_VoltageScalingConfig(PWR_VoltageScaling_Range2);

	/* Set wait state and prefetchaccording to the frequency */
	#if FHCLK <= 8000000
	/* Disable 64-bit access */
	FLASH->ACR &= ~FLASH_ACR_ACC64;
	/* Disable Prefetch Buffer */
	FLASH->ACR &= ~FLASH_ACR_PRFTEN;
	/* Flash 0 wait state */
	FLASH->ACR &= ~FLASH_ACR_LATENCY;
	#endif		
		
	/* DISABLE all further Internal oscillators */
	RCC->CR &= (~RCC_CR_MSION) & (~RCC_CR_HSION) & (~RCC_CR_PLLON);

	/* ENABLE/DISABLE all peripheral clock */
	#if ENABLE_PERIPHERAL_CLOCK
	RCC->AHBENR = 0xFFFFFFFF;
	RCC->APB1ENR = 0xFFFFFFFF;
	RCC->APB2ENR = 0xFFFFFFFF;

	RCC->AHBLPENR  = 0xFFFFFFFF;
	RCC->APB1LPENR = 0xFFFFFFFF;
	RCC->APB2LPENR = 0xFFFFFFFF;

	#else
	/* keep GPIOAEN enable if user want to keep debug capability, will be configured in gpio.c */
	RCC->AHBENR = 0x00000001;
	RCC->APB1ENR = 0x00000000;
	RCC->APB2ENR = 0x00000000;

	RCC->AHBLPENR  = 0x00000000;
	RCC->APB1LPENR = 0x00000000;
	RCC->APB2LPENR = 0x00000000;
	#endif

}

void RCC_Default(void)
{
	/* Enable 64-bit access */
	FLASH->ACR |= FLASH_ACR_ACC64;
	/* Enable Prefetch Buffer */
	FLASH->ACR |= FLASH_ACR_PRFTEN;
	/* Flash 1 wait state */
	FLASH->ACR |= FLASH_ACR_LATENCY;

	/* Enable HSI - High-Speed Internal RC Oscilator */
	RCC->CR |= RCC_CR_HSION;

	/* Wait till HSI is stable and ready */
	while (RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET)
	{}

	/* Set HSI as system clock source */
	RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI);

	/* Wait until the HSI is used as system clock source */
	while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI)
	{}
		
	/* DISABLE all further Internal oscillators */
	RCC->CR &= (~RCC_CR_MSION) & (~RCC_CR_HSEON) & (~RCC_CR_HSEBYP) & (~RCC_CR_PLLON);	
}

void LowPower_Process(void)
{
	if(g_RTCAlarmFlag)			//闹钟唤醒
	{
		g_RTCAlarmFlag = FALSE;
		
		Wake_SysInit();
		
		printf("\r\n Alarm\r\n");
		
		EXTI2_Reset();
		
		EXTI17_Reset();
	}
	
	if(g_Exti2Flag)				//外部唤醒
	{	
		g_Exti2Flag = FALSE;
		
		Wake_SysInit();

		printf("\r\n Aux\r\n");
		
		EXTI2_Reset();
		
		EXTI17_Reset();
	}	
}
void RTC_Alarm_IRQHandler(void)	//闹钟中断
{
	SYSTEMCONFIG *p_sys;
	
	if(RTC_GetFlagStatus(RTC_FLAG_ALRAF) != RESET)
	{		
		SysClockForMSI(RCC_MSIRange_6);
		
		p_sys = GetSystemConfig();	
		
		if(p_sys->Type == NETTYPE_LORA)
			Awake_USART3_Config();
		else if(p_sys->Type == NETTYPE_SIM800C)
		{
//			SetSim800cResetFlag(1);
		}
		
		if(PWR_GetFlagStatus(PWR_FLAG_WU) != RESET)
		{
			PWR_ClearFlag(PWR_FLAG_WU);//????
		}
		
		EXTI_ClearITPendingBit(EXTI_Line17);
		
		RTC_ClearITPendingBit(RTC_IT_ALRA);
		
		g_RTCAlarmFlag = TRUE;
	}	
}


void EXTI2_IRQHandler(void)	//外部中断
{	
	SYSTEMCONFIG *p_sys;
	

	if(EXTI_GetITStatus(EXTI_Line2) != RESET)
	{			
		SysClockForMSI(RCC_MSIRange_6);
		
		p_sys = GetSystemConfig();			
		
		if(p_sys->Type == NETTYPE_LORA)
			Awake_USART3_Config();
		else if(p_sys->Type == NETTYPE_SIM800C)
		{
//			SetSim800cResetFlag(1);
		}
		
		if(PWR_GetFlagStatus(PWR_FLAG_WU) != RESET)
		{
			PWR_ClearFlag(PWR_FLAG_WU);//????
		}
		
		g_Exti2Flag = TRUE;
		
		EXTI_ClearITPendingBit(EXTI_Line2);
	}
	
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

