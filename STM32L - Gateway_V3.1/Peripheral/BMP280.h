#ifndef  _BMP_280_H_
#define  _BMP_280_H_

#include "mytype.h"

#define BMP_SENSOR_STATUS_NORMAL	0
#define BMP_SENSOR_STATUS_ERR		1


UINT8 GetBMP280SensorStatus( void );
void SetBMP280SensorStatus( UINT8 ucStatus );

UINT8 BMP280Detect( UINT16 nMain100ms );

float GetBMPTemp( void );
float GetBMPPress( void );
float GetBMPAltitude( void );



#endif



