/**
  ******************************************************************************
  * @file    Project/STM32L1xx_StdPeriph_Templates/main.h 
  * @author  MCD Application Team
  * @version V1.2.0
  * @date    16-May-2014
  * @brief   Header for main.c module
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx.h"
#include <string.h>
#include <stdio.h>  
#include <stdlib.h>
#include "sys.h"
#include "general_type.h"
#include "stdarg.h"	 
#include "stdio.h"
#include "string.h"
#include "CPU.h"
#include "Uart.h"
#include "Gpio.h"
#include "Timer.h"
#include "rtc.h"
#include "Adc.h"
#include "Uart_Com.h"
#include "Lora.h"
#include "SIM800C.h"
#include "SIM7020.h"
#include "SIM7600CE.h"
#include "ME909S.h"
//#include "LowPower.h"
#include "SysInit.h"
#include "mydef.h"
#include "mytype.h"
#include "map_in.h"
#include "data_id.h"
#include "compileconfig.h"
#include "Comm_Debug.h"
#include "GPRS.h"
#include "Protocol.h"
#include "DMA.h"
#include "PowerManage.h"
#include "Arithmetic.h"
#include "EEPROM.h"
#include "oled.h"
#include "LogStore.h"
#include "TCProtocolManage.h"
#include "GPSDrive.h"
#include "Wifi.h"
//#include "oledfont.h" 
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
