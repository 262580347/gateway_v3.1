#ifndef _PROTOCOL_H_
#define _PROTOCOL_H_

//通信异常码
#define ERR_CODE_NORMAL		(0x00) 	//数据正常
#define ERR_CODE_VERSION	(0x11)	//协议版本号错误
#define ERR_CODE_DEVICE_ID	(0x12) 	//设备ID错误
#define ERR_CODE_DATA		(0x13)	//数据校验错误
#define ERR_CODE_SENSOR_TYPE		(0x14)	//设备类型错误
#define ERR_CODE_CHECKSUM	(0x15)	//数据校验错误
#define ERR_CODE_DATA_LEN	(0x16)	//数据长度
#define	ERR_CODE_DATA_VERSION	(0x17)	//数据协议版本
#define ERR_CODE_SENSOR_CHANNEL	(0x18)

#define PROTOCOL_CONTROL_ID			(500) //采集器，控制器
#define PROTOCOL_GATEWAY_ID			(308) //网关
#define PROTOCOLVERSION		308

#define SENSOR_TYPE_CONTROLLER	100		//控制器

#define SENSOR_TYPE_GATEWAY		200		//网关

#include "mytype.h"
#include "stm32l1xx_usart.h"

unsigned int Mod_Report_Data(void);

void Mod_Send_Alive(void);

unsigned int System_set_config(USART_TypeDef* USARTx, unsigned char *data, unsigned int len);

unsigned int System_get_config(USART_TypeDef* USARTx, CLOUD_HDR *pMsg, unsigned char *data, unsigned int len);

void Mod_Transmit_Data(CLOUD_HDR *hdr, unsigned char *data, unsigned char lenth);

void Mod_Transmit_Vol_Data(unsigned char Type, CLOUD_HDR *hdr, unsigned short vol);

void Mod_Send_Ack(CLOUD_HDR *hdr, unsigned char *data, unsigned char lenth);

unsigned int Transpond_Alive(CLOUD_HDR *n_hdr);

unsigned int Mod_Auto_Alive(unsigned int DeviceID);

unsigned short ProcQuerySysPrm(CLOUD_HDR *pMsg, unsigned char *data, unsigned char lenth);

unsigned short ProcSetSysPrm(CLOUD_HDR *pMsg, unsigned char *data, unsigned char lenth);

void Mod_Transmit_RS485Data(CLOUD_HDR *hdr, unsigned char *data, unsigned char lenth);
	
void Mod_TCP_Test(void);

void Mod_7020_Test(void);

#endif
