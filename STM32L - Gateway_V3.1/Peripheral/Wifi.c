#include "Wifi.h"
#include "main.h"

#define 	COM_DATA_SIZE	256



#define		WIFI_BAND_RATE		115200
static void WiFiUartSendStr(char *str)
{
#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V100)

	WiFiUartSendStr(str);

#elif (HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)

	u2_printf(str);
	
#endif	
}


typedef __packed struct
{
	unsigned char 			magic; 	//0x55
	unsigned short 			length;	//存储内容长度	
	unsigned char 			chksum;	//校验和
	unsigned char 			data[100];		//设备ID
}WIFIPASSWORD;

static char s_WlanName[64];		//WLAN用户名

static char s_WlanPassword[64];	//WLAN密码


void WiFi_Port_Reset(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	USART_DeInit(USART2);
	USART_Cmd(USART2, DISABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	GPIO_ResetBits(GPIOA, GPIO_Pin_2);
	GPIO_ResetBits(GPIOA, GPIO_Pin_3);
	
	WIFI_RST_LOW();
	
	WIFI_POWER_OFF();
}

void WiFi_Port_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB , ENABLE);	 

	GPRS_Uart_Init(WIFI_BAND_RATE);
	
	delay_ms(200);
	
	GPIO_InitStructure.GPIO_Pin = WIFI_POWER_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(WIFI_POWER_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = WIFI_RST_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(WIFI_RST_TYPE, &GPIO_InitStructure);	
	

	WIFI_RST_HIGH();
	
	delay_ms(100);
	
	WIFI_POWER_ON();
	
	delay_ms(100);
	
	
}

static void ReadWlanName(void)
{
	u8 Name_Length, i;
	
	Name_Length = Check_Area_Valid(WIFI_USER_ADDR);
	if (Name_Length)
	{
		EEPROM_ReadBytes(WIFI_USER_ADDR, (u8 *)s_WlanName, sizeof(SYS_TAG) +  Name_Length);
		u1_printf("\r\n Name:");
		for(i=sizeof(SYS_TAG); i<sizeof(SYS_TAG) + Name_Length; i++)
		{
			u1_printf("%c", s_WlanName[i]);
		}
		u1_printf("\r\n");
	}
}

static void ReadWlanPassword(void)
{
	u8 Password_Length, i;
	
	Password_Length = Check_Area_Valid(WIFI_PASSWORD_ADDR);
	if (Password_Length)
	{
		EEPROM_ReadBytes(WIFI_PASSWORD_ADDR, (u8 *)s_WlanPassword, sizeof(SYS_TAG) +  Password_Length);
		u1_printf("\r\n Password:");
		for(i=sizeof(SYS_TAG); i<sizeof(SYS_TAG) + Password_Length; i++)
		{
			u1_printf("%c", s_WlanPassword[i]);
		}
		u1_printf("\r\n");
	}
}

static void TaskForWiFiCommunication(void)
{
	static u8 s_First = FALSE, WiFiStatus = 3, WiFiStep = 1, s_ErrCount = 0, s_RetryCount = 0, s_ErrAckCount = 0;
	static u8 s_CWLAPFirst = FALSE, s_WaitTime = 1;
	static unsigned int s_RTCSumSecond = 0, s_RTCReStart = 0, s_AliveRTCCount;
	char strWlan[128], str[40];
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	p_sys = GetSystemConfig();
	
	if(!s_First)
	{
		if(strncmp((char *)GPRS_UART_BUF, "compiled", GPRS_UART_CNT) == 0)
		{
			u1_printf("%s\r\n", GPRS_UART_BUF);
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			Clear_GPRS_Buff();
		}		
		else 
		{		
			
		}
		
		s_First = TRUE;
		g_TCRecStatus.LinkStatus = NotConnected;
		s_RTCReStart = GetRTCSecond();
		s_RTCSumSecond = GetRTCSecond();
	}
	
	if(g_TCRecStatus.LinkStatus != TCPConnected)
	{
		if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCReStart) > 300)
		{
			if(GetLockinTimeFlag())
			{
				ClearLockinTimeFlag();
				s_RTCReStart = GetRTCSecond();
			}
			else
			{		
				u1_printf("\r\n 5min Unconnect,Reboot\r\n");
				SetLogErrCode(LOG_CODE_RESET);
				StoreOperationalData();
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				Sys_Soft_Reset();
			}
		}
	}
	else
	{
		s_RTCReStart = GetRTCSecond();
	}
	
	switch(WiFiStatus)
	{	
		case 0:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) > 10)
			{
				s_RTCSumSecond = GetRTCSecond();
				u1_printf(" Check ESP Connect...\r\n");
			}
		break;
		
		case 1:
			s_RetryCount++;
			if(s_RetryCount >= 4)
			{
				s_RetryCount = 0;
				WiFiStatus = 0;
				break;
			}
			SetTCProtocolRunFlag(FALSE);
			SetTCModuleReadyFlag(FALSE);
			WiFi_Port_Reset();
			WIFI_POWER_OFF();
			u1_printf("WiFi Power Off...Wait 5s\r\n");
			
			WiFiStatus++;
			
			s_RTCSumSecond = GetRTCSecond();
		break;
				
			
		case 2:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) > 5)
			{
				WiFi_Port_Init();
				WIFI_POWER_ON();
				u1_printf("WiFi Power On...\r\n");
				WiFiStatus++;
				s_RTCSumSecond = GetRTCSecond();
			}
		break;
		
		case 3:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) > 1)
			{
				WiFiStatus++;
				WiFiStep = 1;		
				s_RTCSumSecond = GetRTCSecond();
			}
		break;
		
		case 4:

				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			

				switch(WiFiStep)
				{
					case 1:
						s_WaitTime = 2;
						WiFiUartSendStr("AT\r\n");
					break;
					case 2:
						if(!s_CWLAPFirst)
						{
							s_WaitTime = 2;
							s_CWLAPFirst = TRUE;
							WiFiUartSendStr("AT+GMR\r\n");
							break;
						}
						else
						{
							s_WaitTime = 1;
							WiFiStep = 3;
						}

					case 3:
						s_WaitTime = 1;
						WiFiUartSendStr("AT+CWMODE=1\r\n");
					break;
					
					case 4:
						WiFiUartSendStr("AT+CWAUTOCONN=0\r\n");
					break;
					case 5:
						s_WaitTime = 8;
						if(Check_Area_Valid(WIFI_USER_ADDR) && Check_Area_Valid(WIFI_PASSWORD_ADDR))
						{
							sprintf(strWlan, "AT+CWJAP=\"%s\",\"%s\"\r\n", &s_WlanName[4], &s_WlanPassword[4]);
							WiFiUartSendStr(strWlan);
						}
						else
						{
							WiFiUartSendStr("AT+CWJAP=\"ESP8266WIFI\",\"12344321\"\r\n");
						}
					break;
						
					case 6:		//尝试连接默认热点
						s_WaitTime = 8;
						WiFiUartSendStr("AT+CWJAP=\"ESP8266WIFI\",\"12344321\"\r\n");
					break;
					
					case 7:
						s_WaitTime = 1;
						WiFiUartSendStr("AT+CIPMUX=0\r\n");//设置单链接

					break;
					
					case 8:
						WiFiUartSendStr("AT+CIPMODE=1\r\n");

					break;
					
					case 9:
						u1_printf("[WIFI]Establishing TCP links\r\n");
						sprintf(str,"AT+CIPSTART=\"TCP\",\"%d.%d.%d.%d\",%d\r\n"
						,p_sys->Gprs_ServerIP[0]
						,p_sys->Gprs_ServerIP[1]
						,p_sys->Gprs_ServerIP[2]
						,p_sys->Gprs_ServerIP[3]
						,p_sys->Gprs_Port	);
					
//						u1_printf("%s", str);
						WiFiUartSendStr(str);
						s_WaitTime = 8;
					break;
					
					case 10:
						s_WaitTime = 2;
						WiFiUartSendStr("AT+CIPSEND\r\n");

					break;		
					
					case 20:
						WiFiUartSendStr("AT+CWQAP\r\n");

					break;
				}
				WiFiStatus++;
				s_RTCSumSecond = GetRTCSecond();
		break;
		
		case 5:
			
			if(s_ErrAckCount >= 5)
			{
				u1_printf(" Err Over\r\n");
				s_ErrAckCount = 0;
				WiFiStatus = 1;
				WiFiStep = 1;
				g_TCRecStatus.LinkStatus = NotConnected;
			}
			
			if(GetGPRSUartRecFlag())	//收到一帧数据包
			{			
				if(GPRS_UART_CNT >= USART3_REC_LEN)
				{
					GPRS_UART_BUF[USART3_REC_LEN-1] = 0;
					GPRS_UART_CNT = USART3_REC_LEN;							
				}
				
					u1_printf("%s\r\n", GPRS_UART_BUF);
				
				switch(WiFiStep)
				{
					case 1:
					case 3:
					case 4:
						if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
						{
							WiFiStep++;	
							WiFiStatus--;							
						}
						else if((strstr((const char *)GPRS_UART_BUF,"ERROR")) != NULL )
						{
							s_ErrAckCount++;
						}
					break;
					
					case 2:
						if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
						{
							WiFiStep++;	
							WiFiStatus--;	
						}
						
					break;
					
					case 5:	//连接存储的地址
						if((strstr((const char *)GPRS_UART_BUF,"WIFI CONNECTED")) != NULL )
						{
							s_RTCSumSecond = GetRTCSecond();
						}
						else if((strstr((const char *)GPRS_UART_BUF,"WIFI GOT IP")) != NULL )
						{
							s_RTCSumSecond = GetRTCSecond();
						}
						else if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
						{
							g_TCRecStatus.LinkStatus = GotIP;
							WiFiStep = 7;
							WiFiStatus--;	
						}
						else if((strstr((const char *)GPRS_UART_BUF,"WIFI DISCONNECT")) != NULL )
						{
							g_TCRecStatus.LinkStatus = NotConnected;
							u1_printf("\r\n WIFI Password Error\r\n");
							StoreOperationalData();
							WiFiStep = 6;
							WiFiStatus--;	
						}
						else if((strstr((const char *)GPRS_UART_BUF,"FAIL")) != NULL )
						{
							u1_printf(" Retry\r\n");
							WiFiStatus--;	
							s_ErrAckCount++;
						}
						else if((strstr((const char *)GPRS_UART_BUF,"ERROR")) != NULL )//收到ERROR代表用户或密码错误或不存在该热点，做其他处理
						{
							u1_printf(" No AP\r\n");
							WiFiStep = 6;
							WiFiStatus--;	
						}
						
					break;
					
					case 6:		//连接默认热点
						if((strstr((const char *)GPRS_UART_BUF,"WIFI CONNECTED")) != NULL )
						{
							s_RTCSumSecond = GetRTCSecond();
						}
						else if((strstr((const char *)GPRS_UART_BUF,"WIFI GOT IP")) != NULL )
						{
							s_RTCSumSecond = GetRTCSecond();
						}
						else if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
						{
							g_TCRecStatus.LinkStatus = GotIP;
							WiFiStep = 7;
							WiFiStatus--;	
						}
						else if((strstr((const char *)GPRS_UART_BUF,"WIFI DISCONNECT")) != NULL )
						{
							g_TCRecStatus.LinkStatus = NotConnected;
							u1_printf("\r\n WIFI Password Error\r\n");
							WiFiStep = 5;
							s_ErrAckCount++;
							WiFiStatus--;	
							delay_ms(100);
						}
						else if((strstr((const char *)GPRS_UART_BUF,"FAIL")) != NULL )
						{
							u1_printf(" Retry\r\n");
							WiFiStep = 5;
							WiFiStatus--;	
							s_ErrAckCount++;
						}
						else if((strstr((const char *)GPRS_UART_BUF,"ERROR")) != NULL )//收到ERROR代表用户或密码错误或不存在该热点，做其他处理
						{
							u1_printf(" No AP\r\n");
							WiFiStep = 5;
							WiFiStep = 1;
							WiFiStatus = 1;	
						}
						
					break;
						
					case 7:
					case 8:	
						if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
						{
							WiFiStep++;	
							WiFiStatus--;					
						}
					break;
						
					case 9:
						if((strstr((const char *)GPRS_UART_BUF,"CONNECT")) != NULL )
						{
							WiFiStep++;	
							WiFiStatus--;							
						}
						else if((strstr((const char *)GPRS_UART_BUF,"ERROR")) != NULL )						
						{	
							WiFiStatus--;		
							delay_ms(500);
							s_ErrAckCount++;
						}
							
					break;
						
					case 10:
						if((strstr((const char *)GPRS_UART_BUF,">")) != NULL )
						{
							WiFiStep = 1;	
							WiFiStatus++;								
						}
						else if((strstr((const char *)GPRS_UART_BUF,"ERROR")) != NULL )						
						{	
							WiFiStatus--;		
							delay_ms(500);
							s_ErrAckCount++;
						}
					break;	
					
					
						
				}
				
				if(s_ErrAckCount >= 5)
				{
					WiFiStatus = 1;
					WiFiStep = 1;
					s_ErrAckCount = 0;
				}
					
				Clear_GPRS_Buff();

			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= s_WaitTime)
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				WiFiStatus--;
				u1_printf(" AT No Ack\r\n");
				g_TCRecStatus.LinkStatus = NotConnected;
				s_ErrCount++;
				s_RTCSumSecond = GetRTCSecond();
				if(s_ErrCount >= 4)
				{
					s_ErrCount = 0;
					WiFiStatus = 1;
					WiFiStep = 1;
				}
			}
			
			
		break;
		
		case 6:
			u1_printf("\r\n Success to TCP Connect\r\n");
			SetTCProtocolRunFlag(TRUE);
			SetTCModuleReadyFlag(TRUE);
			WiFiStatus++;
			g_TCRecStatus.LinkStatus = TCPConnected;
			g_TCRecStatus.LastLinkTime = GetRTCSecond();
		break;
		
		case 7:
			if(g_TCRecStatus.ResetFlag)
			{
				g_TCRecStatus.ResetFlag = FALSE;
				u1_printf(" \r\n WIFI Retry Connect\r\n");
				SetTCProtocolRunFlag(FALSE);
				SetTCModuleReadyFlag(FALSE);
				g_TCRecStatus.LinkStatus = NotConnected;
				s_ErrCount = 0;
				WiFiStatus = 1;
				WiFiStep = 1;
				s_RTCSumSecond = GetRTCSecond();
			}
			else if(GetTCModuleReadyFlag())		//SIM就绪
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), s_AliveRTCCount) >= p_sys->Heart_interval)	
				{
					g_TCRecStatus.AliveAckFlag = FALSE;
					s_AliveRTCCount = GetRTCSecond();
					Mod_Send_Alive();	//发送心跳包
					delay_ms(20);
					Mod_Report_Data();	//上报网关电压数据
					s_RTCSumSecond = GetRTCSecond();
					WiFiStatus++;			
				}						
			}
		break;
			
		case 8:
			if(g_TCRecStatus.AliveAckFlag)
			{
				g_TCRecStatus.AliveAckFlag = FALSE;
				WiFiStatus--;
			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= 20)
			{
				WiFiStatus++;
			}
		break;
			
		case 9:
			if(DifferenceOfRTCTime(GetRTCSecond(), g_TCRecStatus.LastLinkTime) >= 120)
			{
				g_TCRecStatus.ResetFlag = TRUE;				
			}
			WiFiStatus = 7;
		break;
		
		
		default:
			
		break;
		
	}
	

}

static void WiFi_Init(void)
{			
	ReadWlanName();
	
	ReadWlanPassword();	
	
	WiFi_Port_Init();

	SetTCProtocolRunFlag(FALSE);
	SetTCModuleReadyFlag(FALSE);
		
}

void WiFiProcess(void)
{
	static u8 s_First = FALSE, s_RunFlag = FALSE;
	static u32 s_RTCTime = 0;
	
	if(s_First == FALSE)
	{
		s_First = TRUE;
			
		s_RTCTime = GetRTCSecond();
		
		u1_printf("WiFi Init...Wait 3s\r\n");
		
		WiFi_Port_Reset();
	}
	
	if((DifferenceOfRTCTime(GetRTCSecond(), s_RTCTime) >= 1)  && s_RunFlag == FALSE)
	{
		WiFi_Init();
		InitGprsLinkedList();		
		s_RunFlag = TRUE;
		WIFI_POWER_ON();
	}
	
	if(s_RunFlag)
	{
		TaskForWiFiCommunication();
	}
}
