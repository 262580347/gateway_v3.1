#ifndef __MAP_IN_H
#define __MAP_IN_H

//输入通道配置描述符
typedef __packed struct
{
	unsigned short int data_id;			//数据标识
	float compensation;		//补偿值
	unsigned char in_type;			//输入类型
	unsigned char in_param[8];		//输入类型相关参数	in_param[0]--关联硬件通道、ADC或脉冲或数字端口；
	unsigned char spec_len;			//特定关联数据长度
	unsigned char spec_data[];		//特定关联数据
}MAP_IN_ITEM;

//Soil Humidity变换公式参数描述
//=========================================================================
//电流输出值(mA)	土湿值（%）
//   X ≤4.06		0
//mark_val1					coef1_val1 coef1_val2 coef1_val3 coef1_val4
//4.06 ≤ X ≤ 18.607		y = 0.045x3 - 1.446x2 + 16.03x - 44.23
//mark_val2					coef2_val1  coef2_val2
//18.607 ≤ X ≤20		y = 39.49x - 689.8
//=========================================================================
typedef __packed struct
{
	float mark_val1;	//0--分界1---分界2--20mA
	float mark_val2;
	float coef1_val1;	//公式1
	float coef1_val2;	//公式1
	float coef1_val3;	//公式1
	float coef1_val4;	//公式1
	float coef2_val1;	//公式2
	float coef2_val2;	//公式2
}SOILWATER_SPEC;

//传感器高低量程描述符
typedef __packed struct
{
	float low_limit;   //量程低限
	float high_limit;  //量程高限
}RANGE_SPEC;

#define MAP_IN_ITEM_SIZE	64

unsigned char calc_area_chksum(unsigned char *data, unsigned int len);
MAP_IN_ITEM *get_map_in_item(unsigned int index);
unsigned int set_map_in_item(unsigned int index, MAP_IN_ITEM *item);

#endif
