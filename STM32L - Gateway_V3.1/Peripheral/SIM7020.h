#ifndef  _SIM7020_H_
#define	 _SIM7020_H_	

#include "compileconfig.h"
#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V100)

#define		SIM7020_ENABLE_TYPE		GPIOA		
#define		SIM7020_ENABLE_PIN		GPIO_Pin_6

#elif (HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)

#define		SIM7020_ENABLE_TYPE		GPIOA		
#define		SIM7020_ENABLE_PIN		GPIO_Pin_8

#endif

#define		SIM7020_PWR_ON()			GPIO_SetBits(SIM7020_ENABLE_TYPE, SIM7020_ENABLE_PIN);
#define		SIM7020_PWR_OFF()			GPIO_ResetBits(SIM7020_ENABLE_TYPE, SIM7020_ENABLE_PIN);

void SIM7020PortInit(void);

void SIM7020Process(unsigned short nMain10ms);

unsigned char GetSIM7020ReadyFlag( void );

void SetSIM7020ResetFlag( unsigned char ucData );

void SetSIM7020ReadyFlag( unsigned char ucFlag );

void SIM7020TestProcess(void);

void WaitSendOK(void);

int EnterSendMode(void);

unsigned char GetTCPSocket(void);

#endif


