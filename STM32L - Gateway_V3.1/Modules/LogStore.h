#ifndef		_LOGSTORE_H_
#define		_LOGSTORE_H_

#define		LOG_CODE_CLEAR				0XFF		//清错误码
#define		LOG_CODE_NOMODULE			0		//SIM模块不存在
#define		LOG_CODE_WEAKSIGNAL			1		//信号弱
#define		LOG_CODE_UNATTACH			2		//附着不上
#define		LOG_CODE_TIMEOUT			3		//连接超时
#define		LOG_CODE_NOLORA				4		//没有LORA模块
#define		LOG_CODE_ATTIMEOUT			5		//AT指令超时
#define		LOG_CODE_NOSERVERACK		6		//没有服务器应答
#define		LOG_CODE_NO_CARD			7		//没有SIM卡


#define		LOG_CODE_OFFLINE			8		//网关离线
#define		LOG_CODE_LOWPOWER			9		//网关进入省电模式
#define		LOG_CODE_HARDERR			10		//网关硬件错误
#define		LOG_CODE_START				11		//启动标志
#define		LOG_CODE_RESET				12		//重启标记
#define		LOG_CODE_UNDEFINE			13		//运营商未识别
#define		LOG_CODE_GPS_OK				14		//GPS获取成功
#define		LOG_CODE_GPS_FAIL			15		//GPS获取失败



void LogStorePointerInit(void);

void SetLogErrCode(unsigned short ErrCode);

void ClearLogErrCode(unsigned short ErrCode);

void StoreOperationalData(void);

void ShowLogContent(void);

void SetCSQValue(unsigned short data);

void SetLinkNetTime(unsigned short Time);

unsigned short GetCSQValue(void);

unsigned short GetLinkNetTime(void);





#endif

