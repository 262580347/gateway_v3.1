/**********************************
说明:与CPU相关的一些常用功能函数
	  延时、获取芯片ID、时钟配置、看门狗、FLASH读写
	  
作者:关宇晟
版本:V2017.4.3
***********************************/
#include "main.h"
#include "CPU.h"

#define IWDG_OVER_TIMER   26
//STM32 12位ID 存储首地址
#define STM32_CHIPID_START_ADDR 0x1FFFF7E8
//STM32 ID长度,单位:字节
#define STM32_ID_SIZE   12


static u16  fac_us=0;//us延时倍乘数
static u16 fac_ms=0;//ms延时倍乘数

void Delay_Init(u32 SYSCLK)	 
{
	fac_us=1;	//为系统时钟的1/8  
	fac_ms=(u16)fac_us*1000;//非ucos下,代表每个ms需要的systick时钟数   	
}								    
		    								   
void delay_us(u32 nus)	
{		
	u32 temp;	

	if(nus < 2)
	{
		nus = 2;
	}
	SysTick->LOAD=(u32)(nus*fac_us*1000/1907); //时间加载	  		30516  261K 
	SysTick->VAL=0x00;        //清空计数器
	SysTick->CTRL|=SysTick_CTRL_ENABLE_Msk ;          //开始倒数	 
	do
	{
		temp=SysTick->CTRL;
	}
	while(temp&0x01&&!(temp&(1<<16)));//等待时间到达   
	SysTick->CTRL&=~SysTick_CTRL_ENABLE_Msk;       //关闭计数器
	SysTick->VAL =0X00;       //清空计数器	 
}

void delay_ms(u16 nms)
{	 		  	  
	u32 temp;		   
	SysTick->LOAD=(u32)(nms*fac_ms*1000/1907);//时间加载(SysTick->LOAD为24bit)
	SysTick->VAL =0x00;           //清空计数器
	SysTick->CTRL|=SysTick_CTRL_ENABLE_Msk ;          //开始倒数  
	do
	{
		temp=SysTick->CTRL;
	}
	while(temp&0x01&&!(temp&(1<<16)));//等待时间到达   
	SysTick->CTRL&=~SysTick_CTRL_ENABLE_Msk;       //关闭计数器
	SysTick->VAL =0X00;       //清空计数器	  	    
} 

void GetSTM32ChipID(void)
{
	unsigned int ID1,ID2,ID3;
	u8 i, ID_Buf[12];

	ID1 = *((unsigned int *)STM32_CHIPID_START_ADDR);
    ID2 = *((unsigned int *)STM32_CHIPID_START_ADDR+4);
	ID3 = *((unsigned int *)STM32_CHIPID_START_ADDR+8);
    ID_Buf[0]  = (u8)(ID1>>24);
	ID_Buf[1]  = (u8)(ID1>>16);
	ID_Buf[2]  = (u8)(ID1>>8);
	ID_Buf[3]  = (u8)(ID1>>0);
	ID_Buf[4]  = (u8)(ID2>>24);
	ID_Buf[5]  = (u8)(ID2>>16);
	ID_Buf[6]  = (u8)(ID2>>8);
	ID_Buf[7]  = (u8)(ID2>>0);
	ID_Buf[8]  = (u8)(ID3>>24);
	ID_Buf[9]  = (u8)(ID3>>16);
	ID_Buf[10] = (u8)(ID3>>8);
	ID_Buf[11] = (u8)(ID3>>0);
	printf("[Hardware]STM32 Only ID :"); 
	for(i=0; i<12; i++)
		printf("%x ",ID_Buf[i]);
	printf("\r\n");
}

void SetIWDG(unsigned int msTime)
{
	if(msTime>IWDG_OVER_TIMER)
	{
		msTime = IWDG_OVER_TIMER;
	}
   	if(msTime>0)
	{
		IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
		IWDG_SetPrescaler(IWDG_Prescaler_256);
		IWDG_SetReload(156*msTime);  //Max: 0xfff
		IWDG_ReloadCounter();
		IWDG_Enable();	
	}
}

void SysClockForHSE(void)
{
	RCC_DeInit();
	
	RCC_HSEConfig(RCC_HSE_ON);
	
	RCC_WaitForHSEStartUp();	
	   
	RCC_HCLKConfig(RCC_SYSCLK_Div1);
	RCC_PCLK1Config(RCC_HCLK_Div2);     
	RCC_PCLK2Config(RCC_HCLK_Div1);  
	FLASH_SetLatency(FLASH_Latency_0);       
	FLASH_PrefetchBufferCmd(ENABLE);     

	RCC_PLLConfig(RCC_PLLSource_HSE, RCC_PLLMul_3, RCC_PLLDiv_3);     
	
	RCC_PLLCmd(ENABLE);	//启动PLL        
	while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET);     
	
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);     
	while(RCC_GetSYSCLKSource()!=RCC_CFGR_SWS_PLL);    
}
void SysClockForMSI(uint32_t RCC_MSIRange)
{
	RCC_DeInit();
	
	RCC_MSICmd(ENABLE);
	
	while (RCC_GetFlagStatus(RCC_FLAG_MSIRDY) == RESET);

	RCC_MSIRangeConfig(RCC_MSIRange);
     
	RCC_HCLKConfig(RCC_SYSCLK_Div1); 
	RCC_PCLK1Config(RCC_HCLK_Div1);       
	RCC_PCLK2Config(RCC_HCLK_Div1);    
	
	FLASH_SetLatency(FLASH_Latency_0);       
	FLASH_PrefetchBufferCmd(ENABLE);    

	RCC_SYSCLKConfig(RCC_SYSCLKSource_MSI);

	while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_MSI);		
}

void Updata_IWDG(u16 nMain100ms)
{
	u8 i ;
	static u16 s_LastTime = 0;
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	p_sys = GetSystemConfig();	
	
	if(CalculateTime(nMain100ms, s_LastTime) > 1)
	{
		s_LastTime = nMain100ms;
		IWDG_ReloadCounter();	
	}
	
	if(p_sys->AutoResetFlag)		//午夜重启标志
	{
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);

		if((RTC_TimeStructure.RTC_Hours == p_sys->AutoResetTime>>8) && (RTC_TimeStructure.RTC_Minutes == (p_sys->AutoResetTime&0xff)))	//重启时间是否吻合
		{
			if(RTC_TimeStructure.RTC_Seconds <= 10)
			{					
				u1_printf("\r\n Time to reset,reboot!\r\n");
				for(i=10; i>0; i--)
				{
					delay_ms(1000);
					u1_printf(" Time:%d\r\n", i);
				}
				delay_ms(1200);	
				SetLogErrCode(LOG_CODE_RESET);
				StoreOperationalData();				
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				Sys_Soft_Reset();
			}
		}
	}
}
