#ifndef  _LOW_POWER_H_
#define  _LOW_POWER_H_

#define		MEASUREMENT_INTERVAL  1 

extern unsigned char g_RTCAlarmFlag,  g_Exti2Flag ;
	
void EnterStopMode(unsigned char Second);
void EnterStopMode_SIM(unsigned char Second);	
void EnterStandbyMode(void);
void EnterSleepMode(void);

void RCC_Default(void);
void RCC_Config(void);

void EXTI2_Config(void);
void EXTI2_Reset(void);
void EXTI17_Reset(void);
unsigned short GetAwakeCount(void);
void ResetAwakeCount(void);
void Clear_Flag(void);
void Exit_Awake_Process(unsigned short g_nMain10ms);

void LowPower_Process(void);
	
extern unsigned char g_AwakeFlag;
#endif
