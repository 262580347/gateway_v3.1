#include "main.h"
#include "SIM7020.h"

#define COM_DATA_SIZE	256

#define APN_CMNET 	1		//中国移动

#define APN_UNINET 	2		//中国联通

//static u8 s_SignalStrength = 0;		//信号强度

static u8 s_TCPSocket = 0;

static void SimUartSendStr(char *str)
{
#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V100)

	u3_printf(str);

#elif (HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)

	u2_printf(str);
	
#endif	
}
unsigned char GetTCPSocket(void)
{
	return s_TCPSocket;
}

int EnterSendMode(void)
{
	int i=99999;
	
	SimUartSendStr("AT+CIPSEND\r");
	
	while(i--)
	{
		if(GetGPRSUartRecFlag())
		{
			if(strstr((char *)GPRS_UART_BUF, ">"))
			{
//				u1_printf(" 开始发送...\r\n");
				Clear_GPRS_Buff();
				return 1;
			}	
			Clear_GPRS_Buff();			
		}
		IWDG_ReloadCounter();	
	}
	Clear_GPRS_Buff();
	u1_printf(" Enter fail\r\n");
	return 0;
}

void WaitSendOK(void)
{
	int i=99999;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	while(i--)
	{
		if(GetGPRSUartRecFlag())
		{
			u1_printf("%s\r\n", GPRS_UART_BUF);
			if(strstr((char *)GPRS_UART_BUF, "DATA ACCEPT"))
			{
//				u1_printf(" 发送成功\r\n");
				Clear_GPRS_Buff();
				return ;
			}	
			Clear_GPRS_Buff();			
		}
		IWDG_ReloadCounter();	
	}
	Clear_GPRS_Buff();
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
	u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);	
	u1_printf(" Send Fail\r\n");
	return ;
}
void SIM7020PortInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA , ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = SIM7020_ENABLE_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(SIM7020_ENABLE_TYPE, &GPIO_InitStructure);	
}

void SIM7020TestProcess(void)
{
	static unsigned char s_First = FALSE;
	unsigned char i = 0;
	
	if(s_First == FALSE)
	{
		s_First = TRUE;
		SIM7020PortInit();
		SIM7020_PWR_OFF();
		GPRS_Uart_Init(SIM_BAND_RATE);
		delay_ms(2000);
		u1_printf(" [NB]Power on\r\n");
		SIM7020_PWR_ON();
	}
	
	if(GetGPRSUartRecFlag())
	{
		u1_printf("nb:%s\r\n", GPRS_UART_BUF);
		for(i=0; i<GPRS_UART_CNT; i++)
		{
			u1_printf("%02X ", GPRS_UART_BUF[i]);
		}
		u1_printf("\r\n");
		Clear_GPRS_Buff();
	}
	
}
void SIM7020Process(unsigned short nMain10ms)
{
	u32 i;
	u16 nTemp = 0;
	static unsigned int s_ErrCount = 0;
	static u16 s_LastTime = 0, s_CheckCSQTime = 0, s_OverCount = 0, s_LastTime2 = 0, s_Interval = 50;
	static u8 s_Status = 1, s_Step = 1, RunOnce = FALSE,  s_SimSavingFlag = FALSE;
	static u8 s_ATAckCount = 0, s_SavingModeFlag = FALSE, s_RetryCount = 0;
	char * p1 = NULL, buf[10], str[100];
	static u16 s_StartTime = 0, s_EndTime = 0, VolCount = 0;
	
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	p_sys = GetSystemConfig();
	
	if(RunOnce == FALSE)
	{
		RunOnce = TRUE;
		s_LastTime = GetSystem10msCount();
		InitGprsLinkedList();
		u1_printf("\r\n Initializes the downstream data buffer...\r\n");
	}
			
//	if(s_Status < 20)
//	{
//		if(GetGPRSUartRecFlag())
//		{
//			u1_printf("NB:%s\r\n", GPRS_UART_BUF);
//			Clear_GPRS_Buff();
//		}
//	}
	
	switch(s_Status)
	{	
		case 1:   //模块断电
			s_RetryCount++;
			SetTCProtocolRunFlag(FALSE);
		
			if(s_RetryCount >= 5)
			{
				u1_printf("\r\n Timeout not connected to the network waiting for restart...\r\n");
				SetLogErrCode(LOG_CODE_TIMEOUT);
				SetLogErrCode(LOG_CODE_RESET);
				StoreOperationalData();
				while (DMA_GetCurrDataCounter(DMA1_Channel4));
				Sys_Soft_Reset();
			}
			SIM7020PortInit();
			SIM7020_PWR_OFF();
			GPRS_Uart_Init(SIM_BAND_RATE);
			s_LastTime = GetSystem10msCount();
			s_Status++;
			u1_printf(" [SIM]Power off,delay 5s\r\n");
			s_StartTime = GetSystem10msCount();
		break;
		 
		case 2:	//断电后延时5S上电，模块上电自动开机
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 500 )
			{
				u1_printf(" [SIM]Power on,delay 20s.\r\n");
				SIM7020_PWR_ON();
				s_LastTime = GetSystem10msCount();
				s_Status++;
			}
		break;
			
		case 3: 	//上电完成后，初始化端口
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 2000 )
			{
				u1_printf(" [SIM]Check baudrate\r\n");
				SimUartSendStr("AT\r");
				delay_ms(50);
				SimUartSendStr("AT\r");
				delay_ms(50);
				Clear_GPRS_Buff();
				s_LastTime = GetSystem10msCount();
				s_Status++;
			}
		break;
			
		case 4:  	//检测模块的波特率
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 5 )
			{
				Clear_GPRS_Buff();
				SimUartSendStr("AT\r");		//发送"AT\r"，查询模块是否连接，返回"OK"
				u1_printf(" [SIM]AT\r\n");
				s_LastTime = GetSystem10msCount();
				s_Status++;
			}
		break;
		
		case 5:	//延时100MS,检测响应数据
			if(GetGPRSUartRecFlag())
			{		
				if(strstr((char *)GPRS_UART_BUF, "OK"))
				{
					s_ErrCount = 0;
					s_Status++;	
					u1_printf(" 模块串口通信正常\r\n");
				}			
				Clear_GPRS_Buff();
				
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 15 )
			{				
					u1_printf(" [SIM]OK无响应\r\n");
					s_Status--;   //回上一个状态
					s_ErrCount++;   //重试次数加1
					if(s_ErrCount >= 4 )
					{
						SetLogErrCode(LOG_CODE_NOMODULE);
						StoreOperationalData();
						s_ErrCount = 0;
						s_Status = 1;	//模块重新上电初始化
					}						
			}
	 	break;
		
		case 6:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= s_Interval) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d](%d)->\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds, s_Step);			

				if(GetGPRSUartRecFlag())
				{
					u1_printf("NB:%s\r\n", GPRS_UART_BUF);
					Clear_GPRS_Buff();
				}
				switch(s_Step)
				{
					case 1:
						SimUartSendStr("ATE0\r");
//						u1_printf("[SIM]->ATE0\r\n");
					break;
					case 2:
						SimUartSendStr("AT+CPIN?\r");
//						u1_printf("AT+CPIN?\r\n");
					break;
					case 3:
						SimUartSendStr("AT+CCID\r");
//						u1_printf("AT+CCID\r\n");
					break;
					case 4:
						SimUartSendStr("AT+CIMI\r");
//						u1_printf("AT+CIMI\r\n");
					break;
					case 5:
						SimUartSendStr("AT+CSQ\r");
//						u1_printf("AT+CSQ\r\n");
					break;
					case 6:
						SimUartSendStr("AT+COPS?\r");
//						u1_printf("AT+COPS?\r\n");
					break;	
					case 7:
//						SimUartSendStr("AT+CSTT\r");
//						u1_printf("AT+CSTT\r\n");
						sprintf(str,"AT+CSOCL=%d\r", s_TCPSocket);
						SimUartSendStr(str);
						u1_printf(str);
					break;				
					case 8:
//						SimUartSendStr("AT+CIICR\r");
//						u1_printf("AT+CIICR\r\n");
						SimUartSendStr("AT+CSOC=1,1,1\r");
						u1_printf("AT+CSOC=1,1,1\r\n");
					break;		
					case 9:
						SimUartSendStr("AT+CIFSR\r");
						u1_printf("AT+CIFSR\r\n");
					break;	
					case 10:
						SimUartSendStr("AT+CIPQSEND=1\r");
//						u1_printf("AT+CIPQSEND=1\r\n");
					break;						
						
				}
				s_LastTime = GetSystem10msCount();
				s_Status++;
			}		
		break;
		
		case 7:

			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 0) 	//OK可能会盖掉之前的数据包
			{
				if(GetGPRSUartRecFlag())
				{
					u1_printf(" nb ack:%s\r\n", GPRS_UART_BUF);
//					for(i=0; i<GPRS_UART_CNT; i++)
//					{
//						u1_printf("%02X ", GPRS_UART_BUF[i]);
//					}
//					u1_printf("\r\n");
//					Clear_GPRS_Buff();
					
					s_Interval = 50;				
					switch(s_Step)
					{
						case 1:
							if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
							{
								s_Step++;	
								s_Status--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();						
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
							}
						break;
							
						case 2:
						//	u1_printf("检测SIM卡： ");
							if((strstr((const char *)GPRS_UART_BUF,"+CPIN: READY")) != NULL )
							{					
								s_Step++;	
								s_Status--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();		
								u1_printf("\r\n  SIM Card OK\r\n");
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
								
								if(s_ErrCount >= 5)
								{
									SetLogErrCode(LOG_CODE_NO_CARD);
									u1_printf(" No Card!\r\n");
									s_ErrCount = 0;
									s_Status = 1;
									s_Step = 1;								
								}
							}						
						break;	
							
						case 3:				//ICCID查询
							//中国移动:898600；898602；898604；898607 
							//中国联通:898601、898606、898609
							//中国电信:898603、898611。
							if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
							{
								s_Step++;	
								s_Status--;	
								s_LastTime = GetSystem10msCount();							
							}
							else if((strstr((const char *)GPRS_UART_BUF,"8")) != NULL )
							{
								s_Step++;	
								s_Status--;	
								s_LastTime = GetSystem10msCount();							
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
								if(s_ErrCount >= 10)
								{
									s_ErrCount = 0;
									s_Status = 1;
									s_Step = 1;
								}
							}
						break;
							
						case 4:			//IMSI查询
							if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
							{
								s_Step++;	
								s_Status--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();							
							}
							else if((strstr((const char *)GPRS_UART_BUF,"46")) != NULL )
							{
								s_Step++;	
								s_Status--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();							
							}
							else
							{
								s_Step++;	
								s_Status--;	
								if(s_ErrCount >= 10)
								{
									s_ErrCount = 0;
									s_Status = 1;
									s_Step = 1;
								}
							}
						break;
							
						case 5:	
						//	u1_printf("检测信号质量： ");
							if((p1 = strstr((const char *)GPRS_UART_BUF,"SQ:")) != NULL )
							{							
								//u1_printf("%s\r\n", GPRS_UART_BUF);	
								memset(buf, 0 ,sizeof(buf));
								strncpy(buf,p1+4,2);
								nTemp = atoi( buf);
								SetCSQValue(nTemp);
								if( nTemp < 8 )								
								{
									s_Status--;	
									s_ErrCount++;
									SetLogErrCode(LOG_CODE_WEAKSIGNAL);
									
									u1_printf(" CSQ=%d, Count=%d ", nTemp, s_ErrCount);
								}
								else
								{
									s_Status--;	
									s_Step++;	
									s_ErrCount = 0;
									ClearLogErrCode(LOG_CODE_WEAKSIGNAL);
									u1_printf("\r\n Signal OK\r\n");
								}
							}
							else
							{
	//							u1_printf("    Error\r\n");
								s_Status--;	
								s_ErrCount++;
							}		
							
							s_Interval = 200;
						break;		
							
						case 6:
							//u1_printf(" 运营商名称： ");
							if((p1 = strstr((const char *)GPRS_UART_BUF,"\",")) != NULL )
							{				
								u1_printf(" 已附着上运营商网络\r\n");
								s_Step = 8;		//不关闭socket
								s_Status--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();		

							}
							else
							{
								s_Status--;	
								s_ErrCount++;
							}
							s_Interval = 100;
						break;		
							
						case 7:
							if(((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL ) || (strstr((const char *)GPRS_UART_BUF,"ERROR")) != NULL )
							{
								s_Step++;	
								s_Status--;		
								s_ErrCount = 0;
								s_LastTime = GetSystem10msCount();						
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
							}
						break;
							
						case 8:
							if((p1 = strstr((const char *)GPRS_UART_BUF,"C: ")) != NULL )
							{							
								memset(buf, 0 ,sizeof(buf));
								strncpy(buf,p1+3,1);
								s_TCPSocket = atoi(buf);
								if(s_TCPSocket < 6)
								{
									u1_printf("Socket:%d\r\n", s_TCPSocket);
									s_Step = 1;	
									s_Status = 10;		
									s_ATAckCount = 0;
									s_ErrCount = 0;
								}
								else
								{
									u1_printf("Err Socket:%d\r\n", s_TCPSocket);
									s_Status--;	
									s_ErrCount++;
								}
								
							}
							else
							{
								s_Status--;	
								s_ErrCount++;
								if(s_ErrCount >= 10)
								{
									s_ErrCount = 0;
									s_Status = 1;
									s_Step = 1;
								}
							}	
							s_Interval = 100;
							
						break;
											
					
					}
						
					
					if(s_Step == 5)
					{
						u1_printf("等待NB获取信号，等待时间较长...\r\n");
						
					}
		
					if(s_ErrCount > 50 )
					{
						SetLogErrCode(LOG_CODE_UNATTACH);
						StoreOperationalData();
						s_ErrCount = 0;
						s_Status = 1;
						s_Step = 1;
					}

										
					Clear_GPRS_Buff();
					s_LastTime = GetSystem10msCount();
				}
				else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1500) 
				{
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
					u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
		
					s_ATAckCount++;
					
					if(s_ATAckCount >= 3)
					{
						s_ATAckCount = 0;
						SetLogErrCode(LOG_CODE_ATTIMEOUT);
						StoreOperationalData();
						u1_printf(" AT No Ack\r\n");
						s_Status = 1;
						s_Step = 1;
					}
					else
					{
						s_Status--;
						u1_printf("AT Timeout\r\n");
					}
					
					s_LastTime = GetSystem10msCount();
					s_ErrCount++;
				}	
			}				
		break;
		
			
		case 10:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 300) 
			{
				u1_printf(" [SIM]Establishing TCP links\r\n");
				//sprintf(str,"AT+CIPSTART=\"TCP\",\"%d.%d.%d.%d\",\"%d\"\r"	//SIM800方法
				sprintf(str,"AT+CSOCON=%d,%d,\"%d.%d.%d.%d\"\r"		//NB方法
				,s_TCPSocket
				,p_sys->Gprs_Port	
				,p_sys->Gprs_ServerIP[0]
				,p_sys->Gprs_ServerIP[1]
				,p_sys->Gprs_ServerIP[2]
				,p_sys->Gprs_ServerIP[3]
				);

				Clear_GPRS_Buff();
				SimUartSendStr(str);	
				u1_printf(str);	//串口输出AT命令
				u1_printf("\r\n");
				s_LastTime = GetSystem10msCount();
				s_Status++;		
			}				
		break;
		
		case 11:
			if(GetGPRSUartRecFlag())
			{
				u1_printf("%s\r\n", GPRS_UART_BUF);
					
				if((strstr((const char *)GPRS_UART_BUF,"ERROR")) != NULL )
				{		
					s_ErrCount++;
					s_Status--;	
//					s_Status = 6;
//					s_Step = 7;	
					if(s_ErrCount >= 3)
					{
						SetLogErrCode(LOG_CODE_ATTIMEOUT);
						StoreOperationalData();
						s_Status = 1;
						s_Step = 1;
						s_ErrCount = 0;
					}
				}				
				//else if((strstr((const char *)GPRS_UART_BUF,"CONNECT OK")) != NULL )
				else if((strstr((const char *)GPRS_UART_BUF,"OK")) != NULL )
				{
					s_Step = 1;	
					s_Status++;		
					s_ErrCount = 0;
					SetTCModuleReadyFlag(TRUE);		//连接成功========================
					u1_printf(" GPRS link successful\r\n");				
				}
				else
				{
					s_Status--;	
					s_ErrCount++;
					u1_printf(" Unrecognized instruction\r\n");
				}
					
				
				if(s_ErrCount > 20 )
				{
					s_ErrCount = 0;
					s_Status = 1;
					s_Step = 1;
				}
				
				Clear_GPRS_Buff();
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1800) 
			{
				Clear_GPRS_Buff();
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				s_Status--;
				u1_printf(" AT No Ack\r\n");
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
				if(s_ErrCount >= 3)
				{
					SetLogErrCode(LOG_CODE_ATTIMEOUT);
					StoreOperationalData();
					s_Status = 1;
					s_Step = 1;
					s_ErrCount = 0;
				}
			}			
		break;
		
		case 12:		//SIM模块准备好
			s_Status = 50;
			s_LastTime = GetSystem10msCount();
			s_EndTime = GetSystem10msCount();
			SetLinkNetTime(CalculateTime(s_EndTime, s_StartTime) );
			u1_printf("\r\n 连接耗时:%.2fs\r\n\r\n", CalculateTime(s_EndTime, s_StartTime)/100.0);
		break;
			
//		case 13:
//			if(GetGPRSUartRecFlag())
//			{		
//				if(strstr((char *)GPRS_UART_BUF, ">"))
//				{
//					s_ErrCount = 0;
//					s_Status = 50;	
//					u1_printf(" 进入TCP通信\r\n");
//				}			
//				Clear_GPRS_Buff();
//				
//			}
//			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 15 )
//			{				
//					u1_printf(" [SIM]OK无响应\r\n");
//					s_Status--;   //回上一个状态
//					s_ErrCount++;   //重试次数加1
//					if(s_ErrCount >= 4 )
//					{
//						SetLogErrCode(LOG_CODE_NOMODULE);
//						StoreOperationalData();
//						s_ErrCount = 0;
//						s_Status = 1;	//模块重新上电初始化
//					}						
//			}
//		break;
		
			case 50:
				SetTCProtocolRunFlag(TRUE);
				s_Status++;
				g_TCRecStatus.AliveAckFlag = FALSE;
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d]", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);	
			
				if(VolCount < 5)
				{
					Mod_Send_Alive();	//发送心跳包
					VolCount++;
					
				}
				else if(VolCount >= 5)
				{
					VolCount = 0;
					Mod_Report_Data();	//上报网关电压数据
				}
				
				if(GetTCPCloseInfoFlag())	//监听到TCP 关闭信号
				{
					SetTCPCloseInfoFlag(FALSE);
					SetLogErrCode(LOG_CODE_NOSERVERACK);
					StoreOperationalData();
					s_OverCount = 0;
					s_Status = 6;
					s_Step = 7;
					s_ErrCount = 0;
					s_LastTime = GetSystem10msCount();
					SetTCModuleReadyFlag(FALSE);
					SetOnline(FALSE);
					
				}
				s_LastTime = GetSystem10msCount();
			break;
			
			case 51:
				if(g_TCRecStatus.AliveAckFlag)	//收到心跳包回复
				{
					s_Status++;
					s_LastTime = GetSystem10msCount();
					g_TCRecStatus.AliveAckFlag = FALSE;
					s_OverCount = 0;
				}
				else
				{
					if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 2000 )	//心跳包超时重发
					{
						s_Status--;
						s_LastTime = GetSystem10msCount();
						u1_printf(" No heartbeat timeout:%d\r\n", s_OverCount);
						Clear_GPRS_Buff();
						s_OverCount++;
						if(s_OverCount >= 4)
						{
							SetLogErrCode(LOG_CODE_NOSERVERACK);
							StoreOperationalData();
							s_OverCount = 0;
							s_Status = 6;
							s_Step = 7;
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();
							SetTCModuleReadyFlag(FALSE);
							SetOnline(FALSE);
							delay_ms(100);
							SimUartSendStr("AT+CFUN=0\r");
							u1_printf(" 关闭RF wait 5s.\r\n");
							delay_ms(1000);
							delay_ms(1000);
							delay_ms(1000);
							delay_ms(1000);
							delay_ms(1000);
							SimUartSendStr("AT+CFUN=1\r");
							u1_printf(" Restart\r\n");
							delay_ms(50);				
							SetTCProtocolRunFlag(FALSE);							
						}
					}
				}
			break;
									
			case 52:
				if( CalculateTime(GetSystem10msCount(),s_LastTime2) >= 200 )	//2S判断一次是否进入省电模式
				{
					if(GetPowerSavingFlag())
					{
						s_SavingModeFlag = GetPowerSavingFlag();
						if(JudgmentTimePoint(NETTYPE_2G))	//达到唤醒通信模块时间点
						{						
							if(s_SimSavingFlag)
							{
								s_Status = 1;
								s_Step = 1;
								s_ErrCount = 0;
								s_SimSavingFlag = FALSE;
							}
						}
						else	//到达省电模式时间点
						{
							s_SimSavingFlag = TRUE;
							SetTCModuleReadyFlag(FALSE);
							SetOnline(FALSE);
							SIM7020_PWR_OFF();
							s_LastTime2 = GetSystem10msCount();
						}
					}
					else
					{
						if(s_SavingModeFlag)	//退出省电模式，重启模块
						{
							s_SavingModeFlag = GetPowerSavingFlag();
							s_Status = 1;
							s_Step = 1;
							s_ErrCount = 0;
						}
						s_Status = 53;
					}
				}
				
				RetransmissionGPRSData();	//是否有GPRS下行包进行重发
				
			break;
				
			case 53:
				if( CalculateTime(GetSystem10msCount(),s_LastTime) >= p_sys->Heart_interval*100 )
				{				
					s_LastTime = GetSystem10msCount();
					s_Status = 50;
//					u1_printf("\r\n Signal strength:%d\r\n", s_SignalStrength);
				}
				else if( CalculateTime(GetSystem100msCount(),s_CheckCSQTime) >= 6000)
				{
					SimUartSendStr("AT+CSQ\r");
					s_CheckCSQTime = GetSystem100msCount();
					i = 99999;
					delay_ms(15);
					while(i--)
					{
						if(GetGPRSUartRecFlag())
						{
							if((p1 = strstr((const char *)GPRS_UART_BUF,"+CSQ:")) != NULL )
							{					
								memset(buf, 0 ,sizeof(buf));
								strncpy(buf,p1+6,2);
								nTemp = atoi( buf);
								SetCSQValue(nTemp);
								u1_printf("\r\n NB CSQ=%d\r\n", nTemp);	
								Clear_GPRS_Buff();
								break;
							}
							else
							{
								u1_printf(" Don't Rec CSQ\r\n");
							}	
							Clear_GPRS_Buff();			
						}
						IWDG_ReloadCounter();	
					}
				}
				else
				{
					RetransmissionGPRSData();	//是否有GPRS下行包进行重发
					s_Status = 52;
					s_LastTime2 = GetSystem10msCount();
				}
			break;		

			
	}
	
}
