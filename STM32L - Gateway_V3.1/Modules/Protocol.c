#include "Protocol.h"
#include "main.h"

#define		NB_PACK_END 	(0X1A)

void GPRS_Send_Buff(unsigned char *buffer, unsigned short buffer_size)
{

#if	(HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V100)

	USART3_DMA_Send(buffer, buffer_size);	

#elif (HARDWARE_VERSION == HARDWARE_VERSION_GATEWAY_V110)

	SYSTEMCONFIG *p_sys;
	int i, HeadLen = 0;
	char str[600] = {0};
	p_sys = GetSystemConfig();	
	
//	USART2_DMA_Send(buffer, buffer_size);	
	if(((p_sys->Type>>4) == NETTYPE_NB_IOT) || ((p_sys->Type) == NETTYPE_NB_IOT))
	{
		sprintf(str,"AT+CSOSEND=%d,%d,", GetTCPSocket(), buffer_size*2);
		HeadLen = strlen(str);
		for(i = 0; (i < buffer_size); i++)
		{
			sprintf(&str[i*2 + HeadLen], "%02X", buffer[i]);
		}
		str[HeadLen + buffer_size*2] = '\r';
		u2_printf(str);	
	}
	else
	{
		USART2_DMA_Send(buffer, buffer_size);	
	}
	
	
#endif
	
}

unsigned int Mod_Report_Data(void)
{
	u16 nMsgLen;
	static u16 seq;
	float val_float;
	unsigned int send_len;
	unsigned int payload_len;
	u8 i, count, Packet[256], send_buf[256];

	CLOUD_HDR *hdr;
	unsigned char *ptr;
	uint16 val_u16;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();	

	
	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(PROTOCOL_CONTROL_ID);			//通信版本号
	hdr->device_id = swap_dword(p_sys->Device_ID);	  //设备号
	hdr->dir = 0;												  //方向
	hdr->seq_no = swap_word(seq);							  //包序号
	seq++;
	hdr->cmd = CMD_REPORT_D;									  //命令字

	ptr = (unsigned char *)&send_buf[sizeof(CLOUD_HDR)];
	ptr++;
	
	count = 0;
	
	val_u16 = swap_word(CH_CSQ);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(CSQ_ID);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_FLOAT;	//数据标识
	ptr++;
	val_float = encode_float(GetCSQValue());	//数据
	memcpy(ptr, (void *)&val_float, sizeof(float));
	ptr += sizeof(float);
	count++;
	
	val_u16 = swap_word(CH_LINKTIME);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(LINKTIME_ID);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_FLOAT;	//数据标识
	ptr++;
	val_float = encode_float(GetLinkNetTime()/100.0);	//数据
	memcpy(ptr, (void *)&val_float, sizeof(float));
	ptr += sizeof(float);
	count++;
	
	if(GetLongitude() !=0 && GetLatitude() != 0)
	{
		val_u16 = swap_word(CH_LONGITUDE);		//通道
		memcpy(ptr, (void *)&val_u16, sizeof(uint16));
		ptr += sizeof(uint16);
		val_u16 = swap_word(LONGITUDE_ID);//类型
		memcpy(ptr, (void *)&val_u16, sizeof(uint16));
		ptr += sizeof(uint16);
		(*ptr) = MARK_FLOAT;	//数据标识
		ptr++;
		val_float = encode_float(GetLongitude());	//数据
		memcpy(ptr, (void *)&val_float, sizeof(float));
		ptr += sizeof(float);
		count++;
		
		val_u16 = swap_word(CH_LATITUDE);		//通道
		memcpy(ptr, (void *)&val_u16, sizeof(uint16));
		ptr += sizeof(uint16);
		val_u16 = swap_word(LATITUDE_ID);//类型
		memcpy(ptr, (void *)&val_u16, sizeof(uint16));
		ptr += sizeof(uint16);
		(*ptr) = MARK_FLOAT;	//数据标识
		ptr++;
		val_float = encode_float(GetLatitude());	//数据
		memcpy(ptr, (void *)&val_float, sizeof(float));
		ptr += sizeof(float);
		count++;
	}	
		
	val_u16 = swap_word(CH_DCPOWERVOL);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(DCPOWER_ID);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);		
	(*ptr) = MARK_FLOAT;	//数据标识
	ptr++;
	val_float = encode_float(GetDCPowerVol()/1000.0);	//数据
	memcpy(ptr, (void *)&val_float, sizeof(float));
	ptr += sizeof(float);
	count++;	
	
	val_u16 = swap_word(CH_BATVOL);		//通道
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	val_u16 = swap_word(BAT_VOL);//类型
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	(*ptr) = MARK_UINT16;//数据标识
	ptr++;
	val_u16 = swap_word(Get_Battery_Vol());	//数据
	memcpy(ptr, (void *)&val_u16, sizeof(uint16));
	ptr += sizeof(uint16);
	count++;

	
	send_buf[sizeof(CLOUD_HDR)] = count;
	send_len = ptr - send_buf;
    payload_len = send_len - sizeof(CLOUD_HDR);
	hdr->payload_len = swap_word(payload_len);
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,100);
	//发送消息
	u1_printf("\r\n [Mod][Vol] -> %d(%d): ", p_sys->Gateway, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",Packet[i]);
	}
	u1_printf("\r\n");	

	GPRS_Send_Buff(Packet, nMsgLen);	
	
	return nMsgLen;	
}

void Mod_Transmit_RS485Data(CLOUD_HDR *hdr, unsigned char *data, unsigned char lenth)
{
	u16 nMsgLen, i;
	unsigned char send_buf[256];
	unsigned char Pack[256];
	unsigned int len = 0;


	send_buf[len++] = hdr->protocol >> 8;
	send_buf[len++] = hdr->protocol;
	
	send_buf[len++] = (hdr->device_id >> 24);		//低功耗设备  高位置1
	send_buf[len++] = hdr->device_id >> 16;	
	send_buf[len++] = hdr->device_id >> 8;
	send_buf[len++] = hdr->device_id;	
	
	send_buf[len++]  = hdr->dir;
	
	send_buf[len++]  = (hdr->seq_no) >> 8;
	send_buf[len++]  = hdr->seq_no;
	
	send_buf[len++]  = hdr->payload_len >> 8;
	send_buf[len++]  = hdr->payload_len;

	send_buf[len++] = hdr->cmd;									//命令字

//	len = sizeof(CLOUD_HDR);									//未加校验的数据包长度

    for(i=0; i<lenth; i++)	
		send_buf[len+i] = data[i];
		
	len += lenth;
	
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, Pack, 256);		//数据包转义处理	
	u1_printf(" [Forward packet] from %d(%d) ->: ", hdr->device_id, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",Pack[i]);
	}
	u1_printf("\r\n");	

	GPRS_Send_Buff(Pack, nMsgLen);		
}

void Mod_Transmit_Data(CLOUD_HDR *hdr, unsigned char *data, unsigned char lenth)
{
	u16 nMsgLen, i;
	unsigned char send_buf[256];
	unsigned char Pack[256];
	unsigned int len = 0;


	send_buf[len++] = hdr->protocol >> 8;
	send_buf[len++] = hdr->protocol;
	
	send_buf[len++] = ((hdr->device_id >> 24) | 0x10);		//低功耗设备  高位置1
	send_buf[len++] = hdr->device_id >> 16;	
	send_buf[len++] = hdr->device_id >> 8;
	send_buf[len++] = hdr->device_id;	
	
	send_buf[len++]  = hdr->dir;
	
	send_buf[len++]  = (hdr->seq_no) >> 8;
	send_buf[len++]  = hdr->seq_no;
	
	send_buf[len++]  = hdr->payload_len >> 8;
	send_buf[len++]  = hdr->payload_len;

	send_buf[len++] = hdr->cmd;									//命令字

//	len = sizeof(CLOUD_HDR);									//未加校验的数据包长度

    for(i=0; i<lenth; i++)	
		send_buf[len+i] = data[i];
		
	len += lenth;
	
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, Pack, 256);		//数据包转义处理	
	u1_printf(" [Forward packet] from %d(%d) ->: ", hdr->device_id, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",Pack[i]);
	}
	u1_printf("\r\n");	

	GPRS_Send_Buff(Pack, nMsgLen);		
}

void Mod_Send_Ack(CLOUD_HDR *hdr, unsigned char *data, unsigned char lenth)
{
	u16 nMsgLen, i;
	unsigned char send_buf[128];
	unsigned char Pack[128];
	unsigned int len = 0;

	send_buf[len++] = hdr->protocol >> 8;
	send_buf[len++] = hdr->protocol;
	
	send_buf[len++] = hdr->device_id >> 24;
	send_buf[len++] = hdr->device_id >> 16;	
	send_buf[len++] = hdr->device_id >> 8;
	send_buf[len++] = hdr->device_id;	
	
	send_buf[len++]  = 1;
	
	send_buf[len++]  = hdr->seq_no >> 8;
	send_buf[len++]  = hdr->seq_no ;
	
	send_buf[len++]  = hdr->payload_len >> 8;
	send_buf[len++]  = hdr->payload_len;

	send_buf[len++] = hdr->cmd;									//命令字

//	len = sizeof(CLOUD_HDR);									//未加校验的数据包长度

    for(i=0; i<lenth; i++)	
		send_buf[len+i] = data[i];
		
	len += lenth;
	
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, Pack, 120);		//数据包转义处理	
	u1_printf(" [Transfer ACK] from %d(%d) ->: ", (hdr->device_id)&0xfffffff, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",Pack[i]);
	}
	u1_printf("\r\n");	

	GPRS_Send_Buff(Pack, nMsgLen);		
}

void Mod_Transmit_Vol_Data(unsigned char Type, CLOUD_HDR *hdr, unsigned short vol)
{
	u16 nMsgLen, i;
	unsigned char send_buf[128];
	unsigned char Pack[128];
	unsigned int len = 0;
	static uint16 seq_no = 0;
	float val_float = 0, f_data = 0;

	send_buf[len++] = PROTOCOL_CONTROL_ID >> 8;
	send_buf[len++] = PROTOCOL_CONTROL_ID&0xff;
	
	send_buf[len++] = ((hdr->device_id >> 24) | 0x10);		//低功耗设备  高位置1
	send_buf[len++] = hdr->device_id >> 16;	
	send_buf[len++] = hdr->device_id >> 8;
	send_buf[len++] = hdr->device_id;	
	
	send_buf[len++]  = 0;
	
	send_buf[len++]  = seq_no >> 8;
	send_buf[len++]  = seq_no++;
	
	send_buf[len++]  = hdr->payload_len >> 8;
	send_buf[len++]  = hdr->payload_len;

	send_buf[len++] = CMD_REPORT_D;									//命令字
	
	if(BAT_VOL == Type)
	{
		send_buf[len++] = 0x01;		//通道
		send_buf[len++] = 0x00;
		send_buf[len++] = CH_BATVOL;		//通道
		send_buf[len++] = 0x00;
		send_buf[len++] = BAT_VOL;		//传感器类型
		send_buf[len++] = MARK_UINT16;		//整形或浮点
		send_buf[len++] = vol>>8;	//数据
		send_buf[len++] = vol;
	}
	else if(DCPOWER_ID == Type)
	{
		send_buf[len++] = 0x01;		//通道
		send_buf[len++] = 0x00;
		send_buf[len++] = CH_DCPOWERVOL;		//通道
		send_buf[len++] = 0x00;
		send_buf[len++] = DCPOWER_ID;		//传感器类型
		send_buf[len++] = MARK_FLOAT;		//整形或浮点
		val_float = (float)vol;
		val_float /= 1000.0;
		val_float = encode_float(val_float);	//数据
		
		memcpy(&send_buf[len], (void *)&val_float, sizeof(float));
		len += sizeof(float);
	}
	
	
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg(send_buf, len, Pack, 120);		//数据包转义处理	
	u1_printf(" [Relay voltage data] from %d(%d) ->: ", hdr->device_id, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",Pack[i]);
	}
	u1_printf("\r\n");	

	GPRS_Send_Buff(Pack, nMsgLen);		
}

void Mod_Send_Alive(void)
{
	u16 nMsgLen, i;
	unsigned char send_buf[200];
	unsigned char AlivePack[200];
	unsigned int len;
	static uint16 seq_no = 0;
	CLOUD_HDR *hdr;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();
	hdr = (CLOUD_HDR *)&send_buf[0];
	hdr->protocol = swap_word(PROTOCOL_GATEWAY_ID);							//通信协议版本号
	hdr->device_id = swap_dword(p_sys->Device_ID);		//设备号
	hdr->dir = 0;													//方向
	hdr->seq_no = swap_word(seq_no++);								//序号

	hdr->payload_len = swap_word(6);							//信息域长度
	hdr->cmd = CMD_HEATBEAT;									//命令字

	len = sizeof(CLOUD_HDR);									//未加校验的数据包长度

	send_buf[len++] = 	SENSOR_TYPE_GATEWAY;	//传感器网关  200
	
	send_buf[len++] = 1;	//通道1
	
	send_buf[len++] = 0;
	send_buf[len++] = 100;	//协议版本
	
	send_buf[len++] = XML_BAT_VOLTAGE;	//电压标识
	send_buf[len++] = 84;

	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
		
	nMsgLen = PackMsg(send_buf, len, AlivePack, 200);	//数据包转义处理	
	

	
	u1_printf(" [Alive] -> %d(%d): ", p_sys->Gateway, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
		u1_printf("%02X ",AlivePack[i]);
	}
	u1_printf("\r\n");	

	GPRS_Send_Buff(AlivePack, nMsgLen);	
}

unsigned int Mod_Auto_Alive(unsigned int DeviceID)
{
	uint8 send_buf[128], DataPack[128], i, len = 0;
	UINT16 nMsgLen;
	static uint16 seq_no = 0;

	send_buf[len++] = PROTOCOL_CONTROL_ID >> 8;
	send_buf[len++] = PROTOCOL_CONTROL_ID & 0xff;
	
	send_buf[len++] = DeviceID >> 24;
	send_buf[len++] = DeviceID >> 16;	
	send_buf[len++] = DeviceID >> 8;
	send_buf[len++] = DeviceID;	
	
	send_buf[len++]  = 0;
	
	send_buf[len++]  = seq_no >> 8;
	send_buf[len++]  = seq_no++;
	
	send_buf[len++]  = 0;
	send_buf[len++]  = 6;

	send_buf[len++] = CMD_HEATBEAT;									//命令字

	//if(g_sys_ctrl.sys_info )
	//信息域
	send_buf[len++] = 84;			//电池电压，传输数据=电压*10;
	Int32ToArray(&send_buf[len], DeviceID);//网关号
	len +=4;
	send_buf[len++] = 20;	//收到网关数据时的信号强度

	//校验
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg( send_buf,len,DataPack,100);
	

	
	u1_printf("Auto Alive(%d) -> ", DeviceID);
	for (i=0;i<nMsgLen && i<200;i++)
	{
		u1_printf("%02X ",DataPack[i]);	
	}
		
	u1_printf(" \r\n");

	GPRS_Send_Buff(DataPack, nMsgLen);	
	
	return 1;
}	  

unsigned int Transpond_Alive(CLOUD_HDR *hdr)
{
	uint8 send_buf[128], DataPack[128], len = 0;
	UINT16 nMsgLen;
	static uint16 seq_no = 0;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();
	
	send_buf[len++] = hdr->protocol >> 8;
	send_buf[len++] = hdr->protocol;
	
	send_buf[len++] = hdr->device_id >> 24;
	send_buf[len++] = hdr->device_id >> 16;	
	send_buf[len++] = hdr->device_id >> 8;
	send_buf[len++] = hdr->device_id;	
	
	send_buf[len++]  = 0;
	
	send_buf[len++]  = seq_no >> 8;
	send_buf[len++]  = seq_no++;
	
	send_buf[len++]  = 0;
	send_buf[len++]  = 6;

	send_buf[len++] = CMD_HEATBEAT;									//命令字

	//if(g_sys_ctrl.sys_info )
	//信息域
	send_buf[len++] = 84;			//电池电压，传输数据=电压*10;
	Int32ToArray(&send_buf[len], hdr->device_id);//网关号
	len +=4;
	send_buf[len++] = 20;	//收到网关数据时的信号强度

	//校验
	send_buf[len] = Calc_Checksum(send_buf, len);		//数据包和校验
	len++;
	
	nMsgLen = PackMsg( send_buf,len,DataPack,100);
	
//	u1_printf(" 自动生成心跳包 -> ");
//	for (i=0;i<nMsgLen && i<200;i++)
//	{
//		u1_printf("%02X ",DataPack[i]);	
//	}

	if( GetOnline() == 1 )
	{
		if((p_sys->Type>>4) == NETTYPE_GPRS)
		{
			GPRS_Send_Buff(DataPack, nMsgLen);	
		}
		else if((p_sys->Type>>4) == NETTYPE_2G)
		{
			GPRS_Send_Buff(DataPack, nMsgLen);	
		}
	}
	else
	{
		if((p_sys->Type>>4) == NETTYPE_GPRS)
		{
			if(GetTCModuleReadyFlag())
			{
				GPRS_Send_Buff(DataPack, nMsgLen);	
			}
			else
			{
				u1_printf("\r\n GPRS Network not connected...\r\n");
			}
		}
		else if((p_sys->Type>>4) == NETTYPE_2G)
		{
			if(GetOnline() == 1)
			{
				GPRS_Send_Buff(DataPack, nMsgLen);	
			}
			else
			{
				u1_printf("\r\n SIM Network not connected...\r\n");
			}
		}
	}
	
	return 1;
}	  
//查询终端参数--CMD = 4
unsigned short ProcQuerySysPrm(CLOUD_HDR *pMsg, unsigned char *data, unsigned char lenth)
{
	unsigned short len = 0, i, nMsgLen;
	unsigned char send_buf[128], Packet[128];
	unsigned int lTemp;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();

	send_buf[len++] = pMsg->protocol >> 8;
	send_buf[len++] = pMsg->protocol&0xff;
	
	send_buf[len++] = pMsg->device_id >> 24;
	send_buf[len++] = pMsg->device_id >> 16;	
	send_buf[len++] = pMsg->device_id >> 8;
	send_buf[len++] = pMsg->device_id;	
	
	send_buf[len++]  = 1;
	
	send_buf[len++]  = pMsg->seq_no>>8;
	send_buf[len++]  = pMsg->seq_no&0xff;
	
	send_buf[len++]  = 0;
	send_buf[len++]  = 46;

	send_buf[len++] = CMD_GET_CONFIG;									//命令字
	
	for(i=0; i<lenth; i++)
		send_buf[len++] = data[i];
	//异常码
	send_buf[ len++ ] = XML_ERR_CODE;
	send_buf[ len++ ] = ERR_CODE_NORMAL;

	//设备ID
	send_buf[ len++ ] = XML_SYS_DEVICE_ID;
	lTemp = p_sys->Device_ID;
	send_buf[ len++ ] = lTemp >> 24;
	send_buf[ len++ ] = lTemp >> 16;
	send_buf[ len++ ] = lTemp >> 8;
	send_buf[ len++ ] = lTemp;

	//Lora网络状态
	send_buf[ len++ ] = XML_SYS_ZIGBEE_EN;
	send_buf[ len++ ] = p_sys->Type%0x10;

	//Lora网络
	send_buf[ len++ ] = XML_SYS_ZIGBEE_NET;
	send_buf[ len++ ] = (UINT8)(p_sys->Net>>8);
	send_buf[ len++ ] = (UINT8)p_sys->Net;

	//Lora节点
	send_buf[ len++ ] = XML_SYS_ZIGBEE_ADDR;
	send_buf[ len++ ] = (UINT8)(p_sys->Node>>8);
	send_buf[ len++ ] = (UINT8)p_sys->Node;

	//主机Lora地址
	send_buf[ len++ ] = XML_SYS_ZIGBEE_SERVER_ID;
	send_buf[ len++ ] = (UINT8)(p_sys->Gateway>>8);
	send_buf[ len++ ] = (UINT8)p_sys->Gateway;

	//Lora频道
	send_buf[ len++ ] = XML_SYS_ZIGBEE_CHANNEL;
	send_buf[ len++ ] = p_sys->Channel;

	//Lora拓扑
	send_buf[ len++ ] = XML_SYS_ZIGBEE_TOPOLOGY;
	send_buf[ len++ ] = p_sys->Topology;

	//GPRS网络状态
	send_buf[ len++ ] = XML_SYS_GPRS_EN;
	send_buf[ len++ ] = p_sys->Type>>4;
	//GPRS服务器IP
	send_buf[ len++ ] = XML_SYS_GPRS_SERVER_IP;
	send_buf[ len++ ] = p_sys->Gprs_ServerIP[0];
	send_buf[ len++ ] = p_sys->Gprs_ServerIP[1];
	send_buf[ len++ ] = p_sys->Gprs_ServerIP[2];
	send_buf[ len++ ] = p_sys->Gprs_ServerIP[3];

	//服务器端口
	send_buf[ len++ ] = XML_SYS_GPRS_SERVER_PORT;
	send_buf[ len++ ] = p_sys->Gprs_Port>>8;
	send_buf[ len++ ] = p_sys->Gprs_Port;

	//重发次数
	send_buf[ len++ ] = XML_SYS_RELINK_TIME;
	send_buf[ len++ ] = p_sys->Retry_Times;

	//心跳间隔时间
	send_buf[ len++ ] = XML_SYS_HEART_BEART_TIME;
	send_buf[ len++ ] = p_sys->Heart_interval>>8;
	send_buf[ len++ ] = p_sys->Heart_interval;

	//是否午夜重启
	send_buf[ len++ ] = XML_SYS_AUTO_RESET;
	send_buf[ len++ ] = p_sys->AutoResetFlag;

	//午夜重启时间
	send_buf[ len++ ] = XML_SYS_AUTO_RESET_TIME;
	send_buf[ len++ ] = p_sys->AutoResetTime >> 8;
	send_buf[ len++ ] = p_sys->AutoResetTime & 0xff;

	send_buf[len] = Calc_Checksum(send_buf, len);
	len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,len,Packet,200);
	//发送消息
	u1_printf("[Mod][Config] -> %d (%d):", p_sys->Device_ID, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ", Packet[i]);
	}	

	GPRS_Send_Buff(Packet, nMsgLen);	
	
	return nMsgLen;
}

//设置终端参数--CMD = 5
unsigned short ProcSetSysPrm(CLOUD_HDR *pMsg, unsigned char *data, unsigned char lenth)
{
	unsigned short len = 0, Data_len, i, nMsgLen, nTemp;
	
	unsigned char send_buf[128], Packet[128];
	unsigned int lTemp;
	static SYSTEMCONFIG New_Sys_Cfg;
	SYSTEMCONFIG *p_sys;
	
	p_sys = GetSystemConfig();

	len = 4;
	//设备ID
	if( data[len++] != XML_SYS_DEVICE_ID )
	{
		len = 0;
		send_buf[ len++ ] = XML_ERR_CODE;
		send_buf[ len++ ] = XML_SYS_DEVICE_ID;
		return 0;
	}
	lTemp = (data[ len ] << 24 )  + (data[ len +1 ] << 16) + (data[len+2] <<8) + data[len+3];
	New_Sys_Cfg.Device_ID = lTemp;
	len+=4;
	u1_printf(" ID:%d \r\n", New_Sys_Cfg.Device_ID);
	
	//Lora通信状态
	if( data[len++] != XML_SYS_ZIGBEE_EN )
	{
		len =0;
		send_buf[ len++ ] = XML_ERR_CODE;
		send_buf[ len++ ] = XML_SYS_ZIGBEE_EN;
		return 0;
	}
	New_Sys_Cfg.Type = data[ len++ ];
	u1_printf(" TY:%d \r\n", New_Sys_Cfg.Type);
	if(New_Sys_Cfg.Type >= 10)
	{
		return 0;
	}
	//Lora网络号
	if( data[len++] != XML_SYS_ZIGBEE_NET )
	{
		len =0;
		send_buf[ len++ ] = XML_ERR_CODE;
		send_buf[ len++ ] = XML_SYS_ZIGBEE_NET;
		return 0;
	}
	nTemp = data[ len++];
	nTemp <<= 8;
	nTemp += data[ len++ ];
	New_Sys_Cfg.Net = nTemp;
	u1_printf(" Net:%d \r\n", New_Sys_Cfg.Net);
	
	//Lora节点地址
	if( data[len++] != XML_SYS_ZIGBEE_ADDR )
	{
		len =0;
		send_buf[ len++ ] = XML_ERR_CODE;
		send_buf[ len++ ] = XML_SYS_ZIGBEE_ADDR;
		return 0;
	}
	nTemp = data[ len++];
	nTemp <<= 8;
	nTemp += data[ len++ ];
	New_Sys_Cfg.Node = nTemp;
	u1_printf(" Node:%d \r\n", New_Sys_Cfg.Node);
	
	if(New_Sys_Cfg.Node != New_Sys_Cfg.Net)
	{
		return 0;
	}
	
	//Lora主机节点地址
	if( data[len++] != XML_SYS_ZIGBEE_SERVER_ID )
	{
		len =0;
		send_buf[ len++ ] = XML_ERR_CODE;
		send_buf[ len++ ] = XML_SYS_ZIGBEE_SERVER_ID;
		return 0;
	}
	nTemp = data[ len++];
	nTemp <<= 8;
	nTemp += data[ len++ ];
	New_Sys_Cfg.Gateway = nTemp;
	u1_printf(" Gateway:%d \r\n", New_Sys_Cfg.Gateway);
	if(New_Sys_Cfg.Node != New_Sys_Cfg.Gateway)
	{
		return 0;
	}
	//Lora频道
	if( data[len++] != XML_SYS_ZIGBEE_CHANNEL )
	{
		len =0;
		send_buf[ len++ ] = XML_ERR_CODE;
		send_buf[ len++ ] = XML_SYS_ZIGBEE_CHANNEL;
		return 0;
	}
		
	New_Sys_Cfg.Channel = data[ len++ ];
	u1_printf(" Channel:%d \r\n", New_Sys_Cfg.Channel);
	
	if(New_Sys_Cfg.Channel > 32)
	{
		return 0;
	}
	//Lora网络拓扑
	if( data[len++] != XML_SYS_ZIGBEE_TOPOLOGY )
	{
		len =0;
		send_buf[ len++ ] = XML_ERR_CODE;
		send_buf[ len++ ] = XML_SYS_ZIGBEE_TOPOLOGY;
		return 0;
	}
	New_Sys_Cfg.Topology = data[ len++ ];

	u1_printf(" Topology:%d \r\n", New_Sys_Cfg.Topology);
	//GPRS状态
	if( data[len++] != XML_SYS_GPRS_EN )
	{
		len =0;
		send_buf[ len++ ] = XML_ERR_CODE;
		send_buf[ len++ ] = XML_SYS_GPRS_EN;
		return 0;
	}
	New_Sys_Cfg.Type |= (data[ len++ ] <<4);
//	u1_printf("Type:%X \r\n", New_Sys_Cfg.Type);
	u1_printf(" Net Type:%X   ", New_Sys_Cfg.Type);
	if((New_Sys_Cfg.Type%0x10) == NETTYPE_LORA)
	{
		u1_printf("LORA\r\n");
	}
	else
	{
		u1_printf(" Node Type Err...\r\n");
		return 0;
	}
	
	u1_printf(" Server  Type:");
	
	switch((New_Sys_Cfg.Type>>4))
	{
		case NETTYPE_2G:
			u1_printf("2G\r\n");
		break;
		
		case NETTYPE_GPRS:
			u1_printf("LAN\r\n");	
		break;
		
		case NETTYPE_NB_IOT:
			u1_printf("NB-IOT\r\n");	
		break;
		
		case NETTYPE_WLAN:
			u1_printf("WLAN\r\n");
		break;
		
		case NETTYPE_4G:
			u1_printf("4G\r\n");
		break;	

		default:
			u1_printf(" Net Type Err...\r\n");
			return 0;

	}
	//GPRS服务器IP地址
	if( data[len++] != XML_SYS_GPRS_SERVER_IP )
	{
		len =0;
		send_buf[ len++ ] = XML_ERR_CODE;
		send_buf[ len++ ] = XML_SYS_GPRS_SERVER_IP;
		return 0;
	}

	New_Sys_Cfg.Gprs_ServerIP[0] = data[ len++ ];
	New_Sys_Cfg.Gprs_ServerIP[1] = data[ len++ ];
	New_Sys_Cfg.Gprs_ServerIP[2] = data[ len++ ];
	New_Sys_Cfg.Gprs_ServerIP[3] = data[ len++ ];

	//GPRS服务器端口
	if( data[len++] != XML_SYS_GPRS_SERVER_PORT )
	{
		len =0;
		send_buf[ len++ ] = XML_ERR_CODE;
		send_buf[ len++ ] = XML_SYS_GPRS_SERVER_PORT;
		return 0;
	}
	nTemp = data[ len++];
	nTemp <<= 8;
	nTemp += data[ len++ ];
	New_Sys_Cfg.Gprs_Port = nTemp;
	u1_printf(" Gprs_Port:%d \r\n", New_Sys_Cfg.Gprs_Port);

	//重连次数
	if( data[len++] != XML_SYS_RELINK_TIME )
	{
		len =0;
		send_buf[ len++ ] = XML_ERR_CODE;
		send_buf[ len++ ] = XML_SYS_RELINK_TIME;
		return 0;
	}
	New_Sys_Cfg.Retry_Times = data[ len++ ];
	u1_printf(" Retry_Times:%d \r\n", New_Sys_Cfg.Retry_Times);
	//心跳间隔时间（秒）
	if( data[len++] != XML_SYS_HEART_BEART_TIME )
	{
		len =0;
		send_buf[ len++ ] = XML_ERR_CODE;
		send_buf[ len++ ] = XML_SYS_HEART_BEART_TIME;
		return 0;
	}
	nTemp = (data[ len ] << 8) +data[len+1];
	New_Sys_Cfg.Heart_interval = nTemp;
	len += 2;
	u1_printf(" Heart_interval:%d \r\n", New_Sys_Cfg.Heart_interval);
	//是否自动重启
	if( data[len++] != XML_SYS_AUTO_RESET )
	{
		len =0;
		send_buf[ len++ ] = XML_ERR_CODE;
		send_buf[ len++ ] = XML_SYS_AUTO_RESET;
		return 0;
	}
	New_Sys_Cfg.AutoResetFlag = data[ len++ ];
	if(New_Sys_Cfg.AutoResetFlag)
	{
		u1_printf(" Auto Reset!\r\n");
	}
	else
	{
		u1_printf(" No Auto Reset!\r\n");
	}
	//自动重启时间
	if( data[len++] != XML_SYS_AUTO_RESET_TIME )
	{
		len =0;
		send_buf[ len++ ] = XML_ERR_CODE;
		send_buf[ len++ ] = XML_SYS_AUTO_RESET_TIME;
		return 0;
	}
	New_Sys_Cfg.AutoResetTime = (data[ len ] << 8 ) + data[ len+1 ];
	len+=2;
	u1_printf(" AutoResetTime: %d:%d  \r\n", New_Sys_Cfg.AutoResetTime>>8, New_Sys_Cfg.AutoResetTime&0xff);
	Data_len = len;
	New_Sys_Cfg.State_interval = p_sys->State_interval;
	New_Sys_Cfg.Data_interval = p_sys->Data_interval;
	if (memcmp(&New_Sys_Cfg, p_sys, sizeof(SYSTEMCONFIG)))
	{
		Set_System_Config(&New_Sys_Cfg);
	}
	//回复消息===================================================================
	//数据包正常，开始生成回复信息，根据不同的设备类型回复不同的数据
	len = 0;	
	send_buf[len++] = pMsg->protocol >> 8;
	send_buf[len++] = pMsg->protocol;
	
	send_buf[len++] = pMsg->device_id >> 24;
	send_buf[len++] = pMsg->device_id >> 16;	
	send_buf[len++] = pMsg->device_id >> 8;
	send_buf[len++] = pMsg->device_id;	
	
	send_buf[len++]  = 1;
	
	send_buf[len++]  = pMsg->seq_no>>8;
	send_buf[len++]  = pMsg->seq_no&0xff;
	
	send_buf[len++]  = Data_len>>8;
	send_buf[len++]  = Data_len;

	send_buf[len++] = CMD_GET_CONFIG;									//命令字
	
	for(i=0; i<4; i++)
		send_buf[len++] = data[i];
		

	send_buf[ len++ ] = XML_ERR_CODE;
	send_buf[ len++ ] = 0;

	//设备ID
	send_buf[ len++ ] = XML_SYS_DEVICE_ID;
	lTemp = New_Sys_Cfg.Device_ID;
	send_buf[ len++ ] = lTemp >> 24;
	send_buf[ len++ ] = lTemp >> 16;
	send_buf[ len++ ] = lTemp >> 8;
	send_buf[ len++ ] = lTemp;

	//Lora网络状态
	send_buf[ len++ ] = XML_SYS_ZIGBEE_EN;
	send_buf[ len++ ] = New_Sys_Cfg.Type%0x10;

	//Lora网络
	send_buf[ len++ ] = XML_SYS_ZIGBEE_NET;
	send_buf[ len++ ] = (UINT8)(New_Sys_Cfg.Net>>8);
	send_buf[ len++ ] = (UINT8)New_Sys_Cfg.Net;

	//Lora节点
	send_buf[ len++ ] = XML_SYS_ZIGBEE_ADDR;
	send_buf[ len++ ] = (UINT8)(New_Sys_Cfg.Node>>8);
	send_buf[ len++ ] = (UINT8)New_Sys_Cfg.Node;

	//主机Lora地址
	send_buf[ len++ ] = XML_SYS_ZIGBEE_SERVER_ID;
	send_buf[ len++ ] = (UINT8)(New_Sys_Cfg.Gateway>>8);
	send_buf[ len++ ] = (UINT8)New_Sys_Cfg.Gateway;

	//Lora频道
	send_buf[ len++ ] = XML_SYS_ZIGBEE_CHANNEL;
	send_buf[ len++ ] = New_Sys_Cfg.Channel;

	//Lora拓扑
	send_buf[ len++ ] = XML_SYS_ZIGBEE_TOPOLOGY;
	send_buf[ len++ ] = New_Sys_Cfg.Topology;

	//GPRS网络状态
	send_buf[ len++ ] = XML_SYS_GPRS_EN;
	send_buf[ len++ ] = New_Sys_Cfg.Type>>4;
	//GPRS服务器IP
	send_buf[ len++ ] = XML_SYS_GPRS_SERVER_IP;
	send_buf[ len++ ] = New_Sys_Cfg.Gprs_ServerIP[0];
	send_buf[ len++ ] = New_Sys_Cfg.Gprs_ServerIP[1];
	send_buf[ len++ ] = New_Sys_Cfg.Gprs_ServerIP[2];
	send_buf[ len++ ] = New_Sys_Cfg.Gprs_ServerIP[3];

	//服务器端口
	send_buf[ len++ ] = XML_SYS_GPRS_SERVER_PORT;
	send_buf[ len++ ] = New_Sys_Cfg.Gprs_Port>>8;
	send_buf[ len++ ] = New_Sys_Cfg.Gprs_Port;

	//重发次数
	send_buf[ len++ ] = XML_SYS_RELINK_TIME;
	send_buf[ len++ ] = New_Sys_Cfg.Retry_Times;

	//心跳间隔时间
	send_buf[ len++ ] = XML_SYS_HEART_BEART_TIME;
	send_buf[ len++ ] = New_Sys_Cfg.Heart_interval >> 8;
	send_buf[ len++ ] = New_Sys_Cfg.Heart_interval;

	//是否午夜重启
	send_buf[ len++ ] = XML_SYS_AUTO_RESET;
	send_buf[ len++ ] = New_Sys_Cfg.AutoResetFlag;

	//午夜重启时间
	send_buf[ len++ ] = XML_SYS_AUTO_RESET_TIME;
	send_buf[ len++ ] = New_Sys_Cfg.AutoResetTime >> 8;
	send_buf[ len++ ] = New_Sys_Cfg.AutoResetTime;

	send_buf[len] = Calc_Checksum(send_buf, len);
	len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,len,Packet,200);
	//发送消息
	u1_printf("[Mod][Updata Config] -> %d (%d):", p_sys->Device_ID, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ", Packet[i]);
	}	

	GPRS_Send_Buff(Packet, nMsgLen);	

	
	return nMsgLen;
}

//写入通道配置信息 0x05 ===============================================
unsigned int System_set_config(USART_TypeDef* USARTx, unsigned char *data, unsigned int len)
{
	unsigned char send_buf[256], Packet[256];
	unsigned int send_len;
	u16 nMsgLen, i;
	
	CLOUD_HDR *hdr;
	unsigned char *ptr;
	
	SYSTEMCONFIG new_sys_cfg;
	SYSTEMCONFIG *sys_cfg;
	COM_SYSTEM_CFG *com_sys_cfg;
	
	uint16 val_u16;
	unsigned int val_u32;
	unsigned int payload_len;

	memcpy(send_buf, data, sizeof(CLOUD_HDR));
	send_len = sizeof(CLOUD_HDR);
	
	ptr = &data[sizeof(CLOUD_HDR)];
	hdr = (CLOUD_HDR *)data;
	
	payload_len = swap_word(hdr->payload_len);
	
	sys_cfg = GetSystemConfig();
	memcpy(&new_sys_cfg, sys_cfg, sizeof(SYSTEMCONFIG));
	
	while(payload_len)
	{
		switch((*ptr))
		{
			case CFG_ID_1:	  	//系统信息分组ID
				ptr++;
				payload_len--;
				val_u32 = *(unsigned int *)ptr;
				new_sys_cfg.Device_ID = swap_dword(val_u32);
				ptr += sizeof(unsigned int);
				new_sys_cfg.Type = (*ptr);
				ptr++;
				payload_len-=5;

				u1_printf("[SET]:Device=%u,Type=%d\n",new_sys_cfg.Device_ID,new_sys_cfg.Type );

				break;
			case CFG_ID_2: 	//Lora信息分组ID
				ptr++;
				payload_len--;
			
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Net = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Node = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Gateway = swap_word(val_u16);
				ptr += sizeof(uint16);
				new_sys_cfg.Channel = (*ptr);
				ptr++;
				new_sys_cfg.Topology = (*ptr);
				ptr++;
				payload_len-=8;

				u1_printf("[SET]:Lora Net=%u,Node=%d,Gateway=%d,Channel=%d,Topology=%d\n"
						,new_sys_cfg.Net
						,new_sys_cfg.Node
						,new_sys_cfg.Gateway
						,new_sys_cfg.Channel
						,new_sys_cfg.Topology
						 );

				break;
			case CFG_ID_3:		//GPRS分组信息ID
				ptr++;
				payload_len--;
			
				memcpy(new_sys_cfg.Gprs_ServerIP, ptr, 4);
				ptr += 4;
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Gprs_Port = swap_word(val_u16);
				ptr += sizeof(uint16);
				payload_len-=6;

				u1_printf("[SET]:GPRS IP=%u.%u.%u.%u,Port=%d\n"
						,(new_sys_cfg.Gprs_ServerIP[0])
						,(new_sys_cfg.Gprs_ServerIP[1])
						,(new_sys_cfg.Gprs_ServerIP[2])
						,(new_sys_cfg.Gprs_ServerIP[3])
						,new_sys_cfg.Gprs_Port
						 );
				break;
			case CFG_ID_4: 	//通信参数分组ID
				ptr++;
				payload_len--;
			
				new_sys_cfg.Retry_Times = (*ptr);
				ptr++;
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Heart_interval = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.Data_interval = swap_word(val_u16);
				ptr += sizeof(uint16);
				val_u16 = *(uint16 *)ptr;
				new_sys_cfg.State_interval = swap_word(val_u16);
				ptr += sizeof(uint16);
				payload_len-=7;

				u1_printf("[SET]:Retry=%u.Heart=%u.Data=%u.Status=%u\n"
						,new_sys_cfg.Retry_Times
						,new_sys_cfg.Heart_interval
						,new_sys_cfg.Data_interval
						,new_sys_cfg.State_interval
						 );
				break;
			default:
				ptr++;
				payload_len--;
				break;
		}
	}
	
	if (memcmp(&new_sys_cfg, sys_cfg, sizeof(SYSTEMCONFIG)))
	{
		Set_System_Config(&new_sys_cfg);
	}
	
	hdr = (CLOUD_HDR *)send_buf;
	hdr->dir = 1;
	hdr->payload_len = swap_word(sizeof(SYSTEMCONFIG)+4);
	send_len = sizeof(CLOUD_HDR);
	
	sys_cfg = GetSystemConfig();
	com_sys_cfg = (COM_SYSTEM_CFG *)&send_buf[sizeof(CLOUD_HDR)];
	
	com_sys_cfg->cfg_id1 = CFG_ID_1;
	com_sys_cfg->device_id = swap_dword(sys_cfg->Device_ID);
	com_sys_cfg->net_type = sys_cfg->Type;
	
	com_sys_cfg->cfg_id2 = CFG_ID_2;
	com_sys_cfg->zigbee_net = swap_word(sys_cfg->Net);
	com_sys_cfg->zigbee_node = swap_word(sys_cfg->Node);
	com_sys_cfg->zigbee_gateway = swap_word(sys_cfg->Gateway);
	com_sys_cfg->zigbee_channel = sys_cfg->Channel;
	com_sys_cfg->zigbee_topology = sys_cfg->Topology;
	
	com_sys_cfg->cfg_id3 = CFG_ID_3;
	memcpy(com_sys_cfg->gprs_server, sys_cfg->Gprs_ServerIP, 4);
	com_sys_cfg->gprs_port = swap_word(sys_cfg->Gprs_Port);
	
	com_sys_cfg->cfg_id4 = CFG_ID_4;
	com_sys_cfg->retry_times = sys_cfg->Retry_Times;		
	com_sys_cfg->h_interval = swap_word(sys_cfg->Heart_interval);		
	com_sys_cfg->d_interval = swap_word(sys_cfg->Data_interval);		
	com_sys_cfg->s_interval = swap_word(sys_cfg->State_interval);	
	
	send_len+=sizeof(SYSTEMCONFIG)+4;
	
	send_buf[send_len] = Calc_Checksum(send_buf, send_len);
	send_len++;
	
	//对数据打包--加包头和包尾
	nMsgLen = PackMsg( send_buf,send_len,Packet,200);
	//发送消息
	u1_printf("[Lora][Updata Config] -> %d (%d):",  sys_cfg->Device_ID, nMsgLen);
	for(i=0;i<nMsgLen;i++)
	{
	 	u1_printf("%02X ", send_buf[i]);
	}	
	u1_printf("\r\n");
	
	if(sys_cfg->Type == NETTYPE_LORA)
	{
		Lora_Send_Data(sys_cfg->Net, sys_cfg->Channel, Packet, nMsgLen);
	}
	else
	{
		GPRS_Send_Buff(Packet, nMsgLen);	
	}
	
	return 1;
}




