#include "main.h"
#include "PowerControl.h"

#define		POWER_DET_TYPE		GPIOB
#define		POWER_DET_PIN		GPIO_Pin_15

#define		POWER_DOWN_TYPE		GPIOC
#define		POWER_DOWN_PIN		GPIO_Pin_13

static u8 s_PowerFlag = TRUE;
unsigned char GetPowerFlag(void)
{
	return s_PowerFlag;
}
void InitPowerControlPort(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = POWER_DET_PIN;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(POWER_DET_TYPE, &GPIO_InitStructure);	
	
	GPIO_InitStructure.GPIO_Pin = POWER_DOWN_PIN;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(POWER_DOWN_TYPE, &GPIO_InitStructure);	
	
	GPIO_SetBits(POWER_DOWN_TYPE, POWER_DOWN_PIN);
}
//电源市电掉电关机处理
void PowerControlProccess( unsigned short nMain100ms )
{
	static unsigned int s_nErrCount = 0; 
	static unsigned int s_LastTime = 0;
	static unsigned char RunOnce = FALSE;
	RTC_TimeTypeDef RTC_TimeStructure;
	
	if( RunOnce == FALSE )
	{
		if( CalculateTime( nMain100ms, s_LastTime) < 50 ) 
		{
		 	return;
		}
	 	RunOnce = TRUE;
		InitPowerControlPort( );
		return;
	}
	
	if( CalculateTime( nMain100ms, s_LastTime) < 10 ) 
	{
	 	return;
	}
	s_LastTime = nMain100ms;

	if(GPIO_ReadInputDataBit(POWER_DET_TYPE, POWER_DET_PIN))
	{
		s_nErrCount = 0;
		s_PowerFlag = TRUE;
		return;
	}
	else
	{
		s_nErrCount++;
		s_PowerFlag = FALSE;
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
		printf("\r\n [%0.2d:%0.2d:%0.2d]  ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
		printf("关机剩余时间：%ds\r\n", 300-s_nErrCount);
		if(s_nErrCount >= 300)
		{
			s_nErrCount = 0;
			GPIO_ResetBits(POWER_DOWN_TYPE, POWER_DOWN_PIN);		//关机
		}
	}
}

